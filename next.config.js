/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
};
module.exports = nextConfig;

const withImages = require('next-images');
module.exports = withImages;

const { i18n } = require('./next-i18next.config');
module.exports = {
  i18n,
  webpack: (config, { isServer }) => {
    if (!isServer) {
      config.resolve.fallback = {
        fs: false,
        path: false,
      };
    }

    return config;
  },
  // async redirects() {
  //     return [
  //         {
  //             source: '/panel/exchange',
  //             destination: '/panel/exchange/rate/fiat',
  //             permanent: false,
  //         },
  //     ]
  // },
};
