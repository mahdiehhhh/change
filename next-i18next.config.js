const HttpBackend = require('i18next-http-backend/cjs');
// const {LOCALIZATION_URL} = require("./src/configs/variables.config");
const ChainedBackend = require('i18next-chained-backend').default;
const LocalStorageBackend = require('i18next-localstorage-backend').default;

const isBrowser = typeof window !== 'undefined';

const LOCALIZATION_URL = 'https://test.zimapay.com/api/public/locale/panel';

module.exports = {
  debug: process.env.NODE_ENV === 'development',
  backend: {
    backendOptions: [
      { expirationTime: 60 * 60 * 1000 },
      {
        loadPath: `${LOCALIZATION_URL}/{{lng}}/{{ns}}.json`,
      },
    ], // 1 hour
    backends: isBrowser ? [LocalStorageBackend, HttpBackend] : [],
  },
  react: {
    // used only for the lazy reload
    bindI18n: 'languageChanged loaded',
    useSuspense: false,
  },
  i18n: {
    locales: ['fa', 'en', 'pa', 'ru', 'ar'],
    defaultLocale: 'fa',
    localeDetection: false,
  },
  // trailingSlash: true,
  serializeConfig: false,
  use: isBrowser ? [ChainedBackend] : [],
  // use: [ChainedBackend],
};
