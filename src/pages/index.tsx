import React, { useEffect, useState } from 'react';
import { useAppSelector, useAppDispatch } from 'store/hooks';
import Login from './user/login';
import Dashboard from 'pages/panel/dashboard';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { UserLayout } from '../Layout';
import { useI18nTrDir } from '../utils/CustomHooks/i18nTrDir';
import { localesNS } from '../utils/Data/locales_NameSpace.utils';

const Home = (props: any) => {
  const { t, i18n, pageDirection } = useI18nTrDir();

  useEffect(() => {
    i18n.reloadResources(i18n.resolvedLanguage, [...localesNS]);
  }, []);

  return props.hasLayout ? (
    <UserLayout>
      <Dashboard />
    </UserLayout>
  ) : (
    <Dashboard />
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      hasLayout: true,
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default Home;
