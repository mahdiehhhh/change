import type { AppProps } from 'next/app';
import { wrapper } from '../store/store';
import '/public/assets/styles/global.style.scss';
import { createTheme } from '@mui/material/styles';
import { ThemeProvider } from '@emotion/react';
import rtlPlugin from 'stylis-plugin-rtl';
import { CacheProvider } from '@emotion/react';
import createCache from '@emotion/cache';
import { prefixer } from 'stylis';
import { appWithTranslation, useTranslation } from 'next-i18next';
import React, { useEffect } from 'react';
import { initReactI18next } from 'react-i18next';
import '../../i18n';

import { UserLayout } from 'Layout';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
// import "@mui/material/styles/createPalette";
// import '../../public/fonts/fontIndex';
// import "components/Entrance/component/SwiperSlider/_SwiperSliderStyle.scss";
import Head from 'next/head';
import nextI18nConfig from '../../next-i18next.config';
import { useAppSelector } from '../store/hooks';
import { localesNS } from '../utils/Data/locales_NameSpace.utils';

const isBrowser = typeof document !== 'undefined';
let insertionPoint: any;

if (isBrowser) {
  const emotionInsertionPoint = document.querySelector('meta[name="emotion-insertion-point"]');
  insertionPoint = emotionInsertionPoint ?? undefined;
}

const cacheRtl = createCache({
  key: 'mui-style-rtl',
  stylisPlugins: [prefixer, rtlPlugin],
  insertionPoint,
});

const cacheLtr = createCache({
  key: 'mui-style-ltr',
  insertionPoint,
});

const theme = createTheme({
  components: {
    MuiButtonBase: {
      defaultProps: {
        // The props to apply
        disableRipple: true, // No more ripple, on the whole application 💣!
      },
    },
    MuiCssBaseline: {
      styleOverrides: {
        body: {
          scrollbarColor: '#D9D9D9 white',
          scrollbarWidth: 'thin',
          '&::-webkit-scrollbar, & *::-webkit-scrollbar': {
            width: '5px',
            backgroundColor: 'transparent',
          },
          '&::-webkit-scrollbar-thumb, & *::-webkit-scrollbar-thumb': {
            width: '5px',
            borderRadius: 8,
            backgroundColor: '#D9D9D9',
            minHeight: 24,
            // border: "1px solid #2b2b2b",
          },
          '&::-webkit-scrollbar-thumb:focus, & *::-webkit-scrollbar-thumb:focus': {
            width: '5px',
            backgroundColor: '#959595',
          },
          '&::-webkit-scrollbar-thumb:active, & *::-webkit-scrollbar-thumb:active': {
            width: '5px',
            backgroundColor: '#959595',
          },
          '&::-webkit-scrollbar-thumb:hover, & *::-webkit-scrollbar-thumb:hover': {
            width: '5px',
            backgroundColor: '#959595',
          },
          '&::-webkit-scrollbar-corner, & *::-webkit-scrollbar-corner': {
            width: '5px',
            backgroundColor: '#D9D9D9',
          },
        },
      },
    },
    MuiTableCell: {
      styleOverrides: {
        root: {
          padding: '10px 13px 10px 0px',
          // '&:last-child': {
          //     padding: '10px 13px 10px 0px'
          // },
        },
      },
    },
    MuiButton: {
      variants: [
        {
          props: { variant: 'contained' },
          style: {
            fontFamily: 'IRANSansXFaNum-DemiBold',
            fontSize: '14px',
            // fontWeight: '600',
            // textTransform: 'none',
            // border: `10px dashed red`,
          },
        },
      ],
    },
  },

  direction: 'rtl',
  palette: {
    primary: {
      main: '#298FFE',
      dark: '#155EAE',
      light: '#D1E7FF',
    },
    secondary: {
      main: '#F6F7FA',
      light: '#EFEFEF',
    },
    text: {
      primary: '#262626',
      secondary: '#606060',
      disabled: '#C3C3C3',
    },
    error: {
      main: '#EB5757',
      light: '#FFEAEA',
    },
    success: {
      main: '#27AE60',
      dark: '#219653',
      light: '#E1FFEE',
    },
    warning: {
      main: '#F99033',
      light: '#FFE4CC',
    },
    info: {
      main: '#AB72FE',
      light: '#F2DCF6',
    },
  },
  typography: {
    fontFamily: [
      'IRANSansXFaNum-Regular',
      'IRANSansX-Regular',
      'IRANSansX-Bold.ttf',
      'IRANSansXFaNum-Bold.ttf',
      'IRANSansXFaNum-DemiBold',
      'IRANSansX-DemiBold',
    ].join(','),
  },
});

interface Lang {
  id: number;
  name: string;
  code: string;
  direction: string;
  is_master: number;
  logo: string;
  status: string;
}

function MyApp({ Component, pageProps }: AppProps) {
  const { i18n } = useTranslation();
  const languagesAPI = useAppSelector((state) => state.Languages.lang);
  let selectedLang: Lang = {
    id: 0,
    name: '',
    code: '',
    direction: '',
    is_master: 0,
    logo: '',
    status: '',
  };
  useEffect(() => {
    i18n.reloadResources(i18n.resolvedLanguage, [...localesNS]);
  }, []);

  useEffect(() => {
    document.body.dir = i18n.dir();
  }, [i18n]);

  // @ts-ignore
  const hasLayout = pageProps.hasLayout;

  return (
    <>
      <Head>
        <link rel="icon" href="/zimapay_favicon.png" />
      </Head>
      <CacheProvider value={i18n.dir() === 'rtl' ? cacheRtl : cacheLtr}>
        <ThemeProvider theme={theme}>
          {hasLayout ? (
            <UserLayout>
              <Component {...pageProps} />
            </UserLayout>
          ) : (
            <Component {...pageProps} />
          )}
        </ThemeProvider>
      </CacheProvider>
    </>
  );
}

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default wrapper.withRedux(appWithTranslation(MyApp, nextI18nConfig));
