import http from 'services/http.service';
import {
  API_ACCESS_TOKEN_URL,
  API_ADD_FAVORITE_CURRENCY,
  API_ALL_NOTIFICATIONS,
  API_BANK_ACCOUNTS,
  API_BANKS_LIST,
  API_BANNER,
  API_CHECK_PHONE,
  API_COUNTRIES,
  API_CREATE_CREDIT_BANK_ACCOUNT,
  API_CREATE_INTERNATIONAL_BANK_ACCOUNT,
  API_CREATE_IRAN_BANK_ACCOUNT,
  API_CURRENCIES_DASHBOARD,
  API_CURRENCIES_LIST,
  API_CURRENCIES_TABLE_INFORMATION,
  API_Fees_List,
  API_FIND_TRANSFER_DESTINATION_USER,
  API_FIND_TRANSFER_DESTINATION_WALLET,
  API_FORGET_PASSWORD,
  API_FORGET_PASSWORD_RESEND,
  API_FORGET_PASSWORD_RESET,
  API_FORGET_PASSWORD_VERIFY,
  API_LANGUAGES,
  API_LATEST_WALLET_TRANSFERS,
  API_LOGIN,
  API_MESSAGE,
  API_MESSAGES,
  API_PHONE_CODES,
  API_PROFILE_AUTHENTICATION_VERIFY,
  API_REGISTER,
  API_REGISTER_RESEND,
  API_REGISTER_VERIFY_PHONE,
  API_REMOVE_FAVORITE_CURRENCY,
  API_TICKETS,
  API_TRANSFER_CONFIRM,
  API_TRANSFER_INITIATE,
  API_UNREAD_NOTIFICATIONS,
  API_USER_ACTIVE_WALLET,
  API_USER_CREATE_TICKET,
  API_USER_FINANCE,
  API_USER_LATEST_TRANSACTION,
  API_USER_PROFILE,
  API_USER_TICKETS,
  API_USER_TICKETS_SUBJECTS,
  API_WALLET_DEPOSIT,
  API_WALLET_DEPOSIT_CASH,
  API_WALLET_DEPOSIT_CASH_RULES,
  API_WALLET_DEPOSIT_RULES,
  API_WALLET_TRANSACTION_TYPES,
  API_WALLET_TRANSFER_RULES,
  API_WALLET_WITHDRAW,
  API_WALLET_WITHDRAW_CASH,
  API_WALLET_WITHDRAW_CASH_RULES,
  API_WALLET_WITHDRAW_RULES,
} from 'configs/api_address.config';
import { CLIENT_ID, CLIENT_SECRET, GRANT_TYPE } from '../../../configs/backend.config';

export async function checkPhone(data: object) {
  try {
    const response = await http.post(API_CHECK_PHONE, data);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function login(data: object) {
  try {
    const response = await http.post(API_LOGIN, data);

    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function register(data: object) {
  try {
    const response = await http.post(API_REGISTER, data);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function registerResend(data: object) {
  try {
    const response = await http.post(API_REGISTER_RESEND, data);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function registerVerifyPhone(data: object) {
  try {
    const response = await http.post(API_REGISTER_VERIFY_PHONE, data);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function forgetPassword(data: object) {
  try {
    const response = await http.post(API_FORGET_PASSWORD, data);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function forgetPassVerify(data: object) {
  try {
    const response = await http.post(API_FORGET_PASSWORD_VERIFY, data);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function forgetPassReset(data: object) {
  try {
    const response = await http.post(API_FORGET_PASSWORD_RESET, data);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function forgetPassResend(data: object) {
  try {
    const response = await http.post(API_FORGET_PASSWORD_RESEND, data);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function getCurrenciesDashboard() {
  try {
    const response = await http.get(API_CURRENCIES_DASHBOARD);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function getCurrenciesList() {
  try {
    const response = await http.get(API_CURRENCIES_LIST);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function getExchangeCurrenciesInfo() {
  try {
    const response = await http.get(API_CURRENCIES_TABLE_INFORMATION);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function getUserProfile() {
  try {
    const response = await http.get(API_USER_PROFILE);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function getUserActiveWallets() {
  try {
    const response = await http.get(API_USER_ACTIVE_WALLET);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function getWalletLatestTransaction(uid: string) {
  try {
    const response = await http.get(`${API_USER_ACTIVE_WALLET}/${uid}/${API_USER_LATEST_TRANSACTION}`);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function getUserUnreadNotifications() {
  try {
    const response = await http.get(`${API_UNREAD_NOTIFICATIONS}`);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function getUserAllNotifications(page: number) {
  try {
    const response = await http.get(`${API_ALL_NOTIFICATIONS}${page}`);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function getBanksLists() {
  try {
    const response = await http.get(API_BANKS_LIST);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function getUserTickets() {
  try {
    const response = await http.get(API_USER_TICKETS);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function getUserTicketsSubjects() {
  try {
    const response = await http.get(API_USER_TICKETS_SUBJECTS);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function getFeesList() {
  try {
    const response = await http.get(API_Fees_List);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function createTicket(data: object) {
  try {
    const response = await http.post(`${API_USER_CREATE_TICKET}`, data);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function createIranBankAccount(data: object) {
  try {
    const response = await http.post(`${API_CREATE_IRAN_BANK_ACCOUNT}`, data);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function postAuthenticationVerifyData(data: object) {
  try {
    const response = await http.post(`${API_PROFILE_AUTHENTICATION_VERIFY}`, data);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function addCurrencyToFavorite(data: object) {
  try {
    const response = await http.post(`${API_ADD_FAVORITE_CURRENCY}`, data);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function removeCurrencyFromFavorite(data: object) {
  try {
    const response = await http.post(`${API_REMOVE_FAVORITE_CURRENCY}`, data);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function createIntBankAccount(data: object) {
  try {
    const response = await http.post(`${API_CREATE_INTERNATIONAL_BANK_ACCOUNT}`, data);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function createCreditBankAccount(data: object) {
  try {
    const response = await http.post(`${API_CREATE_CREDIT_BANK_ACCOUNT}`, data);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function getBanner() {
  try {
    const response = await http.get(API_BANNER);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function getUserFinance() {
  try {
    const response = await http.get(API_USER_FINANCE);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function getLatestWalletTransfers() {
  try {
    const response = await http.get(API_LATEST_WALLET_TRANSFERS);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function getTicketsMessages(id: string) {
  try {
    const response = await http.get(`${API_TICKETS}/${id}/${API_MESSAGES}`);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function getCountries() {
  try {
    const response = await http.get(`${API_COUNTRIES}`);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function sendTicketsMessages(id: string, data: object) {
  try {
    const response = await http.post(`${API_TICKETS}/${id}/${API_MESSAGE}`, data);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function getWalletDepositRules(uid: string) {
  try {
    const response = await http.get(`${API_USER_ACTIVE_WALLET}/${uid}/${API_WALLET_DEPOSIT_RULES}`);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function getWalletWithdrawRules(uid: string) {
  try {
    const response = await http.get(`${API_USER_ACTIVE_WALLET}/${uid}/${API_WALLET_WITHDRAW_RULES}`);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function getWalletDepositCashRules(uid: string) {
  try {
    const response = await http.get(`${API_USER_ACTIVE_WALLET}/${uid}/${API_WALLET_DEPOSIT_CASH_RULES}`);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function getWalletWithdrawCashRules(uid: string) {
  try {
    const response = await http.get(`${API_USER_ACTIVE_WALLET}/${uid}/${API_WALLET_WITHDRAW_CASH_RULES}`);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function getWalletTransferRules(originUid: string, destinationUid: string) {
  try {
    const response = await http.get(
      `${API_USER_ACTIVE_WALLET}/${originUid}/${API_WALLET_TRANSFER_RULES}/${destinationUid}`,
    );
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function postWalletDepositAmount(uid: string, data: object) {
  try {
    const response = await http.post(`${API_USER_ACTIVE_WALLET}/${uid}/${API_WALLET_DEPOSIT}`, data);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function postWalletDepositCashAmount(uid: string, data: object) {
  try {
    const response = await http.post(`${API_USER_ACTIVE_WALLET}/${uid}/${API_WALLET_DEPOSIT_CASH}`, data);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function postWalletWithdrawAmount(uid: string, data: object) {
  try {
    const response = await http.post(`${API_USER_ACTIVE_WALLET}/${uid}/${API_WALLET_WITHDRAW}`, data);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function postWalletWithdrawCashAmount(uid: string, data: object) {
  try {
    const response = await http.post(`${API_USER_ACTIVE_WALLET}/${uid}/${API_WALLET_WITHDRAW_CASH}`, data);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function postWalletTransferInfoCheck(uid: string, data: object) {
  try {
    const response = await http.post(`${API_USER_ACTIVE_WALLET}/${uid}/${API_TRANSFER_INITIATE}`, data);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function postWalletTransferInfoConfirm(uid: string, data: object) {
  try {
    const response = await http.post(`${API_USER_ACTIVE_WALLET}/${uid}/${API_TRANSFER_CONFIRM}`, data);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function postTransferDestinationWallet(uid: string, data: object) {
  try {
    const response = await http.post(`${API_USER_ACTIVE_WALLET}/${uid}/${API_FIND_TRANSFER_DESTINATION_WALLET}`, data);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function postTransferDestinationUser(uid: string, data: object) {
  try {
    const response = await http.post(`${API_USER_ACTIVE_WALLET}/${uid}/${API_FIND_TRANSFER_DESTINATION_USER}`, data);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function getUserBankAccounts(country: string) {
  try {
    const response = await http.get(`${API_BANK_ACCOUNTS}${country}`);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function getWalletTransactionTypes(uid: string, transactionType: string) {
  try {
    const response = await http.get(
      `${API_USER_ACTIVE_WALLET}/${uid}/${API_WALLET_TRANSACTION_TYPES}${transactionType}`,
    );
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function getLanguages() {
  try {
    const response = await http.get(`${API_LANGUAGES}`);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function getPhoneCodes() {
  try {
    const response = await http.get(`${API_PHONE_CODES}`);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function getAccessToken() {
  const authenticationBody = {
    grant_type: GRANT_TYPE,
    client_id: CLIENT_ID,
    client_secret: CLIENT_SECRET,
  };
  try {
    const response = await http.post(`${API_ACCESS_TOKEN_URL}`, authenticationBody);
    return response.data;
  } catch (e) {
    return Promise.reject(e);
  }
}
