import NextAuth from 'next-auth';
import Providers from 'next-auth/providers';

const options = {
  pages: {
    login: '/user/login',
  },
};
