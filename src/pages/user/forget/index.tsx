import React, { useEffect, useState } from 'react';
import { Entrance } from 'Components';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useAppDispatch, useAppSelector } from 'store/hooks';
import { handleSetTokens, setCookie } from 'utils/functions/local';
import { IS_LOGGED_IN, LOGOUT_HAPPENED } from 'configs/variables.config';
import { PATHS } from 'configs/routes.config';
import { useRouter } from 'next/router';
import Head from 'next/head';
import { useI18nTrDir } from '../../../utils/CustomHooks/i18nTrDir';
import { localesNS } from '../../../utils/Data/locales_NameSpace.utils';
import { fetchForgetPassResend } from '../../../store/slices/forget_pass_resend.slice';

const Forget = () => {
  const router = useRouter();
  const UrlPath = router.pathname;

  const { t, i18n, pageDirection } = useI18nTrDir();

  useEffect(() => {
    i18n.reloadResources(i18n.resolvedLanguage, [...localesNS]);
  }, []);

  const dispatch = useAppDispatch();

  const forgetPassResult = useAppSelector((state) => state.ForgetPassword);
  const forgetPassVerifyResult = useAppSelector((state) => state.ForgetPasswordVerify);
  const forgetPassResetResult = useAppSelector((state) => state.ForgetPasswordReset);

  const [mode, setMode] = useState('forgetPassword');

  useEffect(() => {
    forgetPassResult.forgetPassResp ? setMode('forgetVerify') : setMode('forgetPassword');
  }, [forgetPassResult]);

  useEffect(() => {
    forgetPassVerifyResult.forgetPassVerifyResp?.reference && setMode('setNewPass');
  }, [forgetPassVerifyResult]);

  useEffect(() => {
    if (forgetPassResetResult.forgetPassResetResp) {
      // handleSetTokens(IS_LOGGED_IN, JSON.stringify(forgetPassResetResult.forgetPassResetResp?.token))
      setCookie(IS_LOGGED_IN, forgetPassResetResult.forgetPassResetResp?.token);

      typeof window !== 'undefined' && router.replace(`/${PATHS.USER_DASHBOARD}`);
    }
  }, [forgetPassResetResult]);

  useEffect(() => {
    dispatch(fetchForgetPassResend('reset'));
  }, [UrlPath]);

  return (
    <>
      <Head>
        <title>{`${t('layout:zimapay')} | ${t('input_field:forget_pass')}`}</title>
      </Head>
      <Entrance type={mode} />
    </>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      hasLayout: false,
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default Forget;
