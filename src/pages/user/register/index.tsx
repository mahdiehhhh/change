import React, { useEffect, useState } from 'react';
import { Entrance } from 'Components';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useRouter } from 'next/router';
import { useAppDispatch, useAppSelector } from 'store/hooks';
import { handleSetTokens, setCookie } from 'utils/functions/local';
import { ACCESS_TOKEN, IS_LOGGED_IN } from 'configs/variables.config';
import { PATHS } from 'configs/routes.config';
import Head from 'next/head';
import { useI18nTrDir } from '../../../utils/CustomHooks/i18nTrDir';
import { localesNS } from '../../../utils/Data/locales_NameSpace.utils';
import { fetchForgetPassResend } from '../../../store/slices/forget_pass_resend.slice';
import { fetchRegResend } from '../../../store/slices/register_resend.slice';

const Register = () => {
  const router = useRouter();
  const UrlPath = router.pathname;

  const { t, i18n, pageDirection } = useI18nTrDir();

  useEffect(() => {
    i18n.reloadResources(i18n.resolvedLanguage, [...localesNS]);
  }, []);

  const [mode, setMode] = useState('register');

  const registerResult = useAppSelector((state) => state.Register);
  const regVerifyPhoneResult = useAppSelector((state) => state.RegisterVerifyPhone);

  const dispatch = useAppDispatch();

  useEffect(() => {
    if (registerResult.registerResp) {
      setMode('verification');
    } else if (registerResult.error?.errors) {
      setTimeout(() => {
        typeof window !== 'undefined' && router.replace(`/${PATHS.USER_LOGIN}`);
      }, 4000);
    }
  }, [registerResult]);

  useEffect(() => {
    // registerResult.registerResp ? setMode('verification') : setMode('register')
    if (regVerifyPhoneResult.regVerifyPhoneResp) {
      // handleSetTokens(IS_LOGGED_IN, JSON.stringify(regVerifyPhoneResult.regVerifyPhoneResp.token))
      setCookie(IS_LOGGED_IN, regVerifyPhoneResult.regVerifyPhoneResp.token);

      typeof window !== 'undefined' && router.replace(`/${PATHS.USER_DASHBOARD}`);
    }
  }, [regVerifyPhoneResult]);

  useEffect(() => {
    dispatch(fetchRegResend('reset'));
  }, [UrlPath]);

  return (
    <>
      <Head>
        <title>{`${t('layout:zimapay')} | ${t('input_field:register')}`}</title>
      </Head>
      <Entrance type={mode} reverse={true} />
    </>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      hasLayout: false,
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default Register;
