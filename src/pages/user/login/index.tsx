import React, { useEffect, useState } from 'react';
import { useAppSelector, useAppDispatch } from 'store/hooks';
import Head from 'next/head';

import { Entrance } from 'Components';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { PATHS } from 'configs/routes.config';
import { useRouter } from 'next/router';
import { handleGetTokens } from '../../../utils/functions/local';
import { IS_LOGGED_IN, LOGOUT_HAPPENED } from '../../../configs/variables.config';
import { useI18nTrDir } from '../../../utils/CustomHooks/i18nTrDir';
import { localesNS } from '../../../utils/Data/locales_NameSpace.utils';

interface Schema {
  phone?: undefined | string | number;
  password?: undefined | string | number;
  repeatPassword?: undefined | string | number;
}

const Login = () => {
  const router = useRouter();
  const UrlPath = router.pathname;

  const { t, i18n, pageDirection } = useI18nTrDir();

  useEffect(() => {
    i18n.reloadResources(i18n.resolvedLanguage, [...localesNS]);
  }, []);

  const [mode, setMode] = useState('enterViaPhone');
  const checkPhoneResult = useAppSelector((state) => state.CheckPhone);
  const LoginResult = useAppSelector((state) => state.Login);
  const registerResult = useAppSelector((state) => state.Register);
  // const [buttonLoading, setButtonLoading] = useState(false);
  useEffect(() => {
    const isLogOutHappened = localStorage.getItem(LOGOUT_HAPPENED);
    // console.log(checkPhoneResult.checkPhoneResp, 'hhhhhhhhhhh');
    // console.log(LoginResult, 'hhhhhhhhhhh');

    // if (checkPhoneResult.checkPhoneResp != null) {
    //   console.log('hiiii');
    //   setButtonLoading(true);
    // }
    if (checkPhoneResult.checkPhoneResp === 'register' && !!registerResult.error?.errors && !isLogOutHappened) {
      setMode('login');
    } else if (checkPhoneResult.checkPhoneResp === 'register' && !registerResult.error?.errors && !isLogOutHappened) {
      typeof window !== 'undefined' && router.replace(`/${PATHS.USER_REGISTER}`);
    }
    setMode(checkPhoneResult.checkPhoneResp);
    // checkPhoneResult.checkPhoneResp === 'register' ? typeof window !== 'undefined' && router.replace(`/${PATHS.USER_REGISTER}`) : setMode(checkPhoneResult.checkPhoneResp)
  }, [checkPhoneResult, registerResult]);

  useEffect(() => {
    const isLogOutHappened = localStorage.getItem(LOGOUT_HAPPENED);
    if (checkPhoneResult.checkPhoneResp === 'register' && !!registerResult.error?.errors && !isLogOutHappened) {
      setMode('login');
    } else {
      setMode('enterViaPhone');
    }
  }, [UrlPath]);

  // useEffect(() => {
  //     LoginResult.loginResp && typeof window !== 'undefined' && router.replace(`/${PATHS.USER_DASHBOARD}`)
  // }, [LoginResult])

  return (
    // TODO: remove phoneValidity props from Entrance component interface if is unnecessary
    <>
      <Head>
        <title>{`${t('layout:zimapay')} | ${t('layout:login')}`}</title>
      </Head>
      <Entrance type={mode === 'login' ? 'enterViaPass' : 'enterViaPhone'} />
    </>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      hasLayout: false,
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default Login;
