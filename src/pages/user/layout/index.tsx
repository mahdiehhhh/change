import React, { useEffect } from 'react';
import { UserLayout } from 'Layout';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { localesNS } from '../../../utils/Data/locales_NameSpace.utils';

const Layout = () => {
  return (
    <div>
      <UserLayout />
    </div>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      hasLayout: false,
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default Layout;
