import React, { useEffect } from 'react';
import { Box, Button, Typography } from '@mui/material';
import sleepingBear from '/public/assets/images/sleeping-bear.svg';
import notFoundBg from '/public/assets/images/404-BG.svg';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import { useRouter } from 'next/router';
import { useI18nTrDir } from '../../utils/CustomHooks/i18nTrDir';
import { localesNS } from '../../utils/Data/locales_NameSpace.utils';

const NotFound = () => {
  const { t, i18n, pageDirection } = useI18nTrDir();
  const router = useRouter();

  // useEffect(() => {
  //     i18n.reloadResources(i18n.resolvedLanguage, ['notFound', 'common'])
  // }, [])

  return (
    <Box
      sx={{
        width: '100vw',
        height: '100vh',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        overflowX: 'hidden',
        direction: 'rtl',
      }}
    >
      <Box
        id={'contents'}
        sx={{
          width: { xs: '293px', sm: '523px', lg: '663px' },
          height: { xs: '305px', sm: '544px', lg: '690px' },
          px: { xs: '40px', sm: '70px', lg: '130px' },
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          position: 'relative',
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'contain',
          backgroundImage: `url(${notFoundBg.src})`,
        }}
      >
        <Typography
          variant="h1"
          component="h1"
          color={'primary.main'}
          sx={{
            fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Bold',
            fontSize: { xs: '48px', sm: '96px' },
            fontWeight: '800',
            mt: { xs: '40px', sm: '70px', lg: '100px' },
          }}
        >
          404
        </Typography>
        <Typography
          variant="inherit"
          component="span"
          color={'primary.main'}
          sx={{
            mt: '10px',
            // fontWeight: '600',
            fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
            fontSize: { xs: '16px', sm: '24px' },
          }}
        >
          {t('notFound:page_not_found')}
        </Typography>
        <Typography
          variant="inherit"
          component="span"
          color={'text.secondary'}
          sx={{
            mt: '10px',
            fontWeight: '400',
            fontSize: { xs: '12px', sm: '16px' },
            textAlign: 'center',
          }}
        >
          {t('notFound:url_not_found')}
        </Typography>

        <Button
          variant="contained"
          disableRipple={true}
          sx={{
            width: { xs: '90px', sm: '236px' },
            height: { xs: '32px', sm: '40px' },
            '&:hover': {
              boxShadow: 'none',
            },
            boxShadow: 'none',
            fontWeight: '500',
            borderRadius: '12px',
            fontSize: { xs: '12px', sm: '14px' },
            mt: { xs: '10px', sm: '30px' },
          }}
          onClick={() => router.back()}
        >
          {t('common:return')}
        </Button>

        <Box
          component={'img'}
          src={sleepingBear.src}
          alt={'sleeping-bear'}
          sx={{
            width: { xs: '140px', sm: '250px', lg: '390px' },
            height: { xs: '68px', sm: '120px', lg: '190px' },
            position: 'absolute',
            bottom: { xs: '0', sm: '50px', lg: '100px' },
          }}
        />
      </Box>
    </Box>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default NotFound;
