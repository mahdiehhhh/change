import React, { useEffect } from 'react';
import BankAccounts from 'Components/BankAccounts/BankAccounts.component';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useI18nTrDir } from '../../../../utils/CustomHooks/i18nTrDir';
import { localesNS } from '../../../../utils/Data/locales_NameSpace.utils';

const IranianBA = () => {
  const { t, i18n, pageDirection } = useI18nTrDir();

  useEffect(() => {
    i18n.reloadResources(i18n.resolvedLanguage, [...localesNS]);
  }, []);

  return <BankAccounts />;
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      hasLayout: true,
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default IranianBA;
