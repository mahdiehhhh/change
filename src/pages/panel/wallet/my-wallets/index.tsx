import React, { useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from '../../../../store/hooks';
import { fetchCurrenciesDashboard } from '../../../../store/slices/currencies_dashboard.slice';
import { fetchBanner } from '../../../../store/slices/banner.slice';
import { fetchUserFinance } from '../../../../store/slices/user_finance.slice';
import { Box, Chip, Link, Skeleton } from '@mui/material';
import { MyAssets } from '../../../../Components/MyAssets/MyAssets.component';
import { UsefulServices } from '../../../../Components/UsefulServices/UsefulServices.component';
import { MyWallet } from 'Components';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Deposit from '../../../../Components/Deposit/Deposit.component';
import { UserLayout } from '../../../../Layout';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import Head from 'next/head';
import { useI18nTrDir } from '../../../../utils/CustomHooks/i18nTrDir';
import { localesNS } from '../../../../utils/Data/locales_NameSpace.utils';
import bulletPoint from '/public/assets/images/BulletPoint.svg';
import Typography from '@mui/material/Typography';
import { Chart } from '../../../../Components/Chart/Chart.component';
import { CurrencyTable } from '../../../../Components/CurrencyTable/CurrencyTable.component';

const Wallet = () => {
  const dispatch = useAppDispatch();

  const { t, i18n, pageDirection } = useI18nTrDir();
  const theme = useTheme();
  const lg = useMediaQuery(theme.breakpoints.up('lg'));
  const xl = useMediaQuery(theme.breakpoints.up('xl'));

  const mode = 'wallet';
  // @ts-ignore
  const doughnut_chart_width = mode === 'dashboard' ? (xl ? 220 : 190) : 220;

  useEffect(() => {
    i18n.reloadResources(i18n.resolvedLanguage, [...localesNS]);
  }, []);

  const userFinanceResult = useAppSelector((state) => state.UserFinance);

  const [walletReady, setWalletReady] = useState(false);

  useEffect(() => {
    dispatch(fetchUserFinance());
  }, []);

  useEffect(() => {
    if (!userFinanceResult.loading) {
      setWalletReady(true);
    } else {
      setWalletReady(false);
    }
  }, [userFinanceResult]);

  return (
    <>
      <Head>
        <title>{`${t('layout:zimapay')} | ${t('layout:my_wallets')}`}</title>
      </Head>
      <Box
        sx={{
          width: '100%',
          height: 'auto',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Box
          sx={{
            width: '100%',
            height: 'auto',
            display: 'flex',
            alignItems: { lg: 'flex-start' },
            flexDirection: { xs: 'column', sm: 'row' },
          }}
        >
          <MyWallet />

          {!walletReady ? (
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                flexGrow: 1,
              }}
            >
              <Box
                sx={{
                  maxWidth: '100%',
                  height: 'auto',
                  borderRadius: '22px',
                  padding: { xs: '16px 16px 24px 16px', sm: '18px 25px' },
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: { lg: 'flex-start' },
                  mb: '16px',
                  mt: { xs: '16px', sm: '0' },
                  backgroundColor: 'white',
                }}
              >
                <Skeleton
                  animation="wave"
                  sx={{
                    width: '156px',
                    height: '30px',
                    borderRadius: '12px',
                    my: '2px',
                  }}
                />
                <Box
                  sx={{
                    width: '100%',
                    display: 'flex',
                    flexDirection: { xs: 'column', sm: 'row' },
                    alignItems: { xs: 'center', sm: 'flex-start' },
                    justifyContent: { sm: 'space-between', lg: 'space-around' },
                    mt: mode === 'wallet' ? { lg: '20px' } : '0',
                  }}
                >
                  <Box
                    sx={{
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                  >
                    <Box
                      sx={{
                        position: 'relative',
                      }}
                    >
                      <Skeleton
                        animation="wave"
                        variant="circular"
                        width={doughnut_chart_width}
                        height={doughnut_chart_width}
                      />
                      <Chip
                        sx={{
                          width: `${doughnut_chart_width - 70}px`,
                          height: `${doughnut_chart_width - 70}px`,
                          borderRadius: '50%',
                          bgcolor: 'white',
                          position: 'absolute',
                          top: '50%',
                          right: '50%',
                          transform: 'translate(50%, -50%)',
                        }}
                      />

                      <Box
                        sx={{
                          width: `${doughnut_chart_width - 100}px`,
                          height: `${doughnut_chart_width - 100}px`,
                          display: 'flex',
                          flexDirection: 'column',
                          alignItems: 'center',
                          justifyContent: 'center',
                          position: 'absolute',
                          top: '50%',
                          right: '50%',
                          transform: 'translate(50%, -50%)',
                        }}
                      >
                        <Skeleton
                          animation="wave"
                          sx={{
                            width: '90%',
                            height: '30px',
                            borderRadius: '12px',
                            my: '2px',
                          }}
                        />
                        <Skeleton
                          animation="wave"
                          sx={{
                            width: '60%',
                            height: '30px',
                            borderRadius: '12px',
                            my: '2px',
                          }}
                        />
                      </Box>
                    </Box>
                  </Box>

                  <Box
                    sx={{
                      width: '40%',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                      mt: { xs: '32px', sm: '0' },
                      // px: {xs: '10%', sm: '5%', xl: '20%'}
                    }}
                  >
                    {/*<Chart mode={`line-${mode}`} chartData={data} chartWidth={lg ? 270 : 150}*/}
                    {/*       chartHeight={190} color={'#ff00ff'}/>*/}

                    <Box
                      sx={{
                        width: '100%',
                        display: 'flex',
                        flexDirection: 'column',
                      }}
                    >
                      {Array.from(Array(3).keys()).map((item, index) => (
                        <Skeleton
                          key={index}
                          animation="wave"
                          sx={{
                            width: '100%',
                            height: '30px',
                            borderRadius: '12px',
                            my: '20px',
                          }}
                        />
                      ))}
                    </Box>
                  </Box>
                </Box>

                <Box
                  sx={{
                    width: '100%',
                    height: 'auto',
                    display: 'flex',
                    flexDirection: 'column',
                    mt: { lg: '40px' },
                    // justifyContent: 'space-between',
                    // backgroundColor: 'red'
                  }}
                >
                  {Array.from(Array(4).keys()).map((item, index) => (
                    <Box
                      key={index}
                      sx={{
                        with: '100%',
                        display: 'flex',
                        alingItems: 'center',
                        justifyContent: 'space-between',
                        px: { xs: 0, lg: '24px' },
                      }}
                    >
                      <Box
                        sx={{
                          display: 'flex',
                          alignItems: 'center',
                        }}
                      >
                        <Skeleton
                          animation="wave"
                          variant="rectangular"
                          sx={{
                            width: '32px',
                            height: '32px',
                            borderRadius: '6px',
                            mr: '16px',
                          }}
                        />
                        <Skeleton
                          key={index}
                          animation="wave"
                          sx={{
                            width: '70px',
                            height: '30px',
                            borderRadius: '12px',
                            my: '20px',
                          }}
                        />
                      </Box>
                      <Skeleton
                        key={index}
                        animation="wave"
                        sx={{
                          flex: 0.85,
                          height: '30px',
                          borderRadius: '12px',
                          my: '20px',
                        }}
                      />
                    </Box>
                  ))}
                </Box>
              </Box>

              <UsefulServices mode={'wallet'} loading={walletReady} />
            </Box>
          ) : (
            <Box
              sx={{
                display: 'flex',
                flexFlow: 'column',
                // height: '100%',
                flexGrow: 1,
              }}
            >
              <MyAssets mode={'wallet'} />
              <UsefulServices mode={'wallet'} loading={walletReady} />
            </Box>
          )}
        </Box>

        {/*<Deposit operationType={'cash'}/>*/}
      </Box>
    </>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      hasLayout: true,
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default Wallet;
