import React, { useEffect } from 'react';
import Deposit from 'Components/Deposit/Deposit.component';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { Box } from '@mui/material';
import { UserLayout } from '../../../../../../Layout';
import Head from 'next/head';
import { useI18nTrDir } from '../../../../../../utils/CustomHooks/i18nTrDir';
import { localesNS } from '../../../../../../utils/Data/locales_NameSpace.utils';

const RialDeposit = () => {
  const { t, i18n, pageDirection } = useI18nTrDir();

  useEffect(() => {
    i18n.reloadResources(i18n.resolvedLanguage, [...localesNS]);
  }, []);

  return (
    <>
      <Head>
        <title>{`${t('layout:zimapay')} | ${t('layout:withdraw_money')}`}</title>
      </Head>
      <Box
        sx={{
          width: '100%',
          // height: '100%',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Deposit operationType={'rial'} />
      </Box>
    </>
  );
};

export async function getStaticPaths() {
  return {
    paths: [], //indicates that no page needs be created at build time
    fallback: 'blocking', //indicates the type of fallback
  };
}

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      hasLayout: true,
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default RialDeposit;
