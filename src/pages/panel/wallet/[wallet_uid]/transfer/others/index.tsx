import React, { useCallback, useEffect, useState } from 'react';
import Deposit from 'Components/Deposit/Deposit.component';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { Box, Typography } from '@mui/material';
import Head from 'next/head';
import { useI18nTrDir } from 'utils/CustomHooks/i18nTrDir';
import { localesNS } from 'utils/Data/locales_NameSpace.utils';
import Transformation from '../../../../../../Components/Transformation/Transformation.component';
import TransferNewDestination from '../../../../../../Components/TransferNewDestination/TransferNewDestination.component';
import { useAppDispatch, useAppSelector } from '../../../../../../store/hooks';
import DialogTitle from '@mui/material/DialogTitle';
import IconButton from '@mui/material/IconButton';
import backwardArrow from '*.svg';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import Divider from '@mui/material/Divider';
import DialogActions from '@mui/material/DialogActions';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import { fetchFindTransferDestUser } from '../../../../../../store/slices/transfer_destination_user.slice';
import { fetchFindTransferDestWallet } from '../../../../../../store/slices/transfer_destination_wallet.slice';
import { useRouter } from 'next/router';
import PaymentReceipt from '../../../../../../Components/PaymentReceipt/PaymentReceipt.component';

interface LatestTransfersSample {
  owner_name: string;
  owner_avatar: string;
  wallet_code: string;
  wallet_uid: string;
  wallet_title: string;
  wallet_currency: string;
  wallet_iso_code: string;
}

const CashDeposit = () => {
  const { t, i18n, pageDirection } = useI18nTrDir();
  const router = useRouter();
  const wallet_uid: any = router.query.wallet_uid;

  const [othersMode, setOthersMode] = useState('enter_address');
  const [openNumberTransferDB, setOpenNumberTransferDB] = useState(false);
  const [latestTransfersState, setLatestTransfersState] = useState<LatestTransfersSample[]>([]);
  const [destinationWalletType, setDestinationWalletType] = useState('');

  const dispatch = useAppDispatch();
  const findTransferDestWalletResult = useAppSelector((state) => state.FindTransferDestWallet);
  const findTransferDestUserResult = useAppSelector((state) => state.FindTransferDestUser);
  const walletLatestTransfersResult = useAppSelector((state) => state.WalletLatestTransfers);
  const checkWalletTransferResult = useAppSelector((state) => state.CheckWalletTransfer);
  const confirmWalletTransferResult = useAppSelector((state) => state.ConfirmWalletTransfer);

  const [activeDestination, setActiveDestination] = useState('');

  const handleOpenNumberTransferDB = () => {
    setOpenNumberTransferDB(true);
  };

  const handleCloseNumberTransferDB = () => {
    setOpenNumberTransferDB(false);
  };

  useEffect(() => {
    i18n.reloadResources(i18n.resolvedLanguage, [...localesNS]);
  }, []);

  useEffect(() => {
    if (openNumberTransferDB) {
      if (!activeDestination) {
        othersMode.includes('number') &&
          setActiveDestination(findTransferDestUserResult.transferDestUserResp?.destination_wallets_list[0]?.code);
      }
    }
  }, [openNumberTransferDB]);

  useEffect(() => {
    setLatestTransfersState(walletLatestTransfersResult.walletLatestTransfersResp?.latest_transferred_wallets_list);
  }, [walletLatestTransfersResult]);

  const detectWalletDestinationAddress = (address: any) => {
    const checkLetterRejex = /[a-zA-Z]/;
    if (checkLetterRejex.test(address['destination'])) {
      setDestinationWalletType('wallet');
      dispatch(fetchFindTransferDestUser('reset'));
      dispatch(fetchFindTransferDestWallet('reset'));
      dispatch(fetchFindTransferDestWallet(wallet_uid, address));
    } else {
      setDestinationWalletType('number');
      dispatch(fetchFindTransferDestUser('reset'));
      dispatch(fetchFindTransferDestWallet('reset'));
      dispatch(fetchFindTransferDestUser(wallet_uid, address));
    }
  };

  useEffect(() => {
    if (!findTransferDestWalletResult.error && !!findTransferDestWalletResult.transferDestWalletResp) {
      setOthersMode('transformation');
    }

    if (!findTransferDestUserResult.error && !!findTransferDestUserResult.transferDestUserResp) {
      setOthersMode('transfer_via_number');
      handleOpenNumberTransferDB();
    }
  }, [findTransferDestWalletResult, findTransferDestUserResult]);

  useEffect(() => {
    if (!!checkWalletTransferResult.checkWalletTransferResp?.receiver) {
      setOthersMode('check_receipt');
    }
  }, [checkWalletTransferResult]);

  useEffect(() => {
    if (!!confirmWalletTransferResult.confirmWalletTransferResp?.message) {
      setOthersMode('confirm_receipt');
    }
  }, [confirmWalletTransferResult]);

  const handleGoToTransformation = () => {
    if (othersMode === 'transfer_via_number') {
      setOthersMode('transformation');
    }
    handleCloseNumberTransferDB();
  };

  return (
    <>
      <Head>
        <title>{`${t('layout:zimapay')} | ${t('layout:transfer_others')}`}</title>
      </Head>
      <Box
        sx={{
          width: '100%',
          // height: '100%',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        {othersMode === 'enter_address' || othersMode === 'transfer_via_number' ? (
          <TransferNewDestination
            destinationMode={'enter_address'}
            activeDestination={activeDestination}
            setActiveDestination={(event: any) => setActiveDestination(event?.target?.value)}
            detectWalletDestinationAddress={detectWalletDestinationAddress}
            handleGoToTransformation={handleGoToTransformation}
          />
        ) : othersMode.includes('_receipt') ? (
          <PaymentReceipt paymentMode={othersMode} />
        ) : (
          <Transformation destination_uid={activeDestination} destinationWalletType={destinationWalletType} />
        )}
      </Box>
      {openNumberTransferDB && (
        <Dialog
          fullScreen={true}
          open={openNumberTransferDB}
          onClose={handleCloseNumberTransferDB}
          aria-labelledby="responsive-dialog-title"
          sx={{
            backdropFilter: 'blur(4px)',
          }}
          PaperProps={{
            elevation: 0,
            sx: {
              width: { xs: '327px', sm: '474px' },
              minWidth: 'auto',
              height: 'auto',
              minHeight: '140px',
              // backgroundColor: 'teal',
              borderRadius: '22px',
              // maxWidth: "720px!important",
              padding: '0',
            },
          }}
        >
          <DialogContent
            sx={{
              padding: 0,
            }}
          >
            <TransferNewDestination
              destinationMode={'transfer_via_number'}
              activeDestination={activeDestination}
              setActiveDestination={(event: any) => setActiveDestination(event?.target?.value)}
              detectWalletDestinationAddress={detectWalletDestinationAddress}
              handleGoToTransformation={handleGoToTransformation}
            />
          </DialogContent>
        </Dialog>
      )}
    </>
  );
};

export async function getStaticPaths() {
  return {
    paths: [], //indicates that no page needs be created at build time
    fallback: 'blocking', //indicates the type of fallback
  };
}

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      hasLayout: true,
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default CashDeposit;
