import React, { useEffect, useState } from 'react';
import Deposit from 'Components/Deposit/Deposit.component';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { Box } from '@mui/material';
import Head from 'next/head';
import { useI18nTrDir } from 'utils/CustomHooks/i18nTrDir';
import { localesNS } from 'utils/Data/locales_NameSpace.utils';
import Transformation from '../../../../../../Components/Transformation/Transformation.component';
import TransferNewDestination from '../../../../../../Components/TransferNewDestination/TransferNewDestination.component';
import PaymentReceipt from '../../../../../../Components/PaymentReceipt/PaymentReceipt.component';
import { useAppDispatch, useAppSelector } from '../../../../../../store/hooks';

const CashDeposit = () => {
  const { t, i18n, pageDirection } = useI18nTrDir();

  const dispatch = useAppDispatch();
  const checkWalletTransferResult = useAppSelector((state) => state.CheckWalletTransfer);
  const confirmWalletTransferResult = useAppSelector((state) => state.ConfirmWalletTransfer);
  const [transferStep, setTransferStep] = useState('initial');

  useEffect(() => {
    if (!!checkWalletTransferResult.checkWalletTransferResp?.sender) {
      setTransferStep('check_receipt');
    }
  }, [checkWalletTransferResult]);

  useEffect(() => {
    if (!!confirmWalletTransferResult?.confirmWalletTransferResp) {
      setTransferStep('confirm_receipt');
    }
  }, [confirmWalletTransferResult]);

  useEffect(() => {
    i18n.reloadResources(i18n.resolvedLanguage, [...localesNS]);
  }, []);

  return (
    <>
      <Head>
        <title>{`${t('layout:zimapay')} | ${t('layout:transfer_myself')}`}</title>
      </Head>
      <Box
        sx={{
          width: '100%',
          // height: '100%',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        {transferStep === 'initial' ? <Transformation /> : <PaymentReceipt paymentMode={transferStep} />}
      </Box>
    </>
  );
};

export async function getStaticPaths() {
  return {
    paths: [], //indicates that no page needs be created at build time
    fallback: 'blocking', //indicates the type of fallback
  };
}

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      hasLayout: true,
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default CashDeposit;
