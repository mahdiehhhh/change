import React, { useEffect } from 'react';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import ExchangeRate from '../../../../../Components/ExchangeRate/ExchangeRate.component';
import { useI18nTrDir } from '../../../../../utils/CustomHooks/i18nTrDir';
import { localesNS } from '../../../../../utils/Data/locales_NameSpace.utils';
import Head from 'next/head';

const Crypto = () => {
  const { t, i18n, pageDirection } = useI18nTrDir();

  useEffect(() => {
    i18n.reloadResources(i18n.resolvedLanguage, [...localesNS]);
  }, []);

  return (
    <div>
      <Head>
        <title>{`${t('layout:zimapay')} | ${t('layout:exchange_rate')}`}</title>
      </Head>
      <ExchangeRate />
    </div>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      hasLayout: true,
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default Crypto;
