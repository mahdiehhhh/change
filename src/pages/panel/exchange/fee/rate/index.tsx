import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import { Box, Grid, Link, Typography } from '@mui/material';
import { MyWallet } from 'Components';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { MyAssets } from 'Components/MyAssets/MyAssets.component';
import { UsefulServices } from 'Components/UsefulServices/UsefulServices.component';

import { useAppDispatch, useAppSelector } from 'store/hooks';
import ExchangeRate from 'Components/ExchangeRate/ExchangeRate.component';
import { useI18nTrDir } from 'utils/CustomHooks/i18nTrDir';
import { localesNS } from 'utils/Data/locales_NameSpace.utils';
import Fees from 'Components/Fees/Fees.component';

const ExchangeRatePage = () => {
  const { t, i18n, pageDirection } = useI18nTrDir();

  useEffect(() => {
    i18n.reloadResources(i18n.resolvedLanguage, [...localesNS]);
  }, []);

  return (
    <>
      <Head>
        <title>{`${t('layout:zimapay')} | ${t('layout:fees_rate')}`}</title>
      </Head>
      <Fees />
    </>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      hasLayout: true,
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default ExchangeRatePage;
