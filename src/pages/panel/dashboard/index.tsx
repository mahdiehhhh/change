import React, { useEffect, useState } from 'react';
import { Box, Chip, Divider, Grid, Link, Skeleton, Typography } from '@mui/material';
import { MyWallet } from 'Components';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { MyAssets } from 'Components/MyAssets/MyAssets.component';
import { UsefulServices } from 'Components/UsefulServices/UsefulServices.component';
import banner from '/public/assets/images/banner.jpg';
import { UserLayout } from '../../../Layout';
import { useAppDispatch, useAppSelector } from 'store/hooks';
import { fetchCurrenciesDashboard } from 'store/slices/currencies_dashboard.slice';
import { fetchBanner } from '../../../store/slices/banner.slice';
import { fetchUserFinance } from '../../../store/slices/user_finance.slice';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import Head from 'next/head';
import { handleGetTokens } from '../../../utils/functions/local';
import { IS_LOGGED_IN } from '../../../configs/variables.config';
import { PATHS } from '../../../configs/routes.config';
import { useI18nTrDir } from '../../../utils/CustomHooks/i18nTrDir';
import { localesNS } from '../../../utils/Data/locales_NameSpace.utils';
import { fetchRegister } from '../../../store/slices/register.slice';
import { fetchRegVerifyPhone } from '../../../store/slices/register_verify_phone.slice';
import { fetchCheckPhone } from '../../../store/slices/check_phone.slice';
import NextLink from 'next/link';
import { fetchProfileVerifyAuth } from '../../../store/slices/profile_verify_authentication.slice';
import bulletPoint from '/public/assets/images/BulletPoint.svg';

import { Chart } from '../../../Components/Chart/Chart.component';
import { CurrencyTable } from '../../../Components/CurrencyTable/CurrencyTable.component';

interface BannerSample {
  title: string;
  link: string;
  image: string;
}

const Dashboard = () => {
  const dispatch = useAppDispatch();

  const { t, i18n, pageDirection } = useI18nTrDir();

  const theme = useTheme();
  const xs = useMediaQuery(theme.breakpoints.down('sm'));
  const sm = useMediaQuery(theme.breakpoints.up('sm'));
  const lg = useMediaQuery(theme.breakpoints.up('lg'));
  const xl = useMediaQuery(theme.breakpoints.up('xl'));

  useEffect(() => {
    i18n.reloadResources(i18n.resolvedLanguage, [...localesNS]);
  }, []);

  const currenciesDashResult = useAppSelector((state) => state.CurrenciesDashboard);
  const userFinanceResult = useAppSelector((state) => state.UserFinance);
  const bannerResult = useAppSelector((state) => state.Banner);

  const mode = 'dashboard';
  const doughnut_chart_width = mode === 'dashboard' ? (xl ? 220 : 190) : 220;

  const [optimizedBanner, setOptimizedBanner] = useState();
  const [dashboardReady, setDashboardReady] = useState(false);

  useEffect(() => {
    dispatch(fetchCurrenciesDashboard());
    dispatch(fetchBanner());
    dispatch(fetchUserFinance());
    dispatch(fetchRegister('reset'));
    dispatch(fetchRegVerifyPhone('reset'));
    dispatch(fetchCheckPhone('reset'));
    dispatch(fetchProfileVerifyAuth('reset'));
  }, []);

  useEffect(() => {
    if (!currenciesDashResult.loading || !userFinanceResult.loading || !bannerResult.loading) {
      setDashboardReady(true);
    } else {
      setDashboardReady(false);
    }
  }, [currenciesDashResult, userFinanceResult, bannerResult]);

  useEffect(() => {
    if (bannerResult.bannerResp?.length > 0) {
      xs
        ? setOptimizedBanner(bannerResult.bannerResp[0]?.small_image)
        : sm && !lg
        ? setOptimizedBanner(bannerResult.bannerResp[0]?.medium_image)
        : lg
        ? setOptimizedBanner(bannerResult.bannerResp[0]?.large_image)
        : '';
    }
  }, [xs, sm, lg, bannerResult]);

  return (
    <>
      <Head>
        <title>{`${t('layout:zimapay')} | ${t('layout:dashboard')}`}</title>
      </Head>
      <Box
        sx={{
          width: '100%',
          height: 'auto',
          display: 'flex',
          // alignItems: {lg: 'flex-start'},
          flexDirection: { xs: 'column', sm: 'row' },
        }}
      >
        <MyWallet />

        {!dashboardReady ? (
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              flexGrow: 1,
            }}
          >
            <Box
              sx={{
                width: '100%',
                padding: '24px',
                bgcolor: 'white',
                borderRadius: '22px',
                display: 'flex',
                flexDirection: 'column',
                gap: '24px',
                mb: '16px',
              }}
            >
              {Array.from(Array(3).keys()).map((item, index) => (
                <Skeleton
                  key={index}
                  animation="wave"
                  sx={{
                    width: !index ? '40%' : '100%',
                    height: '30px',
                    borderRadius: '12px',
                    my: '2px',
                  }}
                />
              ))}
            </Box>

            {mode === 'dashboard' ? (
              <Box
                sx={{
                  maxWidth: '100%',
                  height: { xs: 'auto', lg: 'auto' },
                  borderRadius: '22px',
                  padding: { xs: '16px 16px 24px 16px', sm: '18px 25px' },
                  display: 'flex',
                  flexDirection: { xs: 'column', lg: 'row' },
                  alignItems: { lg: 'flex-start' },
                  mb: '16px',
                  backgroundColor: 'white',
                }}
              >
                <Box
                  sx={{
                    maxWidth: { xs: '100%', lg: `${doughnut_chart_width}px` },
                    minWidth: { xl: '40%' },
                    height: { xs: '360px', xl: '463px' },
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'flex-start',
                    flexDirection: 'column',
                    // backgroundColor: 'blue'
                    // justifyContent: 'space-between',
                  }}
                >
                  <Box
                    sx={{
                      width: '100%',
                    }}
                  >
                    <Skeleton
                      animation="wave"
                      sx={{
                        width: '156px',
                        height: '30px',
                        borderRadius: '12px',
                        my: '2px',
                      }}
                    />
                  </Box>

                  <Box
                    sx={{
                      display: 'flex',
                      flexDirection: 'column',
                      mt: '48px',
                    }}
                  >
                    <Box
                      sx={{
                        position: 'relative',
                      }}
                    >
                      <Skeleton
                        animation="wave"
                        variant="circular"
                        width={doughnut_chart_width}
                        height={doughnut_chart_width}
                      />
                      <Chip
                        sx={{
                          width: `${doughnut_chart_width - 70}px`,
                          height: `${doughnut_chart_width - 70}px`,
                          borderRadius: '50%',
                          bgcolor: 'white',
                          position: 'absolute',
                          top: '50%',
                          right: '50%',
                          transform: 'translate(50%, -50%)',
                        }}
                      />

                      <Box
                        sx={{
                          width: `${doughnut_chart_width - 100}px`,
                          height: `${doughnut_chart_width - 100}px`,
                          display: 'flex',
                          flexDirection: 'column',
                          alignItems: 'center',
                          justifyContent: 'center',
                          position: 'absolute',
                          top: '50%',
                          right: '50%',
                          transform: 'translate(50%, -50%)',
                        }}
                      >
                        <Skeleton
                          animation="wave"
                          sx={{
                            width: '90%',
                            height: '30px',
                            borderRadius: '12px',
                            my: '2px',
                          }}
                        />
                        <Skeleton
                          animation="wave"
                          sx={{
                            width: '60%',
                            height: '30px',
                            borderRadius: '12px',
                            my: '2px',
                          }}
                        />
                      </Box>
                    </Box>

                    <Box
                      sx={{
                        width: '100%',
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        flexWrap: 'wrap',
                        mt: '24px',
                      }}
                    >
                      {Array.from(Array(4).keys()).map((item, index) => (
                        <Skeleton
                          key={index}
                          animation="wave"
                          sx={{
                            width: '35%',
                            height: '30px',
                            borderRadius: '12px',
                            my: '2px',
                          }}
                        />
                      ))}
                    </Box>
                  </Box>
                </Box>
                <Divider
                  orientation="vertical"
                  variant="fullWidth"
                  flexItem
                  sx={{
                    mx: { xs: '16px', xl: '32px' },
                    display: { xs: 'none', lg: 'block' },
                    borderColor: 'secondary.light',
                  }}
                />
                <Divider
                  orientation="horizontal"
                  variant="fullWidth"
                  flexItem
                  sx={{
                    my: '30px',
                    display: { xs: 'block', lg: 'none' },
                    borderColor: 'secondary.light',
                  }}
                />
                <Box
                  sx={{
                    width: '100%',
                    height: '100%',
                    display: 'flex',
                    flexDirection: 'column',
                    // justifyContent: 'space-between',
                    // backgroundColor: 'red'
                  }}
                >
                  <Skeleton
                    animation="wave"
                    sx={{
                      width: '156px',
                      height: '30px',
                      borderRadius: '12px',
                      my: '2px',
                    }}
                  />

                  <Box
                    sx={{
                      display: 'flex',
                      flexDirection: 'column',
                      alignItems: 'flex-start',
                      mt: '68px',
                      gap: '44px',
                    }}
                  >
                    {Array.from(Array(3).keys()).map((item, index) => (
                      <Box
                        key={index}
                        sx={{
                          width: '100%',
                          display: 'flex',
                          alignItems: 'center',
                          pr: '24px',
                        }}
                      >
                        <Skeleton
                          animation="wave"
                          variant="rectangular"
                          sx={{
                            width: '32px',
                            height: '32px',
                            borderRadius: '6px',
                            mr: '24px',
                          }}
                        />
                        <Skeleton
                          animation="wave"
                          sx={{
                            flex: 1,
                            height: '30px',
                            borderRadius: '12px',
                            my: '2px',
                          }}
                        />
                      </Box>
                    ))}
                  </Box>
                </Box>
              </Box>
            ) : (
              <Box
                sx={{
                  maxWidth: '100%',
                  height: 'auto',
                  borderRadius: '22px',
                  padding: { xs: '16px 16px 24px 16px', sm: '18px 25px' },
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: { lg: 'flex-start' },
                  mb: '16px',
                  mt: { xs: '16px', sm: '0' },
                  backgroundColor: 'white',
                }}
              >
                <Box
                  sx={{
                    width: 'fit-content',
                    display: 'flex',
                    alignSelf: 'flex-start',
                    alignItems: 'center',
                    mb: '25px',
                  }}
                >
                  <Box
                    component={'img'}
                    src={bulletPoint.src}
                    alt={'bullet-point'}
                    sx={{
                      width: '22px',
                      height: '18px',
                      mr: '9px',
                      transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                    }}
                  />
                  <Typography
                    variant="inherit"
                    component="span"
                    color={'text.primary'}
                    sx={{
                      // fontWeight: {xs: '600'},
                      fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                      fontSize: { xs: '16px', xl: '16px' },
                    }}
                  >
                    {t('my_assets:my_assets')}
                  </Typography>
                </Box>
                <Box
                  sx={{
                    width: '100%',
                    display: 'flex',
                    flexDirection: { xs: 'column', sm: 'row' },
                    alignItems: { xs: 'center', sm: 'flex-start' },
                    justifyContent: { sm: 'space-between', lg: 'space-around' },
                    mt: mode === 'wallet' ? { lg: '20px' } : '0',
                  }}
                >
                  <Box
                    sx={{
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                  >
                    {/*<Chart mode={`doughnut-${mode}`} chartData={data}*/}
                    {/*       chartWidth={doughnut_chart_width}*/}
                    {/*       chartHeight={doughnut_chart_width}/>*/}
                  </Box>

                  <Box
                    sx={{
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                      mt: { xs: '32px', sm: '0' },
                    }}
                  >
                    {/*<Chart mode={`line-${mode}`} chartData={data}*/}
                    {/*       chartWidth={lg ? 270 : 150}*/}
                    {/*       chartHeight={190} color={'#ff00ff'}/>*/}
                  </Box>
                </Box>

                <Box
                  sx={{
                    width: '100%',
                    height: 'auto',
                    display: 'flex',
                    flexDirection: 'column',
                    mt: { lg: '40px' },
                    // justifyContent: 'space-between',
                    // backgroundColor: 'red'
                  }}
                >
                  <CurrencyTable mode={mode} />
                </Box>
              </Box>
            )}

            <UsefulServices mode={'dashboard'} loading={dashboardReady} />
          </Box>
        ) : (
          <Box
            sx={{
              display: 'flex',
              flexFlow: 'column',
              // height: '100%',
              flexGrow: 1,
            }}
          >
            <Box
              sx={{
                mb: '8px',
                mt: { xs: '16px', sm: '0' },
              }}
            >
              {bannerResult.bannerResp?.length > 0 && (
                <NextLink href={bannerResult.bannerResp[0].link} passHref>
                  {/*<a*/}
                  {/*    style={{fontFamily: 'none'}}*/}
                  {/*>*/}
                  <Link href="#" underline="none" sx={{ fontFamily: 'none' }}>
                    <Box
                      component={'img'}
                      src={optimizedBanner}
                      alt={bannerResult.bannerResp[0].title}
                      sx={{
                        // 'content': {
                        //     xs: `url(${bannerResult.bannerResp[0].small_image})`,
                        //     sm: `url(${bannerResult.bannerResp[0].medium_image})`,
                        //     lg: `url(${bannerResult.bannerResp[0].large_image})`,
                        // },
                        maxHeight: '157px',
                        width: '100%',
                        height: 'auto',
                        objectFit: 'fill',
                        borderRadius: '22px',
                      }}
                    />
                  </Link>
                  {/*</a>*/}
                </NextLink>
              )}
            </Box>

            <MyAssets mode={'dashboard'} />
            <UsefulServices mode={'dashboard'} loading={dashboardReady} />
          </Box>
        )}
      </Box>
    </>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      hasLayout: true,
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default Dashboard;
