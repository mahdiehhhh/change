import React, { useEffect, useState } from 'react';
import { Box, Grid, Link, Typography } from '@mui/material';
import { MyWallet } from 'Components';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { MyAssets } from 'Components/MyAssets/MyAssets.component';
import { UsefulServices } from 'Components/UsefulServices/UsefulServices.component';
import { useAppDispatch, useAppSelector } from 'store/hooks';
import Head from 'next/head';
import { useI18nTrDir } from '../../../utils/CustomHooks/i18nTrDir';
import { localesNS } from '../../../utils/Data/locales_NameSpace.utils';
import NextLink from 'next/link';
import Ticket from '../../../Components/Ticket/Ticket.component';
import { fetchCreateNewTicket } from '../../../store/slices/create_ticket.slice';

interface BannerSample {
  title: string;
  link: string;
  image: string;
}

const Tickets = () => {
  const dispatch = useAppDispatch();

  const { t, i18n, pageDirection } = useI18nTrDir();

  useEffect(() => {
    i18n.reloadResources(i18n.resolvedLanguage, [...localesNS]);
    dispatch(fetchCreateNewTicket('reset'));
  }, []);

  const currenciesDashResult = useAppSelector((state) => state.CurrenciesDashboard);

  return (
    <>
      <Head>
        <title>{`${t('layout:zimapay')} | ${t('layout:open_tickets')}`}</title>
      </Head>
      <Box
        sx={{
          width: '100%',
        }}
      >
        <Ticket />
      </Box>
    </>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      hasLayout: true,
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default Tickets;
