import React, { useEffect, useState } from 'react';
import { Box, Grid, Link, Typography } from '@mui/material';
import { MyWallet } from 'Components';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { MyAssets } from 'Components/MyAssets/MyAssets.component';
import { UsefulServices } from 'Components/UsefulServices/UsefulServices.component';
import { useAppDispatch, useAppSelector } from 'store/hooks';
import Head from 'next/head';
import { useI18nTrDir } from 'utils/CustomHooks/i18nTrDir';
import { localesNS } from 'utils/Data/locales_NameSpace.utils';
import NextLink from 'next/link';
import Ticket from 'Components/Ticket/Ticket.component';
import RecordNewTicket from '../../../../Components/RecordNewTicket/RecordNewTicket.component';

const NewTicket = () => {
  const dispatch = useAppDispatch();

  const { t, i18n, pageDirection } = useI18nTrDir();

  useEffect(() => {
    i18n.reloadResources(i18n.resolvedLanguage, [...localesNS]);
  }, []);

  const currenciesDashResult = useAppSelector((state) => state.CurrenciesDashboard);

  return (
    <>
      <Head>
        <title>{`${t('layout:zimapay')} | ${t('layout:record_new_ticket')}`}</title>
      </Head>
      <Box
        sx={{
          width: '100%',
        }}
      >
        <RecordNewTicket />
      </Box>
    </>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      hasLayout: true,
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default NewTicket;
