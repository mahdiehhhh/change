import React, { useEffect } from 'react';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import AccountsInfoCards from 'Components/AccountsInfoCards/AccountsInfoCards.component';
import { useI18nTrDir } from '../../../utils/CustomHooks/i18nTrDir';
import { localesNS } from '../../../utils/Data/locales_NameSpace.utils';
import Head from 'next/head';
import Profile from '../../../Components/Profile/Profile.component';
import PaymentReceipt from '../../../Components/PaymentReceipt/PaymentReceipt.component';
import TransferNewDestination from '../../../Components/TransferNewDestination/TransferNewDestination.component';

const MyInfoPage = () => {
  const { t, i18n, pageDirection } = useI18nTrDir();

  useEffect(() => {
    i18n.reloadResources(i18n.resolvedLanguage, [...localesNS]);
  }, []);

  return (
    <>
      <Head>
        <title>{`${t('layout:zimapay')} | ${t('layout:view_my_info')}`}</title>
      </Head>
      <h1>This is My personal info page</h1>
      {/*<PaymentReceipt/>*/}
      {/*<TransferNewDestination/>*/}
    </>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      hasLayout: true,
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default MyInfoPage;
