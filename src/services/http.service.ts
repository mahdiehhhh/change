import axios from 'axios';

import { BASE_URL, ACCESS_TOKEN, IS_LOGGED_IN, NEXT_LOCALE } from 'configs/variables.config';
import { CLIENT_ID, CLIENT_SECRET, GRANT_TYPE } from '../configs/backend.config';
import {
  API_ACCESS_TOKEN_URL,
  API_CHECK_PHONE,
  API_CREATE_INTERNATIONAL_BANK_ACCOUNT,
  API_LOGIN,
  API_PROFILE_AUTHENTICATION_VERIFY,
  API_TICKETS,
  API_USER_CREATE_TICKET,
} from '../configs/api_address.config';
import { getCookie, handleGetTokens } from '../utils/functions/local';
import { useTranslation } from 'next-i18next';
import { PATHS } from '../configs/routes.config';

// import {toast} from 'react-toastify';

class HttpService {
  constructor() {
    axios.defaults.baseURL = BASE_URL;

    axios.interceptors.request.use(
      (config) => {
        // const AccessToken = handleGetTokens(ACCESS_TOKEN)
        const AccessToken = getCookie(ACCESS_TOKEN);
        const IsLoggedIn = getCookie(IS_LOGGED_IN);
        console.log('this is login cookieeeeee', IsLoggedIn);
        const language = this.getCookieVal(NEXT_LOCALE);
        if (AccessToken && !IsLoggedIn) {
          // @ts-ignore
          config.headers['Authorization'] = 'Bearer ' + AccessToken;
          // @ts-ignore
          config.headers['language'] = language;
        } else if (AccessToken && IsLoggedIn) {
          // @ts-ignore
          config.headers['Authorization'] = 'Bearer ' + IsLoggedIn;
          // @ts-ignore
          config.headers['language'] = language;
          if (
            config.url === API_CREATE_INTERNATIONAL_BANK_ACCOUNT ||
            config.url === API_PROFILE_AUTHENTICATION_VERIFY ||
            config.url === API_USER_CREATE_TICKET ||
            config.url?.includes(API_TICKETS)
          ) {
            // @ts-ignore
            config.headers['Content-Type'] = 'multipart/form-data';
          }
        }

        return config;
      },
      (error) => {
        return Promise.reject(error);
      },
    );
    //
    // axios.interceptors.response.use((response) => {
    //         return response;
    //     },
    //     (error) => {
    //         console.log('Interceptor response error', error.response && (error.response.data === 'Token Expired!' || error.response.data === 'Invalid Token!'));
    //         if (error.response.status === 401) {
    //             localStorage.setItem(IS_LOGGED_IN, false.toString());
    //             history.push(PATHS.LOG_IN)
    //             window.location.assign(window.location.search)
    //         } else {
    //             toast.error(error.response.data)
    //         }
    //         return Promise.reject(error);
    //     })
  }

  get(url: string, config?: object) {
    return axios.get(url, config);
  }

  post(url: string, data: object, config?: object) {
    return axios.post(url, data, config);
  }

  put(url: string, data: object, config?: object) {
    return axios.put(url, data, config);
  }

  patch(url: string, data: object, config?: object) {
    return axios.patch(url, data, config);
  }

  delete(url: string, config?: object) {
    return axios.delete(url, config);
  }

  getCookieVal(name: string) {
    let cookies: any = {};
    document.cookie.split(';').forEach((cookie: string) => {
      let [key, value] = cookie.split('=');
      cookies[key.trim()] = value;
    });
    return cookies[name];
  }
}

export default new HttpService();
