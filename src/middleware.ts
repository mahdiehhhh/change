import { NextResponse, NextRequest } from 'next/server';

import { IS_LOGGED_IN } from './configs/variables.config';
import { PATHS } from './configs/routes.config';

export default function middleware(req: NextRequest) {
  let verify = req.cookies.get(IS_LOGGED_IN);

  // let url = req.url
  // if(!verify && url.includes('user/dashboard')){
  //     return NextResponse.redirect('http://localhost:3001/user/login')
  // }

  // const url = req.nextUrl.clone()
  // // console.log('what is req url', !verify, !url.includes('user/login'))
  //
  // if (!verify) {
  //     url.pathname = '/user/login'
  //     return NextResponse.redirect(url)
  // }

  const { pathname } = req.nextUrl;

  if (!verify && pathname === '/') {
    req.nextUrl.pathname = `${PATHS.USER_LOGIN}`;

    return NextResponse.redirect(req.nextUrl);
  } else if (verify && pathname === '/') {
    req.nextUrl.pathname = `${PATHS.USER_DASHBOARD}`;
    return NextResponse.redirect(req.nextUrl);
  } else if (!verify && pathname.startsWith('/user')) {
    return NextResponse.next();
  } else if (verify && pathname.startsWith('/user')) {
    req.nextUrl.pathname = `${PATHS.USER_DASHBOARD}`;
    return NextResponse.redirect(req.nextUrl);
  } else if (verify && pathname === `/${PATHS.USER_EXCHANGE}`) {
    req.nextUrl.pathname = `${PATHS.USER_EXCHANGE}/${PATHS.RATE}/${PATHS.FIAT}`;
    return NextResponse.redirect(req.nextUrl);
  } else if (verify && pathname === `/${PATHS.USER_EXCHANGE}/${PATHS.FEE}`) {
    req.nextUrl.pathname = `${PATHS.USER_EXCHANGE}/${PATHS.FEE}/${PATHS.RATE}`;
    return NextResponse.redirect(req.nextUrl);
  } else if (!verify && pathname.startsWith('/panel')) {
    req.nextUrl.pathname = `${PATHS.USER_LOGIN}`;
    return NextResponse.redirect(req.nextUrl);
  } else if (verify && pathname.startsWith('/panel')) {
    return NextResponse.next();
  }

  return NextResponse.next();
}
