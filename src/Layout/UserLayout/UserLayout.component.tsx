import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import NextLink from 'next/link';
import BankAccount from '/public/assets/images/BankAccount.svg';
import activeBankAccount from '/public/assets/images/active_bankAccount.svg';
import CustomerClub from '/public/assets/images/customerClub.svg';
import activeCustomerClub from '/public/assets/images/active_customerClub.svg';
import ExchangeRate from '/public/assets/images/exchangeRate.svg';
import activeExchangeRate from '/public/assets/images/active_exchangeRate.svg';
import Dashboard from '/public/assets/images/Home.svg';
import activeDashboard from '/public/assets/images/active_home.svg';
import Orders from '/public/assets/images/Orders.svg';
import activeOrders from '/public/assets/images/active_orders.svg';
import Setting from '/public/assets/images/Setting.svg';
import activeSetting from '/public/assets/images/active_setting.svg';
import Wallet from '/public/assets/images/Wallet.svg';
import activeWalletIcon from '/public/assets/images/active_wallet.svg';
import Profile from '/public/assets/images/Profile.svg';
import activeProfile from '/public/assets/images/active_profile.svg';
import Ticket from '/public/assets/images/Ticket.svg';
import activeTicket from '/public/assets/images/active_ticket.svg';
import sideBarRecBG from '/public/assets/images/sideBarRecBG.svg';
import zpLogo from '/public/assets/images/Zimapaylogo.svg';
import helpIcon from '/public/assets/images/Help_icon.svg';
import bell from '/public/assets/images/bell.svg';
import { CircularProgressBar, SearchBar, AccountMenu, NotificationsMenu, CounterBadge } from 'Components';
import { ReactNode, useEffect, useState } from 'react';
import { Button, Chip, MenuItem, Select, SelectChangeEvent, Skeleton } from '@mui/material';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import Collapse from '@mui/material/Collapse';
import activeMenuPointer from '/public/assets/images/Pointer.svg';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import hamburgerMenu from '/public/assets/images/hamburger-icon.svg';
import { fetchUserProfile } from '../../store/slices/user_profile.slice';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { useRouter } from 'next/router';
import { NEXT_LOCALE } from '../../configs/variables.config';
import { fetchLanguages } from '../../store/slices/languages.slice';
import { useI18nTrDir } from '../../utils/CustomHooks/i18nTrDir';
import { PATHS } from '../../configs/routes.config';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import { DialogBox } from '../../Components/DialogBox/DialogBox.component';
import { localesNS } from '../../utils/Data/locales_NameSpace.utils';

interface Lang {
  id: number;
  name: string;
  code: string;
  direction: string;
  is_master: number;
  logo: string;
  status: string;
}

interface sideBarMenuSample {
  id: string;
  name: boolean;
  menu: string;
  icon: any;
  activeIcon: any;
  link?: string;
  pathname?: string;
  query?: any;
  mainPath: string;
  subMenu: subMenuSample[];
}

interface subMenuSample {
  title: string;
  pathname: string;
}

function UserLayout(props: any) {
  const { t, i18n, pageDirection } = useI18nTrDir();

  useEffect(() => {
    i18n.reloadResources(i18n.resolvedLanguage, [...localesNS]);
  }, []);

  const theme = useTheme();
  const lg = useMediaQuery(theme.breakpoints.up('lg'));
  const xl = useMediaQuery(theme.breakpoints.up('xl'));

  const drawerWidth = xl ? 348 : lg ? 300 : 288;

  const router = useRouter();
  const { pathname, asPath, query } = router;

  const layoutPath = router.pathname;

  const { window } = props;
  const dispatch = useAppDispatch();
  const [languages, setLanguages] = useState<Lang[]>([]);
  const [lang, setLang] = React.useState(router.locale);

  const languagesAPI = useAppSelector((state) => state.Languages);
  const userActiveWalletResult = useAppSelector((state) => state.UserActiveWallets);
  const userProfileResult = useAppSelector((state) => state.UserProfile);

  const [pageReady, setPageReady] = useState(false);

  const [mobileOpen, setMobileOpen] = React.useState(false);
  const [openSubMenu, setOpenSubMenu] = useState<any>({
    dashboard: false,
    wallet: false,
    orders: false,
    exchange: false,
    profile: false,
    banking: false,
    ticket: false,
    setting: false,
    club: false,
  });

  const [dialogBox, setDialogBox] = React.useState({
    deposit: {
      display: false,
    },
    withdraw: {
      display: false,
    },
  });

  const handleOpenDialogBox = (dialogType: string) => {
    setDialogBox((prevState) => ({ ...prevState, [dialogType]: { display: true } }));
  };

  const handleCloseDialogBox = (dialogType: string) => {
    setDialogBox((prevState) => ({ ...prevState, [dialogType]: { display: false } }));
  };

  const [authPercent, setAuthPercent] = useState(80);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  useEffect(() => {
    dispatch(fetchUserProfile());
  }, []);

  useEffect(() => {
    if (
      (!userProfileResult.loading || !languagesAPI.loading) &&
      (!!userProfileResult.userProfileResp || !!languagesAPI.lang)
    ) {
      setPageReady(true);
    } else {
      setPageReady(false);
    }
  }, [userProfileResult, languagesAPI]);

  useEffect(() => {
    dispatch(fetchLanguages());
    if (getCookieVal(NEXT_LOCALE)) {
      setLang(getCookieVal(NEXT_LOCALE));
    } else {
      setLang(router.locale);
      document.cookie = `${NEXT_LOCALE}=${router.locale}; path=/`;
    }
  }, []);

  function getCookieVal(name: string) {
    const cookies: any = {};
    document.cookie.split(';').forEach((cookie: string) => {
      const [key, value] = cookie.split('=');
      cookies[key.trim()] = value;
    });
    return cookies[name];
  }

  useEffect(() => {
    setLanguages(languagesAPI.lang?.language_list);
  }, [languagesAPI]);

  const sideBarOptions = [
    {
      id: 'dashboard',
      name: openSubMenu.dashboard,
      menu: t('layout:dashboard'),
      subMenu: [],
      icon: Dashboard,
      activeIcon: activeDashboard,
      pathname: `/${PATHS.USER_DASHBOARD}`,
      mainPath: PATHS.USER_DASHBOARD,
    },
    {
      id: 'wallet',
      name: openSubMenu.wallet,
      menu: t('layout:wallet'),
      subMenu: [
        {
          title: t('layout:my_wallets'),
          pathname: `/${PATHS.USER_WALLET}/${PATHS.MY_WALLETS}`,
        },
        {
          title: t('layout:deposit_money'),
          pathname: `dialog_Box`,
        },
        {
          title: t('layout:withdraw_money'),
          pathname: `dialog_Box`,
        },
        {
          title: t('layout:my_transaction'),
          pathname: `https://my.zimapay.com/wallets/transactions`,
        },
        // {title: t('layout:exchange'), pathname: `${PATHS.USER_WALLET}`}
      ],
      icon: Wallet,
      activeIcon: activeWalletIcon,
      mainPath: PATHS.USER_WALLET,
    },
    {
      id: 'orders',
      name: openSubMenu.orders,
      menu: t('layout:orders'),
      subMenu: [
        {
          title: t('layout:register_new_order'),
          pathname: `https://my.zimapay.com/orders`,
        },
        {
          title: t('layout:my_orders'),
          pathname: `https://my.zimapay.com/orders/my-orders`,
        },
      ],
      icon: Orders,
      activeIcon: activeOrders,
      mainPath: PATHS.USER_ORDERS,
    },
    {
      id: 'exchange',
      name: openSubMenu.exchange,
      menu: t('layout:exchange_rate'),
      subMenu: [
        {
          title: `${t('layout:currency_calculator')}  ${t('layout:soon')}`,
          pathname: `/${PATHS.USER_EXCHANGE}`,
        },
        {
          title: t('layout:exchange_rate'),
          pathname: `/${PATHS.USER_EXCHANGE}/${PATHS.RATE}/${PATHS.FIAT}`,
        },
        {
          title: `${t('layout:wages')}  ${t('layout:soon')}`,
          pathname: `/${PATHS.USER_EXCHANGE}/${PATHS.FEE}`,
        },
      ],
      icon: ExchangeRate,
      activeIcon: activeExchangeRate,
      mainPath: PATHS.USER_EXCHANGE,
    },
    {
      id: 'profile',
      name: openSubMenu.profile,
      menu: t('layout:profile'),
      subMenu: [
        {
          title: t('layout:view_my_info'),
          pathname: `/${PATHS.USER_PROFILE}`,
        },
        {
          title: t('layout:my_bank_accounts'),
          pathname: `/${PATHS.USER_PROFILE}/${PATHS.BANK_ACCOUNTS}`,
        },
        {
          title: t('layout:Authentication'),
          pathname: `/${PATHS.USER_PROFILE}/${PATHS.VERIFY}`,
        },
      ],
      icon: Profile,
      activeIcon: activeProfile,
      mainPath: PATHS.USER_PROFILE,
    },
    {
      id: 'banking',
      name: openSubMenu.banking,
      menu: t('layout:bank_account'),
      subMenu: [],
      icon: BankAccount,
      activeIcon: activeBankAccount,
      // link: `${PATHS.USER_BANK_ACCOUNT}${PATHS.IRANIAN}`,
      pathname: `/${PATHS.USER_BANK_ACCOUNT}/${PATHS.IRANIAN}`,
      mainPath: `/${PATHS.USER_BANK_ACCOUNT}/${PATHS.IRANIAN}`,
    },
    {
      id: 'ticket',
      name: openSubMenu.ticket,
      menu: t('layout:ticket'),
      subMenu: [],
      icon: Ticket,
      activeIcon: activeTicket,
      // link: PATHS.USER_TICKET,
      pathname: `/${PATHS.USER_TICKET}`,
      mainPath: PATHS.USER_TICKET,
    },
    {
      id: 'setting',
      name: openSubMenu.setting,
      menu: t('layout:settings'),
      subMenu: [],
      icon: Setting,
      activeIcon: activeSetting,
      // link: PATHS.USER_SETTINGS,
      pathname: 'https://my.zimapay.com/user/profile/setting',
      mainPath: PATHS.USER_SETTINGS,
    },
    {
      id: 'club',
      name: openSubMenu.club,
      menu: t('layout:customer_club'),
      subMenu: [
        {
          title: t('layout:awards_list'),
          pathname: `https://my.zimapay.com/trophies`,
        },
        {
          title: t('layout:received_awards'),
          pathname: `https://my.zimapay.com/trophies`,
        },
        {
          title: t('layout:news'),
          pathname: `https://my.zimapay.com/trophies`,
        },
      ],
      icon: CustomerClub,
      activeIcon: activeCustomerClub,
      mainPath: PATHS.USER_CUSTOMER_CLUB,
    },
  ];

  const handleOpenMenuOptions = (id: string) => {
    const openSelectedSubMenu = Object.keys(openSubMenu).reduce(
      (accumulator, key) => ({ ...accumulator, [key]: false, [id]: !openSubMenu[id] }),
      {},
    );
    setOpenSubMenu(openSelectedSubMenu);
  };

  const handleChangeLanguage = (event: SelectChangeEvent<string>, child: ReactNode): void => {
    setLang(event.target.value);
    document.cookie = `${NEXT_LOCALE}=${event.target.value}; path=/`;

    // router.push('/', '/', {locale: event.target.value})
    // router.push('/', '/', { locale: event.target.value, scroll: false })
  };

  const drawer = (
    <Box
      sx={{
        width: '100%',
        height: '500vh',
        backgroundColor: '#ffffff',
        borderRadius: '0 28px 28px 0',
        py: '32px',
      }}
    >
      {/*<Toolbar/>*/}
      {/*<Divider />*/}
      <Box
        sx={{
          width: '100%',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          mt: '8px',
          pl: '34px',
          pr: '12px',
          mb: { xs: '0', lg: '38px' },
        }}
      >
        <Box component={'img'} src={zpLogo.src} alt={'zima-pay-logo'} sx={{ width: '166px', height: '33px' }} />

        <NextLink href={`/${PATHS.USER_NOTIFICATIONS}`} passHref>
          <Chip
            label={<Box component={'img'} src={bell.src} alt={'bell'} sx={{ width: '17px', height: '20px' }} />}
            sx={{
              minWidth: '32px',
              minHeight: '32px',
              borderRadius: '50%',
              bgcolor: 'secondary.main',
              display: { sx: 'block', sm: 'none' },
              '& .MuiChip-label': {
                padding: '0',
                margin: '0',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
              },
            }}
          />
        </NextLink>
      </Box>

      <Box
        sx={{
          width: '100%',
          pl: '34px',
          my: '24px',
          display: { sx: 'block', lg: 'none' },
        }}
      >
        <Typography
          variant="h6"
          component="span"
          color={'text.primary'}
          sx={{
            // fontWeight: '600',
            fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
            fontSize: '17px',
          }}
        >
          {`${t('layout:hi_user', { user: userProfileResult?.userProfileResp?.name })}`}
        </Typography>
        &nbsp;
        <Typography
          variant="h6"
          noWrap
          component="span"
          color={'text.primary'}
          sx={{
            fontWeight: '400',
            fontSize: '17px',
          }}
        >
          {t('layout:hi_dear')}
        </Typography>
      </Box>

      <List sx={{ width: '100%' }} component="nav">
        {sideBarOptions.map((option: sideBarMenuSample, index) => (
          // @ts-ignore
          <>
            {option.subMenu.length ? (
              <ListItemButton
                disableRipple={true}
                sx={{
                  pl: '34px',
                  position: 'relative',
                  '& .MuiSvgIcon-root': {
                    color: 'text.secondary',
                  },
                }}
                onClick={option.subMenu.length ? () => handleOpenMenuOptions(option.id) : undefined}
              >
                {layoutPath.includes(option.mainPath) && (
                  <Box
                    component={'img'}
                    src={activeMenuPointer.src}
                    alt={'pointer'}
                    sx={{
                      position: 'absolute',
                      left: '0',
                      width: '5px',
                      height: '31px',
                      transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                    }}
                  />
                )}

                <ListItemIcon
                  sx={{
                    minWidth: 'auto',
                  }}
                >
                  <Box
                    component={'img'}
                    src={layoutPath.includes(option.mainPath) ? option.activeIcon.src : option.icon.src}
                    alt={option.menu}
                    sx={{
                      width: '20px',
                      height: '20px',
                      mr: { xs: '24px', sm: '16px', lg: '32px' },
                      mb: '2px',
                    }}
                  />
                </ListItemIcon>

                <ListItemText
                  primary={
                    <Typography
                      variant="inherit"
                      component="span"
                      color={layoutPath.includes(option.mainPath) ? 'primary.main' : 'text.secondary'}
                      sx={{
                        fontFamily: layoutPath.includes(option.mainPath)
                          ? 'IRANSansXFaNum-DemiBold'
                          : 'IRANSansXFaNum-Regular',
                        // fontWeight: layoutPath.includes(option.mainPath) ? '700' : '400',
                        fontSize: '16px',
                      }}
                    >
                      {option.menu}
                    </Typography>
                  }
                />

                {option.id === 'ticket' && (
                  <CounterBadge
                    value={2}
                    width={'40px'}
                    height={'24px'}
                    bgColor={'primary.light'}
                    textColor={'primary.main'}
                  />
                )}

                {!!option.subMenu.length && (option.name ? <ExpandLess /> : <ExpandMore />)}
              </ListItemButton>
            ) : (
              <NextLink
                href={{
                  pathname: `${option.pathname}`,
                  query: option.query,
                }}
              >
                <ListItemButton
                  disableRipple={true}
                  sx={{
                    pl: '34px',
                    position: 'relative',
                  }}
                  onClick={option.subMenu.length ? () => handleOpenMenuOptions(option.id) : undefined}
                >
                  {layoutPath.includes(option.mainPath) && (
                    <Box
                      component={'img'}
                      src={activeMenuPointer.src}
                      alt={'pointer'}
                      sx={{
                        position: 'absolute',
                        left: '0',
                        width: '5px',
                        height: '31px',
                        transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                      }}
                    />
                  )}

                  <ListItemIcon
                    sx={{
                      minWidth: 'auto',
                    }}
                  >
                    <Box
                      component={'img'}
                      src={layoutPath.includes(option.mainPath) ? option.activeIcon.src : option.icon.src}
                      alt={option.menu}
                      sx={{
                        width: '20px',
                        height: '20px',
                        mr: { xs: '24px', sm: '16px', lg: '32px' },
                        mb: '2px',
                      }}
                    />
                  </ListItemIcon>

                  <ListItemText
                    primary={
                      <Typography
                        variant="inherit"
                        component="span"
                        color={layoutPath.includes(option.mainPath) ? 'primary.main' : 'text.secondary'}
                        sx={{
                          fontFamily: layoutPath.includes(option.mainPath)
                            ? 'IRANSansXFaNum-DemiBold'
                            : 'IRANSansXFaNum-Regular',
                          // fontWeight: layoutPath.includes(option.mainPath) ? '700' : '400',
                          fontSize: '16px',
                        }}
                      >
                        {option.menu}
                      </Typography>
                    }
                  />

                  {option.id === 'ticket' && (
                    <CounterBadge
                      value={2}
                      width={'40px'}
                      height={'24px'}
                      bgColor={'primary.light'}
                      textColor={'primary.main'}
                    />
                  )}

                  {!!option.subMenu.length && (option.name ? <ExpandLess /> : <ExpandMore />)}
                </ListItemButton>
              </NextLink>
            )}

            {!!option.subMenu.length &&
              option.subMenu?.map((subMenuItem: subMenuSample, index) => (
                <Collapse key={index} in={option.name} timeout="auto" unmountOnExit>
                  {subMenuItem.pathname.includes('dialog_Box') ? (
                    <List
                      component="div"
                      sx={{
                        ml: { xs: '51px', sm: '43px', lg: '60px' },
                        py: 0,
                        mb: option.subMenu.length - 1 === index ? '12px' : 0,
                      }}
                      onClick={() =>
                        handleOpenDialogBox(subMenuItem.title === t('layout:deposit_money') ? 'deposit' : 'withdraw')
                      }
                    >
                      <ListItemButton sx={{ pl: 4 }} disableRipple={true}>
                        <ListItemText
                          sx={{ my: 0 }}
                          primary={
                            <Typography
                              variant="inherit"
                              component="span"
                              color={layoutPath === `${subMenuItem.pathname}` ? 'primary.main' : 'text.secondary'}
                              sx={{
                                // fontWeight: '400',
                                fontFamily:
                                  layoutPath === `${subMenuItem.pathname}`
                                    ? 'IRANSansXFaNum-DemiBold'
                                    : 'IRANSansXFaNum-Regular',
                                fontSize: '14px',
                              }}
                            >
                              {subMenuItem.title}
                            </Typography>
                          }
                        />
                      </ListItemButton>
                    </List>
                  ) : (
                    <NextLink
                      href={{
                        pathname: `${subMenuItem.pathname}`,
                      }}
                    >
                      <List
                        component="div"
                        sx={{
                          ml: { xs: '51px', sm: '43px', lg: '60px' },
                          py: 0,
                          mb: option.subMenu.length - 1 === index ? '12px' : 0,
                        }}
                      >
                        <ListItemButton sx={{ pl: 4 }} disableRipple={true}>
                          <ListItemText
                            sx={{ my: 0 }}
                            primary={
                              <Typography
                                variant="inherit"
                                component="span"
                                color={layoutPath === `${subMenuItem.pathname}` ? 'primary.main' : 'text.secondary'}
                                sx={{
                                  // fontWeight: '400',
                                  fontSize: '14px',
                                  fontFamily:
                                    layoutPath === `${subMenuItem.pathname}`
                                      ? 'IRANSansXFaNum-DemiBold'
                                      : 'IRANSansXFaNum-Regular',
                                }}
                              >
                                {subMenuItem.title}
                              </Typography>
                            }
                          />
                        </ListItemButton>
                      </List>
                    </NextLink>
                  )}
                </Collapse>
              ))}
          </>
        ))}
      </List>

      {/*<Divider/>*/}

      <Box
        sx={{
          width: '100%',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          mt: '72px',
        }}
      >
        <Box
          sx={{
            width: { xs: '223px', sm: '235px', lg: '244px', xl: '284px' },
            height: 'auto',
            borderRadius: '22px',
            padding: { xs: '12px', sm: '16px', lg: '14px', xl: '24px' },
            display: 'flex',
            justifyContent: 'center',
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            backgroundImage: `url(${sideBarRecBG.src})`,
          }}
        >
          <Box
            id={'auth-form-wrapper'}
            sx={{
              width: '100%',
              height: '100%',
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              textAlign: 'center',
            }}
          >
            <CircularProgressBar value={authPercent} />
            {authPercent !== 100 && (
              <>
                <Typography
                  variant="inherit"
                  component="span"
                  color={'text.primary'}
                  sx={{
                    fontWeight: '400',
                    fontSize:
                      pageDirection === 'rtl'
                        ? {
                            xs: '14px',
                            lg: '14px',
                            xl: '16px',
                          }
                        : { xs: '12px', lg: '14px' },
                    mt: { xs: '12px', sm: '16px' },
                  }}
                >
                  {t('layout:authentication_not_completed')}
                </Typography>
                <Typography
                  variant="inherit"
                  component="span"
                  color={'text.secondary'}
                  sx={{
                    fontWeight: '400',
                    fontSize: { xs: '12px', lg: '12px', xl: '14px' },
                    mt: { xs: '4px', sm: '8px' },
                  }}
                >
                  {t('layout:enter_info_to_authenticate')}
                </Typography>
                <Button
                  variant="contained"
                  disableRipple={true}
                  sx={{
                    width: '100%',
                    height: { xs: '32px', sm: '40px' },
                    '&:hover': {
                      boxShadow: 'none',
                    },
                    boxShadow: 'none',
                    fontWeight: '400',
                    borderRadius: '12px',
                    fontSize: '14px',
                    mt: { xs: '12px', sm: '24px', lg: '20px' },
                  }}
                >
                  {t('layout:continue')}
                </Button>
              </>
            )}
          </Box>
        </Box>
      </Box>
    </Box>
  );

  const container = window !== undefined ? () => window().document.body : undefined;

  return (
    <>
      <Box sx={{ minWidth: 'fit-content', height: 'auto', bgcolor: '#F6F7FA' }}>
        <Box sx={{ display: 'flex' }}>
          <CssBaseline />

          {!pageReady ? (
            <AppBar
              position="fixed"
              sx={{
                width: { lg: `calc(100% - ${drawerWidth}px)` },
                ml: { lg: `${drawerWidth}px` },
                bgcolor: 'secondary.main',
                boxShadow: 'none',
              }}
            >
              <Toolbar sx={{ pt: '16px', pb: '8px' }}>
                <Skeleton
                  animation="wave"
                  sx={{
                    width: '50px',
                    height: '30px',
                    borderRadius: '12px',
                    mr: 2,
                    display: { lg: 'none' },
                  }}
                />

                <Grid container columnSpacing={2}>
                  <Grid
                    item
                    xs={0}
                    lg={4}
                    sx={{
                      display: { xs: 'none', lg: 'flex' },
                      alignItems: 'center',
                      justifyContent: 'flex-start',
                    }}
                  >
                    <Skeleton
                      animation="wave"
                      sx={{
                        width: '25%',
                        height: '30px',
                        borderRadius: '12px',
                      }}
                    />
                  </Grid>
                  <Grid item xs={12} lg={8} sx={{ display: 'flex', alignItems: 'center', justifyContent: 'flex-end' }}>
                    <Skeleton
                      animation="wave"
                      sx={{
                        width: '60%',
                        height: '30px',
                        borderRadius: '12px',
                      }}
                    />
                  </Grid>
                </Grid>
              </Toolbar>
            </AppBar>
          ) : (
            <AppBar
              position="fixed"
              sx={{
                width: { lg: `calc(100% - ${drawerWidth}px)` },
                ml: { lg: `${drawerWidth}px` },
                bgcolor: 'secondary.main',
                boxShadow: 'none',
              }}
            >
              <Toolbar sx={{ pt: '16px', pb: '8px' }}>
                <IconButton
                  color="inherit"
                  aria-label="open drawer"
                  edge="start"
                  onClick={handleDrawerToggle}
                  sx={{ mr: 2, display: { lg: 'none' } }}
                >
                  <Box
                    component={'img'}
                    src={hamburgerMenu.src}
                    alt={'menu icon'}
                    sx={{
                      width: { xs: '24px', sm: '48px' },
                      height: { xs: '24px', sm: '48px' },
                      transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                    }}
                  />
                </IconButton>

                <Grid container columnSpacing={2}>
                  <Grid
                    item
                    xs={0}
                    lg={4}
                    sx={{
                      display: { xs: 'none', lg: 'flex' },
                      alignItems: 'center',
                      justifyContent: 'flex-start',
                    }}
                  >
                    <Typography
                      variant="h6"
                      noWrap
                      component="div"
                      color={'text.primary'}
                      sx={{
                        // fontWeight: '600',
                        fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                        fontSize: '17px',
                      }}
                    >
                      {`${t('layout:hi_user', { user: userProfileResult?.userProfileResp?.name })}`}
                    </Typography>
                    &nbsp;
                    <Typography
                      variant="h6"
                      noWrap
                      component="div"
                      color={'text.primary'}
                      sx={{
                        fontWeight: '400',
                        fontSize: '17px',
                      }}
                    >
                      {t('layout:hi_dear')}
                    </Typography>
                  </Grid>
                  <Grid item xs={12} lg={8} sx={{ display: 'flex', alignItems: 'center', justifyContent: 'flex-end' }}>
                    <Select
                      value={lang}
                      onChange={handleChangeLanguage}
                      displayEmpty
                      MenuProps={{
                        // elevation: 0,
                        anchorOrigin: {
                          vertical: 'bottom',
                          horizontal: 'left',
                        },
                        transformOrigin: {
                          vertical: 'top',
                          horizontal: 'left',
                        },
                        PaperProps: {
                          sx: {
                            mt: '8px',
                            // width: '150px',
                            boxShadow: '0px 4px 38px rgba(68,68,68,0.1)',
                            borderRadius: '10px',
                          },
                        },
                      }}
                      sx={{
                        mr: { xs: '0', sm: '24px' },
                        fontSize: { xs: '12px', lg: '14px' },
                        // '& .MuiSvgIcon-root': {transform: 'rotate(180deg)'},
                        // '& .muirtl-bpeome-MuiSvgIcon-root-MuiSelect-icon': {transform: 'rotate(0deg)'},
                        '& fieldset': { border: 'none' },
                        '& .MuiSelect-select': { px: '0' },
                      }}
                    >
                      {languages?.map((language) => (
                        <MenuItem value={language.code} key={language.id} sx={{ px: '10px' }}>
                          <NextLink href={{ pathname, query }} as={asPath} locale={language.code}>
                            <Box
                              sx={{
                                display: 'flex',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                              }}
                            >
                              <Box
                                component={'img'}
                                src={language.logo}
                                sx={{
                                  width: '22px',
                                  height: '22px',
                                  borderRadius: '50%',
                                  border: '1px solid',
                                  borderColor: 'text.disabled',
                                  mr: '10px',
                                }}
                                alt=""
                              />
                              <Typography variant="inherit" component="span" color={'text.secondary'}>
                                {language.name}
                              </Typography>
                            </Box>
                          </NextLink>
                        </MenuItem>
                      ))}
                    </Select>
                    <SearchBar />
                    <Box
                      sx={{
                        minWidth: '32px',
                        minHeight: '32px',
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: '50px',
                        backgroundColor: '#ffffff',
                        ml: '20px',
                      }}
                    >
                      <Box
                        component={'img'}
                        src={helpIcon.src}
                        alt={'help icon'}
                        sx={{
                          width: '24px',
                          height: '24px',
                        }}
                      />
                    </Box>

                    <NotificationsMenu mode={'menu'} />

                    <AccountMenu />
                  </Grid>
                </Grid>
              </Toolbar>
            </AppBar>
          )}
          <Box component="nav" sx={{ width: { lg: drawerWidth }, flexShrink: { lg: 0 } }} aria-label="mailbox folders">
            {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
            <Drawer
              SlideProps={{ direction: pageDirection === 'rtl' ? 'left' : 'right' }}
              container={container}
              variant="temporary"
              open={mobileOpen}
              onClose={handleDrawerToggle}
              ModalProps={{
                keepMounted: true, // Better open performance on mobile.
              }}
              sx={{
                display: { xs: 'block', lg: 'none' },
                '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
              }}
            >
              {drawer}
            </Drawer>
            <Drawer
              variant="permanent"
              sx={{
                display: { xs: 'none', lg: 'block' },
                '& .MuiDrawer-paper': {
                  boxSizing: 'border-box',
                  width: drawerWidth,
                  bgcolor: 'secondary.main',
                  border: 'none',
                },
              }}
              open
            >
              {drawer}
            </Drawer>
          </Box>
          <Box
            component="main"
            sx={{
              flexGrow: 1,
              padding: '24px 24px 24px 24px',
              // pt: 3,
              // pb: 1,
              // px: 3,
              width: { lg: `calc(100% - ${drawerWidth}px)` },
              minHeight: '100vh',
            }}
          >
            <Toolbar />

            {props.children}
          </Box>
        </Box>
      </Box>

      {dialogBox.deposit.display && (
        <DialogBox
          operation={'deposit'}
          open={dialogBox.deposit.display}
          close={handleCloseDialogBox}
          activeUid={
            !!userActiveWalletResult?.userActWalletsResp?.length && userActiveWalletResult?.userActWalletsResp[0].uid
          }
        />
      )}

      {dialogBox.withdraw.display && (
        <DialogBox
          operation={'withdraw'}
          open={dialogBox.withdraw.display}
          close={handleCloseDialogBox}
          activeUid={
            !!userActiveWalletResult?.userActWalletsResp?.length && userActiveWalletResult?.userActWalletsResp[0].uid
          }
        />
      )}
    </>
  );
}

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export { UserLayout };
