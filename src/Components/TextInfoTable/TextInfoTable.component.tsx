import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

import { Box, Link, Typography, TableSortLabel, Pagination, Chip } from '@mui/material';
import rectDollarIcon from '/public/assets/images/Rect-dollar-icon.svg';
import rectEuroIcon from '/public/assets/images/Rect-euro-icon.svg';
import rectLiraIcon from '/public/assets/images/Rect-lir-icon.svg';
import increaseIcon from '/public/assets/images/Increase-icon.svg';
import decreaseIcon from '/public/assets/images/decrease-icon.svg';
import starIcon from '/public/assets/images/Star-icon.svg';
import detailIcon from '/public/assets/images/vertical-3point.svg';
import { Chart } from '../Chart/Chart.component';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { useEffect, useState } from 'react';
import curlyVector from '/public/assets/images/curlyVector.svg';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { Trans, useTranslation } from 'next-i18next';
import { useI18nTrDir } from '../../utils/CustomHooks/i18nTrDir';
import { visuallyHidden } from '@mui/utils';
import { PATHS } from '../../configs/routes.config';
import backwardArrowDeposit from '*.svg';
import IconButton from '@mui/material/IconButton';
import coloredStarIcon from '/public/assets/images/colored-star-icon.svg';
import goldenStarIcon from '/public/assets/images/golden-favorite -star-icon.svg';
import upDownSortIcon from '/public/assets/images/up-down-sort-icon.svg';
import { localesNS } from '../../utils/Data/locales_NameSpace.utils';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import { fetchRemoveCurrencyFavorite } from '../../store/slices/remove_currency_from_favorite.slice';
import { fetchAddCurrencyFavorite } from '../../store/slices/add_currency_to_favorite.slice';
import { CounterBadge } from '../CounterBadge/CounterBadge.component';
import jmoment from 'jalali-moment';
import NextLink from 'next/link';
import { val } from 'dom7';
import { useRouter } from 'next/router';

interface CurrencyTableType {
  mode: string;
  AllFilteredResultsArray?: [];
  searchedText?: string;
}

interface DashboardCurrency {
  title?: string;
  image?: string;
  change_percent?: string;
  color?: string;
  iso_code?: string;
  symbol?: string;
  type?: string;
  price_history?: PriceHistorySample[];
  buy_price?: string;
  sell_price?: string;
  currency?: string;
  wallet_logo?: string;
  balance?: number;
  rial_balance?: number;
  benefit?: number;
}

interface ColumnsSample {
  type: string;
  id: string;
  label: string;
  width: string;
  maxWidth: string;
  align: string;
  isSortable?: boolean;
}

interface PriceHistorySample {
  average_buy_price: string;
  average_sell_price: string;
  day: string;
}

const TextInfoTable = ({ mode, AllFilteredResultsArray, searchedText }: CurrencyTableType) => {
  const router = useRouter();
  const { t, i18n, pageDirection } = useI18nTrDir();

  const dispatch = useAppDispatch();
  const userTicketsResult = useAppSelector((state) => state.UserTickets);
  const feesListResult = useAppSelector((state) => state.FeesList);

  const [properResultState, setProperResultState] = useState<any>();
  const [rateResultState, setRateResultState] = useState<any>();
  const [order, setOrder] = React.useState<Order>('asc');
  const [orderBy, setOrderBy] = React.useState<keyof any>('calories');

  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const itemsPerPage = 10;

  useEffect(() => {
    if (mode.includes('ticket')) {
      let rows: [] = [];
      rows = mode.includes('open')
        ? userTicketsResult.userTicketsResp?.open_tickets_list
        : userTicketsResult.userTicketsResp?.archived_tickets_list;
      setProperResultState(rows);
      !!rows?.length && setTotalPages(Math.ceil(+rows?.length / itemsPerPage));
    }
  }, [userTicketsResult]);

  useEffect(() => {
    if (mode.includes('rate')) {
      let rows: [] = [];
      rows = (
        mode.includes('open_account')
          ? feesListResult.feesListResp?.open_accounts_fee
          : feesListResult.feesListResp?.exchange_fee_list
      )?.map((item: any) => createData(item));
      setRateResultState(rows);
      // setProperResultState(rows)
      !!rows?.length && setTotalPages(Math.ceil(+rows?.length / itemsPerPage));
    }
  }, [feesListResult]);

  // useEffect(() => {
  //     console.log('what is proper result state 333', properResultState)
  // }, [properResultState])

  useEffect(() => {
    setProperResultState(rateResultState);
  }, [rateResultState]);

  function createData(object: any) {
    if (mode === 'fees_rate') {
      const titleNameIcon = { title: object.website_name, image: object.website_logo };
      let clone = Object.assign({}, { titleNameIcon, ...object });
      // @ts-ignore
      delete clone.website_name;
      // @ts-ignore
      delete clone.website_logo;
      return { ...clone };
    } else if (mode === 'open_account_rate') {
      const titleNameIcon = { title: object.account_title, image: object.account_icon_app };
      const costNameIcon = { title: object.account_pay, image: object.currency_icon };
      let clone = Object.assign({}, { titleNameIcon, costNameIcon, ...object });
      // @ts-ignore
      delete clone.account_title;
      // @ts-ignore
      delete clone.account_icon_app;
      // @ts-ignore
      delete clone.account_pay;
      // @ts-ignore
      delete clone.currency_icon;
      return { ...clone };
    }
  }

  const handleRequestSort = (event: React.MouseEvent<unknown>, property: keyof any) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  useEffect(() => {
    stableSort(properResultState, getComparator(order, orderBy));
  }, [orderBy, order]);

  function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
    // @ts-ignore
    if (
      Number(
        new Date(
          jmoment(b[orderBy], i18n.language === 'fa' ? 'jYYYY/jM/jD HH:mm' : 'YYYY/M/D HH:mm').format(
            'YYYY-M-D HH:mm:ss',
          ),
        ).getTime(),
      ) <
      Number(
        new Date(
          jmoment(a[orderBy], i18n.language === 'fa' ? 'jYYYY/jM/jD HH:mm' : 'YYYY/M/D HH:mm').format(
            'YYYY-M-D HH:mm:ss',
          ),
        ).getTime(),
      )
    ) {
      return -1;
    }

    // @ts-ignore
    if (
      Number(
        new Date(
          jmoment(b[orderBy], i18n.language === 'fa' ? 'jYYYY/jM/jD HH:mm' : 'YYYY/M/D HH:mm').format(
            'YYYY-M-D HH:mm:ss',
          ),
        ).getTime(),
      ) >
      Number(
        new Date(
          jmoment(a[orderBy], i18n.language === 'fa' ? 'jYYYY/jM/jD HH:mm' : 'YYYY/M/D HH:mm').format(
            'YYYY-M-D HH:mm:ss',
          ),
        ).getTime(),
      )
    ) {
      return 1;
    }
    return 0;
  }

  type Order = 'asc' | 'desc';

  function getComparator<Key extends keyof any>(
    order: Order,
    orderBy: Key,
  ): (a: { [key in Key]: number | string }, b: { [key in Key]: number | string }) => number {
    return order === 'desc'
      ? (a, b) => {
          return descendingComparator(a, b, orderBy);
        }
      : (a, b) => {
          return -descendingComparator(a, b, orderBy);
        };
  }

  function stableSort<T>(array: readonly T[], comparator: (a: T, b: T) => number) {
    const stabilizedThis = array?.map((el, index) => [el, index] as [T, number]);
    stabilizedThis?.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) {
        return order;
      }
      return a[1] - b[1];
    });
    // return stabilizedThis.map((el) => el[0]);
    setProperResultState(stabilizedThis?.map((el) => el[0]));
  }

  const archivedTicketsColumns = [
    {
      type: 'text',
      id: 'title',
      label: t('ticket:ticket_title'),
      width: '40%',
      maxWidth: '40%',
      align: 'left',
      isSortable: false,
    },
    {
      type: 'text',
      id: 'priority',
      label: t('ticket:priority'),
      width: '30%',
      maxWidth: '30%',
      align: 'left',
      isSortable: false,
    },
    {
      type: 'text',
      id: 'created_at',
      label: t('bank_accounts:register_date'),
      width: '28%',
      maxWidth: '28%',
      align: 'left',
      isSortable: true,
    },
    {
      type: 'details-sign',
      id: 'details',
      label: ``,
      width: '1%',
      maxWidth: '1%',
      align: 'center',
      isSortable: false,
    },
  ];

  const openTicketsColumns = [
    {
      type: 'text',
      id: 'title',
      label: t('ticket:ticket_title'),
      width: '25%',
      maxWidth: '25%',
      align: 'left',
      isSortable: false,
    },
    {
      type: 'status',
      id: 'status',
      label: t('bank_accounts:situation'),
      width: '25%',
      maxWidth: '25%',
      align: 'left',
      isSortable: false,
    },
    {
      type: 'text',
      id: 'priority',
      label: t('ticket:priority'),
      width: '23%',
      maxWidth: '23%',
      align: 'left',
      isSortable: false,
    },
    {
      type: 'text',
      id: 'created_at',
      label: t('bank_accounts:register_date'),
      width: '25%',
      maxWidth: '25%',
      align: 'left',
      isSortable: true,
    },
    {
      type: 'details-sign',
      id: 'details',
      label: ``,
      width: '2%',
      maxWidth: '2%',
      align: 'center',
      isSortable: false,
    },
  ];

  const feesRateColumns = [
    {
      type: 'name-icon',
      id: 'titleNameIcon',
      label: t('exchange:site_title'),
      width: '20%',
      maxWidth: '20%',
      align: 'left',
      isSortable: false,
    },
    {
      type: 'link',
      id: 'website_link',
      label: t('exchange:site_link'),
      width: '30%',
      maxWidth: '30%',
      align: 'left',
      isSortable: false,
    },
    {
      type: 'text',
      id: 'currency_title',
      label: t('bank_accounts:ex_type'),
      width: '15%',
      maxWidth: '15%',
      align: 'left',
      isSortable: false,
    },
    {
      type: 'wage',
      id: 'cash_revenue_fee',
      label: t('exchange:cash_out_fee'),
      width: '15%',
      maxWidth: '15%',
      align: 'center',
      isSortable: false,
    },
    {
      type: 'wage',
      id: 'charge_website_fee',
      label: t('exchange:charge_acc_fee'),
      width: '20%',
      maxWidth: '20%',
      align: 'center',
      isSortable: false,
    },
  ];

  const openAccountRateColumns = [
    {
      type: 'name-icon',
      id: 'titleNameIcon',
      label: t('ticket:title'),
      width: '30%',
      maxWidth: '30%',
      align: 'left',
      isSortable: false,
    },
    {
      type: 'cost-icon',
      id: 'costNameIcon',
      label: t('exchange:opening_fee'),
      width: '20%',
      maxWidth: '20%',
      align: 'left',
      isSortable: false,
    },
    {
      type: 'text',
      id: 'currency_title',
      label: t('bank_accounts:ex_type'),
      width: '20%',
      maxWidth: '20%',
      align: 'left',
      isSortable: false,
    },
    {
      type: 'wage',
      id: 'account_fee',
      label: t('exchange:charge_acc_fee'),
      width: '20%',
      maxWidth: '20%',
      align: 'center',
      isSortable: false,
    },
    {
      type: 'button',
      id: '',
      label: '',
      width: '10%',
      maxWidth: '10%',
      align: 'center',
      isSortable: false,
    },
  ];

  const handleChangePage = (event: any, page: number) => {
    setCurrentPage(page);
  };

  let desiredHeader: ColumnsSample[] | any[] = [];

  if (mode === 'archived_ticket') {
    desiredHeader = [...archivedTicketsColumns];
  } else if (mode === 'open_ticket') {
    desiredHeader = [...openTicketsColumns];
  } else if (mode === 'fees_rate') {
    desiredHeader = [...feesRateColumns];
  } else if (mode === 'open_account_rate') {
    desiredHeader = [...openAccountRateColumns];
  }

  return (
    <Box
      sx={{
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      }}
    >
      <TableContainer
        component={Paper}
        sx={{ boxShadow: 'none', display: 'flex', flexDirection: 'column', alignItems: 'center' }}
      >
        <Table aria-label="simple table">
          <TableHead>
            <TableRow>
              {desiredHeader.map((column: ColumnsSample, index) => {
                return !!column.isSortable ? (
                  <TableCell
                    key={column.id}
                    align={column.align as 'inherit'}
                    sx={{
                      display: {
                        xs:
                          (mode === 'open_account_rate' && column.id === 'currency_title') ||
                          (mode === 'fees_rate' && column.id === 'website_link')
                            ? 'none'
                            : 'table-cell',
                        sm: 'table-cell',
                      },
                      width: column.width,
                      maxWidth: column.maxWidth,
                      // borderBottom: mode.includes('ticket') ? 0 : '',
                      fontSize: { xs: '12px', sm: '16px' },
                      fontWeight: '400',
                      color: 'text.secondary',
                    }}
                  >
                    <TableSortLabel
                      active={orderBy === column.id}
                      direction={orderBy === column.id ? order : 'asc'}
                      IconComponent={ArrowDropDownIcon}
                      hideSortIcon={true}
                      onClick={(event) => handleRequestSort(event, column.id)}
                    >
                      {column.label}
                      {orderBy === column.id ? (
                        <Box component="span" sx={visuallyHidden}>
                          {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                        </Box>
                      ) : (
                        <Box
                          component={'img'}
                          src={upDownSortIcon.src}
                          alt={'backward icon'}
                          sx={{
                            width: '9px',
                            height: '14px',
                            ml: '7px',
                            transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                          }}
                        />
                      )}
                    </TableSortLabel>
                  </TableCell>
                ) : (
                  <TableCell
                    key={column.id}
                    align={column.align as 'inherit'}
                    sx={{
                      display: {
                        xs:
                          (mode === 'open_account_rate' && column.id === 'currency_title') ||
                          (mode === 'fees_rate' && column.id === 'website_link')
                            ? 'none'
                            : 'table-cell',
                        sm: 'table-cell',
                      },
                      width: column.width,
                      maxWidth: column.maxWidth,
                      // borderBottom: mode.includes('ticket') ? 0 : '',
                      fontSize: { xs: '12px', sm: '16px' },
                      fontWeight: '400',
                      color: 'text.secondary',
                    }}
                  >
                    {column.label}
                  </TableCell>
                );
              })}
            </TableRow>
          </TableHead>
          <TableBody>
            {(mode.includes('ticket') || mode.includes('rate')
              ? (!!searchedText
                  ? mode.includes('rate')
                    ? AllFilteredResultsArray?.map((item) => createData(item))
                    : AllFilteredResultsArray
                  : properResultState
                )?.slice((currentPage - 1) * itemsPerPage, (currentPage - 1) * itemsPerPage + itemsPerPage)
              : properResultState
            )?.map((row: any, index: number) => (
              <TableRow
                key={index}
                sx={{
                  cursor: mode.includes('ticket') ? 'pointer' : 'default',
                  '& th, & td': {
                    borderColor: 'secondary.light',
                    px: { xs: mode === 'exchange' ? '0' : 'inherit' },
                    py: { xl: '20px' },
                  },
                  '&:last-child th, &:last-child td': {
                    borderBottom: mode !== 'exchange' ? 0 : 1,
                    borderColor: 'secondary.light',
                  },
                }}
                onClick={() => router.push(`/${PATHS.USER_TICKET}/${row.id}`)}
              >
                {desiredHeader.map((column: ColumnsSample) => {
                  const value = row[column.id];
                  return (
                    <TableCell
                      key={index}
                      component="th"
                      scope="row"
                      align={column.align as 'inherit'}
                      sx={{
                        display: {
                          xs:
                            (mode === 'open_account_rate' && column.id === 'currency_title') ||
                            (mode === 'fees_rate' && column.id === 'website_link')
                              ? 'none'
                              : 'table-cell',
                          sm: 'table-cell',
                        },
                        width: column.width,
                        maxWidth: column.maxWidth,
                        '&:last-child, &:last-child':
                          mode === 'dashboard'
                            ? {
                                borderLeft: '1px solid',
                                borderColor: 'secondary.light',
                              }
                            : {},
                      }}
                    >
                      {column.type.includes('icon') ? (
                        <Box
                          sx={{
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'flex-start',
                            flexDirection: 'row',
                          }}
                        >
                          <Box
                            component={'img'}
                            src={value?.image}
                            // alt={}
                            sx={{
                              display: {
                                xs: column.type.includes('name') ? 'none' : 'block',
                                sm: 'block',
                              },
                              width: '24px',
                              height: '24px',
                              mr: '8px',
                            }}
                          />
                          <Typography
                            // dir={'ltr'}
                            variant="inherit"
                            component="span"
                            color={'text.primary'}
                            // fontFamily={pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Demibold'}
                            sx={{
                              fontWeight: '400',
                              fontSize: {
                                xs: '10px',
                                sm: '14px',
                              },
                            }}
                          >
                            {value?.title}
                          </Typography>
                        </Box>
                      ) : column.type.includes('sign') ? (
                        <Box
                          sx={{
                            display: 'flex',
                            alignItems: 'center',
                          }}
                        >
                          {mode.includes('open') && +row?.unread_messages > 0 && (
                            <CounterBadge
                              value={row?.unread_messages}
                              width={'27px'}
                              height={'20px'}
                              bgColor={'error.light'}
                              textColor={'error.main'}
                            />
                          )}
                          <IconButton
                            disableTouchRipple
                            color="inherit"
                            aria-label="prev button"
                            edge="start"
                            // onClick={() => router.push(`/${PATHS.USER_DASHBOARD}`)}
                            sx={{
                              width: { xs: '20px', sm: '30px' },
                              height: { xs: '20px', sm: '30px' },
                              display: 'flex',
                              justifySelf: 'flex-end',
                              padding: '0',
                              borderRadius: '5px',
                              ml: '4px',
                              backgroundColor: 'transparent',
                              '&:hover': {
                                bgcolor: 'secondary.light',
                              },
                            }}
                            onClick={() => mode.includes('ticket') && router.push(`/${PATHS.USER_TICKET}/${row.id}`)}
                          >
                            <Box
                              component={'img'}
                              src={detailIcon.src}
                              alt={'backward icon'}
                              sx={{
                                width: '5px',
                                height: '16px',
                                transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                              }}
                            />
                          </IconButton>
                        </Box>
                      ) : column.type === 'status' ? (
                        <Chip
                          label={
                            value === 'customer_answered'
                              ? t('ticket:waiting_for_answer')
                              : value === 'admin_answered'
                              ? t('ticket:answered')
                              : t('ticket:closed')
                          }
                          sx={{
                            minWidth: { xs: '56px', sm: '112px' },
                            height: '24px',
                            borderRadius: { xs: '30px', sm: '11px' },
                            bgcolor: {
                              xs: 'transparent',
                              sm:
                                value === 'customer_answered'
                                  ? 'info.light'
                                  : value === 'admin_answered'
                                  ? 'warning.light'
                                  : 'success.light',
                            },
                            '& .MuiChip-label': {
                              padding: '0',
                              margin: '0',
                              display: 'flex',
                              alignItems: 'center',
                              justifyContent: 'center',
                              color:
                                value === 'customer_answered'
                                  ? 'info.main'
                                  : value === 'admin_answered'
                                  ? 'warning.main'
                                  : 'success.dark',
                              fontSize: { xs: '10px', sm: '12px' },
                              fontWeight: '600',
                            },
                          }}
                        />
                      ) : column.type === 'link' ? (
                        <NextLink href={value} passHref>
                          <Link href="#" underline="none">
                            <Typography
                              dir={'ltr'}
                              variant="inherit"
                              component="span"
                              color={'primary.main'}
                              // fontFamily={pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Demibold'}
                              sx={{
                                textDecoration: 'underline',
                                fontWeight: '400',
                                fontSize: {
                                  xs: '10px',
                                  sm: '14px',
                                },
                              }}
                            >
                              {value}
                            </Typography>
                          </Link>
                        </NextLink>
                      ) : column.type === 'wage' ? (
                        <Typography
                          dir={'ltr'}
                          variant="inherit"
                          component="span"
                          color={'text.primary'}
                          fontFamily={pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Regular'}
                          sx={{
                            fontWeight: '400',
                            fontSize: {
                              xs: '10px',
                              sm: '14px',
                            },
                          }}
                        >
                          {+value >= 0 ? (Number.isInteger(+value) ? `${Math.trunc(+value)}%` : `${value}%`) : value}
                        </Typography>
                      ) : column.type === 'button' ? (
                        <>
                          <Box
                            sx={{
                              display: {
                                xs: 'none',
                                sm: 'block',
                              },
                            }}
                          >
                            <NextLink href={'https://my.zimapay.com/orders'} passHref>
                              <Link href="#" underline="none">
                                <Typography
                                  dir={'ltr'}
                                  variant="inherit"
                                  component="span"
                                  color={'primary.main'}
                                  // fontFamily={pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Demibold'}
                                  sx={{
                                    fontWeight: '600',
                                    fontSize: {
                                      xs: '10px',
                                      sm: '14px',
                                    },
                                  }}
                                >
                                  {t('my_wallet:set_order')}
                                </Typography>
                              </Link>
                            </NextLink>
                          </Box>
                          <IconButton
                            color="inherit"
                            aria-label="prev button"
                            edge="start"
                            // onClick={() => router.push(`/${PATHS.USER_DASHBOARD}`)}
                            sx={{
                              width: {
                                xs: '20px',
                                sm: '30px',
                              },
                              height: {
                                xs: '20px',
                                sm: '30px',
                              },
                              display: {
                                xs: 'flex',
                                sm: 'none',
                              },
                              // justifySelf: 'center',
                              padding: '0',
                              borderRadius: '5px',
                              ml: '4px',
                              backgroundColor: 'transparent',
                              '&:hover': {
                                bgcolor: 'secondary.light',
                              },
                            }}
                          >
                            <Box
                              component={'img'}
                              src={detailIcon.src}
                              alt={'backward icon'}
                              sx={{
                                width: '5px',
                                height: '16px',
                                transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                              }}
                            />
                          </IconButton>
                        </>
                      ) : (
                        <Typography
                          dir={'ltr'}
                          variant="inherit"
                          component="span"
                          color={
                            column.id === 'priority'
                              ? value === 'urgent'
                                ? 'error.main'
                                : 'text.primary'
                              : 'text.primary'
                          }
                          fontFamily={pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Bold'}
                          sx={{
                            fontWeight: '400',
                            fontSize: {
                              xs: '10px',
                              sm: '14px',
                            },
                          }}
                        >
                          {column.id === 'priority'
                            ? value === 'urgent'
                              ? t('ticket:urgent')
                              : t('ticket:normal')
                            : column.id === 'created_at'
                            ? `${value?.split(' ')[1]} | ${value?.split(' ')[0]}`
                            : mode.includes('ticket') && column.id === 'title' && value?.length > 20
                            ? `...${value?.substring(0, 20)}`
                            : value}
                        </Typography>
                      )}
                    </TableCell>
                  );
                })}
              </TableRow>
            ))}
          </TableBody>
        </Table>
        {totalPages > 1 && (
          <Box
            sx={{
              width: 'auto',
              height: 'auto',
              borderRadius: '14px',
              backgroundColor: 'white',
              padding: '0 24px',
              mt: '24px',
            }}
          >
            <Pagination
              count={
                !!searchedText
                  ? !!AllFilteredResultsArray?.length
                    ? Math.ceil(+AllFilteredResultsArray?.length / itemsPerPage)
                    : 0
                  : totalPages
              }
              page={currentPage}
              // defaultPage={1}
              shape="rounded"
              dir={'ltr'}
              onChange={handleChangePage}
            />
          </Box>
        )}
      </TableContainer>
    </Box>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export { TextInfoTable };
