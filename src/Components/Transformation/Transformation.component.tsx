import React, { ReactNode, useCallback, useEffect, useState } from 'react';
import {
  Box,
  InputAdornment,
  Link,
  MenuItem,
  Select,
  SelectChangeEvent,
  TextField,
  Divider,
  Chip,
  Avatar,
  Button,
} from '@mui/material';
import Typography from '@mui/material/Typography';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import bulletPoint from '/public/assets/images/BulletPoint.svg';
import IconButton from '@mui/material/IconButton';
import backwardArrowDeposit from '/public/assets/images/backwardArrowDeposit.svg';
import dollarWalletIcon from '/public/assets/images/dollar-wallet-icon.svg';
import selectDropdownIcon from '/public/assets/images/select-dropdown-icon.svg';
import { Visibility, VisibilityOff } from '@mui/icons-material';
import RemoveIcon from '@mui/icons-material/Remove';
import ClearIcon from '@mui/icons-material/Clear';
import DragHandleIcon from '@mui/icons-material/DragHandle';
import { useRouter } from 'next/router';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { fetchWalletDepositRules } from '../../store/slices/wallet_deposit_rules.slice';
import { fetchWalletDepositCash } from '../../store/slices/wallet_deposit_cash_rules.slice';
import dangerCircleIcon from '/public/assets/images/Danger-Circle-icon.svg';
import dangerCircleGreenIcon from '/public/assets/images/Danger-Circle-Green-icon.svg';
import rectRialIcon from '/public/assets/images/Rect-rial-icon.svg';
import melliBankLogo from '/public/assets/images/Melli_Bank_Logo.svg';
import { calcCurrencyConversionCosts, calcZimaPayWage } from 'utils/functions/calc_currency_conversion';
import { fetchUserActiveWallets } from '../../store/slices/user_active_wallets.slice';
import { PATHS } from '../../configs/routes.config';
import Tooltip from '@mui/material/Tooltip';
import { fetchWalletWithdrawRules } from '../../store/slices/wallet_withdraw_rules.slice';
import { fetchWalletWithdrawCash } from '../../store/slices/wallet_withdraw_cash_rules.slice';
import { fetchUserBankAccounts } from '../../store/slices/user_bank_accounts.slice';
import { useI18nTrDir } from '../../utils/CustomHooks/i18nTrDir';
import { fetchWalletDepositAmount } from '../../store/slices/wallet_deposit_amount.slice';
import { fetchWalletDepositCashAmount } from '../../store/slices/wallet_deposit_cash_amount.slice';
import { fetchWalletWithdrawCashAmount } from '../../store/slices/wallet_withdraw_cash_amount.slice';
import { fetchWalletWithdrawAmount } from '../../store/slices/wallet_withdraw_amount.slice';
import AlertDialogBox from '../AlertDialogBox/AlertDialogBox.component';
import { localesNS } from '../../utils/Data/locales_NameSpace.utils';
import { fetchWalletTransferRules } from '../../store/slices/wallet_transfer_rules.slice';
import { mockSession } from 'next-auth/client/__tests__/helpers/mocks';
import user = mockSession.user;
import { fetchCheckWalletTransfer } from '../../store/slices/wallet_transfer_info_check.slice';

interface Props {
  destination_uid?: string;
  destinationWalletType?: string;
}

interface RulesSample {
  action_rules: {
    min_amount: string;
    max_amount: string;
    tax: number;
    zimapay_fee: number;
    exchange_rate: number;
  };
  wallet_information: {
    title: string;
    currency: string;
    iso_code: string;
    balance: number;
    logo: string;
    color: {
      one: string;
      two: string;
      five: string;
      four: string;
      three: string;
    };
  };
}

interface WalletSample {
  balance: number;
  benefit: number;
  code: string;
  colors: any;
  currency: string;
  iso_code: string;
  rial_balance: number;
  title: string;
  symbol: string;
  uid: string;
  currency_logo: string;
  currency_iso_code?: string;
  currency_image?: string;
  currency_name?: string;
}

const Transformation = ({ destination_uid, destinationWalletType }: Props) => {
  const { t, pageDirection } = useI18nTrDir();

  const router = useRouter();
  const UrlPath = router.pathname;

  const [openResultDialogBox, setOpenResultDialogBox] = React.useState(false);

  const handleOpenResultDB = () => {
    setOpenResultDialogBox(true);
  };

  const handleCloseResultDB = () => {
    setOpenResultDialogBox(false);
  };

  const [rulesState, setRulesState] = useState<RulesSample>({
    action_rules: {
      min_amount: '',
      max_amount: '',
      tax: 0,
      zimapay_fee: 0,
      exchange_rate: 0,
    },
    wallet_information: {
      title: '',
      currency: '',
      iso_code: '',
      balance: 0,
      logo: '',
      color: {
        one: '',
        two: '',
        five: '',
        four: '',
        three: '',
      },
    },
  });

  const [currency, setCurrency] = useState({
    uid: '',
    title: '',
    code: '',
    currency: '',
    currency_logo: '',
    iso_code: '',
    symbol: '',
    colors: {
      one: '',
      two: '',
      five: '',
      four: '',
      three: '',
    },
    balance: 0,
    rial_balance: 0,
    benefit: 0,
  });

  const [finalCurrency, setFinalCurrency] = useState({
    uid: '',
    title: '',
    code: '',
    currency: '',
    currency_logo: '',
    iso_code: '',
    symbol: '',
    colors: {
      one: '',
      two: '',
      five: '',
      four: '',
      three: '',
    },
    balance: 0,
    rial_balance: 0,
    benefit: 0,
    currency_iso_code: '',
    currency_image: '',
    currency_name: '',
  });

  const [userActiveWalletsState, setUserActiveWalletsState] = useState<WalletSample[]>([]);
  const [allowedReceiverWalletsState, setAllowedReceiverWalletsState] = useState<any>([]);
  const [amountWarning, setAmountWarning] = useState(false);
  const [invalidAmount, setInvalidAmount] = useState(false);
  const [isNotFiftyMultiple, setIsNotFiftyMultiple] = useState(false);
  const [operationResultState, setOperationResultState] = useState<object>({
    status: '',
    messages: {},
  });

  const [allowedDestinationWallets, setAllowedDestinationWallets] = useState([]);

  const dispatch = useAppDispatch();

  const findTransferDestWalletResult = useAppSelector((state) => state.FindTransferDestWallet);
  const findTransferDestUserResult = useAppSelector((state) => state.FindTransferDestUser);

  const userActiveWalletResult = useAppSelector((state) => state.UserActiveWallets);
  const walletDepositAmountResult = useAppSelector((state) => state.WalletDepositAmount);
  const walletDepositCashAmountResult = useAppSelector((state) => state.WalletDepositCashAmount);
  const walletWithdrawAmountResult = useAppSelector((state) => state.WalletWithdrawAmount);
  const walletWithdrawCashAmountResult = useAppSelector((state) => state.WalletWithdrawCashAmount);

  const walletTransferRulesResult = useAppSelector((state) => state.WalletTransferRules);

  const wallet_uid: any = router.query.wallet_uid;
  const isDeposit = router.pathname.includes('deposit');
  const isRial = router.pathname.includes('rial');

  const isTransfer = router.pathname.includes('transfer');
  const isInternal = router.pathname.includes('internal');
  const isOthers = router.pathname.includes('others');

  useEffect(() => {
    dispatch(fetchUserActiveWallets());
  }, []);

  useEffect(() => {
    const totalWalletInfo = userActiveWalletsState?.find((item) => item.uid === wallet_uid);
    totalWalletInfo && setCurrency(totalWalletInfo);
  }, [wallet_uid]);

  useEffect(() => {
    if (userActiveWalletResult?.userActWalletsResp) {
      setUserActiveWalletsState(userActiveWalletResult?.userActWalletsResp);
    }
  }, [userActiveWalletResult]);

  useEffect(() => {
    if (destinationWalletType === 'wallet') {
      setAllowedDestinationWallets(findTransferDestWalletResult?.transferDestWalletResp?.destination_wallets_list);
    } else {
      setAllowedDestinationWallets(findTransferDestUserResult?.transferDestUserResp?.destination_wallets_list);
    }
  }, [findTransferDestUserResult, findTransferDestWalletResult]);

  const handleSetProperInitialWallets = () => {
    const initialSenderWallet = userActiveWalletsState?.find((item) => item.uid === wallet_uid);
    let initialReceiverWallet = isInternal ? userActiveWalletsState : allowedDestinationWallets;
    if (isTransfer && isInternal) {
      // @ts-ignore
      initialReceiverWallet = userActiveWalletsState.filter((item) => item.iso_code !== initialSenderWallet.iso_code);
      setAllowedReceiverWalletsState(initialReceiverWallet);
    }
    setAllowedReceiverWalletsState(initialReceiverWallet);
  };

  useEffect(() => {
    handleSetProperInitialWallets();
    const totalWalletInfo = userActiveWalletsState?.find((item) => item.uid === wallet_uid);
    totalWalletInfo && setCurrency(totalWalletInfo);
  }, [userActiveWalletsState]);

  useEffect(() => {
    if (isInternal) {
      !!allowedReceiverWalletsState && setFinalCurrency(allowedReceiverWalletsState[0]);
    } else {
      const selectedFinalWallet = allowedReceiverWalletsState.find((wallet: any) => wallet.code === destination_uid);
      setFinalCurrency(selectedFinalWallet);
    }
  }, [allowedReceiverWalletsState]);

  useEffect(() => {
    if (isTransfer) {
      if (currency?.uid && finalCurrency?.uid) {
        dispatch(fetchWalletTransferRules(currency?.uid, finalCurrency?.uid));
      }
    }
  }, [currency, finalCurrency]);

  useEffect(() => {
    let rulesObj = { ...rulesState };
    if (walletTransferRulesResult?.walletTransferRulesResp?.transfer_rules) {
      rulesObj = {
        action_rules: {
          ...walletTransferRulesResult?.walletTransferRulesResp?.transfer_rules,
        },
        wallet_information: {
          ...walletTransferRulesResult?.walletTransferRulesResp?.wallet_information,
        },
      };
    }
    setRulesState(rulesObj);
  }, [walletTransferRulesResult]);

  useEffect(() => {
    handleSetProperInitialWallets();
    if (userActiveWalletsState.length) {
      typeof window !== 'undefined' &&
        router.push(
          `/${PATHS.USER_WALLET}/${currency.uid}/${
            isTransfer
              ? isInternal
                ? PATHS.TRANSFER_INTERNAL
                : isOthers
                ? PATHS.TRANSFER_OTHERS
                : PATHS.TRANSFER_ZIMAPAY
              : 'not-developed-yet'
          }`,
        );
    }
  }, [currency]);

  // const [initialValue, setInitialValue] = useState('')
  // const [finalValue, setFinalValue] = useState('')
  // const [descriptionValue, setDescriptionValue] = useState('')

  useEffect(() => {
    if (UrlPath.includes(PATHS.CASH_DEPOSIT)) {
      !!walletDepositCashAmountResult.walletDepositCashAmountResp?.message && setOpenResultDialogBox(true);
      walletDepositCashAmountResult.error
        ? setOperationResultState({
            status: 'error',
            messages: {},
          })
        : setOperationResultState({
            status: 'success',
            messages: walletDepositCashAmountResult.walletDepositCashAmountResp?.message,
          });
      console.log('deposit form data1', walletDepositCashAmountResult);
    }
    if (UrlPath.includes(PATHS.RIAL_DEPOSIT)) {
      // setOpenResultDialogBox(true)
      // walletDepositAmountResult.error ? setOperationResultState({...operationResultState, status: 'error'}) : setOperationResultState({...operationResultState, status: 'success'})
      console.log('deposit form data2', walletDepositAmountResult);
    }
    if (UrlPath.includes(PATHS.CASH_WITHDRAW)) {
      !!walletWithdrawCashAmountResult.walletWithdrawCashAmountResp?.message && setOpenResultDialogBox(true);
      walletWithdrawCashAmountResult.error
        ? setOperationResultState({
            status: 'error',
            messages: {},
          })
        : setOperationResultState({
            status: 'success',
            messages: walletWithdrawCashAmountResult.walletWithdrawCashAmountResp?.message,
          });
      console.log('deposit form data3', walletWithdrawCashAmountResult);
    }
    if (UrlPath.includes(PATHS.RIAL_WITHDRAW)) {
      !!walletWithdrawAmountResult.walletWithdrawAmountResp?.message && setOpenResultDialogBox(true);
      walletWithdrawAmountResult.error
        ? setOperationResultState({
            status: 'error',
            messages: {},
          })
        : setOperationResultState({
            status: 'success',
            messages: walletWithdrawAmountResult.walletWithdrawAmountResp?.message,
          });
      console.log('deposit form data4', walletWithdrawAmountResult);
    }
  }, [
    walletDepositAmountResult,
    walletDepositCashAmountResult,
    walletWithdrawAmountResult,
    walletWithdrawCashAmountResult,
  ]);

  const [inputsValueState, setInputsValueState] = useState({
    initialValue: '',
    finalValue: '',
    descriptionValue: '',
  });

  // useEffect(() => {
  //     if (userActiveWalletsState && rulesState?.wallet_information?.iso_code) {
  //         const selectedCurrency = userActiveWalletsState.find(wallet => wallet.iso_code === rulesState?.wallet_information?.iso_code)
  //         selectedCurrency && setCurrency(selectedCurrency)
  //         selectedCurrency && setFinalCurrency(selectedCurrency)
  //     }
  // }, [userActiveWalletsState, rulesState])

  const calcFinalAmountProcedure = isTransfer
    ? [
        {
          icon: <RemoveIcon fontSize={'small'} />,
          title: t('deposit:tax'),
          amount:
            inputsValueState.initialValue || inputsValueState.finalValue
              ? `%${rulesState?.action_rules?.tax}`
              : '...................',
          margin_top: '38px',
        },
        {
          icon: <ClearIcon fontSize={'small'} />,
          title: t('deposit:currency_exchange_rate'),
          amount: Number(rulesState?.action_rules?.exchange_rate).toLocaleString('en-US'),
          margin_top: '32px',
        },
        {
          icon: <RemoveIcon fontSize={'small'} />,
          title: t('deposit:transfer_wage', { wage: rulesState?.action_rules?.zimapay_fee }),
          amount:
            inputsValueState.initialValue || inputsValueState.finalValue
              ? calcZimaPayWage(
                  +inputsValueState.initialValue.replace(/,(?=\d{3})/g, ''),
                  rulesState?.action_rules?.zimapay_fee,
                )
              : '...................',
          margin_top: '32px',
        },
        {
          icon: <DragHandleIcon fontSize={'small'} />,
          title: '',
          amount: '',
          margin_top: '65px',
        },
      ]
    : [
        // {
        //     icon: <RemoveIcon fontSize={'small'}/>,
        //     title: t('deposit:tax'),
        //     amount: (inputsValueState.initialValue || inputsValueState.finalValue) ? `%${rulesState?.action_rules?.tax}` : '...................',
        //     margin_top: '57px'
        // }, {
        //     icon: <RemoveIcon fontSize={'small'}/>,
        //     title: t('deposit:zimapay_wage', {wage: rulesState?.action_rules?.zimapay_fee}),
        //     amount: (inputsValueState.initialValue || inputsValueState.finalValue) ? calcZimaPayWage(+(inputsValueState.initialValue.replace(/,(?=\d{3})/g, '')), rulesState?.action_rules?.zimapay_fee) : '...................',
        //     margin_top: '57px'
        // }, {
        //     icon: <DragHandleIcon fontSize={'small'}/>,
        //     title: t('deposit:final_amount'),
        //     amount: '',
        //     margin_top: '75px'
        // },
      ];

  const NewIcon = (props: any) => (
    <svg {...props} width="9" height="9" viewBox="0 0 9 9" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M3.65175 7.85617C3.60825 7.81177 3.42225 7.64414 3.26925 7.488C2.307 6.57254 0.732 4.1844 0.25125 2.93445C0.174 2.74462 0.0105 2.2647 0 2.00828C0 1.76258 0.054 1.52836 0.1635 1.30485C0.3165 1.02623 0.55725 0.802727 0.8415 0.680258C1.03875 0.601419 1.629 0.47895 1.6395 0.47895C2.28525 0.356481 3.3345 0.289124 4.494 0.289124C5.59875 0.289124 6.60525 0.356481 7.26075 0.456753C7.27125 0.468234 8.00475 0.590703 8.256 0.724653C8.715 0.970357 9 1.45028 9 1.96389V2.00828C8.98875 2.34277 8.70375 3.0462 8.69325 3.0462C8.21175 4.22879 6.714 6.56183 5.71875 7.49948C5.71875 7.49948 5.463 7.76355 5.30325 7.87837C5.07375 8.05748 4.7895 8.14627 4.50525 8.14627C4.188 8.14627 3.8925 8.04599 3.65175 7.85617Z"
        fill="#298FFE"
      />
    </svg>
  );

  const CreditNewIcon = (props: any) => (
    <svg {...props} width="16" height="9" viewBox="0 0 16 9" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M15 1L8 8L1 1" stroke="#298FFE" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
    </svg>
  );

  const handleChangeCurrency = (event: SelectChangeEvent<any>, child: ReactNode): void => {
    setCurrency(JSON.parse(event.target.value));
  };

  const handleChangeFinalCurrency = (event: SelectChangeEvent<any>, child: ReactNode) => {
    setFinalCurrency(JSON.parse(event.target.value));
  };

  const handleCheckInputValue = (event: any, inputValue: string) => {
    if (inputValue === 'initialValue') {
      const number_reg = /^[0-9]*\.?[0-9]*$/;
      if (event.target.value === '' || number_reg.test(event.target.value)) {
        setInputsValueState((prevState) => ({
          ...prevState,
          [inputValue]: event.target.value,
          finalValue: calcCurrencyConversionCosts(
            isTransfer ? 'transfer' : '',
            isInternal ? 'internal' : 'others',
            +rulesState?.action_rules?.zimapay_fee,
            +rulesState?.action_rules?.tax,
            +rulesState?.action_rules?.exchange_rate,
            +event.target.value > 0 ? +event.target.value : 0,
            '',
            currency.iso_code,
          ),
        }));
      }
    } else if (inputValue === 'finalValue') {
      const number_reg = /^[0-9\b]+$/;
      if (event.target.value === '' || number_reg.test(event.target.value)) {
        setInputsValueState((prevState) => ({
          ...prevState,
          [inputValue]: event.target.value,
          initialValue: calcCurrencyConversionCosts(
            isTransfer ? 'transfer' : '',
            isInternal ? 'internal' : 'others',
            +rulesState?.action_rules?.zimapay_fee,
            +rulesState?.action_rules?.tax,
            +rulesState?.action_rules?.exchange_rate,
            '',
            +event.target.value > 0 ? +event.target.value : 0,
            currency.iso_code,
          ),
        }));
      }
    } else {
      setInputsValueState((prevState) => ({ ...prevState, [inputValue]: event.target.value }));
    }
  };

  const handleCheckAmount = () => {
    if (
      +inputsValueState.initialValue.replace(/,(?=\d{3})/g, '') < +rulesState?.action_rules?.min_amount ||
      +inputsValueState.initialValue.replace(/,(?=\d{3})/g, '') > +rulesState?.action_rules?.max_amount
    ) {
      setInvalidAmount(true);
      setAmountWarning(true);
    } else {
      setInvalidAmount(false);
      setAmountWarning(false);
    }
  };

  useEffect(() => {
    handleCheckAmount();
  }, [inputsValueState.initialValue]);

  const handleSubmitOperationType = (event: any) => {
    event.preventDefault();
    const form = new FormData(event.currentTarget);
    let data = Object.fromEntries(form);
    if (inputsValueState.initialValue && !invalidAmount) {
      data['amount'] = inputsValueState.initialValue.replace(/,(?=\d{3})/g, '');
      data['destination_uid'] = finalCurrency?.uid.toString();
      console.log('what is form data', data);
      dispatch(fetchCheckWalletTransfer(wallet_uid, data));
    }
  };

  return (
    <>
      <Box
        sx={{
          width: '100%',
          display: 'flex',
          alignItems: 'flex-start',
          justifyContent: 'center',
        }}
      >
        <Box
          sx={{
            width: { xs: '327px', sm: '475px' },
            height: 'auto',
            backgroundColor: 'white',
            borderRadius: '22px',
            padding: '24px',
          }}
        >
          <Box
            sx={{
              width: '100%',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'space-between',
              mb: '40px',
            }}
          >
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <Box
                component={'img'}
                src={bulletPoint.src}
                alt={'bullet point'}
                sx={{
                  width: '22px',
                  height: '18px',
                  mr: '9px',
                  transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                }}
              />
              <Typography
                variant="inherit"
                component="span"
                color={'text.primary'}
                sx={{
                  fontWeight: { xs: '600' },
                  fontSize: { xs: '16px', xl: '16px' },
                }}
              >
                {isInternal ? t('deposit:conversion') : t('my_wallet:transfer')}
              </Typography>
            </Box>
            <IconButton
              color="inherit"
              aria-label="prev button"
              edge="start"
              onClick={() => router.push(`/${PATHS.USER_DASHBOARD}`)}
              sx={{
                width: '30px',
                height: '30px',
                display: 'flex',
                justifySelf: 'flex-end',
                padding: '0',
                backgroundColor: '#D9D9D9',
              }}
            >
              <Box
                component={'img'}
                src={backwardArrowDeposit.src}
                alt={'backward icon'}
                sx={{
                  width: '7px',
                  height: '14px',
                  transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                }}
              />
            </IconButton>
          </Box>
          <Box
            component={'form'}
            sx={{
              width: '100%',
              height: 'auto',
              textAlign: 'center',
            }}
            onSubmit={handleSubmitOperationType}
          >
            <Box
              sx={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}
            >
              <Box
                sx={{
                  display: 'flex',
                  alignItems: 'center',
                }}
              >
                <Box
                  component={'img'}
                  src={rulesState?.wallet_information?.logo}
                  // alt={'backward icon'}
                  sx={{
                    width: '50px',
                    height: '30px',
                    mr: '12px',
                  }}
                />
                <Typography
                  variant="inherit"
                  component="span"
                  color={rulesState?.wallet_information?.color?.two}
                  sx={{
                    fontWeight: '600',
                    fontSize: { xs: '14px', sm: '16px' },
                  }}
                >
                  {t('deposit:wallet_balance', { wallet: rulesState?.wallet_information?.currency })}
                </Typography>
              </Box>
              <Typography
                variant="inherit"
                component="span"
                color={'text.primary'}
                fontFamily={pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Bold'}
                sx={{
                  fontWeight: '600',
                  fontSize: { xs: '16px', sm: '24px' },
                }}
              >
                {Number(Number(rulesState?.wallet_information?.balance).toFixed(2)).toLocaleString('en-US')}
              </Typography>
            </Box>

            <Box
              sx={{
                width: '100%',
                height: 'auto',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'flex-start',
                mt: '40px',
              }}
            >
              <Box
                sx={{
                  width: 'fit-content',
                  height: 'auto',
                  display: 'flex',
                  alignItems: 'center',
                }}
              >
                <Typography
                  variant="inherit"
                  component="span"
                  color={'text.primary'}
                  sx={{
                    fontSize: '17px',
                    fontWeight: '400',
                    lineHeight: '24px',
                  }}
                  // onClick={() => focusInput.current?.focus()}
                >
                  {t('deposit:amount')} {t('deposit:transmission')}
                </Typography>

                <Tooltip
                  // title={`حداقل واریز ${Math.trunc(+rulesState?.action_rules?.min_amount).toLocaleString('en-US')} ${rulesState?.wallet_information?.currency} و حداکثر ${Math.trunc(+rulesState?.action_rules?.max_amount).toLocaleString('en-US')} میباشد.`}
                  title={t('deposit:min_max_warning', {
                    min: Math.trunc(+rulesState?.action_rules?.min_amount).toLocaleString('en-US'),
                    max: Math.trunc(+rulesState?.action_rules?.max_amount).toLocaleString('en-US'),
                    currency: rulesState?.wallet_information?.currency,
                  })}
                  placement={pageDirection === 'rtl' ? 'left' : 'right'}
                  arrow
                  // disableInteractive={true}
                  open={amountWarning}
                  PopperProps={{
                    sx: {
                      width: { xs: '200px', sm: 'auto' },
                      '& .MuiTooltip-tooltip': {
                        padding: '8px 12px',
                        borderRadius: '16px',
                        border: '1px solid',
                        borderColor: 'error.main',
                        bgcolor: 'error.light',
                        marginLeft: '12px',
                        color: 'text.primary',
                        fontSize: { xs: '10px', sm: '14px' },
                        fontWeight: '400',
                      },
                      '& .MuiTooltip-arrow': {
                        color: 'error.light',
                        // bgcolor: "blue",
                        right: 'auto',
                        left: { xs: '-7px', sm: '-10px' },
                        '&::before': {
                          // borderLeft: '20px solid transparent',
                          // borderRight: '20px solid transparent',
                          // borderBottom: 'calc(2 * 20px * 0.866) solid green',
                          // borderTop: '20px solid transparent',
                          // display: 'inline-block',

                          width: { xs: '7px', sm: '11px' },
                          height: { xs: '7px', sm: '11px' },

                          // backgroundColor: "blue",
                          border: '1px solid',
                          borderColor: 'error.main',
                          overflow: 'hidden',
                        },
                      },
                    },
                  }}
                >
                  <Box
                    component={'img'}
                    src={dangerCircleIcon.src}
                    // alt={'backward icon'}
                    sx={{
                      width: '18.5px',
                      height: '18.5px',
                      ml: '11px',
                    }}
                    onClick={() => setAmountWarning((prevState) => !prevState)}
                  />
                </Tooltip>
              </Box>
              <TextField
                // inputRef={focusInput-index}
                value={inputsValueState.initialValue}
                // onChange={}
                // error={}
                // helperText={}

                // color={'primary.main'}
                fullWidth
                name={`amount`}
                placeholder={t('deposit:enter_amount')}
                type={'tel'}
                autoComplete="off"
                onChange={(event) => {
                  handleCheckInputValue(event, 'initialValue');
                  // handleCheckAmount(event)
                }}
                // onBlur={(event) => handleCheckAmount(event)}
                sx={{
                  '& input::placeholder': {
                    fontSize: '14px',
                    color: 'text.secondary',
                  },
                  width: '100%',
                  height: '100%',
                  bgcolor: 'secondary.main',
                  borderRadius: '12px',
                  mt: '8px',
                  '& .MuiInputAdornment-root': {
                    ml: '8px',
                    height: '36px',
                    maxHeight: '40px',
                    backgroundColor: 'white',
                    borderRadius: '12px',
                    border: 'none',
                  },
                  '& legend': { display: 'none' },
                  '& fieldset': {
                    top: 0,
                    borderRadius: '12px',
                    borderColor: inputsValueState.initialValue ? 'primary.main' : 'secondary.main',
                  },
                  '& .MuiOutlinedInput-root:hover': {
                    '& > fieldset': {
                      borderColor: inputsValueState.initialValue ? 'primary.main' : 'secondary.main',
                    },
                  },
                  // '& fieldset:hover': {top: 0, borderRadius: '12px', border: '1px solid red'},
                  '& .MuiOutlinedInput-root': {
                    padding: 0,
                    borderRadius: '12px',
                    overflow: 'hidden',
                    height: '100%',
                    // "& > fieldset": {
                    //     border: '1.5px solid green',
                    // },
                    // bgcolor: 'secondary.main',
                    // backgroundColor: '#ffffff',
                    '&.Mui-focused > fieldset': {
                      // border: '1px solid',
                      borderColor: 'primary.main',
                    },
                    '&:hover:before fieldset': {
                      borderColor: 'primary.main',
                    },
                    color: 'primary.main',
                  },
                  '.MuiOutlinedInput-root :focus': {
                    backgroundColor: 'white',
                  },
                  '.MuiInputBase-input': {
                    fontFamily: pageDirection === 'ltr' ? 'IRANSansX-Regular' : 'IRANSansXFaNum-Regular',
                    padding: '14px',
                  },
                  '.MuiOutlinedInput-input': {
                    bgcolor: inputsValueState.initialValue ? 'white' : 'secondary.main',
                  },
                  zIndex: 1,
                }}
                InputProps={{
                  autoComplete: 'off',
                  endAdornment: (
                    <InputAdornment position="start">
                      <Select
                        // dir={'rtl'}
                        value={currency}
                        onChange={handleChangeCurrency}
                        displayEmpty
                        IconComponent={NewIcon}
                        renderValue={(value) => {
                          return (
                            value.uid && (
                              <Box
                                // dir={'ltr'}
                                sx={{
                                  width: '100%',
                                  display: 'flex',
                                  alignItems: 'center',
                                  justifyContent: 'space-between',
                                }}
                              >
                                <Typography
                                  variant="inherit"
                                  component="span"
                                  color={'text.secondary'}
                                  sx={{
                                    mt: '4px',
                                    fontSize: '12px',
                                    fontWeight: '600',
                                    mr: '8px',
                                  }}
                                >
                                  {value.iso_code}
                                </Typography>
                                <Box
                                  component={'img'}
                                  src={value.currency_logo}
                                  // alt={`${value.title} icon`}
                                  sx={{
                                    width: '24px',
                                    height: '24px',
                                  }}
                                />
                              </Box>
                            )
                          );
                        }}
                        MenuProps={{
                          anchorOrigin: {
                            vertical: 'bottom',
                            horizontal: 'left',
                          },
                          transformOrigin: {
                            vertical: 'top',
                            horizontal: 'left',
                          },
                          PaperProps: {
                            sx: {
                              mt: '8px',
                              boxShadow: '0px 4px 38px rgba(68, 68, 68, 0.1)',
                              width: '120px',
                              height: 'auto',
                              // maxHeight: '176px',
                              borderRadius: '10px',
                              overflowY: 'auto',
                            },
                          },
                        }}
                        sx={{
                          // minWidth: '110px',
                          '& .MuiSelect-icon': { top: '40%', mx: '5px' },
                          height: '100%',
                          // ml: '8px',
                          '& .muirtl-bpeome-MuiSvgIcon-root-MuiSelect-icon': { left: 'auto' },
                          '& .muirtl-hfutr2-MuiSvgIcon-root-MuiSelect-icon': { left: 'auto' },
                          '& .muirtl-ittuaa-MuiInputAdornment-root': { margin: '0' },
                          '& .muirtl-keuqm-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input.muirtl-keuqm-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input.muirtl-keuqm-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input':
                            {
                              // px: '12px',
                              pl: '12px',
                              pr: '35px',
                              width: 'fit-content',
                            },
                          '& fieldset': { border: 'none' },
                        }}
                      >
                        {userActiveWalletsState?.map((item: WalletSample, index: number) => {
                          return (
                            <MenuItem
                              value={JSON.stringify(item)}
                              key={index}
                              sx={{
                                width: '100%',
                                px: '8px',
                                my: '5px',
                              }}
                            >
                              <Box
                                // dir={'ltr'}
                                sx={{
                                  width: '100%',
                                  display: 'flex',
                                  alignItems: 'center',
                                  justifyContent: 'space-between',
                                }}
                              >
                                <Box
                                  component={'img'}
                                  src={item.currency_logo}
                                  // alt={`${item.currency} icon`}
                                  sx={{
                                    width: '24px',
                                    height: '24px',
                                  }}
                                />
                                <Box
                                  sx={{
                                    width: 'auto',
                                    height: 'auto',
                                    flexGrow: 1,
                                    display: 'flex',
                                    alignItems: 'center',
                                    justifyContent: 'space-between',
                                    mx: '8px',
                                  }}
                                >
                                  <Typography
                                    variant="inherit"
                                    component="span"
                                    color={'text.secondary'}
                                    sx={{
                                      fontSize: '12px',
                                      fontWeight: '600',
                                    }}
                                  >
                                    {item.iso_code}
                                  </Typography>
                                  <Typography
                                    variant="inherit"
                                    component="span"
                                    color={'text.secondary'}
                                    sx={{
                                      fontSize: '14px',
                                      fontWeight: '400',
                                    }}
                                  >
                                    {item.currency}
                                  </Typography>
                                </Box>
                              </Box>
                            </MenuItem>
                          );
                        })}
                      </Select>
                    </InputAdornment>
                  ),
                }}
              />
            </Box>

            <Box
              sx={{
                width: '100%',
                height: 'auto',
                // bgcolor: 'yellow',
                position: 'relative',
              }}
            >
              <Box
                sx={{
                  // bgcolor: 'blue',
                  width: '100%',
                  height: 'auto',
                  display: 'flex',
                  justifyContent: 'flex-end',
                  position: 'absolute',
                  top: '215px',
                }}
              >
                <Box
                  sx={{
                    width: '94%',
                    height: '2px',
                    // position: 'absolute',
                    // top: '205px',
                    backgroundImage: 'linear-gradient(to right, #C3C3C3 45%, rgba(255,255,255,0) 0%)',
                    backgroundPosition: 'bottom',
                    backgroundSize: '20px 1px',
                    backgroundRepeat: 'repeat-x',
                  }}
                >
                  {/*<Divider*/}
                  {/*    variant={'middle'}*/}
                  {/*    orientation={'horizontal'}*/}
                  {/*    sx={{*/}
                  {/*        borderBottomWidth: '1px',*/}
                  {/*        borderColor: 'text.disabled',*/}
                  {/*        borderStyle: 'dashed'*/}
                  {/*    }}*/}
                  {/*/>*/}
                </Box>
              </Box>
              <Box
                sx={{
                  width: '100%',
                  height: 'auto',
                  position: 'absolute',
                  top: 0,
                }}
              >
                {calcFinalAmountProcedure.map((item, index) => {
                  return (
                    <Box
                      key={item.title}
                      sx={{
                        // bgcolor: 'green',
                        width: 'auto',
                        height: 'auto',
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        mt: item.margin_top,
                      }}
                    >
                      <Box
                        sx={{
                          width: 'fit-content',
                          display: 'flex',
                          alignItems: 'center',
                        }}
                      >
                        <Avatar
                          sx={{
                            width: '24px',
                            height: '24px',
                            bgcolor: 'secondary.main',
                            color: 'text.secondary',
                            mr: '12px',
                            zIndex: 2,
                          }}
                        >
                          {item.icon}
                        </Avatar>
                        <Typography
                          variant="inherit"
                          component="span"
                          color={calcFinalAmountProcedure.length - 1 === index ? 'text.primary' : 'text.secondary'}
                          // fontFamily={pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Regular'}
                          sx={{
                            fontSize:
                              calcFinalAmountProcedure.length - 1 === index
                                ? {
                                    xs: '17px',
                                    sm: '20px',
                                  }
                                : { xs: '14px', sm: '17px' },
                            // fontWeight: calcFinalAmountProcedure.length - 1 === index ? '700' : '400',
                            fontFamily:
                              calcFinalAmountProcedure.length - 1 === index
                                ? 'IRANSansXFaNum-DemiBold'
                                : 'IRANSansXFaNum-Regular',
                          }}
                        >
                          {item.title}
                        </Typography>
                      </Box>
                      <Typography
                        variant="inherit"
                        component="span"
                        color={'text.secondary'}
                        sx={{ fontSize: '14px', fontWeight: '400' }}
                      >
                        {item.amount}
                      </Typography>
                    </Box>
                  );
                })}
              </Box>
              <Box
                sx={{
                  // bgcolor: "red",
                  width: '14.5px',
                  height: '250px',
                  // position: 'relative'
                }}
              >
                <Divider
                  variant={'fullWidth'}
                  orientation={'vertical'}
                  sx={{
                    borderRightWidth: '5px',
                    borderColor: 'secondary.main',
                  }}
                />
              </Box>
            </Box>

            <Box
              sx={{
                width: '100%',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'flex-start',
                mt: '30px',
              }}
            >
              <Typography
                variant="inherit"
                component="span"
                color={'text.secondary'}
                sx={{
                  fontSize: '17px',
                  fontWeight: '400',
                }}
              >
                {t('deposit:destination_received_amount')}
              </Typography>
              <TextField
                // inputRef={focusInput-index}
                value={inputsValueState.finalValue}
                // onChange={}
                // error={}
                // helperText={}

                // color={'primary.main'}
                fullWidth
                // name={`final_${operationType}`}
                placeholder={t('deposit:enter_amount')}
                type={'tel'}
                autoComplete="off"
                onChange={(event) => {
                  handleCheckInputValue(event, 'finalValue');
                  // handleCheckAmount(event)
                }}
                // onBlur={(event) => handleCheckAmount(event)}

                sx={{
                  '& input::placeholder': {
                    fontSize: '14px',
                    color: 'text.secondary',
                  },
                  width: '100%',
                  height: '100%',
                  bgcolor: 'secondary.main',
                  borderRadius: '12px',
                  mt: '10px',
                  '& .MuiInputAdornment-root': {
                    ml: '8px',
                    height: '36px',
                    maxHeight: '40px',
                    backgroundColor: 'white',
                    borderRadius: '12px',
                  },
                  '& legend': { display: 'none' },
                  '& fieldset': {
                    top: 0,
                    borderRadius: '12px',
                    borderColor: inputsValueState.finalValue ? 'primary.main' : 'secondary.main',
                  },
                  '& .MuiOutlinedInput-root:hover': {
                    '& > fieldset': {
                      borderColor: inputsValueState.finalValue ? 'primary.main' : 'secondary.main',
                    },
                  },
                  // '& fieldset:hover': {top: 0, borderRadius: '12px', border: '1px solid red'},
                  '& .MuiOutlinedInput-root': {
                    padding: 0,
                    borderRadius: '12px',
                    overflow: 'hidden',
                    height: '100%',
                    '&.Mui-focused fieldset': {
                      borderColor: 'primary.main',
                    },
                    '&:hover:before fieldset': {
                      borderColor: 'primary.main',
                    },

                    color: 'primary.main',
                  },
                  '.MuiOutlinedInput-root :focus': {
                    backgroundColor: 'white',
                  },
                  '.MuiInputBase-input': {
                    fontFamily: pageDirection === 'ltr' ? 'IRANSansX-Regular' : 'IRANSansXFaNum-Regular',
                    padding: '14px',
                  },
                  '.MuiOutlinedInput-input': {
                    bgcolor: inputsValueState.finalValue ? 'white' : 'secondary.main',
                  },
                  zIndex: 1,
                }}
                InputProps={{
                  autoComplete: 'off',
                  endAdornment: (
                    <InputAdornment position="start">
                      {
                        <Select
                          // dir={'rtl'}
                          value={finalCurrency}
                          onChange={handleChangeFinalCurrency}
                          displayEmpty
                          IconComponent={NewIcon}
                          renderValue={(value) => {
                            return (
                              value?.uid && (
                                <Box
                                  // dir={'ltr'}
                                  sx={{
                                    width: '100%',
                                    display: 'flex',
                                    alignItems: 'center',
                                    justifyContent: 'space-between',
                                  }}
                                >
                                  <Typography
                                    variant="inherit"
                                    component="span"
                                    color={'text.secondary'}
                                    sx={{
                                      mt: '4px',
                                      fontSize: '12px',
                                      fontWeight: '600',
                                      mr: '8px',
                                    }}
                                  >
                                    {isInternal ? value.iso_code : value.currency_iso_code}
                                  </Typography>
                                  <Box
                                    component={'img'}
                                    src={isInternal ? value.currency_logo : value.currency_image}
                                    // alt={`${value.title} icon`}
                                    sx={{
                                      width: '24px',
                                      height: '24px',
                                    }}
                                  />
                                </Box>
                              )
                            );
                          }}
                          MenuProps={{
                            anchorOrigin: {
                              vertical: 'bottom',
                              horizontal: 'left',
                            },
                            transformOrigin: {
                              vertical: 'top',
                              horizontal: 'left',
                            },
                            PaperProps: {
                              sx: {
                                mt: '8px',
                                width: '120px',
                                height: 'auto',
                                // maxHeight: '176px',
                                borderRadius: '10px',
                                overflowY: 'auto',
                              },
                            },
                          }}
                          sx={{
                            // minWidth: '110px',
                            '& .MuiSelect-icon': { top: '40%', mx: '5px' },
                            height: '100%',
                            // ml: '8px',
                            '& .muirtl-bpeome-MuiSvgIcon-root-MuiSelect-icon': { left: 'auto' },
                            '& .muirtl-hfutr2-MuiSvgIcon-root-MuiSelect-icon': { left: 'auto' },
                            '& .muirtl-ittuaa-MuiInputAdornment-root': { margin: '0' },
                            '& .muirtl-keuqm-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input.muirtl-keuqm-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input.muirtl-keuqm-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input':
                              {
                                // px: '12px',
                                pl: '12px',
                                pr: '35px',
                                width: 'fit-content',
                              },
                            '& fieldset': { border: 'none' },
                          }}
                        >
                          {allowedReceiverWalletsState?.map((item: WalletSample, index: number) => {
                            return (
                              <MenuItem
                                value={JSON.stringify(item)}
                                key={index}
                                sx={{
                                  width: '100%',
                                  px: '8px',
                                  my: '5px',
                                }}
                              >
                                <Box
                                  // dir={'ltr'}
                                  sx={{
                                    width: '100%',
                                    display: 'flex',
                                    alignItems: 'center',
                                    justifyContent: 'space-between',
                                  }}
                                >
                                  <Box
                                    component={'img'}
                                    src={isInternal ? item.currency_logo : item.currency_image}
                                    // alt={`${item.currency} icon`}
                                    sx={{
                                      width: '24px',
                                      height: '24px',
                                    }}
                                  />
                                  <Box
                                    sx={{
                                      width: 'auto',
                                      height: 'auto',
                                      flexGrow: 1,
                                      display: 'flex',
                                      alignItems: 'center',
                                      justifyContent: 'space-between',
                                      mx: '8px',
                                    }}
                                  >
                                    <Typography
                                      variant="inherit"
                                      component="span"
                                      color={'text.secondary'}
                                      sx={{
                                        fontSize: '12px',
                                        fontWeight: '600',
                                      }}
                                    >
                                      {isInternal ? item.iso_code : item.currency_iso_code}
                                    </Typography>
                                    <Typography
                                      variant="inherit"
                                      component="span"
                                      color={'text.secondary'}
                                      sx={{
                                        fontSize: '14px',
                                        fontWeight: '400',
                                      }}
                                    >
                                      {isInternal ? item.currency : item.currency_name}
                                    </Typography>
                                  </Box>
                                </Box>
                              </MenuItem>
                            );
                          })}
                        </Select>
                      }
                    </InputAdornment>
                  ),
                }}
              />
            </Box>

            <Box sx={{ display: 'flex', alignItems: 'center', mt: '45px' }}>
              <Box
                component={'img'}
                src={bulletPoint.src}
                alt={'bullet point'}
                sx={{
                  width: '22px',
                  height: '18px',
                  mr: '9px',
                  transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                }}
              />
              <Typography
                variant="inherit"
                component="span"
                color={'text.primary'}
                sx={{
                  fontWeight: { xs: '600' },
                  fontSize: { xs: '16px', xl: '16px' },
                }}
              >
                {t('deposit:description')}
              </Typography>
            </Box>

            <TextField
              // inputRef={focusInput-index}
              value={inputsValueState.descriptionValue}
              // onChange={}
              // error={}
              // helperText={}

              // color={'primary.main'}
              fullWidth
              name={'description'}
              placeholder={t('deposit:enter_your_description')}
              type={'tel'}
              autoComplete="off"
              multiline
              rows={6}
              onChange={(event) => handleCheckInputValue(event, 'descriptionValue')}
              sx={{
                width: '100%',
                height: '100%',
                // bgcolor: 'secondary.main',
                bgcolor: inputsValueState.descriptionValue ? 'white' : 'secondary.main',
                borderRadius: '12px',
                mt: '10px',
                '& .MuiInputAdornment-root': { margin: '0' },
                '& legend': { display: 'none' },
                '& fieldset': {
                  top: 0,
                  borderRadius: '12px',
                  borderColor: inputsValueState.descriptionValue ? 'primary.main' : 'secondary.main',
                },
                '&:hover .fieldset': {
                  border: '1px solid red',
                },
                '& .MuiOutlinedInput-root:hover': {
                  '& > fieldset': {
                    borderColor: inputsValueState.descriptionValue ? 'primary.main' : 'secondary.main',
                  },
                },
                // '& fieldset:hover': {top: 0, borderRadius: '12px', border: '1px solid red'},
                '& .MuiOutlinedInput-root': {
                  padding: 0,
                  borderRadius: '12px',
                  '&.Mui-focused fieldset': {
                    borderColor: 'primary.main',
                  },
                  '&:hover:before fieldset': {
                    borderColor: 'primary.main',
                  },
                },
                '.MuiOutlinedInput-root :focus': {
                  backgroundColor: 'white',
                },
                '.MuiInputBase-input': {
                  fontFamily: pageDirection === 'ltr' ? 'IRANSansX-Regular' : 'IRANSansXFaNum-Regular',
                  padding: '14px',
                },
                '.MuiInputBase-input::placeholder': {
                  fontSize: '14px',
                  color: 'text.secondary',
                },
                zIndex: 1,
              }}
              InputProps={{
                autoComplete: 'off',
              }}
            />

            <Button
              variant="contained"
              disableRipple={true}
              type={'submit'}
              // disabled={!inputsValueState.initialValue}
              sx={{
                width: { xs: '72%', sm: '68%' },
                height: { xs: '40px', sm: '48px' },
                '&:hover': {
                  boxShadow: 'none',
                },
                boxShadow: 'none',
                fontWeight: '500',
                borderRadius: '12px',
                fontSize: { xs: '12px', sm: '14px' },
                mt: '32px',
              }}
              // onClick={handleSubmitOperationType}
            >
              {t('deposit:final_confirm_convert')}
            </Button>
          </Box>
        </Box>
      </Box>

      {openResultDialogBox && (
        <AlertDialogBox
          mode={'result'}
          data={operationResultState}
          open={openResultDialogBox}
          close={handleCloseResultDB}
        />
      )}
    </>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default Transformation;
