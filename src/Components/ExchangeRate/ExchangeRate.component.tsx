import React, { useEffect, useState, ChangeEvent, ReactNode } from 'react';
import {
  Box,
  ButtonGroup,
  Button,
  InputAdornment,
  IconButton,
  Select,
  MenuItem,
  TextField,
  InputLabel,
  Tabs,
  Tab,
  Chip,
  FormHelperText,
  FormControl,
} from '@mui/material';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useRouter } from 'next/router';
import { PATHS } from '../../configs/routes.config';
import starIcon from '/public/assets/images/Star-icon.svg';
import searchIconActive from '/public/assets/images/Search-icon-active.svg';
import coloredStarIcon from '/public/assets/images/colored-star-icon.svg';
import coloredPlusIcon from '*.svg';
import Typography from '@mui/material/Typography';
import { SearchBar } from '../SearchBar/SearchBar.component';
import FlexibleSearchBarComponent from '../FlexibleSearchBar/FlexibleSearchBar.component';
import FlexibleSearchBar from '../FlexibleSearchBar/FlexibleSearchBar.component';
import { dispatch } from 'jest-circus/build/state';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { fetchExchangeCurrenciesInfo } from '../../store/slices/currencies_exchange_infos.slice';
import { CurrencyTable } from '../CurrencyTable/CurrencyTable.component';
import { Trans } from 'next-i18next';
import { useI18nTrDir } from '../../utils/CustomHooks/i18nTrDir';
import { localesNS } from '../../utils/Data/locales_NameSpace.utils';

const ExchangeRate = () => {
  const router = useRouter();
  const UrlPath = router.pathname;
  const { t, pageDirection } = useI18nTrDir();

  let titleFilteredArray: any = [];
  let isoCodeFilteredArray: any = [];
  let AllFilteredResultsArray: any = [];

  const dispatch = useAppDispatch();
  const exchangeCurrenciesInfoResult = useAppSelector((state) => state.ExchangeCurrenciesInfo);

  const [favoriteBtnHover, setFavoriteBtnHover] = useState(false);
  const [filterBtnHover, setFilterBtnHover] = useState(false);
  const [SearchedResults, setSearchedResults] = useState<[]>([]);
  const [searchedText, setSearchedText] = useState('');

  const exchangeRateTabs = [
    {
      id: 1,
      title: t('exchange:exchange_calc'),
      link: `${PATHS.USER_EXCHANGE}/${PATHS.CALCULATOR}`,
    },
    {
      id: 2,
      title: t('exchange:exchange_rate'),
      link: `${PATHS.USER_EXCHANGE}`,
    },
    {
      id: 3,
      title: t('exchange:wages'),
      link: `${PATHS.USER_EXCHANGE}/${PATHS.FEE}`,
    },
  ];

  const CurrencyTabs = [
    {
      id: 1,
      title: t('exchange:currencies'),
      link: `${PATHS.USER_EXCHANGE}/${PATHS.RATE}/${PATHS.FIAT}`,
    },
    {
      id: 2,
      title: t('exchange:crypto_currencies'),
      link: `${PATHS.USER_EXCHANGE}/${PATHS.RATE}/${PATHS.CRYPTO}`,
    },
  ];

  const [activeExchangeTab, setActiveExchangeTab] = useState(exchangeRateTabs[1]);
  const [activeCurrencyTab, setActiveCurrencyTab] = useState(CurrencyTabs[0]);

  useEffect(() => {
    if (UrlPath.includes(exchangeRateTabs[1].link)) {
      dispatch(fetchExchangeCurrenciesInfo());
    }
  }, []);

  useEffect(() => {
    const selectedExchangeTab = exchangeRateTabs.find((tab) => UrlPath.includes(tab.link));
    const selectedCurrencyTab = CurrencyTabs.find((tab) => `/${tab.link}` === UrlPath);
    // @ts-ignore
    setActiveExchangeTab(selectedExchangeTab);
    // @ts-ignore
    setActiveCurrencyTab(selectedCurrencyTab);
  }, [UrlPath]);

  const handleSearchForResult = (event: any) => {
    setSearchedText(event.target.value);
    setSearchedText(event.target.value);
    if (!!exchangeCurrenciesInfoResult.ExchangeCurrenciesInfoResp?.currencies) {
      titleFilteredArray = exchangeCurrenciesInfoResult.ExchangeCurrenciesInfoResp?.currencies?.filter((item: any) => {
        return item.title.includes(event.target.value.toLowerCase());
      });
      isoCodeFilteredArray = exchangeCurrenciesInfoResult.ExchangeCurrenciesInfoResp?.currencies?.filter(
        (item: any) => {
          return item.iso_code.includes(event.target.value.toUpperCase());
        },
      );
    }
    AllFilteredResultsArray = titleFilteredArray.concat(isoCodeFilteredArray);
    // @ts-ignore
    const uniqueFilteredResults = [...new Set(AllFilteredResultsArray)];
    // @ts-ignore
    setSearchedResults(uniqueFilteredResults);
  };

  return (
    <>
      <Box
        sx={{
          width: '100%',
          height: 'auto',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'flex-start',
          justifyContent: 'flex-start',
        }}
      >
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            mb: { xs: '24px', sm: '32px' },
            mt: { xs: '15px', sm: 0 },
          }}
        >
          <Tabs
            value={+activeExchangeTab?.id - 1}
            // onChange={handleChangeTabs}
            aria-label="nav tabs example"
            TabIndicatorProps={{ style: { display: 'none' } }}
          >
            {exchangeRateTabs.map((tab) => {
              return (
                <Tab
                  key={tab.id}
                  label={tab.title}
                  disableRipple={true}
                  sx={{
                    fontSize: { xs: '14px', sm: '16px' },
                    // fontWeight: +activeExchangeTab?.id === +tab.id ? '600' : '400',
                    fontFamily:
                      +activeExchangeTab?.id === +tab.id ? 'IRANSansXFaNum-DemiBold' : 'IRANSansXFaNum-Regular',
                    borderRadius: '12px',
                    // transform: 'perspective(10px) rotateX(5deg)',
                    // transformOrigin: 'bottom',
                    bgcolor: +activeExchangeTab?.id === +tab.id ? 'white' : 'transparent',
                  }}
                  component="a"
                  onClick={(event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
                    event.preventDefault();
                    router.push(`/${tab.link}`);
                  }}
                  // href={tab.link}
                />
              );
            })}
          </Tabs>
        </Box>

        <Box
          sx={{
            width: '100%',
            display: { xs: 'flex', sm: 'none' },
            alignItems: 'flex-start',
            justifyContent: 'flex-end',
            mb: '24px',
          }}
        >
          <Button
            variant="contained"
            // type={'submit'}
            disableRipple
            sx={{
              '&:hover': {
                boxShadow: 'none',
                bgcolor: 'white',
              },
              bgcolor: 'white',
              boxShadow: 'none',
              borderRadius: '11px',
              width: '48px',
              height: '40px',
              minWidth: '45px',
              mr: '12px',
              padding: { xs: '10px 12px' },
              '& .MuiButton-startIcon': {
                mx: '0',
                // mr: {xs: '0', sm: '9px'}
              },
            }}
            startIcon={
              <Box
                component={'img'}
                src={favoriteBtnHover ? coloredStarIcon.src : starIcon.src}
                alt={'star icon'}
                sx={{
                  width: '24px',
                  height: '24px',
                  mx: 0,
                }}
              />
            }
            onClick={() => router.push(`/${PATHS.USER_BANK_ACCOUNT}/${PATHS.IRANIAN}`)}
            onMouseEnter={() => setFavoriteBtnHover(true)}
            onMouseLeave={() => setFavoriteBtnHover(false)}
          ></Button>
          <FlexibleSearchBar handleSearchForResult={handleSearchForResult} />
        </Box>

        <Box
          sx={{
            width: '100%',
            display: 'grid',
            gridAutoColumns: 'row',
            gridTemplateColumns: '1fr 1fr 1fr',
          }}
        >
          <Box></Box>

          <Box
            sx={{
              height: 'auto',
              display: 'flex',
              justifyContent: 'center',
            }}
          >
            <Tabs
              value={+activeCurrencyTab?.id - 1}
              // onChange={handleChangeTabs}
              aria-label="nav tabs example"
              TabIndicatorProps={{ style: { display: 'none' } }}
            >
              {CurrencyTabs.map((tab) => {
                return (
                  <Tab
                    key={tab.id}
                    label={tab.title}
                    disableRipple={true}
                    sx={{
                      width: 'auto',
                      px: { xs: '35px', lg: '50px' },
                      position: 'relative',
                      fontSize: { xs: '14px', sm: '16px' },
                      // fontWeight: +activeCurrencyTab?.id === +tab.id ? '600' : '400',
                      fontFamily:
                        +activeCurrencyTab?.id === +tab.id ? 'IRANSansXFaNum-DemiBold' : 'IRANSansXFaNum-Regular',
                      borderRadius: '12px',
                      pb: '28px',
                      pt: '18px',
                      // bgcolor: +activeCurrencyTab?.id === +tab.id ? 'white' : 'transparent',
                      bgcolor: 'transparent',
                      zIndex: 0,
                      '&::before': {
                        content: '" "',
                        display: 'block',
                        background: +activeCurrencyTab?.id === +tab.id ? 'white' : 'transparent',
                        width: 'calc(100% - 40%)',
                        height: '80px',
                        position: 'absolute',
                        top: 0,
                        right: pageDirection === 'rtl' ? '10px' : 'auto',
                        left: pageDirection === 'rtl' ? 'auto' : '10px',
                        transform: 'skew(-17deg)',
                        borderRadius: { xs: '15px', lg: '20px' },
                        zIndex: -1,
                      },
                      '&::after': {
                        content: '" "',
                        display: 'block',
                        background: +activeCurrencyTab?.id === +tab.id ? 'white' : 'transparent',
                        width: 'calc(100% - 40%)',
                        height: '80px',
                        position: 'absolute',
                        top: 0,
                        left: pageDirection === 'rtl' ? '10px' : 'auto',
                        right: pageDirection === 'rtl' ? 'auto' : '10px',
                        transform: 'skew(17deg)',
                        borderRadius: { xs: '15px', lg: '20px' },
                        zIndex: -1,
                      },
                    }}
                    component="a"
                    onClick={(event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
                      event.preventDefault();
                      router.push(`/${tab.link}`);
                    }}
                    // href={tab.link}
                  />
                );
              })}
            </Tabs>
          </Box>

          <Box
            sx={{
              height: 'auto',
              display: { xs: 'none', sm: 'flex' },
              alignItems: 'flex-start',
              justifyContent: 'flex-end',
            }}
          >
            <FlexibleSearchBar handleSearchForResult={handleSearchForResult} />

            <Button
              variant="contained"
              // type={'submit'}
              disableRipple
              sx={{
                '&:hover': {
                  boxShadow: 'none',
                  bgcolor: 'white',
                },
                bgcolor: 'white',
                boxShadow: 'none',
                borderRadius: '11px',
                width: '40px',
                height: '40px',
                minWidth: '40px',
                ml: '12px',
                padding: { xs: '10px 12px' },
                '& .MuiButton-startIcon': {
                  mx: '0',
                  // mr: {xs: '0', sm: '9px'}
                },
              }}
              startIcon={
                <Box
                  component={'img'}
                  src={favoriteBtnHover ? coloredStarIcon.src : starIcon.src}
                  alt={'star icon'}
                  sx={{
                    width: '24px',
                    height: '24px',
                    mx: 0,
                  }}
                />
              }
              onClick={() => router.push(`/${PATHS.USER_BANK_ACCOUNT}/${PATHS.IRANIAN}`)}
              onMouseEnter={() => setFavoriteBtnHover(true)}
              onMouseLeave={() => setFavoriteBtnHover(false)}
            ></Button>
          </Box>
        </Box>

        <Box
          sx={{
            width: '100%',
            height: 'auto',
            padding: '24px',
            bgcolor: 'white',
            mt: '-10px',
            zIndex: '2',
            borderRadius: '24px',
          }}
        >
          <CurrencyTable mode={'exchange'} AllFilteredResultsArray={SearchedResults} searchedText={searchedText} />
        </Box>

        <Box
          sx={{
            width: '100%',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            mt: '24px',
          }}
        >
          <Typography
            variant="inherit"
            component="span"
            color={'text.secondary'}
            sx={{
              fontWeight: '400',
              fontSize: '14px',
              mb: '12px',
            }}
          >
            {t('my_assets:prices_are_rials')}
          </Typography>
          <Typography
            variant="inherit"
            component="span"
            color={'text.disabled'}
            fontFamily={pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Regular'}
            sx={{
              fontWeight: '400',
              fontSize: '12px',
              unicodeBidi: 'embed',
              direction: 'ltr',
            }}
          >
            <Trans
              i18nKey="my_assets:last_update"
              values={{
                date: exchangeCurrenciesInfoResult?.ExchangeCurrenciesInfoResp?.latest_update.split(' ')[0],
                clock: exchangeCurrenciesInfoResult?.ExchangeCurrenciesInfoResp?.latest_update.split(' ')[1],
              }}
            >
              اخرین به روز رسانی در{' '}
              <bdi
                dir={'ltr'}
                style={{ fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Regular' }}
              ></bdi>{' '}
              ساعت
            </Trans>
          </Typography>
        </Box>
      </Box>
    </>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      hasLayout: true,
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default ExchangeRate;
