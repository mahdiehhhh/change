import React, { SetStateAction, useEffect, useState } from 'react';
import {
  Box,
  IconButton,
  InputLabel,
  List,
  TextField,
  ListItemButton,
  ListItemText,
  ListItemIcon,
  Collapse,
  FormControlLabel,
  Radio,
  RadioGroup,
  Avatar,
  Button,
  FormHelperText,
  FormControl,
} from '@mui/material';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useI18nTrDir } from '../../utils/CustomHooks/i18nTrDir';
import bulletPoint from '/public/assets/images/BulletPoint.svg';
import backwardArrowIcon from '/public/assets/images/backwardArrowDeposit.svg';

import Typography from '@mui/material/Typography';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { useRouter } from 'next/router';
import { localesNS } from 'utils/Data/locales_NameSpace.utils';
import * as yup from 'yup';
import { useFormik } from 'formik';
import { ExpandLess, ExpandMore } from '@mui/icons-material';
import { fetchWalletLatestTransfers } from '../../store/slices/wallet_latest_transfers.slice';
import { fetchFindTransferDestWallet } from '../../store/slices/transfer_destination_wallet.slice';
import { fetchFindTransferDestUser } from '../../store/slices/transfer_destination_user.slice';

interface LatestTransfersSample {
  owner_name: string;
  owner_avatar: string;
  wallet_code: string;
  wallet_uid: string;
  wallet_title: string;
  wallet_currency: string;
  wallet_iso_code: string;
}

interface Schema {
  destination: string;
}

interface Props {
  destinationMode: string;
  setActiveDestination: Function;
  activeDestination: any;
  detectWalletDestinationAddress: Function;
  handleGoToTransformation: Function;
}

const TransferNewDestination = ({
  destinationMode,
  activeDestination,
  setActiveDestination,
  detectWalletDestinationAddress,
  handleGoToTransformation,
}: Props) => {
  const { t, pageDirection } = useI18nTrDir();
  const router = useRouter();
  const wallet_uid: any = router.query.wallet_uid;
  console.log('this si ia active destination', activeDestination);

  const dispatch = useAppDispatch();
  const walletLatestTransfersResult = useAppSelector((state) => state.WalletLatestTransfers);
  const findTransferDestWalletResult = useAppSelector((state) => state.FindTransferDestWallet);
  const findTransferDestUserResult = useAppSelector((state) => state.FindTransferDestUser);

  const [lastDestListOpen, setLastDestListOpen] = React.useState(false);
  const [latestTransfersState, setLatestTransfersState] = useState<LatestTransfersSample[]>([]);
  // const [activeDestination, setActiveDestination] = useState( destinationMode === 'transfer_via_number' ? findTransferDestUserResult.transferDestUserResp?.destination_wallets_list[0].code : !!latestTransfersState && latestTransfersState[0]?.wallet_code)
  const [destinationState, setDestinationState] = useState('');
  const [destinationApiErrorsState, setDestinationApiErrorsState] = useState<any>('');

  useEffect(() => {
    if (!destinationState) {
      formik.resetForm();
      setDestinationApiErrorsState('');
    }
  }, [destinationState]);

  const handleClick = () => {
    setLastDestListOpen(!lastDestListOpen);
  };

  useEffect(() => {
    if (destinationMode !== 'transfer_via_number') {
      dispatch(fetchWalletLatestTransfers());
    }
  }, []);

  useEffect(() => {
    setLatestTransfersState(walletLatestTransfersResult.walletLatestTransfersResp?.latest_transferred_wallets_list);
  }, [walletLatestTransfersResult]);

  const selectInitialValueByType = (): Schema => {
    return {
      destination: '',
    };
  };

  let schemaDeclaration: object = {
    destination: yup.string().required(t('bank_accounts:required_text')),
  };
  // @ts-ignore
  const validationSchema = yup.object(schemaDeclaration);

  const formik = useFormik({
    enableReinitialize: true,
    // @ts-ignore
    initialValues: selectInitialValueByType(),
    validationSchema: validationSchema,
    validateOnChange: false,
    validateOnBlur: false,
    onSubmit: async (values) => {
      detectWalletDestinationAddress(values);
    },
  });

  // const detectWalletDestinationAddress = (address: any) => {
  //     const checkLetterRejex = /[a-zA-Z]/
  //     if (checkLetterRejex.test(address['destination'])) {
  //         dispatch(fetchFindTransferDestUser('reset'))
  //         dispatch(fetchFindTransferDestWallet('reset'))
  //         dispatch(fetchFindTransferDestWallet(wallet_uid, address))
  //     } else {
  //         dispatch(fetchFindTransferDestUser('reset'))
  //         dispatch(fetchFindTransferDestWallet('reset'))
  //         dispatch(fetchFindTransferDestUser(wallet_uid, address))
  //     }
  // }

  useEffect(() => {
    const checkLetterRejex = /[a-zA-Z]/;
    if (checkLetterRejex.test(destinationState)) {
      if (!!findTransferDestWalletResult.error) {
        setDestinationApiErrorsState(findTransferDestWalletResult.error.message);
      } else {
        setDestinationApiErrorsState('');
      }
    } else {
      if (!!findTransferDestUserResult.error) {
        setDestinationApiErrorsState(findTransferDestUserResult.error.message);
      } else {
        setDestinationApiErrorsState('');
      }
    }
  }, [findTransferDestWalletResult, findTransferDestUserResult]);

  const handleChangeAddressInput = (event: any) => {
    setDestinationState(event.target.value);
  };

  // TODO: fix last 5 transfer from backend
  return (
    <Box
      sx={{
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
      }}
    >
      <Box
        sx={{
          width: { xs: '100%', sm: '496px' },
          height: 'auto',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'flex-start',
          bgcolor: 'white',
          padding: '16px',
          borderRadius: '22px',
        }}
      >
        <Box
          sx={{
            width: '100%',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}
        >
          <Box
            sx={{
              width: 'auto',
              display: 'flex',
              alignItems: 'center',
            }}
          >
            <Box
              component={'img'}
              src={bulletPoint.src}
              alt={'bullet-point'}
              sx={{
                width: '22px',
                height: '18px',
                mr: '9px',
                transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
              }}
            />
            <Typography
              variant="inherit"
              component="span"
              color={'text.primary'}
              sx={{
                fontWeight: { xs: '600' },
                fontSize: { xs: '16px', xl: '16px' },
              }}
            >
              {destinationMode === 'enter_address' ? t('deposit:new_destination') : t('deposit:transfer_to')}
            </Typography>
          </Box>
          <IconButton
            color="inherit"
            aria-label="prev button"
            edge="start"
            // onClick={() => router.push(`/${PATHS.USER_DASHBOARD}`)}
            sx={{
              width: '28px',
              height: '28px',
              display: 'flex',
              justifySelf: 'flex-end',
              padding: '0',
              backgroundColor: '#D9D9D9',
            }}
          >
            <Box
              component={'img'}
              src={backwardArrowIcon.src}
              alt={'backward icon'}
              sx={{
                width: '7px',
                height: '14px',
                transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
              }}
            />
          </IconButton>
        </Box>

        {destinationMode === 'enter_address' ? (
          <Box
            sx={{
              width: '100%',
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              px: { xs: 0, sm: '18px' },
            }}
            component={'form'}
            onSubmit={formik.handleSubmit}
          >
            <Box
              sx={{
                width: '100%',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'flex-start',
                mt: '60px',
              }}
            >
              <InputLabel
                htmlFor={'destination'}
                sx={{
                  fontSize: { xs: '12px', sm: '17px' },
                  fontWeight: '400',
                  color: 'text.primary',
                }}
              >
                {t('deposit:new_wallet_destination')}
              </InputLabel>

              <FormControl fullWidth error>
                <TextField
                  value={formik.values.destination}
                  onChange={(event) => {
                    formik.handleChange('destination')(event);
                    handleChangeAddressInput(event);
                  }}
                  error={
                    (formik.touched.destination && Boolean(formik.errors.destination)) || !!destinationApiErrorsState
                  }
                  helperText={formik.touched.destination && formik.errors.destination}
                  // color={'primary.main'}
                  fullWidth
                  id="destination"
                  name={'destination'}
                  placeholder={t('deposit:enter_wallet_address')}
                  type={'text'}
                  autoComplete="current-password"
                  sx={{
                    '& input::placeholder': {
                      fontSize: '14px',
                      color: 'text.secondary',
                    },
                    width: '100%',
                    mt: '16px',
                    // bgcolor: option.id === 'passport_image' ? preview ? 'white' : 'secondary.main' : inputValues[option.api_id] ? 'white' : 'secondary.main',
                    borderRadius: '12px',
                    '& .MuiInputAdornment-root': {
                      ml: '8px',
                      height: '40px',
                      maxHeight: '40px',
                      backgroundColor: destinationState ? 'white' : 'secondary.main',
                      borderRadius: '12px',
                      // border: option.id === 'passport_image' ? '1px dash' : 'none'
                    },
                    '& legend': { display: 'none' },
                    '& fieldset': {
                      top: 0,
                      borderRadius: '12px',
                      borderColor: destinationState ? 'primary.main' : 'secondary.main',
                    },
                    '& .MuiOutlinedInput-root:hover': {
                      '& > fieldset': {
                        // border: option.id === 'passport_image' ? '1px dash' : '',
                        borderColor: destinationState ? 'primary.main' : 'secondary.main',
                      },
                    },
                    // '& fieldset:hover': {top: 0, borderRadius: '12px', border: '1px solid red'},
                    '& .MuiOutlinedInput-root': {
                      padding: 0,
                      borderRadius: '12px',
                      overflow: 'hidden',
                      height: '100%',
                      bgcolor: destinationState ? 'white' : 'secondary.main',
                      // "& > fieldset": {
                      //     border: '1.5px solid green',
                      // },
                      // bgcolor: 'secondary.main',
                      // backgroundColor: '#ffffff',
                      '&.Mui-focused > fieldset': {
                        // border: '1px solid',
                        borderColor: 'primary.main',
                      },
                      '&:hover:before fieldset': {
                        borderColor: 'primary.main',
                      },
                      color: destinationApiErrorsState ? 'error.main' : 'text.primary',
                    },
                    '.MuiOutlinedInput-root :focus': {
                      backgroundColor: 'white',
                    },
                    '.MuiInputBase-input': {
                      padding: '14px',
                    },
                    '.MuiOutlinedInput-input': {
                      bgcolor: destinationState ? 'white' : 'secondary.main',
                    },
                    zIndex: 1,
                  }}
                  InputProps={{
                    autoComplete: 'off',
                  }}
                />
                {destinationApiErrorsState && <FormHelperText error={true}>{destinationApiErrorsState}</FormHelperText>}
              </FormControl>
            </Box>

            <List
              sx={{ width: '100%', bgcolor: 'background.paper' }}
              component="nav"
              aria-labelledby="nested-list-subheader"
            >
              <ListItemButton
                onClick={handleClick}
                disableRipple={true}
                sx={{
                  padding: 0,
                  ml: '16px',
                  '& .MuiSvgIcon-root': {
                    color: 'primary.main',
                  },
                  '&:hover': {
                    bgcolor: 'transparent',
                  },
                }}
              >
                <ListItemText
                  primary={
                    <Typography
                      variant="inherit"
                      component="span"
                      color={'primary.main'}
                      sx={{
                        fontWeight: '600',
                        fontSize: { xs: '12px', sm: '16px' },
                      }}
                    >
                      {t('deposit:last_destination')}
                    </Typography>
                  }
                />
                {lastDestListOpen ? <ExpandLess /> : <ExpandMore />}
              </ListItemButton>
              <Collapse
                in={lastDestListOpen}
                timeout="auto"
                unmountOnExit
                sx={{
                  width: '100%',
                }}
              >
                <List
                  component="div"
                  disablePadding
                  sx={{
                    width: '100%',
                  }}
                >
                  <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    value={activeDestination}
                    // defaultValue={latestTransfersState && latestTransfersState[0]?.wallet_code}
                    name="radio-buttons-group"
                    onChange={(event) => {
                      setActiveDestination(event);
                      formik.handleChange('destination')(event.target.value);
                    }}
                    sx={{
                      width: '100%',
                    }}
                  >
                    {latestTransfersState?.slice(0, 5)?.map((transfer) => (
                      <ListItemButton
                        disableRipple={true}
                        key={transfer.owner_name}
                        sx={{
                          width: '100%',
                          px: 0,
                          pl: '16px',
                          borderRadius: '9px',
                        }}
                      >
                        <FormControlLabel
                          value={transfer.wallet_code}
                          control={<Radio disableRipple={true} size={'small'} />}
                          label={
                            <Box
                              sx={{
                                width: '100%',
                                display: 'flex',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                              }}
                            >
                              <Box
                                sx={{
                                  display: 'flex',
                                  alignItems: 'center',
                                  mr: '12px',
                                }}
                              >
                                <Avatar
                                  alt={transfer.owner_name}
                                  src={transfer.owner_avatar}
                                  sx={{
                                    width: '24px',
                                    height: '24px',
                                  }}
                                />
                                <Typography
                                  variant="inherit"
                                  component="span"
                                  color={'text.primary'}
                                  sx={{
                                    fontWeight: activeDestination === transfer.wallet_code ? '600' : '400',
                                    fontSize: {
                                      xs: '12px',
                                      sm: '16px',
                                    },
                                    color: activeDestination === transfer.wallet_code ? 'primary.main' : 'text.primary',
                                    ml: '8px',
                                  }}
                                >
                                  {transfer.owner_name}
                                </Typography>
                              </Box>
                              <Typography
                                variant="inherit"
                                component="span"
                                color={'text.primary'}
                                sx={{
                                  fontWeight: activeDestination === transfer.wallet_code ? '600' : '400',
                                  fontSize: {
                                    xs: '11px',
                                    sm: '14px',
                                  },
                                  color: activeDestination === transfer.wallet_code ? 'primary.main' : 'text.primary',
                                  letterSpacing: {
                                    xs: '2px',
                                    sm: '3px',
                                  },
                                }}
                              >
                                {transfer.wallet_code}
                              </Typography>
                            </Box>
                          }
                          sx={{
                            width: '100%',
                            '& .MuiFormControlLabel-label': {
                              flexGrow: 1,
                            },
                          }}
                        />

                        {/*<ListItemText primary={transfer.owner_name}/>*/}
                      </ListItemButton>
                    ))}
                  </RadioGroup>
                </List>
              </Collapse>
            </List>

            {lastDestListOpen && (
              <Box
                sx={{
                  width: '100%',
                  height: '2px',
                  mt: '20px',
                  // position: 'absolute',
                  // top: '205px',
                  backgroundImage: 'linear-gradient(to right, #C3C3C3 45%, rgba(255,255,255,0) 0%)',
                  backgroundPosition: 'bottom',
                  backgroundSize: '20px 1px',
                  backgroundRepeat: 'repeat-x',
                }}
              >
                {/*<Divider*/}
                {/*    variant={'middle'}*/}
                {/*    orientation={'horizontal'}*/}
                {/*    sx={{*/}
                {/*        borderBottomWidth: '1px',*/}
                {/*        borderColor: 'text.disabled',*/}
                {/*        borderStyle: 'dashed'*/}
                {/*    }}*/}
                {/*/>*/}
              </Box>
            )}

            <Button
              variant="contained"
              type={'submit'}
              disableRipple={true}
              sx={{
                width: { xs: '236px', sm: '236px' },
                height: { xs: '40px', sm: '40px' },
                '&:hover': {
                  boxShadow: 'none',
                },
                boxShadow: 'none',
                fontWeight: '500',
                borderRadius: '12px',
                fontSize: { xs: '12px', sm: '14px' },
                mt: '60px',
                mb: '13px',
              }}
            >
              {t('dialog_box:transfer')}
            </Button>
          </Box>
        ) : (
          <Box
            sx={{
              width: '100%',
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              px: { xs: 0, sm: destinationMode === 'enter_address' ? '18px' : 0 },
            }}
          >
            <Box
              sx={{
                width: '100%',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                mt: '41px',
              }}
            >
              <Avatar
                alt={findTransferDestUserResult.transferDestUserResp?.destination_user?.name}
                src={findTransferDestUserResult.transferDestUserResp?.destination_user?.avatar}
                sx={{
                  width: '68px',
                  height: '68px',
                }}
              />
              <Typography
                variant="inherit"
                component="span"
                color={'text.primary'}
                sx={{
                  fontWeight: '600',
                  fontSize: '14px',
                  my: '6px',
                }}
              >
                {findTransferDestUserResult.transferDestUserResp?.destination_user?.name}
              </Typography>
              <Typography
                variant="inherit"
                component="span"
                color={'text.disabled'}
                sx={{
                  fontWeight: '400',
                  fontSize: '12px',
                }}
              >
                {activeDestination}
              </Typography>
            </Box>

            <RadioGroup
              aria-labelledby="demo-radio-buttons-group-label"
              value={activeDestination}
              // defaultValue={latestTransfersState && latestTransfersState[0]?.wallet_code}
              name="radio-buttons-group"
              onChange={(event) => setActiveDestination(event)}
              sx={{
                width: '100%',
                mt: '20px',
              }}
            >
              {findTransferDestUserResult.transferDestUserResp?.destination_wallets_list?.map((wallet: any) => (
                <FormControlLabel
                  key={wallet.code}
                  value={wallet.code}
                  control={<Radio disableRipple={true} size={'small'} />}
                  label={
                    <Box
                      sx={{
                        width: '100%',
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                      }}
                    >
                      <Box
                        sx={{
                          display: 'flex',
                          alignItems: 'center',
                          mr: '12px',
                        }}
                      >
                        <Box
                          component={'img'}
                          src={wallet.logo}
                          alt={'bullet-point'}
                          sx={{
                            width: { xs: '29px', sm: '35px' },
                            height: '22px',
                            mr: '9px',
                          }}
                        />
                        <Typography
                          variant="inherit"
                          component="span"
                          color={'text.primary'}
                          sx={{
                            fontWeight: activeDestination === wallet.code ? '600' : '400',
                            fontSize: { xs: '14px', sm: '16px' },
                            color: activeDestination === wallet.code ? 'primary.main' : 'text.primary',
                            ml: '8px',
                          }}
                        >
                          {wallet.title}
                        </Typography>
                      </Box>
                      <Typography
                        variant="inherit"
                        component="span"
                        color={'text.primary'}
                        sx={{
                          fontWeight: activeDestination === wallet.code ? '600' : '400',
                          fontSize: { xs: '11px', sm: '14px' },
                          color: activeDestination === wallet.code ? 'primary.main' : 'text.primary',
                          letterSpacing: { xs: '2px', sm: '3px' },
                        }}
                      >
                        {wallet.code}
                      </Typography>
                    </Box>
                  }
                  sx={{
                    my: '10px',
                    width: '100%',
                    '& .MuiFormControlLabel-label': {
                      flexGrow: 1,
                    },
                  }}
                />
              ))}
            </RadioGroup>

            {lastDestListOpen && (
              <Box
                sx={{
                  width: '100%',
                  height: '2px',
                  mt: '20px',
                  // position: 'absolute',
                  // top: '205px',
                  backgroundImage: 'linear-gradient(to right, #C3C3C3 45%, rgba(255,255,255,0) 0%)',
                  backgroundPosition: 'bottom',
                  backgroundSize: '20px 1px',
                  backgroundRepeat: 'repeat-x',
                }}
              >
                {/*<Divider*/}
                {/*    variant={'middle'}*/}
                {/*    orientation={'horizontal'}*/}
                {/*    sx={{*/}
                {/*        borderBottomWidth: '1px',*/}
                {/*        borderColor: 'text.disabled',*/}
                {/*        borderStyle: 'dashed'*/}
                {/*    }}*/}
                {/*/>*/}
              </Box>
            )}

            <Button
              variant="contained"
              type={'submit'}
              disableRipple={true}
              sx={{
                width: { xs: '236px', sm: '236px' },
                height: { xs: '40px', sm: '40px' },
                '&:hover': {
                  boxShadow: 'none',
                },
                boxShadow: 'none',
                fontWeight: '500',
                borderRadius: '12px',
                fontSize: { xs: '12px', sm: '14px' },
                mt: '60px',
                mb: '13px',
              }}
              onClick={() => handleGoToTransformation('transformation')}
            >
              {t('dialog_box:transfer')}
            </Button>
          </Box>
        )}
      </Box>
    </Box>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default TransferNewDestination;
