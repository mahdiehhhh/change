import React, { useEffect, useState, ChangeEvent, ReactNode, useRef } from 'react';
import {
  Box,
  ButtonGroup,
  Button,
  InputAdornment,
  IconButton,
  Select,
  MenuItem,
  TextField,
  InputLabel,
  Tabs,
  Tab,
  Chip,
  FormHelperText,
  FormControl,
  SelectChangeEvent,
  Skeleton,
  Avatar,
} from '@mui/material';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useRouter } from 'next/router';
import { PATHS } from '../../configs/routes.config';
import starIcon from '/public/assets/images/Star-icon.svg';
import filterIcon from '/public/assets/images/notif-filter-icon.svg';
import searchIconActive from '/public/assets/images/Search-icon-active.svg';
import coloredStarIcon from '/public/assets/images/colored-star-icon.svg';
import coloredPlusIcon from '/public/assets/images/colored_plus_icon.svg';
import Typography from '@mui/material/Typography';
import { SearchBar } from '../SearchBar/SearchBar.component';
import FlexibleSearchBarComponent from '../FlexibleSearchBar/FlexibleSearchBar.component';
import FlexibleSearchBar from '../FlexibleSearchBar/FlexibleSearchBar.component';
import { dispatch } from 'jest-circus/build/state';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { fetchExchangeCurrenciesInfo } from '../../store/slices/currencies_exchange_infos.slice';
import { CurrencyTable } from '../CurrencyTable/CurrencyTable.component';
import { Trans } from 'next-i18next';
import { useI18nTrDir } from '../../utils/CustomHooks/i18nTrDir';
import { localesNS } from '../../utils/Data/locales_NameSpace.utils';
import headphoneIcon from '/public/assets/images/headphone-icon.svg';
import { TextInfoTable } from '../TextInfoTable/TextInfoTable.component';
import { fetchUserTickets } from '../../store/slices/user_tickets.slice';
import noTransactionSmileIcon from '/public/assets/images/no-transaction-smile.svg';
import AlertDialogBox from '../AlertDialogBox/AlertDialogBox.component';
import bulletPoint from '/public/assets/images/BulletPoint.svg';
import dangerCirclePurple from '/public/assets/images/Danger-Circle-purple-icon.svg';
import { fetchCurrenciesDashboard } from '../../store/slices/currencies_dashboard.slice';
import dangerCircleGreenIcon from '*.svg';
import { fetchFeesList } from '../../store/slices/fees_list.slice';
import backwardArrowIcon from '/public/assets/images/backwardArrowDeposit.svg';
import successTransactionIcon from '/public/assets/images/transaction-success.svg';
import infinityLogo from '/public/assets/images/infinity-logo.svg';
import downloadIcon from '/public/assets/images/Download-icon.svg';
import shareIcon from '/public/assets/images/Share-icon.svg';
import exitIcon from '/public/assets/images/exit-icon.svg';
import { fetchConfirmWalletTransfer } from '../../store/slices/wallet_transfer_info_confirm.slice';
import html2canvas from 'html2canvas';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import TransferNewDestination from '../TransferNewDestination/TransferNewDestination.component';
import {
  FacebookShareButton,
  WhatsappShareButton,
  WhatsappIcon,
  FacebookIcon,
  TelegramShareButton,
  TelegramIcon,
  EmailShareButton,
  EmailIcon,
} from 'react-share';

interface DashboardCurrency {
  title: string;
  type: string;
  symbol: string;
  iso_code: string;
  image: string;
  color: string;
  sell_price: string;
  buy_price: string;
  id: number;
  price_history: PriceHistorySample[];
  change_percent: string;
  change: number;
}

interface PriceHistorySample {
  average_buy_price: string;
  average_sell_price: string;
  day: string;
}

interface Props {
  paymentMode: string;
}

const PaymentReceipt = ({ paymentMode }: Props) => {
  const router = useRouter();
  const UrlPath = router.pathname;
  const { t, pageDirection } = useI18nTrDir();
  const dispatch = useAppDispatch();
  const transferReceiptRef = useRef();

  const isInternal = router.pathname.includes('internal');
  const isOthers = router.pathname.includes('others');

  const checkWalletTransferResult = useAppSelector((state) => state.CheckWalletTransfer);
  const confirmWalletTransferResult = useAppSelector((state) => state.ConfirmWalletTransfer);
  const userActiveWalletResult = useAppSelector((state) => state.UserActiveWallets);

  const [receiptInfo, setReceiptInfo] = useState<any>({
    sender: {
      title: '',
      wallet_code: '',
    },
    receiver: {
      title: '',
      wallet_code: '',
    },
  });
  const [receiptImage, setReceiptImage] = useState('');
  const [openShareWithDB, setOpenShareWithDB] = useState(false);

  const handleOpenShareWithDB = () => {
    setOpenShareWithDB(true);
  };

  const handleCloseShareWithDB = () => {
    setOpenShareWithDB(false);
  };

  useEffect(() => {
    prepareDemonstratingDataText();
  }, [checkWalletTransferResult]);

  const prepareDemonstratingDataText = () => {
    // let transactionPartiesWalletInfo: any = {}
    userActiveWalletResult.userActWalletsResp?.forEach((wallet: any) => {
      if (wallet.iso_code === checkWalletTransferResult.checkWalletTransferResp?.transaction?.source_iso_code) {
        setReceiptInfo((prevState: any) => ({
          ...prevState,
          sender: isInternal
            ? {
                title: wallet?.title,
                wallet_code: wallet?.code,
              }
            : {
                ...checkWalletTransferResult.checkWalletTransferResp?.sender,
              },
          senderWalletUid: wallet?.uid,
        }));
      }
      if (wallet.iso_code === checkWalletTransferResult.checkWalletTransferResp?.transaction?.destination_iso_code) {
        setReceiptInfo((prevState: any) => ({
          ...prevState,
          receiver: isInternal
            ? {
                title: wallet?.title,
                wallet_code: wallet?.code,
              }
            : {
                ...checkWalletTransferResult.checkWalletTransferResp?.receiver,
              },
        }));
      }
    });
    const receiptDataText = {
      send_amount: `${checkWalletTransferResult.checkWalletTransferResp?.transaction?.source_amount} ${checkWalletTransferResult.checkWalletTransferResp?.transaction?.source_iso_code}`,
      received_amount: `${checkWalletTransferResult.checkWalletTransferResp?.transaction?.destination_amount} ${checkWalletTransferResult.checkWalletTransferResp?.transaction?.destination_iso_code}`,
      transaction_code: checkWalletTransferResult.checkWalletTransferResp?.transaction?.transaction_id,
      exchange_rate: checkWalletTransferResult.checkWalletTransferResp?.transaction?.exchange_rate,
      transaction_fee: `% ${checkWalletTransferResult.checkWalletTransferResp?.transaction?.transaction_fee}`,
      transaction_date: dateFormatter(),
      transaction_status: checkWalletTransferResult.checkWalletTransferResp?.transaction?.status,
    };
    setReceiptInfo((prevState: any) => ({ ...prevState, ...receiptDataText }));
  };

  const dateFormatter = () => {
    return `${checkWalletTransferResult.checkWalletTransferResp?.transaction?.created_at.split('|')[1]} | ${
      checkWalletTransferResult.checkWalletTransferResp?.transaction?.created_at.split('|')[0]
    }`;
  };

  const paymentReceiptContents = [
    {
      id: 1,
      type: 'double',
      title: t('deposit:sender'),
      first_val: receiptInfo['sender'][isOthers && paymentMode.includes('confirm') ? 'name' : 'title'],
      Second_val: receiptInfo['sender']['code'],
      color: 'text.primary',
    },
    {
      id: 2,
      type: 'double',
      title: t('deposit:receiver'),
      first_val: receiptInfo['receiver'][isOthers && paymentMode.includes('confirm') ? 'name' : 'title'],
      Second_val: receiptInfo['receiver']['code'],
      color: 'text.primary',
    },
    {
      id: 3,
      type: 'separator',
      title: '',
      first_val: '',
      Second_val: '',
      color: 'text.primary',
    },
    {
      id: 4,
      type: 'single',
      title: t('deposit:send_amount'),
      first_val: receiptInfo['send_amount'],
      Second_val: '',
      color: 'text.primary',
    },
    {
      id: 5,
      type: 'single',
      title: t('deposit:received_amount'),
      first_val: receiptInfo['received_amount'],
      Second_val: '',
      color: 'text.primary',
    },
    {
      id: 6,
      type: 'single',
      title: t('deposit:transaction_code'),
      first_val: receiptInfo['transaction_code'],
      Second_val: '',
      color: 'text.secondary',
    },
    {
      id: 7,
      type: 'single',
      title: t('deposit:rate'),
      first_val: receiptInfo['exchange_rate'],
      Second_val: '',
      color: 'text.secondary',
    },
    {
      id: 8,
      type: 'single',
      title: t('deposit:transaction_fee'),
      first_val: receiptInfo['transaction_fee'],
      Second_val: '',
      color: 'text.secondary',
    },
    {
      id: 9,
      type: 'single',
      title: t('deposit:transaction_date'),
      first_val: receiptInfo['transaction_date'],
      Second_val: '',
      color: 'text.secondary',
    },
    {
      id: 10,
      type: 'status',
      title: t('deposit:payment_status'),
      first_val: receiptInfo['transaction_status'],
      Second_val: '',
      color: '',
    },
  ];

  const transferPartiesInformation = [
    {
      id: 1,
      type: 'dealer',
      avatar: receiptInfo['sender']['avatar'],
      name: receiptInfo['sender']['name'],
      role: t('deposit:sender'),
      wallet_code: receiptInfo['sender']['code'],
    },
    {
      id: 2,
      type: 'pointer',
    },
    {
      id: 3,
      type: 'dealer',
      avatar: receiptInfo['receiver']['avatar'],
      name: receiptInfo['receiver']['name'],
      role: t('deposit:receiver'),
      wallet_code: receiptInfo['receiver']['code'],
    },
  ];

  const handleConfirmTransfer = () => {
    const data = {
      transaction_id: receiptInfo['transaction_code'],
    };
    dispatch(fetchConfirmWalletTransfer(receiptInfo['senderWalletUid'], data));
  };

  const handlePrepareImageForShare = async (element: any) => {
    const canvas = await html2canvas(element);
    // const image = canvas.toBlob(element);
    canvas.toBlob((blob) => {
      const newImg = document.createElement('img');
      // @ts-ignore
      const url = URL.createObjectURL(blob);

      newImg.onload = () => {
        // no longer need to read the blob so it's revoked
        URL.revokeObjectURL(url);
      };

      newImg.src = url;
      setReceiptImage('whatsapp://send?text=' + encodeURIComponent(url));

      // document.body.appendChild(newImg);
    });
    // setReceiptImage(image)
  };

  const exportAsImage = async (element: any, imageFileName: string) => {
    const canvas = await html2canvas(element);
    const image = canvas.toDataURL('image/png', 1.0);
    downloadImage(image, imageFileName);
  };

  const downloadImage = (blob: any, fileName: string) => {
    let fakeLink = window.document.createElement('a');
    // @ts-ignore
    fakeLink.style.display = 'none';
    fakeLink.download = fileName;

    fakeLink.href = blob;

    document.body.appendChild(fakeLink);
    fakeLink.click();
    document.body.removeChild(fakeLink);

    fakeLink.remove();
  };

  useEffect(() => {
    if (receiptImage) {
      handleOpenShareWithDB();
    }
  }, [receiptImage]);

  return (
    <>
      <Box
        sx={{
          width: '100%',
          display: 'flex',
          justifyContent: 'center',
        }}
      >
        <Box
          ref={transferReceiptRef}
          sx={{
            width: { xs: '100%', sm: '462px' },
            padding: '16px',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'flex-start',
            bgcolor: 'white',
            borderRadius: '24px',
            backgroundImage: paymentMode.includes('confirm')
              ? `url(${infinityLogo.src}), radial-gradient(ellipse at bottom, #F4F6FA 45%, #F4F6FA 10%, transparent 0%)`
              : 'radial-gradient(ellipse at bottom, #F4F6FA 45%, #F4F6FA 10%, transparent 0%)',
            backgroundRepeat: paymentMode.includes('confirm') ? `no-repeat, repeat-x` : 'repeat-x',
            backgroundPosition: paymentMode.includes('confirm') ? `center, 11px bottom` : '11px bottom',
            backgroundSize: paymentMode.includes('confirm') ? `85%, 20px 18px` : '20px 18px',
          }}
        >
          <Box
            sx={{
              width: '100%',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}
          >
            <Box
              sx={{
                width: 'auto',
                display: 'flex',
                alignItems: 'center',
              }}
            >
              <Box
                component={'img'}
                src={bulletPoint.src}
                alt={'bullet-point'}
                sx={{
                  width: '22px',
                  height: '18px',
                  mr: '9px',
                  transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                }}
              />
              <Typography
                variant="inherit"
                component="span"
                color={'text.primary'}
                sx={{
                  fontWeight: { xs: '600' },
                  fontSize: { xs: '16px', xl: '16px' },
                }}
              >
                {t('deposit:confirm_transaction')}
              </Typography>
            </Box>
            {paymentMode.includes('check') ? (
              <IconButton
                color="inherit"
                aria-label="prev button"
                edge="start"
                // onClick={() => router.push(`/${PATHS.USER_DASHBOARD}`)}
                sx={{
                  width: '28px',
                  height: '28px',
                  display: 'flex',
                  justifySelf: 'flex-end',
                  padding: '0',
                  backgroundColor: '#D9D9D9',
                }}
              >
                <Box
                  component={'img'}
                  src={backwardArrowIcon.src}
                  alt={'backward icon'}
                  sx={{
                    width: '7px',
                    height: '14px',
                    transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                  }}
                />
              </IconButton>
            ) : (
              <IconButton
                color="inherit"
                aria-label="close button"
                edge="start"
                disableRipple={true}
                // onClick={() => router.push(`/${PATHS.USER_DASHBOARD}`)}
                sx={{
                  width: '32px',
                  height: '32px',
                  display: 'flex',
                  justifySelf: 'flex-end',
                  backgroundColor: 'white',
                }}
                onClick={() => router.push(`/${PATHS.USER_DASHBOARD}`)}
              >
                <Box
                  component={'img'}
                  src={exitIcon.src}
                  alt={'bullet-point'}
                  sx={{
                    width: '16px',
                    height: '16px',
                  }}
                />
              </IconButton>
            )}
          </Box>

          {paymentMode.includes('confirm') && (
            <Box
              sx={{
                width: '100%',
                display: 'flex',
                justifyContent: 'center',
                mt: '36px',
              }}
            >
              <Box
                component={'img'}
                src={successTransactionIcon.src}
                alt={'bullet-point'}
                sx={{
                  width: { xs: '98px', sm: '153px' },
                  height: { xs: '67px', sm: '104px' },
                  mr: '9px',
                  transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                }}
              />
            </Box>
          )}

          {isInternal ? (
            <Box
              sx={{
                width: '100%',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                mt: '30px',
              }}
            >
              {paymentReceiptContents.map((item) =>
                item.type === 'double' ? (
                  <Box
                    sx={{
                      width: '100%',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      mb: '8px',
                    }}
                  >
                    <Typography
                      variant="inherit"
                      component="span"
                      color={'text.primary'}
                      sx={{
                        fontWeight: '400',
                        fontSize: { xs: '12px', sm: '16px' },
                      }}
                    >
                      {item.title}
                    </Typography>
                    <Box
                      sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'flex-end',
                      }}
                    >
                      <Typography
                        variant="inherit"
                        component="span"
                        color={item.color}
                        sx={{
                          fontWeight: '600',
                          fontSize: '14px',
                        }}
                      >
                        {item.first_val}
                      </Typography>
                      <Typography
                        variant="inherit"
                        component="span"
                        color={'text.disabled'}
                        sx={{
                          fontWeight: '400',
                          fontSize: '12px',
                        }}
                      >
                        {item.Second_val}
                      </Typography>
                    </Box>
                  </Box>
                ) : item.type === 'single' ? (
                  <Box
                    sx={{
                      width: '100%',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      mt: '24px',
                    }}
                  >
                    <Typography
                      variant="inherit"
                      component="span"
                      color={'text.primary'}
                      sx={{
                        fontWeight: '400',
                        fontSize: { xs: '12px', sm: '16px' },
                      }}
                    >
                      {item.title}
                    </Typography>
                    <Box
                      sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'flex-end',
                      }}
                    >
                      <Typography
                        variant="inherit"
                        component="span"
                        color={item.color}
                        sx={{
                          fontWeight: '600',
                          fontSize: '14px',
                        }}
                      >
                        {item.first_val}
                      </Typography>
                    </Box>
                  </Box>
                ) : item.type === 'separator' ? (
                  <Box
                    sx={{
                      width: '100%',
                      height: '2px',
                      // position: 'absolute',
                      // top: '205px',
                      backgroundImage: 'linear-gradient(to right, #C3C3C3 45%, rgba(255,255,255,0) 0%)',
                      backgroundPosition: 'bottom',
                      backgroundSize: '20px 1px',
                      backgroundRepeat: 'repeat-x',
                    }}
                  ></Box>
                ) : (
                  item.type === 'status' && (
                    <Box
                      sx={{
                        width: '100%',
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        mt: '24px',
                      }}
                    >
                      <Typography
                        variant="inherit"
                        component="span"
                        color={'text.primary'}
                        sx={{
                          fontWeight: '400',
                          fontSize: { xs: '12px', sm: '16px' },
                        }}
                      >
                        {item.title}
                      </Typography>
                      <Chip
                        label={
                          item.first_val === 'customer_answered'
                            ? t('ticket:waiting_for_answer')
                            : item.first_val === 'admin_answered'
                            ? t('ticket:answered')
                            : t('ticket:closed')
                        }
                        sx={{
                          minWidth: { xs: '56px', sm: '112px' },
                          height: '24px',
                          borderRadius: { xs: '30px', sm: '11px' },
                          bgcolor: {
                            xs: 'transparent',
                            sm:
                              item.first_val === 'customer_answered'
                                ? 'info.light'
                                : item.first_val === 'admin_answered'
                                ? 'warning.light'
                                : 'success.light',
                          },
                          '& .MuiChip-label': {
                            padding: '0',
                            margin: '0',
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center',
                            color:
                              item.first_val === 'customer_answered'
                                ? 'info.main'
                                : item.first_val === 'admin_answered'
                                ? 'warning.main'
                                : 'success.dark',
                            fontSize: { xs: '10px', sm: '12px' },
                            fontWeight: '600',
                          },
                        }}
                      />
                    </Box>
                  )
                ),
              )}
            </Box>
          ) : (
            <Box
              sx={{
                width: '100%',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                mt: '30px',
                px: '8px',
              }}
            >
              <Box
                sx={{
                  width: '100%',
                  display: 'grid',
                  gridTemplateColumns: '0.75fr 1.5fr 0.75fr',
                  px: paymentMode.includes('check') ? { xs: 0, sm: '20px' } : 0,
                  // alignItems: 'flex-start',
                  // justifyContent: 'space-between',
                  // position: 'relative'
                }}
              >
                {paymentMode.includes('check') &&
                  transferPartiesInformation.map((item) =>
                    item.type === 'dealer' ? (
                      <Box
                        sx={{
                          width: 'auto',
                          display: 'flex',
                          flexDirection: 'column',
                          alignItems: 'center',
                        }}
                      >
                        <Avatar
                          alt={''}
                          src={item.avatar}
                          sx={{
                            width: { xs: '48px', sm: '68px' },
                            height: { xs: '48px', sm: '68px' },
                          }}
                        />
                        <Typography
                          variant="inherit"
                          component="span"
                          color={'text.primary'}
                          sx={{
                            // fontWeight: '600',
                            fontSize: { xs: '12px', sm: '14px' },
                            fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                            mt: '8px',
                            mb: '4px',
                          }}
                        >
                          {item.name}
                        </Typography>
                        <Typography
                          variant="inherit"
                          component="span"
                          color={'text.secondary'}
                          sx={{
                            fontWeight: '400',
                            fontSize: { xs: '10px', sm: '12px' },
                            mb: '4px',
                          }}
                        >
                          {item.role}
                        </Typography>
                        <Typography
                          variant="inherit"
                          component="span"
                          color={'text.disabled'}
                          sx={{
                            fontWeight: '400',
                            fontSize: { xs: '10px', sm: '12px' },
                          }}
                        >
                          {item.wallet_code}
                        </Typography>
                      </Box>
                    ) : (
                      <Box
                        sx={{
                          position: 'relative',
                        }}
                      >
                        <Box
                          sx={{
                            width: '100%',
                            flexGrow: 1,
                            display: 'flex',
                            alignItems: 'center',
                            position: 'absolute',
                            top: { xs: '14px', sm: '24px' },
                            right: '50%',
                            transform: 'translate(50%, 50%)',
                          }}
                        >
                          <Box
                            sx={{
                              width: '90%',
                              height: '2px',
                              // position: 'absolute',
                              // top: '205px',
                              backgroundImage: 'linear-gradient(to right, #298FFE 45%, rgba(255,255,255,0) 0%)',
                              backgroundPosition: 'bottom',
                              backgroundSize: '20px 2px',
                              backgroundRepeat: 'repeat-x',
                            }}
                          ></Box>
                          <Box
                            sx={{
                              width: 0,
                              height: 0,
                              borderTop: '5px solid transparent',
                              borderBottom: '5px solid transparent',
                              borderLeft: '10px solid #298FFE',
                            }}
                          ></Box>
                        </Box>
                      </Box>
                    ),
                  )}
              </Box>

              {paymentReceiptContents.map((item) =>
                paymentMode.includes('confirm') && item.type === 'double' ? (
                  <Box
                    sx={{
                      width: '100%',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      mb: '8px',
                    }}
                  >
                    <Typography
                      variant="inherit"
                      component="span"
                      color={'text.primary'}
                      sx={{
                        fontWeight: '400',
                        fontSize: { xs: '12px', sm: '16px' },
                      }}
                    >
                      {item.title}
                    </Typography>
                    <Box
                      sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'flex-end',
                      }}
                    >
                      <Typography
                        variant="inherit"
                        component="span"
                        color={item.color}
                        sx={{
                          fontWeight: '600',
                          fontSize: '14px',
                        }}
                      >
                        {item.first_val}
                      </Typography>
                      <Typography
                        variant="inherit"
                        component="span"
                        color={'text.disabled'}
                        sx={{
                          fontWeight: '400',
                          fontSize: '12px',
                        }}
                      >
                        {item.Second_val}
                      </Typography>
                    </Box>
                  </Box>
                ) : item.type === 'single' ? (
                  <Box
                    sx={{
                      width: '100%',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      mt: '24px',
                    }}
                  >
                    <Typography
                      variant="inherit"
                      component="span"
                      color={'text.primary'}
                      sx={{
                        fontWeight: '400',
                        fontSize: { xs: '12px', sm: '16px' },
                      }}
                    >
                      {item.title}
                    </Typography>
                    <Box
                      sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'flex-end',
                      }}
                    >
                      <Typography
                        variant="inherit"
                        component="span"
                        color={item.color}
                        sx={{
                          fontWeight: '600',
                          fontSize: '14px',
                        }}
                      >
                        {item.first_val}
                      </Typography>
                    </Box>
                  </Box>
                ) : item.type === 'separator' ? (
                  <Box
                    sx={{
                      width: '100%',
                      height: '2px',
                      // position: 'absolute',
                      // top: '205px',
                      backgroundImage: 'linear-gradient(to right, #C3C3C3 45%, rgba(255,255,255,0) 0%)',
                      backgroundPosition: 'bottom',
                      backgroundSize: '20px 1px',
                      backgroundRepeat: 'repeat-x',
                      mt: paymentMode.includes('check') ? '24px' : '10px',
                    }}
                  ></Box>
                ) : (
                  item.type === 'status' && (
                    <Box
                      sx={{
                        width: '100%',
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        mt: '24px',
                      }}
                    >
                      <Typography
                        variant="inherit"
                        component="span"
                        color={'text.primary'}
                        sx={{
                          fontWeight: '400',
                          fontSize: { xs: '12px', sm: '16px' },
                        }}
                      >
                        {item.title}
                      </Typography>
                      <Chip
                        label={
                          paymentMode.includes('check')
                            ? t('deposit:waiting_user_confirm')
                            : t('deposit:success_transaction')
                        }
                        sx={{
                          minWidth: { xs: '56px', sm: '112px' },
                          height: '24px',
                          borderRadius: { xs: '30px', sm: '11px' },
                          bgcolor: paymentMode.includes('check') ? 'info.light' : 'success.light',
                          '& .MuiChip-label': {
                            // padding: '0',
                            margin: '0',
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center',
                            color: paymentMode.includes('check') ? 'info.main' : 'success.main',
                            fontSize: '12px',
                            // fontWeight: '600',
                            fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                          },
                        }}
                      />
                    </Box>
                  )
                ),
              )}
            </Box>
          )}

          <Box
            sx={{
              width: '100%',
              display: 'flex',
              justifyContent: 'center',
              mt: '64px',
              mb: '24px',
            }}
          >
            {paymentMode.includes('check') && (
              <Button
                variant="contained"
                disableRipple={true}
                sx={{
                  width: { xs: '236px', sm: '320px' },
                  height: { xs: '40px', sm: '48px' },
                  '&:hover': {
                    boxShadow: 'none',
                  },
                  boxShadow: 'none',
                  borderRadius: '12px',
                  fontWeight: '600',
                  fontSize: '14px',
                }}
                onClick={handleConfirmTransfer}
              >
                {t('deposit:final_confirm_send')}
              </Button>
            )}

            {paymentMode.includes('confirm') && (
              <Box
                sx={{
                  width: '100%',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'space-around',
                }}
              >
                <Button
                  variant="contained"
                  // type={'submit'}
                  disableRipple
                  disableElevation
                  sx={{
                    '&:hover': {
                      bgcolor: 'transparent',
                    },
                    '&:active': {
                      transition: 'none',
                      color: 'primary.main',
                      '& .MuiButton-endIcon': {
                        filter:
                          'invert(42%) sepia(49%) saturate(2461%) hue-rotate(195deg) brightness(103%) contrast(99%)',
                      },
                    },
                    padding: '0',
                    bgcolor: 'transparent',
                    color: 'text.secondary',
                    fontWeight: '600',
                    borderRadius: '12px',
                    height: '40px',
                    fontSize: { xs: '10px', sm: '14px' },
                    minWidth: '45px',
                  }}
                  startIcon={
                    <Box
                      component={'img'}
                      src={downloadIcon.src}
                      alt={'bullet-point'}
                      sx={{
                        width: { xs: '16px', sm: '24px' },
                        height: { xs: '16px', sm: '24px' },
                        mx: 0,
                      }}
                    />
                  }
                  onClick={() => exportAsImage(transferReceiptRef.current, 'transfer_receipt')}
                >
                  {t('deposit:save')}
                </Button>

                <Button
                  variant="contained"
                  // type={'submit'}
                  disableRipple
                  disableElevation
                  sx={{
                    '&:hover': {
                      bgcolor: 'transparent',
                    },
                    '&:active': {
                      transition: 'none',
                      color: 'primary.main',
                      '& .MuiButton-endIcon': {
                        filter:
                          'invert(42%) sepia(49%) saturate(2461%) hue-rotate(195deg) brightness(103%) contrast(99%)',
                      },
                    },
                    padding: '0',
                    bgcolor: 'transparent',
                    color: 'text.secondary',
                    fontWeight: '600',
                    borderRadius: '12px',
                    height: '40px',
                    fontSize: { xs: '10px', sm: '14px' },
                    minWidth: '45px',
                  }}
                  startIcon={
                    <Box
                      component={'img'}
                      src={shareIcon.src}
                      alt={'bullet-point'}
                      sx={{
                        width: { xs: '16px', sm: '24px' },
                        height: { xs: '16px', sm: '24px' },
                        mx: 0,
                      }}
                    />
                  }
                  onClick={() => handlePrepareImageForShare(transferReceiptRef.current)}
                >
                  {t('deposit:share')}
                </Button>
              </Box>
            )}
          </Box>
        </Box>
      </Box>

      {openShareWithDB && (
        <Dialog
          fullScreen={true}
          open={openShareWithDB}
          onClose={handleCloseShareWithDB}
          aria-labelledby="responsive-dialog-title"
          sx={{
            backdropFilter: 'blur(4px)',
          }}
          PaperProps={{
            elevation: 0,
            sx: {
              width: { xs: 'auto', sm: 'auto' },
              minWidth: 'auto',
              height: 'auto',
              minHeight: '140px',
              borderRadius: '22px',
              px: '30px',
            },
          }}
        >
          <DialogContent
            sx={{
              padding: 0,
            }}
          >
            <Box
              sx={{
                width: '100%',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                padding: '16px',
              }}
            >
              <Typography
                variant="inherit"
                component="span"
                color={'text.secondary'}
                sx={{
                  fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                  fontSize: { xs: '12px', sm: '16px' },
                }}
              >
                {t('deposit:share_by')}
              </Typography>
              <Box
                sx={{
                  width: '100%',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  gap: '16px',
                  mt: '22px',
                }}
              >
                <WhatsappShareButton
                  url={receiptImage}
                  // quote={'what is this'}
                  // hashtag={'#portfolio...'}
                >
                  <WhatsappIcon size={48} round={true} />
                </WhatsappShareButton>

                <TelegramShareButton
                  url={receiptImage}
                  // quote={'what is this'}
                  // hashtag={'#portfolio...'}
                >
                  <TelegramIcon size={48} round={true} />
                </TelegramShareButton>

                <EmailShareButton
                  url={receiptImage}
                  // quote={'what is this'}
                  // hashtag={'#portfolio...'}
                >
                  <EmailIcon size={48} round={true} />
                </EmailShareButton>
              </Box>
            </Box>
          </DialogContent>
        </Dialog>
      )}
    </>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      hasLayout: true,
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default PaymentReceipt;
