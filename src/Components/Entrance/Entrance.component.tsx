import React, { ReactNode, useEffect, useState, useRef } from 'react';
import styles from './Entrance.module.scss';
import { EntryForm, SwiperSlider } from './component';
import { Box, Divider, Grid, Link, MenuItem, Select, SelectChangeEvent, Typography } from '@mui/material';
import Image from 'next/image';
import { useRouter } from 'next/router';

import waveBg from '/public/assets/images/wave-bg.svg';
import burlyGreen from '/public/assets/images/burlyGreen.png';
import burlyOrangeLight from '/public/assets/images/burlyOrangeLight.png';
import burlyPink from '/public/assets/images/burlyPink.png';
import curlyVector from '/public/assets/images/curlyVector.svg';
import tabletBurlyGreen from '/public/assets/images/tablet-burlyGreen.svg';
import tabletBurlyPink from '/public/assets/images/tablet-burlyPink.svg';
import tabletBurlyOrangeLight from '/public/assets/images/tablet-burlyOrangeLight.svg';
import tablet3in1 from '/public/assets/images/Group 20119.svg';
import tablet3in1Forget from '/public/assets/images/tablet3in1-forget.svg';
import mobile3in1 from '/public/assets/images/mobile3in1.svg';
import mobile3in1Forget from '/public/assets/images/mobile3in1-forget.svg';
import iranFlag from '/public/assets/images/IranCountry.svg';
import desktopBC from '/public/assets/images/desktop-burliesCurve (2).svg';
import desktop3in1Forget from '/public/assets/images/desktop3in1-forget.svg';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { fetchLanguages } from '../../store/slices/languages.slice';
import { NEXT_LOCALE } from 'configs/variables.config';
import { useI18nTrDir } from '../../utils/CustomHooks/i18nTrDir';
import NextLink from 'next/link';
import { localesNS } from '../../utils/Data/locales_NameSpace.utils';

interface Props {
  type: string;
  reverse?: boolean;
  phoneValidity?: ({ phone, password, repeatPassword }: Schema) => void;
}

interface Schema {
  phone?: undefined | string | number;
  password?: undefined | string | number;
  repeatPassword?: undefined | string | number;
}

interface Lang {
  id: number;
  name: string;
  code: string;
  direction: string;
  is_master: number;
  logo: string;
  status: string;
}

const Entrance = ({ type, reverse = false, phoneValidity }: Props) => {
  const { t, pageDirection } = useI18nTrDir();

  const router = useRouter();
  const { pathname, asPath, query } = router;

  const languagesAPI = useAppSelector((state) => state.Languages.lang);
  const dispatch = useAppDispatch();

  const guideLine = useRef<HTMLDivElement>(null);
  const [languages, setLanguages] = useState<Lang[]>([]);
  const [lang, setLang] = React.useState(router.locale);

  useEffect(() => {
    dispatch(fetchLanguages());
    if (getCookieVal(NEXT_LOCALE)) {
      setLang(getCookieVal(NEXT_LOCALE));
    } else {
      setLang(router.locale);
      document.cookie = `${NEXT_LOCALE}=${router.locale}; path=/`;
    }
  }, []);

  // useEffect(() => {
  //     router.push(router.pathname, router.pathname, {locale: lang, scroll: false})
  // }, [lang])

  function getCookieVal(name: string) {
    let cookies: any = {};
    document.cookie.split(';').forEach((cookie: string) => {
      let [key, value] = cookie.split('=');
      cookies[key.trim()] = value;
    });
    return cookies[name];
  }

  useEffect(() => {
    setLanguages(languagesAPI?.language_list);
    // console.log('this lang is from api ', languagesAPI)
    // if (Object.keys(languagesAPI).length !== 0) {
    //     if (getCookieVal(NEXT_LOCALE)) {
    //         setLang(getCookieVal(NEXT_LOCALE))
    //     } else {
    //         setLang(languagesAPI?.default_language?.code)
    //         // document.cookie = `${NEXT_LOCALE}=${languagesAPI?.default_language?.code}`
    //     }
    // }
    // router.push('/', '/', {locale: getCookieVal(NEXT_LOCALE)})
  }, [languagesAPI]);

  const handleChangeLanguage = (event: SelectChangeEvent<string>, child: ReactNode): void => {
    setLang(event.target.value);
    document.cookie = `${NEXT_LOCALE}=${event.target.value}; path=/`;
    // router.push('/', '/', {locale: event.target.value})
    // router.push('/', '/', { locale: event.target.value, scroll: false })
  };

  return (
    <Box
      sx={{
        width: '100%',
        height: { lg: '100vh', xs: 'auto' },
        position: 'relative',
        overflow: 'clip',
        display: 'flex',
        // bgcolor: 'teal',
        flexDirection: { xs: 'column', lg: 'row' },
        justifyContent: 'center',
      }}
    >
      <Box
        component={'img'}
        src={
          type === 'forgetPassword' || type === 'forgetVerify' || type === 'setNewPass'
            ? mobile3in1Forget.src
            : mobile3in1.src
        }
        alt={''}
        sx={{
          width: '100%',
          height: 'auto',
          zIndex: '-2',
          position: 'absolute',
          top: '0',
          display: { xs: 'inline', sm: 'none', lg: 'none' },
        }}
      />

      <Box
        component={'img'}
        src={
          type === 'forgetPassword' || type === 'forgetVerify' || type === 'setNewPass'
            ? tablet3in1Forget.src
            : tablet3in1.src
        }
        alt={''}
        sx={{
          width: '100%',
          height: 'auto',
          zIndex: '-2',
          position: 'absolute',
          top: '0',
          display: { xs: 'none', sm: 'inline', lg: 'none' },
        }}
      />

      {(type === 'forgetPassword' || type === 'forgetVerify' || type === 'setNewPass') && (
        <Box
          component={'img'}
          src={desktop3in1Forget.src}
          alt={''}
          sx={{
            width: '100%',
            height: '100%',
            zIndex: '-2',
            position: 'absolute',
            top: '0',
            objectFit: 'cover',
            display: { xs: 'none', sm: 'none', lg: 'inline' },
          }}
        />
      )}

      {!(type === 'forgetPassword' || type === 'forgetVerify' || type === 'setNewPass') && (
        <Box sx={{ width: '100%', display: { xs: 'none', lg: 'block' } }}>
          <Box
            component={'img'}
            src={curlyVector.src}
            alt={''}
            // className={reverse ? styles.curlyVec_Rev : styles.curlyVec}
            sx={{
              width: 'auto',
              height: '100vh',
              position: 'absolute',
              top: 0,
              right: reverse ? 0 : 'auto',
              left: reverse ? 'auto' : 0,
              transform:
                reverse && pageDirection === 'ltr'
                  ? 'scale(1, 1)'
                  : reverse && pageDirection === 'rtl'
                  ? 'scale(-1,1)'
                  : pageDirection === 'ltr'
                  ? 'scale(-1,1)'
                  : 'scale(1,1)',
            }}
          />
          <Box
            component={'img'}
            src={burlyGreen.src}
            alt={''}
            // className={reverse ? styles.burlyGreen_Rev : styles.burlyGreen}
            sx={{
              width: 'fit-content',
              height: 'fit-content',
              position: 'absolute',
              right: reverse ? '-360px' : 'auto',
              left: reverse ? 'auto' : '-360px',
              top: '-280px',
            }}
          />
          <Box
            component={'img'}
            src={burlyOrangeLight.src}
            alt={''}
            // className={reverse ? styles.burlyOrangeLight_Rev : styles.burlyOrangeLight}
            sx={{
              width: 'fit-content',
              height: 'fit-content',
              position: 'absolute',
              right: reverse ? '-340px' : 'auto',
              left: reverse ? 'auto' : '-340px',
              top: '240px',
            }}
          />
          <Box
            component={'img'}
            src={burlyPink.src}
            alt={''}
            // className={reverse ? styles.burlyPink_Rev : styles.burlyPink}
            sx={{
              width: 'fit-content',
              height: 'fit-content',
              position: 'absolute',
              right: reverse ? '10px' : 'auto',
              left: reverse ? 'auto' : '10px',
              top: '30px',
            }}
          />
        </Box>
      )}

      {!(reverse || type === 'forgetPassword' || type === 'forgetVerify' || type === 'setNewPass') && (
        <Box
          dir={'rtl'}
          component={'img'}
          src={waveBg.src}
          alt={''}
          sx={{
            width: { xs: '330px', sm: '510px', lg: 'fit-content' },
            height: { xs: '330px', sm: '510px', lg: 'fit-content' },
            position: 'fixed',
            top: { xs: '40px', sm: '-140px', lg: '76px' },
            right: { xs: '-67px', lg: '0' },
            transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
            // transform: {xs: 'scale(0.4, 0.4)', sm: 'scale(0.7, 0.7)'},
          }}
        />
      )}

      {/*<img src={desktopBC.src} className={reverse ? styles.desktopBC_Rev : styles.desktopBC} alt=""/>*/}
      <Grid
        container
        id={'contents'}
        // bgcolor={'rgba(256,0,0,0.2)'}
        sx={{ height: '100%', flexDirection: { lg: reverse ? 'row' : 'row-reverse' } }}
        // flexDirection={lg: {reverse ? 'row' : 'row-reverse'}}
        // sx={{
        //     width: '100%',
        //     height: '100%',
        //     display: 'flex',
        //     flexDirection: reverse ? 'row' : 'row-reverse',
        //     alignItems: 'center'
        // }}
      >
        <Grid
          item
          xs={12}
          lg={type === 'forgetPassword' || type === 'forgetVerify' || type === 'setNewPass' ? 12 : 6}
          xl={type === 'forgetPassword' || type === 'forgetVerify' || type === 'setNewPass' ? 12 : 5}
          id={'form-section'}
          sx={{
            height: '100%',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
          }}

          // sx={{width: '47%', height: '100%', position: 'relative'}}
        >
          <EntryForm type={type} reverse={reverse} phoneValidity={phoneValidity} />
          <Box
            ref={guideLine}
            sx={{
              height: '50px',
              display: 'flex',
              alignItems: 'center',
              mt: { xs: '100px', lg: '0' },
              position: { xs: 'static', lg: 'absolute' },
              bottom: { xs: 'auto', lg: '20px' },
              zIndex: '1',
            }}
            // className={styles.guideLine}
          >
            <NextLink href={'https://zimapay.com/faq/'} passHref>
              <Link href="#" underline="none" color={'text.secondary'} sx={{ fontSize: { xs: '12px', lg: '14px' } }}>
                {/*{t('notFound:url_not_found')}*/}
                {t('common:frequently_questions')}
              </Link>
            </NextLink>

            {/*<Typography variant="inherit" component="span" color={'text.secondary'}*/}
            {/*            sx={{cursor: 'pointer'}}>*/}
            {/*    سوالات متداول*/}
            {/*</Typography>*/}
            <Divider orientation="vertical" variant="middle" flexItem sx={{ mx: '8px', mt: '12px', mb: '12px' }} />

            <NextLink href={'https://zimapay.com/services/'} passHref>
              <Link href="#" underline="none" color={'text.secondary'} sx={{ fontSize: { xs: '12px', lg: '14px' } }}>
                {t('common:services')}
              </Link>
            </NextLink>

            {/*<Typography variant="inherit" component="span" color={'text.secondary'}*/}
            {/*            sx={{cursor: 'pointer'}}>*/}
            {/*    خدمات*/}
            {/*</Typography>*/}
            <Divider orientation="vertical" variant="middle" flexItem sx={{ mx: '8px', mt: '12px', mb: '12px' }} />

            <NextLink href={'#'} passHref>
              <Link href="#" underline="none" color={'text.secondary'} sx={{ fontSize: { xs: '12px', lg: '14px' } }}>
                {t('common:support')}
              </Link>
            </NextLink>

            {/*<Typography variant="inherit" component="span" color={'text.secondary'}*/}
            {/*            sx={{cursor: 'pointer'}}>*/}
            {/*    پشتیبانی*/}
            {/*</Typography>*/}
            <Divider orientation="vertical" variant="middle" flexItem sx={{ mx: '8px', mt: '12px', mb: '12px' }} />
            <Select
              value={lang}
              onChange={handleChangeLanguage}
              displayEmpty
              MenuProps={{
                // elevation: 0,
                anchorOrigin: {
                  vertical: 'top',
                  horizontal: 'left',
                },
                transformOrigin: {
                  vertical: 'bottom',
                  horizontal: 'left',
                },
                PaperProps: {
                  sx: {
                    mt: '8px',
                    // width: '150px',
                    boxShadow: '0px 4px 38px rgba(68,68,68,0.1)',
                    borderRadius: '10px',
                  },
                },
              }}
              sx={{
                fontSize: { xs: '12px', lg: '14px' },
                '& .MuiSvgIcon-root': { transform: 'rotate(180deg)' },
                '& .muirtl-bpeome-MuiSvgIcon-root-MuiSelect-icon': { transform: 'rotate(0deg)' },
                '& fieldset': { border: 'none' },
                '& .MuiSelect-select': { px: '0' },
              }}
            >
              {languages?.map((language) => {
                return (
                  <MenuItem value={language.code} key={language.id} sx={{ px: '10px' }}>
                    <NextLink href={{ pathname, query }} as={asPath} locale={language.code} passHref>
                      <Link href="#" underline="none">
                        <Box
                          sx={{
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'space-between',
                          }}
                        >
                          <Box
                            component={'img'}
                            src={language.logo}
                            sx={{
                              width: '22px',
                              height: '22px',
                              borderRadius: '50%',
                              border: '1px solid',
                              borderColor: 'text.disabled',
                              mr: '10px',
                            }}
                            alt=""
                          />
                          <Typography variant="inherit" component="span" color={'text.secondary'}>
                            {language.name}
                          </Typography>
                        </Box>
                      </Link>
                    </NextLink>
                  </MenuItem>
                );
              })}
            </Select>
          </Box>
        </Grid>
        <Grid
          item
          xs={0}
          lg={type === 'forgetPassword' || type === 'forgetVerify' || type === 'setNewPass' ? 0 : 6}
          xl={type === 'forgetPassword' || type === 'forgetVerify' || type === 'setNewPass' ? 12 : 7}
          id={'slider-section'}
          sx={{
            // bgcolor: 'yellow',
            display: {
              xs: 'none',
              lg: type === 'forgetPassword' || type === 'forgetVerify' || type === 'setNewPass' ? 'none' : 'flex',
            },
          }}
          flexDirection={'column'}
          alignItems={'center'}
          justifyContent={'center'}
          // sx={{height: '100%', width: '53%'}}
        >
          <Typography
            variant="inherit"
            component="span"
            color={'black'}
            fontFamily={'IRANSansX-DemiBold'}
            sx={{
              fontSize: '34px',
              fontWeight: '400',
            }}
          >
            {t('common:without_boundary')}
          </Typography>
          {/*<span*/}
          {/*    className={reverse ? styles.unlimitedText_Rev : styles.unlimitedText}*/}
          {/*>*/}
          {/*    {t('common:without_boundary')}*/}
          {/*</span>*/}
          <SwiperSlider reverse={reverse} />
        </Grid>
      </Grid>
    </Box>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export { Entrance };
