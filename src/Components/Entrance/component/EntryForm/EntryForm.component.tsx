import React from 'react';
import { Box, FormControl, Grid } from '@mui/material';
import { SwiperSlider } from '../index';
import styles from './EntryForm.module.scss';
import zimaPayLogo from '/public/assets/images/Zimapaylogo.svg';

import { InputField } from 'Components';
import curlyVector from '../../../../assets/images/curlyVector.svg';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useWindowSize } from '../../../../utils/CustomHooks/windowSize';
import { localesNS } from '../../../../utils/Data/locales_NameSpace.utils';

interface Props {
  type: string;
  phoneNumber?: number | string;
  reverse?: boolean;
  phoneValidity?: ({ phone, password, repeatPassword }: Schema) => void;
}

interface Schema {
  phone?: undefined | string | number;
  password?: undefined | string | number;
  repeatPassword?: undefined | string | number;
}

const EntryForm = ({ type, reverse = false, phoneNumber = '09123456789', phoneValidity }: Props) => {
  const { windowSizes } = useWindowSize();

  return (
    <FormControl
      className={styles.main}
      variant="outlined"
      sx={{
        width: { xs: '100%', lg: '448px' },
        backgroundColor: { xs: 'transparent', lg: '#ffffff' },
        boxShadow: { xs: 'none', lg: '0 16px 165px rgba(112, 144, 176, 0.2)' },
        padding:
          type === 'forgetVerify' || type === 'forgetPassword' || type === 'setNewPass'
            ? {
                xs: '130px 0 30px 0',
                sm: '250px 0 30px 0',
                lg: '65px 0 30px 0',
              }
            : reverse
            ? windowSizes.height < 900
              ? '40px 0 30px 0'
              : '100px 0 80px 0'
            : '65px 0 30px 0',
      }}
    >
      <Box component={'img'} src={zimaPayLogo.src} alt={'zima-pay-logo'} />

      <InputField type={type} phoneNumber={phoneNumber} phoneValidity={phoneValidity} />
    </FormControl>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export { EntryForm };
