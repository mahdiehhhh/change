import React from 'react';
import { Box } from '@mui/material';

import { Swiper, SwiperSlide } from 'swiper/react';
import { Pagination, Navigation, Autoplay, EffectFlip } from 'swiper';
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/navigation';
// import "./_SwiperSliderStyle.scss";

import SwiperCore, { EffectFade } from 'swiper';

import zimaCard from '/public/assets/images/zima-card.svg';
import transfer from '/public/assets/images/wallet-transfer.svg';
import deposit from '/public/assets/images/Deposit&withdrawal.svg';
import cardFeatures from '/public/assets/images/card-features.svg';
import iphoneMockup from '/public/assets/images/iPhone-mockup.png';
import laptopMockup from '/public/assets/images/laptop-mockup.png';
import cardGroup from '/public/assets/images/tablet-cardGroup.svg';
import cardGroupRev from '/public/assets/images/cardGroup-rev.svg';
import iphoneMockupEn from '/public/assets/images/mobile_mockup_en.svg';
import laptopMockupEn from '/public/assets/images/laptop_mockup_en.svg';
import cardGroupEn from '/public/assets/images/cardGroup_en.svg';
import cardGroup2 from '/public/assets/images/cardGroup2.svg';
import { useTranslation } from 'next-i18next';
import { useI18nTrDir } from '../../../../utils/CustomHooks/i18nTrDir';

interface Props {
  reverse?: boolean;
}

// SwiperCore.use([EffectFade]);

const SwiperSlider = ({ reverse = false }: Props) => {
  const { t, pageDirection } = useI18nTrDir();

  const mockups = [
    // [
    //     {image: zimaCard, name: 'zimaCard'},
    //     {image: transfer, name: 'transfer'},
    //     {image: deposit, name: 'deposit'},
    //     {image: cardFeatures, name: 'cardFeatures'}
    // ],
    { image: pageDirection === 'rtl' ? iphoneMockup : iphoneMockupEn, name: 'iphoneMockup' },
    { image: pageDirection === 'rtl' ? laptopMockup : laptopMockupEn, name: 'laptopMockup' },
    { image: [pageDirection === 'rtl' ? cardGroup2 : cardGroupEn, cardGroupRev], name: 'cardGroup' },
  ];

  return (
    <Box className={'headerSlider'}>
      <Box sx={{ width: '100%', height: { xs: '200px', sm: '350px', lg: '600px' }, mt: { xs: '25px' } }}>
        <Swiper
          dir={reverse ? 'ltr' : 'rtl'}
          spaceBetween={30}
          pagination={{
            clickable: true,
          }}
          loop={true}
          autoplay={{
            delay: 5000,
            disableOnInteraction: false,
          }}
          modules={[Pagination, Autoplay]}
          className="mySwiper"

          // fadeEffect={{
          //     crossFade: true,
          // }}
          // effect={"fade"}
        >
          {mockups.map((item: any) => (
            <SwiperSlide key={item.name}>
              <Box
                component={'img'}
                src={item.name === 'cardGroup' ? (reverse ? item.image[1].src : item.image[0].src) : item.image.src}
                className={reverse ? `${item.name}_Rev` : `${item.name}`}
                alt=""
                sx={{
                  // backgroundColor:'green',
                  maxWidth: { xs: '300px', sm: '450px', lg: 'fit-content' },
                  maxHeight: { xs: '300px', sm: '450px', lg: 'fit-content' },
                  position: { xs: 'static', lg: 'relative' },
                  // display: {xs: 'flex', lg: 'block'},
                }}
              />
            </SwiperSlide>
          ))}
        </Swiper>
      </Box>
    </Box>
  );
};

export { SwiperSlider };
