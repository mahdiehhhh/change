import React, { useEffect, useState, ChangeEvent, ReactNode } from 'react';
import {
  Box,
  ButtonGroup,
  Button,
  InputAdornment,
  IconButton,
  Select,
  MenuItem,
  TextField,
  InputLabel,
  Tabs,
  Tab,
  Chip,
  FormHelperText,
  FormControl,
} from '@mui/material';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useRouter } from 'next/router';
import { PATHS } from '../../configs/routes.config';
import starIcon from '/public/assets/images/Star-icon.svg';
import filterIcon from '/public/assets/images/notif-filter-icon.svg';
import searchIconActive from '/public/assets/images/Search-icon-active.svg';
import coloredStarIcon from '/public/assets/images/colored-star-icon.svg';
import coloredPlusIcon from '/public/assets/images/colored_plus_icon.svg';
import Typography from '@mui/material/Typography';
import { SearchBar } from '../SearchBar/SearchBar.component';
import FlexibleSearchBarComponent from '../FlexibleSearchBar/FlexibleSearchBar.component';
import FlexibleSearchBar from '../FlexibleSearchBar/FlexibleSearchBar.component';
import { dispatch } from 'jest-circus/build/state';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { fetchExchangeCurrenciesInfo } from '../../store/slices/currencies_exchange_infos.slice';
import { CurrencyTable } from '../CurrencyTable/CurrencyTable.component';
import { Trans } from 'next-i18next';
import { useI18nTrDir } from '../../utils/CustomHooks/i18nTrDir';
import { localesNS } from '../../utils/Data/locales_NameSpace.utils';
import headphoneIcon from '/public/assets/images/headphone-icon.svg';
import { TextInfoTable } from '../TextInfoTable/TextInfoTable.component';
import { fetchUserTickets } from '../../store/slices/user_tickets.slice';
import noTransactionSmileIcon from '/public/assets/images/no-transaction-smile.svg';
import AlertDialogBox from '../AlertDialogBox/AlertDialogBox.component';

const Ticket = () => {
  const router = useRouter();
  const UrlPath = router.pathname;
  const { t, pageDirection } = useI18nTrDir();

  let titleFilteredArray: any = [];
  let isoCodeFilteredArray: any = [];
  let AllFilteredResultsArray: any = [];

  const dispatch = useAppDispatch();
  const userTicketsResult = useAppSelector((state) => state.UserTickets);

  const [favoriteBtnHover, setFavoriteBtnHover] = useState(false);
  const [filterBtnHover, setFilterBtnHover] = useState(false);
  const [searchedResults, setSearchedResults] = useState<[]>([]);
  const [searchedText, setSearchedText] = useState('');
  const [ticketMode, setTicketMode] = useState('open_ticket');

  const [openCallUsDialogBox, setOpenCallUsDialogBox] = React.useState(false);
  const [operationResultState, setOperationResultState] = useState<object>({
    status: '',
    messages: {},
  });

  const handleOpenCallUsDB = () => {
    setOpenCallUsDialogBox(true);
  };

  const handleCloseCallUsDB = () => {
    setOpenCallUsDialogBox(false);
  };

  const TicketTabs = [
    {
      id: 1,
      key: 'open_ticket',
      title: t('ticket:open_tickets'),
      link: `${PATHS.USER_TICKET}`,
    },
    {
      id: 2,
      key: 'archived_ticket',
      title: t('ticket:archived_tickets'),
      link: `${PATHS.USER_TICKET}/${PATHS.ARCHIVED}`,
    },
  ];

  const [activeTicketTab, setActiveTicketTab] = useState(TicketTabs[0]);

  useEffect(() => {
    dispatch(fetchUserTickets());
  }, []);

  useEffect(() => {
    const selectedTicketTab = TicketTabs.find((tab) => `/${tab.link}` === UrlPath);
    // @ts-ignore
    setActiveTicketTab(selectedTicketTab);
    // @ts-ignore
    setTicketMode(selectedTicketTab?.key);
  }, [UrlPath]);

  const handleSearchForResult = (event: any) => {
    setSearchedText(event.target.value);
    if (!!userTicketsResult.userTicketsResp) {
      titleFilteredArray = (
        ticketMode === 'open_ticket'
          ? userTicketsResult.userTicketsResp?.open_tickets_list
          : userTicketsResult.userTicketsResp?.archived_tickets_list
      )?.filter((item: any) => {
        return item.title.includes(event.target.value);
      });
    }
    // const uniqueFilteredResults = [...new Set(AllFilteredResultsArray)]
    // @ts-ignore
    setSearchedResults(titleFilteredArray);
  };

  return (
    <>
      <Box
        sx={{
          width: '100%',
          height: 'auto',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'flex-start',
          justifyContent: 'flex-start',
        }}
      >
        <Box
          sx={{
            width: '100%',
            display: 'flex',
            alingItems: 'center',
          }}
        >
          <Button
            variant="contained"
            // type={'submit'}
            disableRipple
            sx={{
              '&:hover': {
                boxShadow: 'none',
              },
              width: { xs: '45px', sm: '169px' },
              bgcolor: 'primary.light',
              color: 'text.primary',
              boxShadow: 'none',
              fontWeight: '600',
              borderRadius: '12px',
              height: '40px',
              fontSize: '14px',
              minWidth: '45px',
              padding: { xs: '10px 12px' },
              '& .MuiButton-startIcon': {
                mx: '0',
                mr: { xs: '0', sm: '9px' },
              },
            }}
            startIcon={
              <Box
                component={'img'}
                src={coloredPlusIcon.src}
                alt={'bullet-point'}
                sx={{
                  width: '20px',
                  height: '20px',
                  mx: 0,
                }}
              />
            }
            onClick={() => router.push(`/${PATHS.USER_TICKET}/${PATHS.NEW}`)}
          >
            <Typography
              variant="inherit"
              component="span"
              color={'text.primary'}
              sx={{
                display: { xs: 'none', sm: 'block' },
                fontWeight: '600',
                fontSize: '14px',
              }}
            >
              {t('ticket:record_new_ticket')}
            </Typography>
          </Button>
          <Box
            sx={{
              flex: 1,
              display: { xs: 'flex', sm: 'none' },
              alignItems: 'flex-start',
              justifyContent: 'flex-end',
              ml: '16px',
            }}
          >
            <FlexibleSearchBar handleSearchForResult={handleSearchForResult} />
          </Box>
        </Box>

        <Box
          sx={{
            width: '100%',
            display: 'grid',
            gridAutoColumns: 'row',
            gridTemplateColumns: { xs: '0.2fr 2.6fr 0.2fr', sm: '0.6fr 1.8fr 0.6fr' },
            mt: { xs: '28px', sm: '40px' },
          }}
        >
          <Box>
            <IconButton
              // color="primary"
              aria-label="upload picture"
              component="label"
              disableRipple={true}
              sx={{
                width: { xs: '32px', sm: '48px' },
                height: { xs: '32px', sm: '40px' },
                borderRadius: { xs: '8px', sm: '11px' },
                display: 'flex',
                justifySelf: 'flex-end',
                padding: '0',
                backgroundColor: 'white',
              }}
              onClick={handleOpenCallUsDB}
            >
              <Box
                component={'img'}
                src={headphoneIcon.src}
                alt={'backward icon'}
                sx={{
                  width: { xs: '16px', sm: '24px' },
                  height: { xs: '16px', sm: '24px' },
                }}
              />
            </IconButton>
          </Box>

          <Box
            sx={{
              height: 'auto',
              display: 'flex',
              justifyContent: 'center',
            }}
          >
            <Tabs
              value={+activeTicketTab?.id - 1}
              // onChange={handleChangeTabs}
              aria-label="nav tabs example"
              TabIndicatorProps={{ style: { display: 'none' } }}
            >
              {TicketTabs.map((tab) => {
                return (
                  <Tab
                    key={tab.id}
                    label={tab.title}
                    disableRipple={true}
                    sx={{
                      width: 'auto',
                      px: { xs: '35px', lg: '50px' },
                      position: 'relative',
                      fontSize: { xs: '14px', sm: '16px' },
                      fontWeight: +activeTicketTab?.id === +tab.id ? '600' : '400',
                      borderRadius: '12px',
                      pb: { xs: '20px', sm: '28px' },
                      pt: '18px',
                      // bgcolor: +activeTicketTab?.id === +tab.id ? 'white' : 'transparent',
                      bgcolor: 'transparent',
                      zIndex: 0,
                      '&::before': {
                        content: '" "',
                        display: 'block',
                        background: +activeTicketTab?.id === +tab.id ? 'white' : 'transparent',
                        width: 'calc(100% - 40%)',
                        height: '80px',
                        position: 'absolute',
                        top: 0,
                        right: pageDirection === 'rtl' ? '10px' : 'auto',
                        left: pageDirection === 'rtl' ? 'auto' : '10px',
                        transform: 'skew(-17deg)',
                        borderRadius: { xs: '15px', lg: '20px' },
                        zIndex: -1,
                      },
                      '&::after': {
                        content: '" "',
                        display: 'block',
                        background: +activeTicketTab?.id === +tab.id ? 'white' : 'transparent',
                        width: 'calc(100% - 40%)',
                        height: '80px',
                        position: 'absolute',
                        top: 0,
                        left: pageDirection === 'rtl' ? '10px' : 'auto',
                        right: pageDirection === 'rtl' ? 'auto' : '10px',
                        transform: 'skew(17deg)',
                        borderRadius: { xs: '15px', lg: '20px' },
                        zIndex: -1,
                      },
                    }}
                    component="a"
                    onClick={(event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
                      event.preventDefault();
                      router.push(`/${tab.link}`);
                    }}
                    // href={tab.link}
                  />
                );
              })}
            </Tabs>
          </Box>

          <Box
            sx={{
              height: 'auto',
              display: 'flex',
              alignItems: 'flex-start',
              justifyContent: 'flex-end',
            }}
          >
            <Box
              sx={{
                display: { xs: 'none', sm: 'block' },
              }}
            >
              <FlexibleSearchBar handleSearchForResult={handleSearchForResult} />
            </Box>

            <Button
              variant="contained"
              // type={'submit'}
              disableRipple
              sx={{
                '&:hover': {
                  boxShadow: 'none',
                  bgcolor: 'white',
                },
                bgcolor: 'white',
                boxShadow: 'none',
                borderRadius: { xs: '8px', sm: '11px' },
                width: { xs: '32px', sm: '48px' },
                height: { xs: '32px', sm: '40px' },
                minWidth: { xs: '32px', sm: '45px' },
                ml: '12px',
                padding: { xs: '0px', sm: '10px 12px' },
                '& .MuiButton-startIcon': {
                  mx: '0',
                  // mr: {xs: '0', sm: '9px'}
                },
              }}
              startIcon={
                <Box
                  component={'img'}
                  src={filterIcon.src}
                  alt={'star icon'}
                  sx={{
                    width: { xs: '16px', sm: '24px' },
                    height: { xs: '16px', sm: '24px' },
                    mx: 0,
                  }}
                />
              }
              // onClick={() => router.push(`/${PATHS.USER_BANK_ACCOUNT}/${PATHS.IRANIAN}`)}
              // onMouseEnter={() => setFavoriteBtnHover(true)}
              // onMouseLeave={() => setFavoriteBtnHover(false)}
            ></Button>
          </Box>
        </Box>

        <Box
          sx={{
            width: '100%',
            height: 'auto',
            padding: '12px 24px',
            bgcolor: 'white',
            mt: '-10px',
            zIndex: '2',
            borderRadius: '24px',
          }}
        >
          {(ticketMode === 'open_ticket' &&
            !userTicketsResult.loading &&
            !userTicketsResult.userTicketsResp?.open_tickets_list.length) ||
          (ticketMode === 'archived_ticket' &&
            !userTicketsResult.loading &&
            !userTicketsResult.userTicketsResp?.archived_tickets_list.length) ? (
            <Box
              sx={{
                width: '100%',
                height: 'auto',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                my: { xs: '100px', sm: '160px', lg: '30px' },
              }}
            >
              <Box
                component={'img'}
                src={noTransactionSmileIcon.src}
                alt={'spotify icon'}
                sx={{
                  width: { xs: '149px', sm: '208px' },
                  height: { xs: '153px', sm: '214px' },
                  mt: '20px',
                  mb: '16px',
                }}
              />
              <Typography
                variant="inherit"
                component="span"
                color={'text.primary'}
                sx={{
                  fontWeight: '400',
                  fontSize: { xs: '12px', sm: '20px' },
                  textAlign: 'center',
                }}
              >
                {t('ticket:no_tickets_recorded')}
              </Typography>
              <Typography
                variant="inherit"
                component="span"
                color={'text.secondary'}
                sx={{
                  fontWeight: '400',
                  fontSize: { xs: '10px', sm: '16px' },
                  textAlign: 'center',
                  mt: '8px',
                  mb: '16px',
                }}
              >
                {t('my_wallet:register_your_new_record')}
              </Typography>
              <Button
                variant="contained"
                disableRipple={true}
                sx={{
                  width: '236px',
                  height: '40px',
                  '&:hover': {
                    boxShadow: 'none',
                  },
                  boxShadow: 'none',
                  borderRadius: '12px',
                  fontWeight: '600',
                  fontSize: '14px',
                  mt: '25px',
                }}
              >
                {t('ticket:record_new_ticket')}
              </Button>
            </Box>
          ) : (
            <TextInfoTable mode={ticketMode} AllFilteredResultsArray={searchedResults} searchedText={searchedText} />
          )}
        </Box>
      </Box>

      {openCallUsDialogBox && (
        <AlertDialogBox
          mode={'call_us'}
          data={operationResultState}
          open={openCallUsDialogBox}
          close={handleCloseCallUsDB}
        />
      )}
    </>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      hasLayout: true,
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default Ticket;
