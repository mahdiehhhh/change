import React, { useEffect, useState, ChangeEvent, ReactNode, useCallback, useRef } from 'react';
import {
  Box,
  ButtonGroup,
  Button,
  InputAdornment,
  IconButton,
  Select,
  MenuItem,
  TextField,
  InputLabel,
  Tabs,
  Tab,
  Chip,
  FormHelperText,
  FormControl,
  Link,
} from '@mui/material';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useRouter } from 'next/router';
import { PATHS } from '../../configs/routes.config';
import starIcon from '/public/assets/images/Star-icon.svg';
import filterIcon from '/public/assets/images/notif-filter-icon.svg';
import searchIconActive from '/public/assets/images/Search-icon-active.svg';
import coloredStarIcon from '/public/assets/images/colored-star-icon.svg';
import coloredPlusIcon from '/public/assets/images/colored_plus_icon.svg';
import Typography from '@mui/material/Typography';
import { SearchBar } from '../SearchBar/SearchBar.component';
import FlexibleSearchBarComponent from '../FlexibleSearchBar/FlexibleSearchBar.component';
import FlexibleSearchBar from '../FlexibleSearchBar/FlexibleSearchBar.component';
import { dispatch } from 'jest-circus/build/state';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { fetchExchangeCurrenciesInfo } from '../../store/slices/currencies_exchange_infos.slice';
import { CurrencyTable } from '../CurrencyTable/CurrencyTable.component';
import { Trans } from 'next-i18next';
import { useI18nTrDir } from '../../utils/CustomHooks/i18nTrDir';
import { localesNS } from '../../utils/Data/locales_NameSpace.utils';
import headphoneIcon from '/public/assets/images/headphone-icon.svg';
import { TextInfoTable } from '../TextInfoTable/TextInfoTable.component';
import { fetchUserTickets } from '../../store/slices/user_tickets.slice';
import noTransactionSmileIcon from '/public/assets/images/no-transaction-smile.svg';
import AlertDialogBox from '../AlertDialogBox/AlertDialogBox.component';
import backwardArrowIcon from '/public/assets/images/backwardArrowDeposit.svg';
import bulletPoint from '/public/assets/images/BulletPoint.svg';
import boldChar from '/public/assets/images/Bold-char.svg';
import italicChar from '/public/assets/images/Italic-char.svg';
import attachmentGrayIcon from '/public/assets/images/attachment-gray-icon.svg';
import garbageGrayIcon from '/public/assets/images/garbage-gray-icon.svg';
import garbageWhiteIcon from '/public/assets/images/grabage-white-icon.svg';
import { fetchGetTicketsMessages } from '../../store/slices/get_tickets_messages.slice';
import Avatar from '@mui/material/Avatar';
import { calcCurrencyConversionCosts } from '../../utils/functions/calc_currency_conversion';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import TransferNewDestination from '../TransferNewDestination/TransferNewDestination.component';
import documentGrayIcon from '/public/assets/images/Document-gray-icon.svg';
import { fetchSendTicketsMessages } from '../../store/slices/send_tickets_messages.slice';
import jmoment from 'jalali-moment';
import NextLink from 'next/link';

interface TicketsMessageSample {
  sender_type: string;
  sender_name: string;
  sender_avatar: string;
  content: string;
  attachment: any;
  attachment_type: string;
  created_at: string;
}

const TicketChat = () => {
  const router = useRouter();
  const UrlPath = router.pathname;
  const { t, pageDirection } = useI18nTrDir();
  const ticket_id: any = router.query.ticket_id;
  const messagesList = useRef();

  const dispatch = useAppDispatch();
  const getTicketsMsgResult = useAppSelector((state) => state.GetTicketsMessages);
  const userProfileResult = useAppSelector((state) => state.UserProfile);

  const [openTicketAttachDB, setOpenTicketAttachDB] = React.useState(false);
  const [contentEdState, setContentEdState] = React.useState(false);

  const [ticketDescription, setTicketDescription] = useState('');
  const [edButtonsActivationState, setEdButtonsActivationState] = useState<any>({});
  const [ticketAttachmentFile, setTicketAttachmentFile] = useState<File>();
  const [ticketAttachmentPreview, setTicketAttachmentPreview] = useState<string>();
  const [ticketsMsgState, setTicketsMsgState] = useState<TicketsMessageSample[]>([]);

  const handleOpenTicketAttachDB = () => {
    setOpenTicketAttachDB(true);
  };

  const handleCloseTicketAttachDB = () => {
    setOpenTicketAttachDB(false);
  };

  useEffect(() => {
    dispatch(fetchGetTicketsMessages(ticket_id));
    handleCreateEditorsButtonsState();
  }, []);

  // useEffect(() => {
  //     if(!openTicketAttachDB) {
  //         setTicketAttachmentFile(undefined)
  //     }
  // }, [openTicketAttachDB])

  const editorButtons = [
    {
      id: 1,
      type: 'style',
      action: 'bold',
      icon: boldChar,
    },
    {
      id: 2,
      type: 'style',
      action: 'italic',
      icon: italicChar,
    },
    {
      id: 3,
      type: 'button',
      action: '',
      icon: attachmentGrayIcon,
    },
  ];

  const handleCreateEditorsButtonsState = () => {
    const editorsButtonsStateObj = editorButtons.reduce((accumulator, value) => {
      let styleButtonsActivation = { ...accumulator };
      if (value.type === 'style') {
        styleButtonsActivation = { ...styleButtonsActivation, [value.action]: false };
      }
      return styleButtonsActivation;
    }, {});
    setEdButtonsActivationState(editorsButtonsStateObj);
  };

  const handleAddTicketText = (event: any, origin: string) => {
    if (origin === 'content-editable') {
      if (event.target.innerText) {
        setTicketDescription(event.target.innerText);
      }
    } else {
      setTicketDescription(event.target.value);
    }
  };
  useEffect(() => {
    handleCheckEmptyBox();
  }, [ticketDescription]);

  useEffect(() => {
    setTicketsMsgState(getTicketsMsgResult.getTicketsMsgResp?.ticket_messages);
  }, [getTicketsMsgResult]);

  useEffect(() => {
    scrollChatDown(750);
  }, [ticketsMsgState]);

  const handleSetStyleTicketDescription = (action: string) => {
    setEdButtonsActivationState((prevState: any) => ({ ...prevState, [action]: !edButtonsActivationState[action] }));
    document.execCommand(action);
  };

  const handleCheckStyles = () => {
    // @ts-ignore
    const editorsButtonsActiveStatusObj = editorButtons.reduce((accumulator, value) => {
      let styleButtonsActivation = { ...accumulator };
      if (value.type === 'style') {
        styleButtonsActivation = {
          ...styleButtonsActivation,
          [value.action]: document.queryCommandState(value.action),
        };
      }
      return styleButtonsActivation;
    }, {});
    setEdButtonsActivationState(editorsButtonsActiveStatusObj);
  };

  useEffect(() => {
    // if (!ticketAttachmentFile) {
    //     setTicketAttachmentPreview(undefined)
    //     return
    // }

    const objectUrl = ticketAttachmentFile && URL.createObjectURL(ticketAttachmentFile);
    setTicketAttachmentPreview(objectUrl);
    // ticketAttachmentFile && handleOpenTicketAttachDB()

    // free memory when ever this component is unmounted
    return () => URL.revokeObjectURL(objectUrl as string);
  }, [ticketAttachmentFile]);

  const handleGetImageFile = (e: ChangeEvent<HTMLInputElement>) => {
    // @ts-ignore
    const selectedFile = e.target.files[0];
    if (selectedFile) {
      const SUPPORTED_FORMATS = ['image/jpg', 'image/jpeg', 'image/png', 'application/pdf'];
      if (!SUPPORTED_FORMATS.includes(selectedFile.type)) {
        // setInvalidFileType(true)
      } else {
        // setInvalidFileType(false)
      }
      handleOpenTicketAttachDB();
      setTicketAttachmentFile(selectedFile);
      // @ts-ignore
      // formik.handleChange('document_image')(selectedFile.name)
    }
  };

  const scrollChatDown = (timeOut: number) => {
    setTimeout(() => {
      // @ts-ignore
      messagesList.current.scrollTop = messagesList.current.scrollHeight;
    }, timeOut);
  };

  const formatBytes = (bytes: number = 0, decimals = 2) => {
    if (!+bytes) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = [t('common:B'), t('common:KB'), t('common:MB'), t('common:GB'), t('common:TB')];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return `${parseFloat((bytes / Math.pow(k, i)).toFixed(dm))} ${sizes[i]}`;
  };

  const handleSendTicketMessage = () => {
    if (ticketDescription || ticketAttachmentFile) {
      let data = {
        message: ticketDescription,
        attachment: ticketAttachmentFile,
      };
      dispatch(fetchSendTicketsMessages(ticket_id, data));

      const todayTimeDate = jmoment(new Date(), 'YYYY/MM/DD HH:mm').locale('fa').format('YYYY/MM/DD HH:mm');
      const sender_data = {
        sender_type: 'users',
        sender_name: userProfileResult?.userProfileResp?.name,
        sender_avatar: userProfileResult?.userProfileResp?.avatar,
        content: ticketDescription,
        attachment: ticketAttachmentPreview,
        attachment_type: 'image/jpeg',
        created_at: todayTimeDate,
      };
      let ticketsMsgStateCopy = [...ticketsMsgState];
      ticketsMsgStateCopy.push(sender_data);
      setTicketsMsgState(ticketsMsgStateCopy);
      scrollChatDown(100);
      handleCloseTicketAttachDB();
      setTicketDescription('');
      setTicketAttachmentFile(undefined);
    }
    scrollChatDown(100);
  };

  const handleCheckEmptyBox = (event?: any) => {
    if (event?.target?.innerText || ticketDescription) {
      setContentEdState(true);
    } else {
      setContentEdState(false);
    }
  };

  const handleDeleteTicketAttachment = () => {
    setTicketAttachmentPreview(undefined);
    setTicketAttachmentFile(undefined);
    handleCloseTicketAttachDB();
  };

  const modifyLinks = (content: any) => {
    const expression = /(https?:\/\/)?[\w\-~]+(\.[\w\-~]+)+(\/[\w\-~@:%]*)*(#[\w\-]*)?(\?[^\s]*)?/gi;
    return expression.exec(content);
  };

  return (
    <>
      <Box
        sx={{
          width: '100%',
          height: 'auto',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'flex-start',
          justifyContent: 'flex-start',
          bgcolor: 'white',
          borderRadius: '22px',
          // padding: '19px'
        }}
      >
        <Box
          sx={{
            width: '100%',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
            padding: '19px 19px 0 19px',
            mb: '19px',
          }}
        >
          <Box
            sx={{
              width: 'auto',
              display: 'flex',
              alignItems: 'center',
            }}
          >
            <Box
              component={'img'}
              src={bulletPoint.src}
              alt={'bullet-point'}
              sx={{
                width: '22px',
                height: '18px',
                mr: '9px',
                transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
              }}
            />
            <Typography
              variant="inherit"
              component="span"
              color={'text.primary'}
              sx={{
                fontWeight: { xs: '600' },
                fontSize: { xs: '16px', xl: '16px' },
              }}
            >
              {t('ticket:messages')}
            </Typography>
          </Box>

          <IconButton
            disableTouchRipple
            color="inherit"
            aria-label="prev button"
            edge="start"
            // onClick={() => router.push(`/${PATHS.USER_DASHBOARD}`)}
            sx={{
              width: '28px',
              height: '28px',
              display: 'flex',
              justifySelf: 'flex-end',
              padding: '0',
              backgroundColor: '#D9D9D9',
            }}
            onClick={() => router.back()}
          >
            <Box
              component={'img'}
              src={backwardArrowIcon.src}
              alt={'backward icon'}
              sx={{
                width: '7px',
                height: '14px',
                transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
              }}
            />
          </IconButton>
        </Box>

        <Box
          ref={messagesList}
          sx={{
            scrollBehavior: 'smooth',
            width: '100%',
            height: '575px',
            overflowY: 'auto',
            display: 'flex',
            flexDirection: 'column',
            // mt: '48px',
            px: '19px',
          }}
        >
          {ticketsMsgState?.map((message: any, index: number) => (
            <Box
              key={index}
              sx={{
                width: '100%',
                display: 'flex',
                flexDirection: 'column',
                alignItems: message.sender_type === 'users' ? 'flex-start' : 'flex-end',
                mb: '24px',
              }}
            >
              {message.sender_type === 'admins' && (
                <Box
                  sx={{
                    display: 'flex',
                    alignItems: 'center',
                    mb: '16px',
                  }}
                >
                  <Typography
                    variant="inherit"
                    component="span"
                    color={'text.primary'}
                    sx={{
                      fontSize: { xs: '14px', sm: '17px' },
                      fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                      mt: '6px',
                    }}
                  >
                    {message?.sender_name}
                  </Typography>
                  <Avatar
                    alt={message?.sender_name}
                    src={message?.sender_avatar}
                    sx={{
                      width: { xs: '32px', sm: '48px' },
                      height: { xs: '32px', sm: '48px' },
                      ml: '8px',
                    }}
                  />
                </Box>
              )}

              <Box
                sx={{
                  width: 'fit-content',
                  maxWidth: '55%',
                  borderRadius: message.sender_type === 'users' ? '15px 15px 15px 0px' : '15px 15px 0px 15px',
                  bgcolor: '#F6F7FA',
                  overflow: 'hidden',
                }}
              >
                <Box
                  sx={{
                    width: '100%',
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'flex-start',
                  }}
                >
                  {!!message?.attachment && (
                    <Box
                      sx={{
                        width: '100%',
                      }}
                    >
                      {message?.attachment_type?.includes('application/pdf') ? (
                        <Box
                          sx={{
                            width: '100%',
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'space-between',
                            padding: '16px',
                          }}
                        >
                          <Box
                            sx={{
                              display: 'flex',
                              alignItems: 'center',
                            }}
                          >
                            <Chip
                              label={
                                <Box
                                  component={'img'}
                                  src={documentGrayIcon.src}
                                  // alt={'bell'}
                                  sx={{ width: '24px', height: '24px' }}
                                />
                              }
                              sx={{
                                minWidth: '40px',
                                minHeight: '40px',
                                borderRadius: '8px',
                                bgcolor: 'white',
                                '& .MuiChip-label': {
                                  padding: '0',
                                  margin: '0',
                                  display: 'flex',
                                  alignItems: 'center',
                                  justifyContent: 'center',
                                },
                              }}
                            />
                            <Box
                              sx={{
                                display: 'flex',
                                flexDirection: 'column',
                                alignItems: 'flex-start',
                                ml: '8px',
                              }}
                            >
                              <Typography
                                variant="inherit"
                                component="span"
                                color={'text.primary'}
                                sx={{
                                  fontSize: '14px',
                                  fontWeight: '400',
                                }}
                                // onClick={open}
                              >
                                {'pdf'}
                              </Typography>
                              <Typography
                                variant="inherit"
                                component="span"
                                color={'text.secondary'}
                                sx={{
                                  fontSize: '12px',
                                  fontWeight: '400',
                                }}
                                // onClick={open}
                              >
                                {message?.attachment_size}
                              </Typography>
                            </Box>
                          </Box>
                        </Box>
                      ) : (
                        <Box
                          component={'img'}
                          src={message.attachment}
                          // alt={'backward icon'}
                          sx={{
                            width: '100%',
                            height: '100%',
                            objectFit: 'fill',
                            backgroundPosition: 'center',
                            backgroundRepeat: 'no-repeat',
                            maxWidth: '300px',
                            maxHeight: '104px',
                          }}
                        />
                      )}
                    </Box>
                  )}

                  {modifyLinks(message.content) ? (
                    <NextLink href={message.content} passHref>
                      <Link
                        href="#"
                        underline="none"
                        target={'_blank'}
                        sx={{
                          width: 'fit-content',
                          // maxWidth: '55%',
                          padding: '16px',
                        }}
                      >
                        <Typography
                          dir={'ltr'}
                          variant="inherit"
                          component="span"
                          color={'primary.main'}
                          // fontFamily={pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Demibold'}
                          sx={{
                            textDecoration: 'underline',
                            fontWeight: '400',
                            fontSize: {
                              xs: '10px',
                              sm: '14px',
                            },
                          }}
                        >
                          {message.content}
                        </Typography>
                      </Link>
                    </NextLink>
                  ) : (
                    <Typography
                      variant="inherit"
                      component="span"
                      color={'text.primary'}
                      sx={{
                        fontSize: { xs: '12px', sm: '14px' },
                        padding: !!message.attachment ? '0px 16px 16px 16px' : '16px',
                        // padding: !!message.attachment ? '8px 8px 12px 8px' : '12px'
                      }}
                    >
                      {message.content}
                    </Typography>
                  )}
                </Box>
              </Box>

              <Typography
                variant="inherit"
                component="span"
                color={'text.disabled'}
                sx={{
                  fontSize: '14px',
                  mt: '6px',
                }}
              >
                {message.created_at.replace(' ', ' ، ')}
              </Typography>
            </Box>
          ))}
        </Box>

        <Box
          sx={{
            width: '100%',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            padding: '19px 19px 19px 19px',
          }}
        >
          <Box
            sx={{
              width: '100%',
              height: '144px',
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'space-between',
              bgcolor: '#F6F7FA',
              borderRadius: '15px',
              py: '16px',
            }}
          >
            <Box
              contentEditable
              datatype={t('ticket:enter_text')}
              sx={{
                direction: 'ltr',
                '&:empty:before': {
                  content: 'attr(datatype)',
                },
                width: '100%',
                height: '50px',
                outline: 'none',
                px: '16px',
                fontSize: '14px',
                color: contentEdState ? 'text.primary' : 'text.disabled',
              }}
              onKeyDown={handleCheckStyles}
              onClick={handleCheckStyles}
              onBlur={(event) => handleAddTicketText(event, 'content-editable')}
              onInput={handleCheckEmptyBox}
            >
              {ticketDescription}
            </Box>
            <Box
              sx={{
                width: '100%',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
                px: '16px',
              }}
            >
              <Box
                sx={{
                  display: 'flex',
                  alignItems: 'center',
                  gap: '16px',
                }}
              >
                {editorButtons.map((button) =>
                  button.type === 'style' ? (
                    <IconButton
                      key={button.id}
                      disableTouchRipple
                      color="inherit"
                      aria-label="prev button"
                      edge="start"
                      onClick={() => handleSetStyleTicketDescription(button.action)}
                      sx={{
                        width: '24px',
                        height: '24px',
                        display: 'flex',
                        // padding: '10px',
                        borderRadius: '4px',
                        margin: 0,
                        bgcolor: edButtonsActivationState[button.action] ? 'primary.light' : 'transparent',
                      }}
                    >
                      <Box
                        component={'img'}
                        src={button.icon.src}
                        alt={'backward icon'}
                        sx={{
                          width: '18px',
                          height: '18px',
                          // transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)'
                        }}
                      />
                    </IconButton>
                  ) : (
                    <IconButton
                      color="inherit"
                      aria-label="upload picture"
                      component="label"
                      disableTouchRipple
                      sx={{
                        width: '24px',
                        height: '24px',
                        display: 'flex',
                        // padding: '10px',
                        borderRadius: '4px',
                        margin: 0,
                      }}
                    >
                      <input hidden accept="image/*" type="file" onChange={handleGetImageFile} />
                      <Box
                        component={'img'}
                        src={button.icon.src}
                        alt={'backward icon'}
                        sx={{
                          width: '24px',
                          height: '24px',
                          // transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)'
                        }}
                      />
                    </IconButton>
                  ),
                )}
              </Box>

              <Button
                variant="contained"
                disableElevation
                disableRipple
                sx={{
                  width: { xs: '90px', sm: '90px' },
                  height: '40px',
                  '&:hover': {
                    backgroundColor: 'primary.main',
                    // boxShadow: 'none',
                  },
                  // boxShadow: 'none',
                  backgroundColor: 'primary.main',
                  borderRadius: '12px',
                  fontWeight: '600',
                  fontSize: '14px',
                }}
                onClick={handleSendTicketMessage}
              >
                {t('ticket:send')}
              </Button>
            </Box>
          </Box>
        </Box>
      </Box>

      {openTicketAttachDB && (
        <Dialog
          fullScreen={true}
          open={openTicketAttachDB}
          onClose={handleCloseTicketAttachDB}
          aria-labelledby="responsive-dialog-title"
          sx={{
            backdropFilter: 'blur(4px)',
          }}
          PaperProps={{
            elevation: 0,
            sx: {
              position: { xs: 'absolute', sm: 'static' },
              bottom: { xs: 0, sm: 'auto' },
              width: {
                xs: '100%',
                sm: ticketAttachmentFile && ticketAttachmentFile.type.includes('application/pdf') ? '450px' : '544px',
                lg: ticketAttachmentFile && ticketAttachmentFile.type.includes('application/pdf') ? '450px' : '664px',
              },
              minWidth: 'auto',
              height: 'auto',
              minHeight: '140px',
              // backgroundColor: 'teal',
              borderRadius: { xs: '22px 22px 0 0', sm: '22px' },
              // maxWidth: "720px!important",
              padding: '0',
            },
          }}
        >
          <DialogContent
            sx={{
              width: '100%',
              padding: '24px',
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            {ticketAttachmentFile && ticketAttachmentFile.type.includes('application/pdf') ? (
              <Box
                sx={{
                  width: '100%',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}
              >
                <Box
                  sx={{
                    display: 'flex',
                    alignItems: 'center',
                  }}
                >
                  <Chip
                    label={
                      <Box
                        component={'img'}
                        src={documentGrayIcon.src}
                        // alt={'bell'}
                        sx={{ width: '24px', height: '24px' }}
                      />
                    }
                    sx={{
                      minWidth: '40px',
                      minHeight: '40px',
                      borderRadius: '8px',
                      bgcolor: '#F2F5FF',
                      '& .MuiChip-label': {
                        padding: '0',
                        margin: '0',
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                      },
                    }}
                  />
                  <Box
                    sx={{
                      display: 'flex',
                      flexDirection: 'column',
                      alignItems: 'flex-start',
                      ml: '8px',
                    }}
                  >
                    <Typography
                      variant="inherit"
                      component="span"
                      color={'text.primary'}
                      sx={{
                        fontSize: '14px',
                        fontWeight: '400',
                      }}
                      // onClick={open}
                    >
                      {ticketAttachmentFile?.name}
                    </Typography>
                    <Typography
                      variant="inherit"
                      component="span"
                      color={'text.secondary'}
                      sx={{
                        fontSize: '12px',
                        fontWeight: '400',
                      }}
                      // onClick={open}
                    >
                      {formatBytes(ticketAttachmentFile?.size)}
                    </Typography>
                  </Box>
                </Box>
                <IconButton
                  disableTouchRipple
                  color="inherit"
                  aria-label="prev button"
                  edge="start"
                  // onClick={() => handleSetStyleTicketDescription(button.action)}
                  sx={{
                    width: '40px',
                    height: '40px',
                    display: 'flex',
                    // padding: '10px',
                    borderRadius: '8px',
                    margin: 0,
                    bgcolor: '#F2F5FF',
                  }}
                  onClick={handleDeleteTicketAttachment}
                >
                  <Box
                    component={'img'}
                    src={garbageGrayIcon.src}
                    alt={'backward icon'}
                    sx={{
                      width: '24px',
                      height: '24px',
                      // transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)'
                    }}
                  />
                </IconButton>
              </Box>
            ) : (
              <Box
                sx={{
                  width: '100%',
                  height: '50%',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  position: 'relative',
                }}
              >
                <Box
                  component={'img'}
                  src={ticketAttachmentPreview}
                  // alt={'backward icon'}
                  sx={{
                    width: '100%',
                    height: 'auto',
                    maxHeight: '307px',
                    borderRadius: '15px',
                  }}
                />

                <IconButton
                  disableTouchRipple
                  color="inherit"
                  aria-label="prev button"
                  edge="start"
                  // onClick={() => handleSetStyleTicketDescription(button.action)}
                  sx={{
                    width: '40px',
                    height: '40px',
                    display: 'flex',
                    // padding: '10px',
                    borderRadius: '8px',
                    margin: 0,
                    bgcolor: 'rgba(26, 26, 26, 0.4)',
                    position: 'absolute',
                    top: '24px',
                    right: '24px',
                    zIndex: '200000',
                  }}
                  onClick={handleDeleteTicketAttachment}
                >
                  <Box
                    component={'img'}
                    src={garbageWhiteIcon.src}
                    alt={'backward icon'}
                    sx={{
                      width: '24px',
                      height: '24px',
                      // transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)'
                    }}
                  />
                </IconButton>
              </Box>
            )}

            <Box
              sx={{
                width: '100%',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'flex-start',
                mt: '16px',
              }}
            >
              <InputLabel
                htmlFor={'ticket description'}
                sx={{
                  fontSize: '17px',
                  fontWeight: '400',
                  color: 'text.primary',
                }}
              >
                {t('ticket:text')}
              </InputLabel>

              <Box
                sx={{
                  width: '100%',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  mt: '8px',
                  // padding: '19px 19px 19px 19px'
                }}
              >
                <Box
                  sx={{
                    width: '100%',
                    height: { xs: '144px', sm: '129px' },
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'space-between',
                    bgcolor: '#F6F7FA',
                    borderRadius: '15px',
                    py: '16px',
                  }}
                >
                  <Box
                    contentEditable
                    datatype={t('ticket:enter_text')}
                    sx={{
                      '&:empty:before': {
                        content: 'attr(datatype)',
                      },
                      width: '100%',
                      height: '50px',
                      outline: 'none',
                      px: '16px',
                      fontSize: '14px',
                      color: contentEdState ? 'text.primary' : 'text.disabled',
                    }}
                    onKeyDown={handleCheckStyles}
                    onClick={handleCheckStyles}
                    onBlur={(event) => handleAddTicketText(event, 'content-editable')}
                    onInput={handleCheckEmptyBox}
                  >
                    {ticketDescription}
                  </Box>
                  <Box
                    sx={{
                      width: '100%',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      px: '16px',
                    }}
                  >
                    <Box
                      sx={{
                        display: 'flex',
                        alignItems: 'center',
                        gap: '16px',
                      }}
                    >
                      {editorButtons.map((button) =>
                        button.type === 'style' ? (
                          <IconButton
                            key={button.id}
                            disableTouchRipple
                            color="inherit"
                            aria-label="prev button"
                            edge="start"
                            onClick={() => handleSetStyleTicketDescription(button.action)}
                            sx={{
                              width: '24px',
                              height: '24px',
                              display: 'flex',
                              // padding: '10px',
                              borderRadius: '4px',
                              margin: 0,
                              bgcolor: edButtonsActivationState[button.action] ? 'primary.light' : 'transparent',
                            }}
                          >
                            <Box
                              component={'img'}
                              src={button.icon.src}
                              alt={'backward icon'}
                              sx={{
                                width: '18px',
                                height: '18px',
                                // transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)'
                              }}
                            />
                          </IconButton>
                        ) : (
                          <IconButton
                            color="inherit"
                            aria-label="upload picture"
                            component="label"
                            disableTouchRipple
                            sx={{
                              width: '24px',
                              height: '24px',
                              display: 'flex',
                              // padding: '10px',
                              borderRadius: '4px',
                              margin: 0,
                            }}
                          >
                            <input hidden accept="image/*" type="file" onChange={handleGetImageFile} />
                            <Box
                              component={'img'}
                              src={button.icon.src}
                              alt={'backward icon'}
                              sx={{
                                width: '24px',
                                height: '24px',
                                // transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)'
                              }}
                            />
                          </IconButton>
                        ),
                      )}
                    </Box>

                    <Button
                      variant="contained"
                      disableElevation
                      disableRipple
                      sx={{
                        display: { xs: 'block', sm: 'none' },
                        width: { xs: '90px', sm: '90px' },
                        height: '40px',
                        '&:hover': {
                          backgroundColor: 'primary.main',
                          // boxShadow: 'none',
                        },
                        // boxShadow: 'none',
                        backgroundColor: 'primary.main',
                        borderRadius: '12px',
                        fontWeight: '600',
                        fontSize: '14px',
                      }}
                      onClick={handleSendTicketMessage}
                    >
                      {t('ticket:send')}
                    </Button>
                  </Box>
                </Box>
              </Box>
            </Box>
            <Button
              variant="contained"
              disableElevation
              disableRipple
              sx={{
                display: { xs: 'none', sm: 'block' },
                width: { xs: '90px', sm: '236px' },
                height: '40px',
                '&:hover': {
                  backgroundColor: 'primary.main',
                  // boxShadow: 'none',
                },
                // boxShadow: 'none',
                backgroundColor: 'primary.main',
                borderRadius: '12px',
                fontWeight: '600',
                fontSize: '14px',
                mt: '40px',
              }}
              onClick={handleSendTicketMessage}
            >
              {t('ticket:send')}
            </Button>
          </DialogContent>
        </Dialog>
      )}
    </>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      hasLayout: true,
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default TicketChat;
