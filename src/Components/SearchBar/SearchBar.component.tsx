import React from 'react';
import { styled, alpha } from '@mui/material/styles';
import Box from '@mui/material/Box';
import InputBase from '@mui/material/InputBase';

import searchIcon from '/public/assets/images/Search-icon.svg';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import { localesNS } from '../../utils/Data/locales_NameSpace.utils';

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: '12px',
  backgroundColor: '#fff',
  '&:hover': {
    backgroundColor: '#fff',
  },
  marginRight: '4px',
  marginLeft: 0,
  width: '100%',
  height: '48px',
  [theme.breakpoints.up('xs')]: {
    display: 'none',
  },
  [theme.breakpoints.up('sm')]: {
    display: 'block',
    width: '272px',
  },
  [theme.breakpoints.up('lg')]: {
    display: 'block',
    width: '320px',
  },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'text.disabled',
  width: '100%',
  height: '48px',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
  },
}));

const SearchBar = () => {
  const { t } = useTranslation();

  return (
    <Search>
      <SearchIconWrapper>
        <Box
          component={'img'}
          src={searchIcon.src}
          alt={'search-icon'}
          sx={{
            width: '24px',
            height: '24px',
          }}
        />
      </SearchIconWrapper>
      <StyledInputBase placeholder={t('common:search')} inputProps={{ 'aria-label': 'search' }} />
    </Search>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export { SearchBar };
