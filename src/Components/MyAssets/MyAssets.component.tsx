import React, { useEffect } from 'react';
import { Box, Divider } from '@mui/material';
import bulletPoint from '/public/assets/images/BulletPoint.svg';
import Typography from '@mui/material/Typography';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
// @ts-ignore
import { Chart } from '../Chart/Chart.component';
// @ts-ignore
import { CurrencyTable } from '../CurrencyTable/CurrencyTable.component';
import type { ChartData, ChartOptions } from 'chart.js';
import { useI18nTrDir } from '../../utils/CustomHooks/i18nTrDir';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import { localesNS } from '../../utils/Data/locales_NameSpace.utils';

interface Props {
  mode: string;
}

const MyAssets = ({ mode }: Props) => {
  const { t, pageDirection } = useI18nTrDir();

  const theme = useTheme();
  const lg = useMediaQuery(theme.breakpoints.up('lg'));
  const xl = useMediaQuery(theme.breakpoints.up('xl'));

  const doughnut_chart_width = mode === 'dashboard' ? (xl ? 220 : 190) : 220;

  const data: ChartData<'doughnut'> = {
    labels: ['1', '2', '3', '4', '5', '6', '7'],
    datasets: [
      {
        label: t('my_assets:profit'),
        data: [120, 80, 120, 180, 110, 160, 250],
        borderColor: 'green',
        backgroundColor: ['rgb(0, 150, 20)'],
      },
    ],
  };

  return (
    <>
      {mode === 'dashboard' ? (
        <Box
          sx={{
            maxWidth: '100%',
            height: { xs: 'auto', lg: 'auto' },
            borderRadius: '22px',
            padding: { xs: '16px 16px 24px 16px', sm: '18px 25px' },
            display: 'flex',
            flexDirection: { xs: 'column', lg: 'row' },
            alignItems: { lg: 'flex-start' },
            mb: '16px',
            backgroundColor: 'white',
          }}
        >
          <Box
            sx={{
              maxWidth: { xs: '100%', lg: `${doughnut_chart_width}px` },
              minWidth: { xl: '40%' },
              height: { xs: '360px', xl: '463px' },
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'flex-start',
              flexDirection: 'column',
              // backgroundColor: 'blue'
              // justifyContent: 'space-between',
            }}
          >
            <Box
              sx={{
                width: 'fit-content',
                display: 'flex',
                alignSelf: 'flex-start',
                alignItems: 'center',
                mb: '25px',
              }}
            >
              <Box
                component={'img'}
                src={bulletPoint.src}
                alt={'bullet-point'}
                sx={{
                  width: '22px',
                  height: '18px',
                  mr: '9px',
                  transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                }}
              />
              <Typography
                variant="inherit"
                component="span"
                color={'text.primary'}
                sx={{
                  // fontWeight: {xs: '600'},
                  fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                  fontSize: { xs: '16px', xl: '16px' },
                }}
              >
                {t('my_assets:my_assets')}
              </Typography>
            </Box>
            <Box
              sx={{
                padding: { lg: '0', xl: '35px 0 0 0' },
              }}
            >
              <Chart
                mode={`doughnut-${mode}`}
                chartData={data}
                chartWidth={doughnut_chart_width}
                chartHeight={doughnut_chart_width}
              />
            </Box>
          </Box>
          <Divider
            orientation="vertical"
            variant="fullWidth"
            flexItem
            sx={{
              mx: { xs: '16px', xl: '32px' },
              display: { xs: 'none', lg: 'block' },
              borderColor: 'secondary.light',
            }}
          />
          <Divider
            orientation="horizontal"
            variant="fullWidth"
            flexItem
            sx={{ my: '30px', display: { xs: 'block', lg: 'none' }, borderColor: 'secondary.light' }}
          />
          <Box
            sx={{
              width: '100%',
              height: '100%',
              display: 'flex',
              flexDirection: 'column',
              // justifyContent: 'space-between',
              // backgroundColor: 'red'
            }}
          >
            <Box
              sx={{
                width: 'fit-content',
                height: 'auto',
                display: 'flex',
                alignItems: 'center',
                // backgroundColor: 'yellow',
                mb: '25px',
              }}
            >
              <Box
                component={'img'}
                src={bulletPoint.src}
                alt={'bullet-point'}
                sx={{
                  width: '22px',
                  height: '18px',
                  mr: '9px',
                  transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                }}
              />
              <Typography
                variant="inherit"
                component="span"
                color={'text.primary'}
                sx={{
                  // fontWeight: {xs: '600'},
                  fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                  fontSize: { xs: '16px', xl: '16px' },
                }}
              >
                {t('my_assets:exchange_rate')}
              </Typography>
            </Box>

            <CurrencyTable mode={mode} />

            {/*<Box*/}
            {/*    sx={{*/}
            {/*        display: 'flex',*/}
            {/*        flexDirection: 'column',*/}
            {/*        alignItems: 'center',*/}
            {/*        justifyContent: 'center',*/}
            {/*        mt: '50px'*/}
            {/*    }}*/}
            {/*>*/}
            {/*    <Typography variant="inherit" component="span" color={'text.secondary'}*/}
            {/*                sx={{*/}
            {/*                    fontWeight: '400',*/}
            {/*                    fontSize: '14px',*/}
            {/*                }}>*/}
            {/*        {t('my_assets:prices_are_rials')}*/}
            {/*    </Typography>*/}
            {/*    <Typography variant="inherit" component="span" color={'text.disabled'}*/}
            {/*                sx={{*/}
            {/*                    fontWeight: '400',*/}
            {/*                    fontSize: '12px',*/}
            {/*                }}>*/}
            {/*        {t('my_assets:last_update', {date: '6/3/1401', clock: '12:30'})}*/}
            {/*    </Typography>*/}
            {/*</Box>*/}
          </Box>
        </Box>
      ) : (
        <Box
          sx={{
            maxWidth: '100%',
            height: 'auto',
            borderRadius: '22px',
            padding: { xs: '16px 16px 24px 16px', sm: '18px 25px' },
            display: 'flex',
            flexDirection: 'column',
            alignItems: { lg: 'flex-start' },
            mb: '16px',
            mt: { xs: '16px', sm: '0' },
            backgroundColor: 'white',
          }}
        >
          <Box
            sx={{
              width: 'fit-content',
              display: 'flex',
              alignSelf: 'flex-start',
              alignItems: 'center',
              mb: '25px',
            }}
          >
            <Box
              component={'img'}
              src={bulletPoint.src}
              alt={'bullet-point'}
              sx={{
                width: '22px',
                height: '18px',
                mr: '9px',
                transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
              }}
            />
            <Typography
              variant="inherit"
              component="span"
              color={'text.primary'}
              sx={{
                // fontWeight: {xs: '600'},
                fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                fontSize: { xs: '16px', xl: '16px' },
              }}
            >
              {t('my_assets:my_assets')}
            </Typography>
          </Box>
          <Box
            sx={{
              width: '100%',
              display: 'flex',
              flexDirection: { xs: 'column', sm: 'row' },
              alignItems: { xs: 'center', sm: 'flex-start' },
              justifyContent: { sm: 'space-between', lg: 'space-around' },
              mt: mode === 'wallet' ? { lg: '20px' } : '0',
            }}
          >
            <Box
              sx={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <Chart
                mode={`doughnut-${mode}`}
                chartData={data}
                chartWidth={doughnut_chart_width}
                chartHeight={doughnut_chart_width}
              />
            </Box>

            <Box
              sx={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                mt: { xs: '32px', sm: '0' },
              }}
            >
              <Chart
                mode={`line-${mode}`}
                chartData={data}
                chartWidth={lg ? 270 : 150}
                chartHeight={190}
                color={'#ff00ff'}
              />
            </Box>
          </Box>

          <Box
            sx={{
              width: '100%',
              height: 'auto',
              display: 'flex',
              flexDirection: 'column',
              mt: { lg: '40px' },
              // justifyContent: 'space-between',
              // backgroundColor: 'red'
            }}
          >
            <CurrencyTable mode={mode} />
          </Box>
        </Box>
      )}
    </>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export { MyAssets };
