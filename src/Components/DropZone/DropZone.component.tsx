import React, { useState, useEffect, ReactNode } from 'react';
import { useDropzone } from 'react-dropzone';
import { Box, FormHelperText, IconButton } from '@mui/material';
import Typography from '@mui/material/Typography';
import uploadIcon from '/public/assets/images/paper-upload-icon.svg';
import garbageIcon from '/public/assets/images/garbage_icon.svg';
import dangerCircleIcon from '/public/assets/images/Danger-Circle-icon.svg';
import { useI18nTrDir } from '../../utils/CustomHooks/i18nTrDir';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { localesNS } from '../../utils/Data/locales_NameSpace.utils';

interface Props {
  id: string;
  handleSetDocsImgValidation: Function;
  images: any;
}

export default function Dropzone({ id, handleSetDocsImgValidation, images }: Props) {
  const { t, pageDirection } = useI18nTrDir();

  const [invalidVol, setInvalidVol] = useState('');
  const [invalidType, setInvalidType] = useState('');

  const docsValidationArray = [invalidVol, invalidType];

  const { getRootProps, getInputProps, open, acceptedFiles } = useDropzone({
    // Disable click and keydown behavior
    noClick: true,
    noKeyboard: true,
    maxFiles: 1,
    accept: {
      'image/jpeg': [],
      'image/png': [],
    },
    onDrop: (acceptedFiles) => {
      setImportedImg(
        acceptedFiles.map((file) =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          }),
        ),
      );
    },
  });
  const [importedImg, setImportedImg] = useState<any>(!!images[id] ? [images[id]] : []);

  useEffect(() => {
    setImportedImg(
      importedImg.map((file: any) =>
        Object.assign(file, {
          preview: URL.createObjectURL(file),
        }),
      ),
    );
    // Make sure to revoke the data uris to avoid memory leaks, will run on unmount
    return () => importedImg.forEach((file: any) => URL.revokeObjectURL(file.preview));
  }, []);

  useEffect(() => {
    const SUPPORTED_FORMATS = ['image/jpg', 'image/jpeg', 'image/png'];
    if (!!importedImg.length) {
      if (importedImg[0]?.size > 8 * 1024 * 1024) {
        handleSetDocsImgValidation({
          result: `${false}/${id}`,
          imageFile: false,
        });
        setInvalidVol(t('common:limited_max_vol', { vol: '8', unit: t('common:MB') }));
      } else {
        handleSetDocsImgValidation({
          result: `${true}/${id}`,
          imageFile: importedImg[0],
        });
        setInvalidVol('');
      }
      if (!SUPPORTED_FORMATS.includes(importedImg[0]?.type)) {
        handleSetDocsImgValidation({
          result: `${false}/${id}`,
          imageFile: false,
        });
        setInvalidType(t('common:allowed_jpg_png'));
      } else {
        handleSetDocsImgValidation({
          result: `${true}/${id}`,
          imageFile: importedImg[0],
        });
        setInvalidType('');
      }
    } else {
      handleSetDocsImgValidation({
        result: `${null}/${id}`,
        imageFile: null,
      });
    }
  }, [importedImg]);

  const formatBytes = (bytes: number, decimals = 2) => {
    if (!+bytes) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = [t('common:B'), t('common:KB'), t('common:MB'), t('common:GB'), t('common:TB')];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return `${parseFloat((bytes / Math.pow(k, i)).toFixed(dm))} ${sizes[i]}`;
  };

  return (
    <Box
      sx={{
        display: { xs: 'none', lg: 'block' },
      }}
    >
      <div className="container">
        {!importedImg.length ? (
          <Box
            {...getRootProps({ className: 'dropzone' })}
            sx={{
              width: '100%',
              height: '187px',
              borderRadius: '16px',
              backgroundColor: !!importedImg.length ? 'white' : '#F2F5FF',
              border: '1px dashed',
              borderColor: 'text.disabled',
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              justifyContent: 'center',
              mt: '24px',
            }}
          >
            <input {...getInputProps()} />
            <IconButton
              color="primary"
              aria-label="upload picture"
              component="label"
              disableRipple={true}
              sx={{
                width: '52px',
                height: '52px',
                borderRadius: '50%',
                display: 'flex',
                justifySelf: 'flex-end',
                padding: '0',
                backgroundColor: '#D1E7FF',
              }}
              onClick={open}
            >
              <Box
                component={'img'}
                src={uploadIcon.src}
                alt={'backward icon'}
                sx={{
                  width: '24px',
                  height: '24px',
                }}
              />
            </IconButton>

            <Typography
              variant="inherit"
              component="span"
              color={'text.secondary'}
              sx={{
                fontSize: '14px',
                fontWeight: '600',
                mt: '24px',
              }}
            >
              {t('profile:drag_drop_or')}
              <Typography
                variant="inherit"
                component="span"
                color={'primary.main'}
                sx={{
                  fontSize: '14px',
                  fontWeight: '600',
                  cursor: 'pointer',
                }}
                onClick={open}
              >
                {t('profile:choose')}
              </Typography>
            </Typography>
            <Typography
              variant="inherit"
              component="span"
              color={'text.disabled'}
              sx={{
                fontSize: '12px',
                fontWeight: '400',
                cursor: 'pointer',
                mt: '6px',
              }}
              onClick={open}
            >
              {t('common:limited_max_vol', { vol: '8', unit: t('common:MB') })}
            </Typography>
          </Box>
        ) : (
          <>
            <Box
              sx={{
                width: '100%',
                height: '187px',
                borderRadius: '16px',
                backgroundColor: !!importedImg.length ? 'white' : '#F2F5FF',
                border: '1px dashed',
                borderColor: !!importedImg.length ? 'primary.main' : 'text.disabled',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                mt: '24px',
                padding: '8px',
              }}
            >
              <Box
                sx={{
                  width: '100%',
                  height: '114px',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                <Box
                  component={'img'}
                  src={importedImg[0]?.preview}
                  sx={{
                    width: '100%',
                    height: '100%',
                    maxWidth: '100%',
                    maxHeight: '100%',
                    borderRadius: '12px',
                    objectFit: 'fill',
                  }}
                />
              </Box>

              <Box
                sx={{
                  width: '100%',
                  height: '49px',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  backgroundColor: '#F2F5FF',
                  borderRadius: '10px',
                  padding: '6px 8px',
                  mt: '8px',
                }}
              >
                <Box
                  sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'flex-start',
                  }}
                >
                  <Typography
                    variant="inherit"
                    component="span"
                    color={'text.primary'}
                    sx={{
                      fontSize: '14px',
                      fontWeight: '400',
                    }}
                    onClick={open}
                  >
                    {importedImg[0]?.path}
                  </Typography>
                  <Typography
                    variant="inherit"
                    component="span"
                    color={'text.secondary'}
                    sx={{
                      fontSize: '12px',
                      fontWeight: '400',
                    }}
                    onClick={open}
                  >
                    {formatBytes(importedImg[0]?.size)}
                  </Typography>
                </Box>
                <Box
                  component={'img'}
                  src={garbageIcon.src}
                  sx={{
                    width: '24px',
                    height: '24px',
                    margin: '6px 4px',
                    objectFit: 'fill',
                    cursor: 'pointer',
                  }}
                  onClick={() => setImportedImg([])}
                />
              </Box>
            </Box>
            {docsValidationArray.map((item) => {
              return (
                !!item && (
                  <Box
                    sx={{
                      display: 'flex',
                      alignItems: 'center',
                      mt: '8px',
                    }}
                  >
                    <Box
                      component={'img'}
                      src={dangerCircleIcon.src}
                      alt={'bullet-point'}
                      sx={{
                        width: '19px',
                        height: '19px',
                        mr: '8px',
                      }}
                    />
                    <FormHelperText error={true}>{item}</FormHelperText>
                  </Box>
                )
              );
            })}
          </>
        )}
      </div>
    </Box>
  );
}

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}
