import React, { useEffect, useState, ChangeEvent, ReactNode } from 'react';
import {
  Box,
  ButtonGroup,
  Button,
  InputAdornment,
  IconButton,
  Select,
  MenuItem,
  TextField,
  InputLabel,
  Tabs,
  Tab,
  Chip,
  FormHelperText,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio,
} from '@mui/material';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useRouter } from 'next/router';
import { PATHS } from '../../configs/routes.config';
import starIcon from '/public/assets/images/Star-icon.svg';
import searchIconActive from '/public/assets/images/Search-icon-active.svg';
import coloredStarIcon from '/public/assets/images/colored-star-icon.svg';
import coloredPlusIcon from '*.svg';
import Typography from '@mui/material/Typography';
import { SearchBar } from '../SearchBar/SearchBar.component';
import FlexibleSearchBarComponent from '../FlexibleSearchBar/FlexibleSearchBar.component';
import FlexibleSearchBar from '../FlexibleSearchBar/FlexibleSearchBar.component';
import { dispatch } from 'jest-circus/build/state';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { fetchExchangeCurrenciesInfo } from '../../store/slices/currencies_exchange_infos.slice';
import { CurrencyTable } from '../CurrencyTable/CurrencyTable.component';
import { Trans } from 'next-i18next';
import { useI18nTrDir } from '../../utils/CustomHooks/i18nTrDir';
import { localesNS } from '../../utils/Data/locales_NameSpace.utils';
import bulletPoint from '/public/assets/images/BulletPoint.svg';
import garbageIcon from '/public/assets/images/garbage_icon.svg';
import dangerCircleIcon from '/public/assets/images/Danger-Circle-icon.svg';
import backwardArrowIcon from '/public/assets/images/backwardArrowDeposit.svg';
import attachmentIcon from '/public/assets/images/Attachment-icon.svg';
import editProfileIcon from '/public/assets/images/edit-profile-icon.svg';
import passportSampleImg from '/public/assets/images/correct-passport-sample-img.png';
import selfiSampleImg from '/public/assets/images/correct-selfi-sample-img.png';
import StepButton from '@mui/material/StepButton';
import Badge from '@mui/material/Badge';
import Avatar from '@mui/material/Avatar';
import jmoment from 'jalali-moment';

import { styled, useTheme } from '@mui/material/styles';
import Stack from '@mui/material/Stack';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';
import Check from '@mui/icons-material/Check';
import SettingsIcon from '@mui/icons-material/Settings';
import GroupAddIcon from '@mui/icons-material/GroupAdd';
import VideoLabelIcon from '@mui/icons-material/VideoLabel';
import StepConnector, { stepConnectorClasses } from '@mui/material/StepConnector';
import { StepIconProps } from '@mui/material/StepIcon';
import * as yup from 'yup';
import Tooltip from '@mui/material/Tooltip';
import { useFormik, Field, ErrorMessage } from 'formik';

import theme from 'tailwindcss/defaultTheme';
import Dropzone from '../DropZone/DropZone.component';
import { fetchProfileVerifyAuth } from '../../store/slices/profile_verify_authentication.slice';
import AlertDialogBox from '../AlertDialogBox/AlertDialogBox.component';
import { fetchUserProfile } from '../../store/slices/user_profile.slice';
import { calcCurrencyConversionCosts } from '../../utils/functions/calc_currency_conversion';
import useMediaQuery from '@mui/material/useMediaQuery';
import backwardArrow from '/public/assets/images/backwardArrowDeposit.svg';
import { fetchUserTicketsSubjects } from '../../store/slices/user_tickets_subjects.slice';
import { fetchCreateNewTicket } from '../../store/slices/create_ticket.slice';

const RecordNewTicket = () => {
  const router = useRouter();
  const UrlPath = router.pathname;
  const { t, i18n, pageDirection } = useI18nTrDir();

  const dispatch = useAppDispatch();
  const theme = useTheme();
  const lg = useMediaQuery(theme.breakpoints.up('lg'));

  const userProfileResult = useAppSelector((state) => state.UserProfile);
  const profileVerifyAuthResult = useAppSelector((state) => state.ProfileVerifyAuthentication);
  const userTicketsSubjectsResult = useAppSelector((state) => state.UserTicketsSubjects);
  const createTicketResult = useAppSelector((state) => state.CreateTicket);

  const recordNewTicketOptions_lg = {
    id: 1,
    // urlKey: PATHS.IRANIAN,
    formTitle: t('ticket:record_new_ticket'),
    formOptions: [
      {
        id: 'title',
        api_id: 'title',
        title: t('ticket:title'),
        placeholder: t('ticket:enter_ticket_title'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'false',
        width: '50%',
      },
      {
        id: 'subject_id',
        api_id: 'subject_id',
        title: t('ticket:subject'),
        placeholder: t('bank_accounts:choose'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'true-simple',
        width: '50%',
      },
      {
        id: 'priority',
        api_id: 'priority',
        title: t('ticket:priority'),
        placeholder: '',
        validation: false,
        hasSelect: 'false',
        width: '50%',
      },
      {
        id: 'FAQ',
        api_id: 'FAQ',
        title: t('ticket:frequent_questions'),
        placeholder: '',
        validation: false,
        hasSelect: 'false',
        width: '100%',
      },
      {
        id: 'message',
        api_id: 'message',
        title: t('ticket:message'),
        placeholder: t('ticket:enter_ticket_message'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'false',
        width: '100%',
      },
      {
        id: 'attachment',
        api_id: 'attachment',
        title: t('ticket:attachment'),
        placeholder: t('ticket:enter_ticket_attach'),
        // validation: yup.string().required(t('bank_accounts:required_text')),
        validation: false,
        hasSelect: 'false',
        width: '50%',
      },
    ],
  };

  const recordNewTicketOptions_xs = {
    id: 1,
    // urlKey: PATHS.IRANIAN,
    formTitle: t('ticket:record_new_ticket'),
    formOptions: [
      {
        id: 'title',
        api_id: 'title',
        title: t('ticket:title'),
        placeholder: t('ticket:enter_ticket_title'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'false',
        width: '50%',
      },
      {
        id: 'subject_id',
        api_id: 'subject_id',
        title: t('ticket:subject'),
        placeholder: t('bank_accounts:choose'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'true-simple',
        width: '50%',
      },
      {
        id: 'FAQ',
        api_id: 'FAQ',
        title: t('ticket:frequent_questions'),
        placeholder: '',
        validation: false,
        hasSelect: 'false',
        width: '100%',
      },
      {
        id: 'message',
        api_id: 'message',
        title: t('ticket:message'),
        placeholder: t('ticket:enter_ticket_message'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'false',
        width: '100%',
      },
      {
        id: 'priority',
        api_id: 'priority',
        title: t('ticket:priority'),
        placeholder: '',
        validation: false,
        hasSelect: 'false',
        width: '50%',
      },
      {
        id: 'attachment',
        api_id: 'attachment',
        title: t('ticket:attachment'),
        placeholder: t('bank_accounts:enter_ticket_attach'),
        // validation: yup.string().required(t('bank_accounts:required_text')),
        validation: false,
        hasSelect: 'false',
        width: '50%',
      },
    ],
  };

  let createTicketOptionsArr = [lg ? recordNewTicketOptions_lg : recordNewTicketOptions_xs];

  const [activeAuthStep, setActiveAuthStep] = useState(createTicketOptionsArr[0].id);
  const [createTicketOptionsState, setCreateTicketOptionsState] = useState(
    lg ? recordNewTicketOptions_lg : recordNewTicketOptions_xs,
  );
  const [profileImgIsValid, setProfileImgIsValid] = useState('');

  const [openResultDialogBox, setOpenResultDialogBox] = React.useState(false);
  const [operationResultState, setOperationResultState] = useState<object>({
    status: '',
    messages: {},
  });
  const [inputValues, setInputValues] = useState<any>({});
  const [personalInfosInputValuesState, setPersonalInfosInputValuesState] = useState<any>({});
  const [locationInfosInputValuesState, setLocationInfosInputValuesState] = useState<any>({});
  const [subjectState, setSubjectState] = useState(undefined);
  const [subjectsListState, setSubjectsListState] = useState([]);
  const [createTicketImageFile, setCreateTicketImageFile] = useState<any>();

  const [preview, setPreview] = useState<string>();
  const [showEmailTooltip, setShowEmailTooltip] = useState(false);
  const [invalidFileType, setInvalidFileType] = useState(false);
  const [activePriority, setActivePriority] = useState('normal');

  const priorityList = [
    { title: t('ticket:normal'), key: 'normal' },
    { title: t('ticket:urgent'), key: 'urgent' },
  ];

  const fileTypeWarningText = [t('bank_accounts:file_format_type'), t('bank_accounts:file_max_vol')];
  const FAQ_List = [
    {
      id: 1,
      title: t('ticket:required_bank_accounts_docs'),
    },
    {
      id: 2,
      title: t('ticket:required_physical_present'),
    },
    {
      id: 3,
      title: t('ticket:estimated_time'),
    },
  ];

  const handleCloseResultDB = () => {
    setOpenResultDialogBox(false);
  };

  useEffect(() => {
    dispatch(fetchUserTicketsSubjects());
  }, []);

  useEffect(() => {
    setSubjectsListState(userTicketsSubjectsResult.userTicketsSubjectsResp?.ticket_subjects_list);
  }, [userTicketsSubjectsResult]);

  useEffect(() => {
    lg
      ? setCreateTicketOptionsState(recordNewTicketOptions_lg)
      : setCreateTicketOptionsState(recordNewTicketOptions_xs);
  }, [lg]);

  useEffect(() => {
    // if (!imageFile) {
    //     setPreview(undefined)
    //     return
    // }

    const objectUrl = createTicketImageFile && URL.createObjectURL(createTicketImageFile);
    setPreview(objectUrl);

    // free memory when ever this component is unmounted
    return () => URL.revokeObjectURL(objectUrl as string);
  }, [createTicketImageFile]);

  useEffect(() => {
    createNewTicketInitialInputsValue();
  }, []);

  useEffect(() => {
    // if (userProfileResult.userProfileResp?.user_type_key === 'validated') {
    //     router.push(`/${PATHS.USER_PROFILE}`)
    // }
  }, [userProfileResult]);

  useEffect(() => {
    if (!!createTicketResult.createTicketResp?.data) {
      setOpenResultDialogBox(true);
      setOperationResultState({
        status: 'success',
        messages: createTicketResult.createTicketResp?.message,
      });
    }
  }, [createTicketResult]);

  const createNewTicketInitialInputsValue = () => {
    // @ts-ignore
    const activeTabInitialInputsValueObj = createTicketOptionsState.formOptions.reduce((accumulator, value) => {
      return { ...accumulator, [value.api_id]: '' };
    }, {});
    return { activeTabInitialInputsValueObj };
  };

  const selectNewIconSimple = (props: any) => (
    <svg {...props} width="16" height="9" viewBox="0 0 16 9" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M15 1L8 8L1 1" stroke="#606060" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
    </svg>
  );

  const handleChangeSelectValues = (event: any, input: string) => {
    setSubjectState(event.target.value);
    setInputValues((prevState: any) => ({ ...prevState, [input]: JSON.parse(event.target.value).id }));
  };

  const handleGetImageFile = (e: ChangeEvent<HTMLInputElement>) => {
    // @ts-ignore
    const selectedFile = e.target.files[0];
    if (selectedFile) {
      const SUPPORTED_FORMATS = ['image/jpg', 'image/jpeg', 'image/png', 'application/pdf'];
      if (!SUPPORTED_FORMATS.includes(selectedFile.type) || +selectedFile.size > 16 * 1024 * 1024) {
        setInvalidFileType(true);
      } else {
        setInvalidFileType(false);
      }
      setCreateTicketImageFile(selectedFile);
      formik.handleChange('attachment')(selectedFile.name);
    }
  };

  const handleSetInputValues = (event: any, inputValue: string) => {
    setInputValues((prevState: any) => ({ ...prevState, [inputValue]: event.target.value }));
  };

  const selectInitialValueByType = (): any => {
    const { activeTabInitialInputsValueObj } = createNewTicketInitialInputsValue();
    // @ts-ignore
    createTicketOptionsState.formOptions.forEach((item) => {
      if (!item.validation) {
        // @ts-ignore
        delete activeTabInitialInputsValueObj[item.api_id];
      }
    });
    // let bankAccountInitialObject = activeTabInitialInputsValueObj;
    return activeTabInitialInputsValueObj;
  };

  const createBankAccountYupSchema = () => {
    // @ts-ignore
    const activeTabYupSchema = createTicketOptionsState.formOptions.reduce((accumulator, value) => {
      let schemaAcc = { ...accumulator };
      if (!!value.validation) {
        schemaAcc = { ...schemaAcc, [value.api_id]: value.validation };
      }
      return schemaAcc;
    }, {});

    return activeTabYupSchema;
  };

  let bankAccountSchemaDeclaration: object = createBankAccountYupSchema();
  const bankAccountValidationSchema = yup.object({ ...bankAccountSchemaDeclaration });

  const formik = useFormik({
    enableReinitialize: true,
    // initialValues: selectInitialValueByType(),
    initialValues: selectInitialValueByType(),
    validationSchema: bankAccountValidationSchema,
    validateOnChange: true,
    validateOnBlur: false,
    onSubmit: async (values) => {
      const completeData = {
        ...values,
        priority: activePriority,
        attachment: createTicketImageFile,
      };

      if (preview) {
        if (!invalidFileType) {
          dispatch(fetchCreateNewTicket(completeData));
        }
      } else {
        dispatch(fetchCreateNewTicket(completeData));
      }
    },
  });

  const handleDeleteImage = () => {
    setPreview(undefined);
    setCreateTicketImageFile(undefined);
    setInvalidFileType(false);
    formik.handleChange('attachment')('');
  };

  const handleSetFrequentlyQuestions = (text: string) => {
    formik.handleChange('message')(text);
  };

  return (
    <Box
      sx={{
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
      }}
    >
      <Box
        sx={{
          width: { xs: '100%', md: '800px', lg: '100%', xl: '1000px' },
          height: 'auto',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'flex-start',
          bgcolor: 'white',
          padding: '19px 24px',
          borderRadius: '22px',
        }}
      >
        <Box
          sx={{
            width: '100%',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}
        >
          <Box
            sx={{
              width: 'auto',
              display: 'flex',
              alignItems: 'center',
              mr: '30px',
            }}
          >
            <Box
              component={'img'}
              src={bulletPoint.src}
              alt={'bullet-point'}
              sx={{
                width: '22px',
                height: '18px',
                mr: '9px',
                transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
              }}
            />
            <Typography
              variant="inherit"
              component="span"
              color={'text.primary'}
              sx={{
                // fontWeight: {xs: '600'},
                fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                fontSize: { xs: '16px', xl: '16px' },
              }}
            >
              {t('ticket:record_new_ticket')}
            </Typography>
          </Box>
          <IconButton
            color="inherit"
            disableRipple={true}
            aria-label="prev button"
            edge="start"
            // onClick={() => close(operation)}
            sx={{
              width: '30px',
              height: '30px',
              display: 'flex',
              justifySelf: 'flex-end',
              padding: '0',
              backgroundColor: '#D9D9D9',
            }}
          >
            <Box
              component={'img'}
              src={backwardArrow.src}
              alt={'backward icon'}
              sx={{
                width: '8px',
                height: '15px',
                transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
              }}
            />
          </IconButton>
        </Box>

        <Box
          sx={{
            width: '100%',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
          }}
          component={'form'}
          onSubmit={formik.handleSubmit}
        >
          <Box
            sx={{
              width: '100%',
              height: 'auto',
              display: 'flex',
              alignItems: 'flex-start',
              justifyContent: 'space-between',
              flexWrap: 'wrap',
              mt: '48px',
            }}
          >
            {createTicketOptionsState.formOptions.map((option) => {
              return (
                <Box
                  key={option.id}
                  sx={{
                    width: {
                      xs: '100%',
                      sm: option.id === 'message' || option.id === 'FAQ' ? '100%' : '46%',
                      lg: option.id === 'message' || option.id === 'FAQ' ? '100%' : '30%',
                    },
                    display: option.id === 'FAQ' ? (subjectState ? 'block' : 'none') : 'block',
                    flexDirection: 'column',
                    alignItems: 'flex-start',
                    mb: { xs: '40px', sm: '48px' },
                  }}
                >
                  <Box
                    sx={{
                      display: 'flex',
                      alignItems: 'center',
                    }}
                  >
                    <InputLabel
                      htmlFor={option.id}
                      sx={{
                        fontSize: {
                          xs: '17px',
                          sm: option.api_id === 'document_image' ? '14px' : '17px',
                          lg: '17px',
                        },
                        fontWeight: '400',
                        color: 'text.primary',
                      }}
                    >
                      {option.title}
                    </InputLabel>

                    {option.id === 'attachment' && (
                      <Tooltip
                        title={
                          <Box
                            sx={{
                              display: 'flex',
                              flexDirection: 'column',
                              alignItems: 'flex-start',
                            }}
                          >
                            {fileTypeWarningText.map((warning) => {
                              return (
                                <>
                                  <Box
                                    sx={{
                                      display: 'flex',
                                      alignItems: 'center',
                                    }}
                                  >
                                    <Chip
                                      sx={{
                                        width: '5px',
                                        height: '5px',
                                        borderRadius: '50%',
                                        bgcolor: 'error.main',
                                        mr: '10px',
                                        '& .MuiChip-label': {
                                          padding: '0',
                                          margin: '0',
                                          display: 'flex',
                                          alignItems: 'center',
                                          justifyContent: 'center',
                                        },
                                      }}
                                    />
                                    <Typography
                                      variant="inherit"
                                      component="span"
                                      color={'error.main'}
                                      sx={{
                                        fontWeight: '400',
                                        fontSize: '12px',
                                        lineHeight: '18px',
                                      }}
                                    >
                                      {warning}
                                    </Typography>
                                  </Box>
                                </>
                              );
                            })}
                          </Box>
                        }
                        placement={pageDirection === 'rtl' ? (!lg ? 'right' : 'left') : !lg ? 'left' : 'right'}
                        arrow={lg}
                        // disableInteractive={true}
                        open={invalidFileType}
                        PopperProps={{
                          sx: {
                            '& .MuiTooltip-tooltip': {
                              padding: '10px',
                              borderRadius: '16px',
                              border: '1px solid',
                              borderColor: 'error.main',
                              bgcolor: 'error.light',
                              mx: '12px',
                              color: 'text.primary',
                              fontSize: { xs: '10px', sm: '14px' },
                              fontWeight: '400',
                            },

                            '& .MuiTooltip-arrow': {
                              width: '52px',
                              height: '52px',
                              color: 'error.light',
                              // bgcolor: "blue",
                              // right: 'auto',
                              left: { xs: 'auto', sm: 'auto', lg: '-10px' },
                              right: { xs: '-7px', sm: '-10px', lg: 'auto' },
                              '&::before': {
                                // borderLeft: '20px solid transparent',
                                // borderRight: '20px solid transparent',
                                // borderBottom: 'calc(2 * 20px * 0.866) solid green',
                                // borderTop: '20px solid transparent',
                                // display: 'inline-block',
                                width: { xs: '7px', sm: '11px' },
                                height: { xs: '7px', sm: '11px' },

                                // backgroundColor: "blue",
                                border: '1px solid',
                                borderColor: 'error.main',
                                overflow: 'hidden',
                              },
                            },
                          },
                        }}
                      >
                        <Box
                          component={'img'}
                          src={dangerCircleIcon.src}
                          // alt={'backward icon'}
                          sx={{
                            width: '18.5px',
                            height: '18.5px',
                            ml: '11px',
                          }}
                        />
                      </Tooltip>
                    )}
                  </Box>

                  {option.hasSelect.includes('false') ? (
                    option.id === 'priority' ? (
                      <FormControl
                        sx={{
                          mt: '8px',
                        }}
                      >
                        <RadioGroup
                          row
                          defaultValue={activePriority}
                          name="ticket-radio-buttons-group"
                          onChange={(event) => setActivePriority(event.target.value)}
                          sx={{
                            justifyContent: 'space-between',
                          }}
                        >
                          {priorityList.map((item) => (
                            <FormControlLabel
                              key={item.key}
                              value={item.key}
                              control={<Radio />}
                              label={item.title}
                              sx={{
                                '& .MuiFormControlLabel-label': {
                                  color: activePriority === item.key ? 'primary.main' : 'text.primary',
                                  fontSize: { xs: '14px', sm: '16px' },
                                  // fontWeight: '600',
                                  fontFamily:
                                    pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                                  mr: '16px',
                                },
                              }}
                            />
                          ))}
                        </RadioGroup>
                      </FormControl>
                    ) : option.id === 'FAQ' ? (
                      !!subjectState && (
                        <Box
                          sx={{
                            width: '100%',
                            mt: '16px',
                          }}
                        >
                          {FAQ_List.map((question) => (
                            <Button
                              key={question.id}
                              variant="contained"
                              disableRipple={true}
                              sx={{
                                minWidth: 'fit-content',
                                height: { xs: '40px', sm: '40px' },
                                '&:hover': {
                                  backgroundColor: 'white',
                                  // color: 'primary.main',
                                  boxShadow: 'none',
                                },
                                bgcolor: 'white',
                                color: 'text.secondary',
                                border: '1px solid',
                                borderColor: 'secondary.light',
                                boxShadow: 'none',
                                fontWeight: '400',
                                borderRadius: '12px',
                                fontSize: { xs: '12px', sm: '14px' },
                                mr: '16px',
                                mb: '16px',
                              }}
                              onClick={() => handleSetFrequentlyQuestions(question.title)}
                            >
                              {question.title}
                            </Button>
                          ))}
                        </Box>
                      )
                    ) : (
                      <Box
                        sx={{
                          width: '100%',
                          display: 'flex',
                          alignItems: 'flex-start',
                          justifyContent: 'space-between',
                          mt: '8px',
                        }}
                      >
                        <FormControl fullWidth error>
                          <TextField
                            // value={option.id === 'passport_image' ? preview && imageFile ? imageFile.name : '' : formik["values"][option.api_id]}
                            value={formik['values'][option.api_id]}
                            onChange={(event) => {
                              formik.handleChange(option.api_id)(event);
                              handleSetInputValues(event, option.api_id);
                            }}
                            error={formik['touched'][option.api_id] && Boolean(formik['errors'][option.api_id])}
                            helperText={
                              formik['touched'][option.api_id] && (formik['errors'][option.api_id] as ReactNode)
                            }
                            name={option.api_id}
                            fullWidth
                            id={option.api_id}
                            placeholder={option.placeholder}
                            type={'text'}
                            autoComplete="off"
                            multiline={option.id === 'message'}
                            rows={7}
                            // onChange={(event) => handleSetInputValues(event, option.api_id)}
                            sx={{
                              '& input::placeholder': {
                                fontSize: '14px',
                                color: 'text.secondary',
                              },
                              width: '100%',
                              // flexGrow: '1',
                              height: '100%',
                              // bgcolor: option.id === 'passport_image' ? preview ? 'white' : 'secondary.main' : inputValues[option.api_id] ? 'white' : 'secondary.main',
                              borderRadius: '12px',
                              '& .MuiInputAdornment-root': {
                                ml: '8px',
                                height: '40px',
                                maxHeight: '40px',
                                backgroundColor:
                                  option.id === 'attachment'
                                    ? preview
                                      ? 'white'
                                      : 'secondary.main'
                                    : 'secondary.main',
                                borderRadius: '12px',
                                // border: option.id === 'passport_image' ? '1px dash' : 'none'
                              },
                              '& legend': { display: 'none' },
                              '& fieldset': {
                                top: 0,
                                borderRadius: '12px',
                                border: option.id === 'attachment' ? (preview ? '1px solid' : '1px dashed') : '',
                                borderColor: inputValues[option.api_id]
                                  ? 'primary.main'
                                  : option.id === 'attachment'
                                  ? preview
                                    ? 'primary.main'
                                    : 'text.disabled'
                                  : 'secondary.main',
                              },
                              '& .MuiOutlinedInput-root:hover': {
                                '& > fieldset': {
                                  border: option.id === 'attachment' ? '1px dash' : '',
                                  borderColor: inputValues[option.api_id]
                                    ? 'primary.main'
                                    : option.id === 'attachment'
                                    ? preview
                                      ? 'primary.main'
                                      : 'text.disabled'
                                    : 'secondary.main',
                                },
                              },
                              // '& fieldset:hover': {top: 0, borderRadius: '12px', border: '1px solid red'},
                              '& .MuiOutlinedInput-root': {
                                padding: 0,
                                borderRadius: '12px',
                                overflow: 'hidden',
                                height: '100%',
                                bgcolor:
                                  option.id === 'attachment'
                                    ? preview
                                      ? 'white'
                                      : 'secondary.main'
                                    : inputValues[option.api_id]
                                    ? 'white'
                                    : 'secondary.main',
                                // "& > fieldset": {
                                //     border: '1.5px solid green',
                                // },
                                // bgcolor: 'secondary.main',
                                // backgroundColor: '#ffffff',
                                '&.Mui-focused > fieldset': {
                                  // border: '1px solid',
                                  borderColor: 'primary.main',
                                },
                                '&:hover:before fieldset': {
                                  borderColor: 'primary.main',
                                },
                                color: 'text.primary',
                              },
                              '.MuiOutlinedInput-root :focus': {
                                backgroundColor:
                                  option.id === 'attachment' ? (preview ? 'white' : 'secondary.main') : 'white',
                              },
                              '.MuiInputBase-input': {
                                padding: '14px',
                              },
                              '.MuiOutlinedInput-input': {
                                bgcolor:
                                  option.id === 'attachment'
                                    ? preview
                                      ? 'white'
                                      : 'secondary.main'
                                    : inputValues[option.api_id]
                                    ? 'white'
                                    : 'secondary.main',
                              },
                              zIndex: 1,
                            }}
                            InputProps={{
                              readOnly: option.id === 'attachment',
                              autoComplete: 'off',
                              startAdornment: option.id === 'attachment' && preview && (
                                <InputAdornment
                                  position="end"
                                  sx={{
                                    minWidth: '36px',
                                    height: '40px',
                                    borderRadius: '11px',
                                  }}
                                >
                                  <Box
                                    component={'img'}
                                    src={preview}
                                    sx={{
                                      width: '36px',
                                      height: '40px',
                                      borderRadius: '11px',
                                      objectFit: 'fill',
                                    }}
                                  />
                                </InputAdornment>
                              ),
                              endAdornment: option.id === 'attachment' && preview && (
                                <InputAdornment
                                  position="start"
                                  sx={{
                                    width: 'fit-content',
                                    height: '40px',
                                    backgroundColor: 'transparent',
                                    '&:hover': {
                                      cursor: 'pointer',
                                    },
                                  }}
                                  onClick={handleDeleteImage}
                                >
                                  <Box
                                    component={'img'}
                                    src={garbageIcon.src}
                                    sx={{
                                      width: '24px',
                                      height: '24px',
                                      objectFit: 'fill',
                                    }}
                                  />
                                </InputAdornment>
                              ),
                            }}
                          />
                        </FormControl>
                        {option.id === 'attachment' && !preview && (
                          <Button
                            variant="contained"
                            component="label"
                            disableRipple
                            sx={{
                              width: 'auto',
                              minWidth: '45px',
                              px: '10px',
                              height: '51px',
                              flexGrow: '1',
                              borderRadius: '12px',
                              ml: '8px',
                              boxShadow: 'none',
                              '&:hover': {
                                boxShadow: 'none',
                              },
                            }}
                          >
                            <Box
                              component={'img'}
                              src={attachmentIcon.src}
                              sx={{
                                width: '24px',
                                height: '24px',
                              }}
                            />
                            <input
                              hidden
                              type="file"
                              accept="image/png, image/bmp, image/jpeg"
                              onChange={handleGetImageFile}
                            />
                          </Button>
                        )}
                      </Box>
                    )
                  ) : (
                    option.hasSelect.includes('simple') && (
                      <FormControl fullWidth>
                        <Select
                          // dir={'rtl'}
                          value={formik['values'][option.api_id]}
                          // value={option.id === 'passport_image' ? preview && imageFile && imageFile.name : formik["values"][option.api_id]}
                          onChange={(event) => {
                            console.log(
                              'to string what',
                              JSON.parse(event.target.value),
                              JSON.parse(event.target.value)['id'].toString(),
                            );
                            formik.handleChange(option.api_id)(JSON.parse(event.target.value)['id']?.toString());
                            handleChangeSelectValues(event, 'subject_id');
                          }}
                          error={formik['touched'][option.api_id] && Boolean(formik['errors'][option.api_id])}
                          // helperText={(formik["touched"][option.api_id]) && (formik["errors"][option.api_id])}
                          name={option.api_id}
                          // onChange={(event) => handleChangeSelectValues(event, option.id === 'bank_name' ? 'bank_id' : option.id === 'card_type' ? 'type' : 'currency_type')}
                          displayEmpty={true}
                          placeholder={option.placeholder}
                          IconComponent={selectNewIconSimple}
                          renderValue={(value: string) => {
                            return (
                              <Typography
                                variant="inherit"
                                component="span"
                                color={'text.secondary'}
                                sx={{
                                  fontSize: '14px',
                                  fontWeight: '400',
                                  opacity: option.id === 'subject_id' && subjectState ? 1 : '0.4',
                                }}
                              >
                                {option.id === 'subject_id' && subjectState
                                  ? JSON.parse(subjectState).title
                                  : option.placeholder}
                              </Typography>
                            );
                          }}
                          MenuProps={{
                            anchorOrigin: {
                              vertical: 'bottom',
                              horizontal: 'left',
                            },
                            transformOrigin: {
                              vertical: 'top',
                              horizontal: 'left',
                            },
                            PaperProps: {
                              sx: {
                                mt: '8px',
                                boxShadow: '0px 4px 38px rgba(68, 68, 68, 0.1)',
                                maxHeight: '250px',
                                borderRadius: '15px',
                              },
                            },
                          }}
                          sx={{
                            width: '100%',
                            mt: '8px',
                            bgcolor: option.id === 'subject_id' && subjectState ? 'white' : 'secondary.main',
                            borderRadius: '12px',
                            '& .MuiSelect-icon': { top: '40%', mx: '5px' },
                            height: '100%',
                            '.MuiInputBase-input': {
                              py: '14px',
                            },
                            '& fieldset': {
                              border:
                                (option.id === 'subject_id' && subjectState) || formik['touched'][option.api_id]
                                  ? '1px solid'
                                  : 'none',
                              borderColor: 'primary.main',
                            },
                          }}
                        >
                          {subjectsListState?.map((item: any, index: number) => {
                            return (
                              <MenuItem
                                value={JSON.stringify(item)}
                                key={item.id}
                                sx={{
                                  width: '100%',
                                  px: '16px',
                                  my: '5px',
                                }}
                              >
                                <Typography
                                  variant="inherit"
                                  component="span"
                                  color={'text.secondary'}
                                  sx={{
                                    fontSize: '16px',
                                    fontWeight: '400',
                                  }}
                                >
                                  {item.title}
                                </Typography>
                              </MenuItem>
                            );
                          })}
                        </Select>
                        {formik['touched'][option.api_id] && (
                          <FormHelperText error={true}>{formik['errors'][option.api_id] as ReactNode}</FormHelperText>
                        )}
                      </FormControl>
                    )
                  )}
                </Box>
              );
            })}
          </Box>

          <Button
            variant="contained"
            type={'submit'}
            disableRipple={true}
            sx={{
              width: { xs: '47%', sm: '236px' },
              height: { xs: '40px', sm: '40px' },
              '&:hover': {
                boxShadow: 'none',
              },
              boxShadow: 'none',
              // fontWeight: '600',
              borderRadius: '12px',
              fontSize: { xs: '12px', sm: '14px' },
              mt: '80px',
              mb: '14px',
            }}
          >
            {t('ticket:send_ticket')}
          </Button>
        </Box>
      </Box>
      {openResultDialogBox && (
        <AlertDialogBox
          mode={'result'}
          data={operationResultState}
          open={openResultDialogBox}
          close={handleCloseResultDB}
        />
      )}
    </Box>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      hasLayout: true,
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default RecordNewTicket;
