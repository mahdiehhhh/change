import React, { ReactNode, useEffect, useRef, useState } from 'react';
import {
  Box,
  Button,
  Checkbox,
  CircularProgress,
  CircularProgressProps,
  FormControl,
  FormControlLabel,
  FormHelperText,
  IconButton,
  InputAdornment,
  Link,
  MenuItem,
  Select,
  SelectChangeEvent,
  TextField,
  Typography,
  circularProgressClasses,
} from '@mui/material';
import NextLink from 'next/link';
import { CheckCircle, RadioButtonUnchecked, Visibility, VisibilityOff } from '@mui/icons-material';

import iranFlag from '/public/assets/images/IranCountry.svg';
import VerificationInput from 'react-verification-input';
import styles from './InputField.module.scss';
import editMobileNum from '/public/assets/images/Edit-mobile-num.svg';
import retry from '/public/assets/images/Retry.svg';
import disabledRetry from '/public/assets/images/disabled-retry.svg';
import forwardArrow from '/public/assets/images/forward-arrow.svg';
import backwardArrow from '/public/assets/images/backwardArrowDeposit.svg';
import visibleEye from '/public/assets/images/visible_eye_icon.svg';
import invisibleEye from '/public/assets/images/invisible_eye_icon.svg';
import visibleEyeDisabled from '/public/assets/images/visible_eye_disabled_icon.svg';
import invisibleEyeDisabled from '/public/assets/images/invisible_eye_disabled_icon.svg';
import libphonenumber from 'google-libphonenumber';

import { useNavigate } from 'react-router-dom';
import { useFormik } from 'formik';

import * as yup from 'yup';
import 'yup-phone';
import { PATHS } from 'configs/routes.config';
import { SwiperSlider } from 'Components/Entrance/component';
import { ACCESS_TOKEN, ACCESS_TOKEN_EXPIRE, IS_LOGGED_IN } from 'configs/variables.config';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation, Trans } from 'next-i18next';
import { useAppDispatch, useAppSelector } from 'store/hooks';
import { fetchPhoneCodes } from 'store/slices/phone_codes.slice';
import { array } from 'yup';
import { default_country } from '../../utils/Data/data.utils';
import { fetchAccessToken } from '../../store/slices/authentication.slice';
import { fetchCheckPhone } from '../../store/slices/check_phone.slice';
import { fetchLogin } from '../../store/slices/login.slice';
import { CLIENT_ID, CLIENT_SECRET, GRANT_TYPE } from '../../configs/backend.config';
import { fetchRegister } from '../../store/slices/register.slice';
import {
  eraseCookie,
  getCookie,
  handleGetTokens,
  handleRemoveTokens,
  handleSetTokens,
  setCookie,
} from '../../utils/functions/local';
import { fetchRegResend } from '../../store/slices/register_resend.slice';
import { fetchRegVerifyPhone } from '../../store/slices/register_verify_phone.slice';
import { fetchForgetPassword } from '../../store/slices/forget_password.slice';
import { fetchForgetPassVerify } from '../../store/slices/forget_pass_verify.slice';
import { fetchForgetPassReset } from '../../store/slices/forget_pass_reset.slice';
import { fetchForgetPassResend } from '../../store/slices/forget_pass_resend.slice';
import { useRouter } from 'next/router';
import { login } from '../../pages/api/dynamic/getData';
import { handleReadUserLoginInfos, handleSaveUserLoginInfos } from '../../utils/functions/general';
import { useI18nTrDir } from '../../utils/CustomHooks/i18nTrDir';
import { useWindowSize } from '../../utils/CustomHooks/windowSize';
import { localesNS } from '../../utils/Data/locales_NameSpace.utils';

interface Props {
  type: string;
  phoneNumber?: number | string;
  phoneValidity?: ({ phone, password, repeatPassword }: Schema) => void;
  buttonLoading: boolean;
}

interface Schema {
  phone?: string | number;
  password?: string | number;
  repeatPassword?: string | number;
}

interface InternationalCode {
  icon: string;
  iso_code: string;
  phone_prefix: string;
}

const InputField = ({ type, phoneNumber }: Props) => {
  const inputRef = useRef<HTMLInputElement>(null);

  const { t, pageDirection } = useI18nTrDir();

  const router = useRouter();

  const { windowSizes } = useWindowSize();

  const countriesInfoAPI = useAppSelector((state): any => state.PhoneCodes.codes);
  const registerResult = useAppSelector((state) => state.Register);
  const loginResult = useAppSelector((state) => state.Login);
  const forgetPassResult = useAppSelector((state) => state.ForgetPassword);
  const forgetPassVerifyResult = useAppSelector((state) => state.ForgetPasswordVerify);
  const registerVerifyPhoneResult = useAppSelector((state) => state.RegisterVerifyPhone);
  const registerVerificationResendResult = useAppSelector((state) => state.RegisterResend);
  const forgetPassVerificationResendResult = useAppSelector((state) => state.ForgetPasswordResend);
  const checkPhoneResult = useAppSelector((state) => state.CheckPhone);
  const dispatch = useAppDispatch();

  const selectInitialValueByType = (type: string): Schema => {
    const userPhoneNumber =
      typeof window !== 'undefined' && handleReadUserLoginInfos() ? handleReadUserLoginInfos().phone : '';

    let initialObject = {};
    if (type === 'enterViaPhone' || type === 'forgetPassword') {
      initialObject = {
        phone: '',
      };
    } else if (type === 'enterViaPass') {
      initialObject = {
        password: '',
      };
    } else if (type === 'setNewPass') {
      initialObject = {
        password: '',
        repeatPassword: '',
      };
    } else if (type === 'register') {
      initialObject = {
        phone: userPhoneNumber,
        password: '',
        repeatPassword: '',
      };
    }
    return initialObject;
  };

  const phoneUtil = libphonenumber.PhoneNumberUtil.getInstance();

  // console.log(registerResult, '.....................');

  const [verificationCode, setVerificationCode] = useState('');
  const [verifyTimer, setVerifyTimer] = useState('');
  const [resendVerifyCode, setResendVerifyCode] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [showRepeatPassword, setShowRepeatPassword] = useState(false);
  const [verificationError, setVerificationError] = useState(false);
  const [phoneCodes, setPhoneCodes] = useState([]);
  const [completeDataState, setCompleteDataState] = useState<any>();
  const [formInputValues, setFormInputValues] = useState({
    phone: '',
    password: '',
    repeatPassword: '',
  });
  // TODO: what should we do for declaring default country ?????
  const [areaCode, setAreaCode] = useState<InternationalCode>();

  const [wrongCode, setWrongCode] = useState('');
  const [initialValObject, setInitialValObject] = useState(selectInitialValueByType(type));
  const [registerDuplicatePhoneNum, setRegisterDuplicatePhoneNum] = useState('');

  const [buttonLoading, setButtonLoading] = useState(false);

  // console.log(loginResult, 'hiiii');
  useEffect(() => {
    if (checkPhoneResult.checkPhoneResp == 'null') {
      console.log('hiiii');
      // setButtonLoading(true);
    }
  }, []);
  const codeResendTime = 2;
  // const focusInput-1 = useRef<HTMLDivElement>(null);
  // const focusInput-2 = useRef<HTMLDivElement>(null);
  // const focusInput-3 = useRef<HTMLDivElement>(null);

  useEffect(() => {
    // dispatch(fetchAccessToken())
    dispatch(fetchPhoneCodes());
  }, [dispatch]);

  useEffect(() => {
    setPhoneCodes(countriesInfoAPI);
    const IRPhoneCode = countriesInfoAPI?.find((item: InternationalCode) => item.phone_prefix === '+98');
    setAreaCode(IRPhoneCode);
  }, [countriesInfoAPI]);

  useEffect(() => {
    console.log('this is register verification resend result', registerVerificationResendResult);
  }, [registerVerificationResendResult]);

  useEffect(() => {
    (type === 'forgetVerify' || type === 'verification') && counterDownTimer(codeResendTime);
    setInitialValObject(selectInitialValueByType(type));

    if (type === 'register') {
      const userPhoneNumber = handleReadUserLoginInfos().phone;
      formik.values.phone = userPhoneNumber;
    }
  }, [type]);

  function FacebookCircularProgress(props: CircularProgressProps) {
    return (
      <Box sx={{ position: 'relative', display: 'flex' }}>
        <CircularProgress
          variant="determinate"
          sx={{
            color: (theme) => theme.palette.grey[theme.palette.mode === 'light' ? 200 : 800],
          }}
          size={15}
          thickness={4}
          {...props}
          value={100}
        />
        <CircularProgress
          variant="indeterminate"
          disableShrink
          sx={{
            color: (theme) => (theme.palette.mode === 'light' ? '##FFFFFF' : '#308fe8'),
            animationDuration: '550ms',
            position: 'absolute',
            left: 0,
            [`& .${circularProgressClasses.circle}`]: {
              strokeLinecap: 'round',
            },
          }}
          size={15}
          thickness={4}
          {...props}
        />
      </Box>
    );
  }

  let schemaDeclaration: object =
    type === 'enterViaPhone' || type === 'forgetPassword'
      ? {
          phone: yup
            .string()
            .required(t('input_field:required_phone'))
            .phone(areaCode?.iso_code, true, t('input_field:wrong_phone'))
            .test(
              'check-mobile-number',
              t('input_field:phone_must_be_mobile'),
              (value: string | undefined = '09123456789') => {
                try {
                  return !!phoneUtil.getNumberType(phoneUtil.parseAndKeepRawInput(value, areaCode?.iso_code));
                } catch (e) {
                  return false;
                }
              },
            ),
        }
      : type === 'enterViaPass'
      ? {
          password: yup
            .string()
            .min(8, t('input_field:pass_least_8'))
            .required(t('input_field:required_pass'))
            .test('check-password', t('input_field:wrong_pass'), async (value: string | undefined) => {
              let result = true;
              if (value && value?.length > 7) {
                // @ts-ignore
                const userLoginInfo = completeDataState || handleReadUserLoginInfos();
                userLoginInfo.password = value;
                // TODO: loginResult is always one step behind even by await
                // await dispatch(fetchLogin(completeDataState))

                await login(userLoginInfo)
                  .then((resp) => {
                    // handleSetTokens(IS_LOGGED_IN, JSON.stringify(resp.data.token));
                    setCookie(IS_LOGGED_IN, resp.data.token);

                    // const { pathname, asPath, query } = router
                    // router.push({ pathname, query }, asPath, { locale: router.locale })
                    router.replace(`/${PATHS.USER_DASHBOARD}`);
                    // dispatch(fetchLogin(completeDataState))
                    result = true;
                  })
                  .catch(() => {
                    result = false;
                  });
              }
              return result;
            }),
        }
      : type === 'setNewPass'
      ? {
          password: yup.string().min(8, t('input_field:pass_least_8')).required(t('input_field:required_pass')),
          repeatPassword: yup
            .string()
            .min(8, t('input_field:pass_least_8'))
            .required(t('input_field:required_repeatPass'))
            .oneOf([yup.ref('password'), null], t('input_field:repeatPass_must_match')),
        }
      : {
          password: yup.string().min(8, t('input_field:pass_least_8')).required(t('input_field:required_pass')),
          repeatPassword: yup
            .string()
            .min(8, t('input_field:pass_least_8'))
            .required(t('input_field:required_repeatPass'))
            .oneOf([yup.ref('password'), null], t('input_field:repeatPass_must_match')),
          phone: yup
            .string()
            .required(t('input_field:required_phone'))
            .phone(areaCode?.iso_code, true, t('input_field:wrong_phone'))
            .test(
              'check-mobile-number',
              t('input_field:phone_must_be_mobile'),
              (value: string | undefined = '09123456789') => {
                try {
                  return !!phoneUtil.getNumberType(phoneUtil.parseAndKeepRawInput(value, areaCode?.iso_code));
                } catch (e) {
                  return false;
                }
              },
            ),
        };

  // @ts-ignore
  const validationSchema = yup.object(schemaDeclaration);

  const formik = useFormik({
    enableReinitialize: true,
    // @ts-ignore
    initialValues: initialValObject,
    validationSchema: validationSchema,
    validateOnChange: false,
    validateOnBlur: false,
    onSubmit: async (values) => {
      // const AccessToken = handleGetTokens(ACCESS_TOKEN)
      const AccessToken = getCookie(ACCESS_TOKEN);
      // const IsLoggedIn = handleGetTokens(IS_LOGGED_IN)
      const IsLoggedIn = getCookie(IS_LOGGED_IN);
      // const AccessTokenExpTime = handleGetTokens(ACCESS_TOKEN_EXPIRE)
      const AccessTokenExpTime = getCookie(ACCESS_TOKEN_EXPIRE);

      let completeData: any = {
        ...values,
        phone_prefix: areaCode?.phone_prefix,
      };

      if (type !== 'verification') {
        setCompleteDataState(completeData);
      }

      if (type === 'enterViaPhone' || type === 'forgetPassword') {
        handleSaveUserLoginInfos(completeData);
      }

      (!AccessToken || (AccessTokenExpTime && +Date.now() > +AccessTokenExpTime)) &&
        (await dispatch(fetchAccessToken()));

      if (type === 'enterViaPhone') {
        console.log('type is enter via phone');
        // handleRemoveTokens(IS_LOGGED_IN)
        eraseCookie(IS_LOGGED_IN);
        dispatch(fetchCheckPhone(completeData));
      } else if (type === 'enterViaPass') {
        // await dispatch(fetchLogin(completeData))
      } else if (type === 'register') {
        // handleRemoveTokens(IS_LOGGED_IN)
        eraseCookie(IS_LOGGED_IN);
        delete completeData['repeatPassword'];
        handleSaveUserLoginInfos(completeData);
        dispatch(fetchRegister(completeData));
      } else if (type === 'forgetPassword') {
        dispatch(fetchForgetPassword(completeData));
      } else if (type === 'setNewPass') {
        const requiredUserInfos = { ...completeData, phone: handleReadUserLoginInfos().phone };
        delete requiredUserInfos['repeatPassword'];
        requiredUserInfos['reference'] = forgetPassVerifyResult.forgetPassVerifyResp?.reference;
        dispatch(fetchForgetPassReset(requiredUserInfos));
      }
    },
  });

  useEffect(() => {
    !formInputValues.phone && formik.resetForm();
  }, [formInputValues.phone]);

  useEffect(() => {
    !formInputValues.password && formik.resetForm();
  }, [formInputValues.password]);

  useEffect(() => {
    if (registerResult.error?.errors) {
      setRegisterDuplicatePhoneNum(registerResult.error?.errors?.phone[0]);
    } else {
      setRegisterDuplicatePhoneNum('');
    }
  }, [registerResult]);

  const formTypes: { [index: string]: any } = {
    register: {
      title: t('input_field:register'),
      inputs: [
        [t('input_field:mobile_number'), t('input_field:enter_mobile_number'), 'phone'],
        [t('input_field:pass'), t('input_field:enter_pass'), 'password'],
        [t('input_field:repeatPass'), t('input_field:enter_repeatPass'), 'repeatPassword'],
      ],
    },
    enterViaPhone: {
      title: t('input_field:welcome'),
      inputs: [[t('input_field:mobile_number'), t('input_field:enter_mobile_number'), 'phone']],
    },
    enterViaPass: {
      title: t('input_field:welcome'),
      inputs: [[t('input_field:pass'), t('input_field:enter_pass'), 'password']],
    },
    verification: {
      title: t('input_field:enter_verification'),
      subTitle: `${t('input_field:verification_sent', { number: phoneNumber })}`,
    },
    forgetVerify: {
      title: t('input_field:enter_verification'),
      subTitle: `${t('input_field:verification_sent', { number: phoneNumber })}`,
    },
    forgetPassword: {
      title: t('input_field:forget_pass'),
      subTitle: t('input_field:enter_mobile_to_recover_pass'),
      inputs: [['', t('input_field:mobile_number'), 'phone']],
    },
    setNewPass: {
      title: t('input_field:enter_new_pass'),
      inputs: [
        ['', t('input_field:new_pass'), 'password'],
        ['', t('input_field:repeat_new_pass'), 'repeatPassword'],
      ],
    },
  };

  const handleChangeAreaCode = (event: SelectChangeEvent<any>, child: ReactNode): void => {
    setAreaCode(JSON.parse(event.target.value));
  };

  const handleClickShowPassword = (repeat: boolean) => {
    repeat ? setShowRepeatPassword(!showRepeatPassword) : setShowPassword(!showPassword);
  };

  const handleGoToPrevious = (pageType: string) => {
    if (pageType === 'forgetPassword' || pageType === 'register') {
      if (pageType === 'register') {
        dispatch(fetchCheckPhone('reset'));
      }
      typeof window !== 'undefined' && router.replace(`/${PATHS.USER_LOGIN}`);
    } else {
      typeof window !== 'undefined' && router.reload();
    }
    // (pageType === 'enterViaPass' || pageType === 'forgetPassword') ? typeof window !== 'undefined' && router.replace(`/${PATHS.USER_LOGIN}`) :
    //     (pageType === 'forgetVerify' || pageType === 'setNewPass') && typeof window !== 'undefined' && router.replace(`/${PATHS.USER_FORGET}`)
  };

  const handleSubmitForm = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (verificationCode.length !== 6) {
      setVerificationError(true);
      setWrongCode(
        verificationCode.length !== 6
          ? t('input_field:verification_not_complete')
          : t('input_field:wrong_verification'),
      );
      window.setTimeout(function () {
        setVerificationError(false);
      }, 600);
    } else if (verificationCode.length === 6) {
      handleConfirmVerificationCode(`confirm-${type}`, verificationCode);
      setVerificationError(false);
      setWrongCode('');
    }
  };

  const handleConfirmVerificationCode = (verifyType: string, code?: string) => {
    completeDataState && delete completeDataState.password;
    const requiredUserInfos = completeDataState ? completeDataState : handleReadUserLoginInfos();
    const verificationData = {
      ...requiredUserInfos,
      reference:
        type === 'forgetVerify'
          ? forgetPassVerificationResendResult.forgetPassResendResp?.reference
            ? forgetPassVerificationResendResult.forgetPassResendResp?.reference
            : forgetPassResult.forgetPassResp?.reference
          : registerVerificationResendResult.regResendResp?.data?.reference
          ? registerVerificationResendResult.regResendResp?.data?.reference
          : registerResult.registerResp?.reference,
      ...(verifyType.includes('confirm') && { code: code }),
    };
    if (verifyType.includes('resend') && resendVerifyCode) {
      counterDownTimer(codeResendTime);
      verifyType.includes('verification')
        ? dispatch(fetchRegResend(verificationData))
        : dispatch(fetchForgetPassResend(verificationData));
    } else if (verifyType.includes('confirm')) {
      verifyType.includes('verification')
        ? dispatch(fetchRegVerifyPhone(verificationData))
        : dispatch(fetchForgetPassVerify(verificationData));
    }
  };

  useEffect(() => {
    if (registerVerifyPhoneResult.error?.includes('401')) {
      setVerificationError(true);
      setWrongCode(t('input_field:wrong_verification'));
      window.setTimeout(function () {
        setVerificationError(false);
      }, 600);
    }
  }, [registerVerifyPhoneResult]);

  useEffect(() => {
    if (forgetPassVerifyResult.error?.includes('401')) {
      setVerificationError(true);
      setWrongCode(t('input_field:wrong_verification'));
      window.setTimeout(function () {
        setVerificationError(false);
      }, 600);
    }
  }, [forgetPassVerifyResult]);

  const counterDownTimer = (duration: number) => {
    if (verifyTimer === '' || verifyTimer === '00:00') {
      setResendVerifyCode(false);
      let timer = duration * 60,
        minutes,
        seconds;
      const timerInterval = setInterval(function () {
        minutes = parseInt(String(timer / 60), 10);
        seconds = parseInt(String(timer % 60), 10);

        minutes = minutes < 10 ? '0' + minutes : minutes;
        seconds = seconds < 10 ? '0' + seconds : seconds;
        const leftTime = `${minutes}:${seconds}`;
        setVerifyTimer(leftTime);

        if (--timer < 0) {
          clearInterval(timerInterval);
          setResendVerifyCode(true);
        }
      }, 1000);
    }
  };

  const handleChangeInputValue = (inputName: string, event: any) => {
    setFormInputValues((prevState) => ({ ...prevState, [inputName]: event.target.value }));
  };

  return (
    <>
      {(type === 'enterViaPass' ||
        type === 'forgetPassword' ||
        type === 'forgetVerify' ||
        type === 'setNewPass' ||
        type === 'register') && (
        <Box
          component={'img'}
          src={pageDirection === 'rtl' ? forwardArrow.src : backwardArrow.src}
          sx={{
            width: '20px',
            height: '20px',
            position: 'absolute',
            top: { xs: '25px', sm: '55px', lg: '24px' },
            left: { xs: '25px', sm: '55px', lg: '24px' },
            cursor: 'pointer',
          }}
          onClick={() => handleGoToPrevious(type)}
          alt=""
        />
      )}

      {!(type === 'verification' || type === 'forgetPassword' || type === 'forgetVerify') && (
        <Typography
          variant="inherit"
          component="span"
          color={'text.primary'}
          sx={{ mt: '12px', fontWeight: '400', fontSize: '20px' }}
        >
          {formTypes[type]?.title}
        </Typography>
      )}

      {/*<Typography variant="inherit" component="span" color={'text.primary'}*/}
      {/*            sx={{mt: '12px', fontWeight: '400', fontSize: '20px',}}>*/}
      {/*    {*/}
      {/*        formTypes[type].title*/}
      {/*    }*/}
      {/*</Typography>*/}

      {!(type === 'register' || type === 'forgetPassword' || type === 'forgetVerify' || type === 'setNewPass') && (
        <Box sx={{ width: '100%', height: '100%', display: { lg: 'none', xs: 'block' } }}>
          <SwiperSlider />
        </Box>
      )}

      <Box className={styles.formWrapper} sx={{ width: { xs: '327px', lg: '320px' } }}>
        {!!formTypes[type]?.subTitle && (
          <>
            <Typography
              variant="inherit"
              component="span"
              color={'text.primary'}
              sx={{ mt: '12px', fontWeight: '400', fontSize: '20px' }}
            >
              {formTypes[type].title}
            </Typography>
            <Typography
              variant="inherit"
              component="span"
              color={'text.secondary'}
              fontFamily={pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Regular'}
              sx={{ fontSize: '14px', fontWeight: '400', mt: '8px', textAlign: 'center' }}
            >
              {formTypes[type].subTitle}
            </Typography>
          </>
        )}

        {(type === 'forgetVerify' || type === 'verification') && (
          <Typography
            variant="inherit"
            component="span"
            color={'primary.main'}
            fontFamily={pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Regular'}
            sx={{
              fontSize: '14px',
              fontWeight: '400',
              mt: '8px',
              textAlign: 'center',
            }}
          >
            {verifyTimer}
          </Typography>
        )}

        <Box
          component={'form'}
          onSubmit={type === 'verification' || type === 'forgetVerify' ? handleSubmitForm : formik.handleSubmit}
          sx={{ width: '100%' }}
        >
          {type === 'verification' || type === 'forgetVerify' ? (
            <Box
              sx={{
                width: '100%',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                mt: { xs: '40px', lg: type === 'forgetVerify' ? '48px' : '67px' },
              }}
              id={type}
            >
              <VerificationInput
                value={verificationCode}
                onChange={(value) => setVerificationCode(value)}
                removeDefaultStyles
                validChars={'0-9'}
                placeholder={' '}
                autoFocus={true}
                classNames={{
                  container: `${styles.vrfChar_container} ${verificationError && styles.verifyShake}`,
                  characterInactive: styles.vrfChar_inactive,
                  character: `${styles.vrfChar} ${verificationError && styles.verifyShake} ${
                    !!wrongCode && styles.verifyError
                  } ${pageDirection === 'ltr' ? styles.enFont : styles.faFont}`,
                  characterSelected: `${styles.vrfChar_selected}`,
                }}
              />

              {!!wrongCode && (
                <Box sx={{ width: '100%', mt: '8px' }}>
                  <Typography
                    variant="inherit"
                    component="span"
                    color={'error.main'}
                    sx={{ fontWeight: '400', fontSize: '14px' }}
                  >
                    {wrongCode}
                  </Typography>
                </Box>
              )}
            </Box>
          ) : (
            formTypes[type]?.inputs.map((item: string[], index: string | number) => {
              return (
                <Box
                  key={index}
                  sx={{
                    width: '100%',
                    mt:
                      formTypes[type].inputs.length > 2 && item[2].includes('assword')
                        ? '30px'
                        : formTypes[type].inputs.length === 1 && item[2].includes('password') && type === 'setNewPass'
                        ? {
                            xs: '120px',
                            sm: '175px',
                            lg: '56px',
                          }
                        : {
                            xs: '60px',
                            lg: '56px',
                          },
                  }}
                >
                  <Typography
                    variant="inherit"
                    component="span"
                    color={'text.secondary'}
                    fontFamily={'IRANSansXFaNum-DemiBold'}
                    sx={{
                      fontSize: '16px',
                      // fontWeight: '600',
                      fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                      lineHeight: '24px',
                      alignSelf: 'flex-start',
                    }}
                    // onClick={() => focusInput.current?.focus()}
                  >
                    {item[0]}
                  </Typography>

                  <FormControl fullWidth error={type === 'register' && item[2] === 'phone'}>
                    <TextField
                      // inputRef={focusInput-index}
                      dir={item[2] === 'phone' ? 'rtl' : ''}
                      // ref={inputRef}
                      value={
                        item[2] === 'phone'
                          ? formik.values.phone
                          : item[2] === 'password'
                          ? formik.values.password
                          : formik.values.repeatPassword
                      }
                      onChange={(event) => {
                        formik.handleChange(item[2])(event);
                        handleChangeInputValue(item[2], event);
                      }}
                      error={
                        (item[2] === 'phone'
                          ? formik.touched.phone
                          : item[2] === 'password'
                          ? formik.touched.password
                          : formik.touched.repeatPassword) &&
                        Boolean(
                          item[2] === 'phone'
                            ? formik.errors.phone
                            : item[2] === 'password'
                            ? formik.errors.password
                            : formik.errors.repeatPassword,
                        )
                      }
                      helperText={
                        (item[2] === 'phone'
                          ? formik.touched.phone
                          : item[2] === 'password'
                          ? formik.touched.password
                          : formik.touched.repeatPassword) &&
                        (item[2] === 'phone'
                          ? formik.errors.phone
                          : item[2] === 'password'
                          ? formik.errors.password
                          : formik.errors.repeatPassword)
                      }
                      // color={'primary.main'}
                      fullWidth
                      id="phone-input"
                      name={item[2]}
                      placeholder={item[1]}
                      type={
                        item[2].includes('phone')
                          ? 'tel'
                          : (!item[2].includes('repeat') && showPassword) ||
                            (item[2].includes('repeat') && showRepeatPassword)
                          ? 'tel'
                          : 'password'
                      }
                      autoComplete="current-password"
                      sx={{
                        '& input::placeholder': {
                          fontSize: '14px',
                          color: 'text.secondary',
                        },
                        width: '100%',
                        height: '48px',
                        mt: '8px',
                        '& .MuiInputAdornment-root': { margin: '0' },
                        '& legend': { display: 'none' },
                        '& fieldset': { top: 0, borderRadius: '12px' },
                        '&:hover .fieldset': {
                          border: '1px solid red',
                        },
                        '& .MuiOutlinedInput-root:hover': {
                          '& > fieldset': {
                            borderColor: 'primary.main',
                          },
                        },
                        // '& fieldset:hover': {top: 0, borderRadius: '12px', border: '1px solid red'},
                        '& .MuiOutlinedInput-root': {
                          borderRadius: '12px',
                          backgroundColor: '#ffffff',
                          '&.Mui-focused fieldset': {
                            borderColor: 'primary.dark',
                          },
                          '&:hover:before fieldset': {
                            borderColor: 'primary.main',
                          },
                          padding: '0',
                          color:
                            (item[2] === 'phone' && formik.errors.phone) ||
                            (item[2] === 'password' && formik.errors.password) ||
                            (item[2] === 'repeatPassword' && formik.errors.repeatPassword)
                              ? 'error.main'
                              : 'primary.main',
                          // color: 'primary.main',
                          margin: '0',
                        },
                        '& .MuiInputBase-input':
                          pageDirection === 'ltr' && item[2] === 'phone'
                            ? {
                                // fontFamily: pageDirection === "ltr" ? 'IRANSansX-Regular' : 'IRANSansXFaNum-Regular',
                                fontFamily: pageDirection === 'ltr' ? 'IRANSansX-DemiBold' : 'IRANSansXFaNum-DemiBold',
                                fontSize: '14px',
                                // fontWeight: '600',
                                padding: '16.5px 14px',
                                paddingLeft: '8px',
                              }
                            : {
                                // fontFamily: pageDirection === "ltr" ? 'IRANSansX-Regular' : 'IRANSansXFaNum-Regular',
                                fontFamily: pageDirection === 'ltr' ? 'IRANSansX-DemiBold' : 'IRANSansXFaNum-DemiBold',
                                fontSize: '14px',
                                // fontWeight: '600',
                              },
                        '& .MuiOutlinedInput-notchedOutline': {
                          padding: 0,
                        },
                      }}
                      InputProps={{
                        autoComplete: 'off',
                        endAdornment: (
                          <InputAdornment position="start">
                            {item[2].includes('word') ? (
                              <IconButton
                                sx={{ mr: '4px' }}
                                aria-label="toggle password visibility"
                                onClick={() => handleClickShowPassword(item[2].includes('repeat'))}
                                edge="end"
                              >
                                {item[2].includes('repeat') ? (
                                  showRepeatPassword ? (
                                    <Box
                                      component={'img'}
                                      src={formInputValues.repeatPassword ? visibleEye.src : visibleEyeDisabled.src}
                                      sx={{
                                        width: '24px',
                                        height: '24px',
                                      }}
                                    />
                                  ) : (
                                    <Box
                                      component={'img'}
                                      src={formInputValues.repeatPassword ? invisibleEye.src : invisibleEyeDisabled.src}
                                      sx={{
                                        width: '24px',
                                        height: '24px',
                                      }}
                                    />
                                  )
                                ) : showPassword ? (
                                  <Box
                                    component={'img'}
                                    src={formInputValues.password ? visibleEye.src : visibleEyeDisabled.src}
                                    sx={{
                                      width: '24px',
                                      height: '24px',
                                    }}
                                  />
                                ) : (
                                  <Box
                                    component={'img'}
                                    src={formInputValues.password ? invisibleEye.src : invisibleEyeDisabled.src}
                                    sx={{
                                      width: '24px',
                                      height: '24px',
                                    }}
                                  />
                                )}
                              </IconButton>
                            ) : (
                              <Select
                                value={areaCode}
                                onChange={handleChangeAreaCode}
                                displayEmpty
                                renderValue={(value: any) => {
                                  // TODO: optimize setting default country for areaCode
                                  return (
                                    areaCode && (
                                      <Box
                                        dir={'ltr'}
                                        sx={{
                                          width: '55px',
                                          display: 'flex',
                                          alignItems: 'center',
                                          justifyContent: 'center',
                                          ml: '5px',
                                        }}
                                      >
                                        <Box
                                          component={'img'}
                                          src={areaCode?.icon}
                                          sx={{
                                            width: '22px',
                                            height: '22px',
                                            ml: pageDirection === 'ltr' && item[2] === 'phone' ? '0' : '5px',
                                            mr: pageDirection === 'ltr' && item[2] === 'phone' ? '5px' : '0',
                                          }}
                                        />
                                        <Typography
                                          variant="inherit"
                                          component="span"
                                          fontFamily={
                                            pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold'
                                          }
                                          // fontWeight={'600'}
                                          color={'text.secondary'}
                                          sx={{ mt: '3px' }}
                                        >
                                          {areaCode?.phone_prefix}
                                        </Typography>
                                      </Box>
                                    )
                                  );
                                }}
                                MenuProps={{
                                  elevation: 0,
                                  anchorOrigin: {
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                  },
                                  transformOrigin: {
                                    vertical: 'top',
                                    horizontal: 'left',
                                  },
                                  PaperProps: {
                                    sx: {
                                      mt: '8px',
                                      boxShadow: '0px 4px 38px rgba(68,68,68,0.1)',
                                      width: '200px',
                                      height: 'auto',
                                      maxHeight: '176px',
                                      borderRadius: '10px',
                                      overflowY: 'auto',
                                    },
                                  },
                                }}
                                sx={{
                                  // minWidth: '110px',
                                  height: '100%',
                                  ml: '8px',
                                  '& .muirtl-bpeome-MuiSvgIcon-root-MuiSelect-icon': { right: 'auto' },
                                  '& .muirtl-hfutr2-MuiSvgIcon-root-MuiSelect-icon': { right: 'auto' },
                                  '& .muirtl-ittuaa-MuiInputAdornment-root': { margin: '0' },
                                  '& .muirtl-keuqm-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input.muirtl-keuqm-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input.muirtl-keuqm-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input':
                                    {
                                      px: '12px',
                                      pl: '22px',
                                      width: 'fit-content',
                                    },
                                  '& fieldset': { border: 'none' },
                                }}
                              >
                                {phoneCodes?.map((item: InternationalCode, index: number) => {
                                  return (
                                    <MenuItem value={JSON.stringify(item)} key={index}>
                                      <Box
                                        sx={{
                                          width: '100%',
                                          display: 'flex',
                                          alignItems: 'center',
                                          justifyContent: 'space-between',
                                        }}
                                      >
                                        <Typography
                                          variant="inherit"
                                          component="span"
                                          color={'text.secondary'}
                                          fontFamily={'IRANSansX-Regular'}
                                          fontStyle={'normal'}
                                          sx={{ mt: '3px' }}
                                        >
                                          {item.phone_prefix}
                                        </Typography>
                                        <Box
                                          sx={{
                                            display: 'flex',
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                            ml: '5px',
                                          }}
                                        >
                                          <Typography
                                            variant="inherit"
                                            component="span"
                                            color={'text.secondary'}
                                            fontFamily={'IRANSansX-Regular'}
                                            sx={{ mt: '3px' }}
                                          >
                                            {item.iso_code}
                                          </Typography>
                                          <Box
                                            component={'img'}
                                            src={item.icon}
                                            sx={{
                                              width: '22px',
                                              height: '22px',
                                              ml: '13px',
                                            }}
                                            alt=""
                                          />
                                        </Box>
                                      </Box>
                                    </MenuItem>
                                  );
                                })}
                              </Select>
                            )}
                          </InputAdornment>
                        ),
                      }}
                    />
                    {type === 'register' && !!registerDuplicatePhoneNum && item[2] === 'phone' && (
                      <FormHelperText sx={{ mt: '10px' }} error={true}>
                        {registerDuplicatePhoneNum}
                      </FormHelperText>
                    )}
                  </FormControl>
                </Box>
              );
            })
          )}

          {type === 'enterViaPass' && (
            <Box
              sx={{
                width: '100%',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
                mt: { xs: '22px', sm: '50px', lg: '26px' },
              }}
            >
              <FormControlLabel
                label={
                  <Typography variant="inherit" component="span" color={'text.disabled'} fontSize={'14px'}>
                    {t('input_field:remind_me')}
                  </Typography>
                }
                control={
                  <Checkbox
                    size={'small'}
                    icon={<RadioButtonUnchecked color={'disabled'} />}
                    checkedIcon={<CheckCircle />}
                  />
                }
              />
              <NextLink href={`/${PATHS.USER_FORGET}`} passHref>
                <Link href={`#`} underline="none" fontSize={'14px'}>
                  {t('input_field:forget_pass')}
                </Link>
              </NextLink>
            </Box>
          )}

          {(type === 'verification' || type === 'forgetVerify') && (
            <Box
              sx={{
                width: '100%',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
                mt: { xs: !!wrongCode ? '20px' : '52px', lg: !!wrongCode ? '3px' : '35px' },
              }}
            >
              <Link
                href={`#`}
                underline="none"
                sx={{ display: 'flex', alignItems: 'center' }}
                onClick={() => router.reload()}
              >
                <Box component={'img'} src={editMobileNum.src} alt="edit-mobile-num" />
                <Typography variant="inherit" component="span" fontSize={'14px'} sx={{ ml: '4px' }}>
                  {t('input_field:modify_mobile')}
                </Typography>
              </Link>
              <Link
                href="#"
                underline="none"
                color={resendVerifyCode ? 'primary.main' : 'text.disabled'}
                onClick={(e) => {
                  e.preventDefault();
                  handleConfirmVerificationCode(`resend-${type}`);
                }}
                sx={{ display: 'flex', alignItems: 'center' }}
              >
                <Box component={'img'} src={resendVerifyCode ? retry.src : disabledRetry.src} alt="retry" />
                <Typography variant="inherit" component="span" fontSize={'14px'} sx={{ ml: '4px' }}>
                  {t('input_field:resend_verification')}
                </Typography>
              </Link>
            </Box>
          )}

          <Button
            variant="contained"
            type={'submit'}
            fullWidth
            sx={{
              '&:hover': {
                boxShadow: 'none',
              },
              boxShadow: 'none',
              // fontWeight: '600',
              borderRadius: '12px',
              height: '48px',
              mt:
                type === 'enterViaPhone'
                  ? {
                      xs: '64px',
                      sm: '93px',
                      lg: '68px',
                    }
                  : type === 'verification' || type === 'forgetVerify'
                  ? '12px'
                  : type === 'register'
                  ? '60px'
                  : type === 'forgetPassword'
                  ? {
                      xs: '72px',
                      lg: '40px',
                    }
                  : type === 'setNewPass'
                  ? '70px'
                  : '5px',
              // mb: type === 'register' ? '35px' : 'inherit',
              fontSize: '14px',
            }}
            disabled={(type === 'forgetVerify' || type === 'verification') && resendVerifyCode}
          >
            {buttonLoading == true ? (
              <FacebookCircularProgress />
            ) : (
              <Typography variant="body2" pl={2}>
                {type === 'register'
                  ? t('input_field:register')
                  : type === 'verification' || type === 'forgetPassword' || type === 'forgetVerify'
                  ? t('input_field:confirm')
                  : type === 'setNewPass'
                  ? t('input_field:confirm_new_pass')
                  : type === 'enterViaPass'
                  ? t('input_field:enter')
                  : t('input_field:enter_register')}
              </Typography>
            )}
          </Button>
        </Box>

        {!(type === 'register' || type === 'forgotPassword') && (
          <Typography
            variant="inherit"
            component="span"
            color={'text.secondary'}
            sx={{
              fontSize: pageDirection === 'rtl' ? '14px' : '12px',
              fontWeight: '400',
              mt: '12px',
            }}
          >
            <Trans i18nKey="input_field:accept_zimapay_rules" values={{ rule: t('common:rules') }}>
              زیماپی است <Link href="https://zimapay.com/rules/" underline="none" fontSize={'14px'}></Link> ورود شما به
              منزله پذیرش
            </Trans>
          </Typography>
        )}
      </Box>
    </>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export { InputField };
