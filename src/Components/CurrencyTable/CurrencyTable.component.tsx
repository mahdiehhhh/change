import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

import { Box, Link, Typography, TableSortLabel, Pagination } from '@mui/material';
import rectDollarIcon from '/public/assets/images/Rect-dollar-icon.svg';
import rectEuroIcon from '/public/assets/images/Rect-euro-icon.svg';
import rectLiraIcon from '/public/assets/images/Rect-lir-icon.svg';
import increaseIcon from '/public/assets/images/Increase-icon.svg';
import decreaseIcon from '/public/assets/images/decrease-icon.svg';
import starIcon from '/public/assets/images/Star-icon.svg';
import detailIcon from '/public/assets/images/vertical-3point.svg';
import { Chart } from '../Chart/Chart.component';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { useEffect, useState } from 'react';
import curlyVector from '/public/assets/images/curlyVector.svg';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { Trans, useTranslation } from 'next-i18next';
import { useI18nTrDir } from '../../utils/CustomHooks/i18nTrDir';
import { visuallyHidden } from '@mui/utils';
import { PATHS } from '../../configs/routes.config';
import backwardArrowDeposit from '*.svg';
import IconButton from '@mui/material/IconButton';
import coloredStarIcon from '/public/assets/images/colored-star-icon.svg';
import goldenStarIcon from '/public/assets/images/golden-favorite -star-icon.svg';
import upDownSortIcon from '/public/assets/images/up-down-sort-icon.svg';
import { localesNS } from '../../utils/Data/locales_NameSpace.utils';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import { fetchRemoveCurrencyFavorite } from '../../store/slices/remove_currency_from_favorite.slice';
import { fetchAddCurrencyFavorite } from '../../store/slices/add_currency_to_favorite.slice';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';

interface CurrencyTableType {
  mode: string;
  AllFilteredResultsArray?: [];
  searchedText?: string;
}

interface DashboardCurrency {
  title?: string;
  image?: string;
  change_percent?: string;
  color?: string;
  iso_code?: string;
  symbol?: string;
  type?: string;
  price_history?: PriceHistorySample[];
  buy_price?: string;
  sell_price?: string;
  currency?: string;
  wallet_logo?: string;
  balance?: number;
  rial_balance?: number;
  benefit?: number;
}

interface ColumnsSample {
  type: string;
  id: string;
  label: string;
  width: string;
  maxWidth: string;
  align: string;
  isSortable?: boolean;
}

interface PriceHistorySample {
  average_buy_price: string;
  average_sell_price: string;
  day: string;
}

const CurrencyTable = ({ mode, AllFilteredResultsArray, searchedText }: CurrencyTableType) => {
  const { t, pageDirection } = useI18nTrDir();
  const theme = useTheme();
  const xl = useMediaQuery(theme.breakpoints.up('xl'));

  let rows: [] = [];
  const currenciesDashResult = useAppSelector((state) => state.CurrenciesDashboard.currenciesDashResp);
  const userFinanceResult = useAppSelector((state) => state.UserFinance.userFinanceResp);
  const exchangeCurrenciesInfoResult = useAppSelector((state) => state.ExchangeCurrenciesInfo);

  const dispatch = useAppDispatch();

  const [currenciesResultState, setCurrenciesResultState] = useState<any>();
  const [order, setOrder] = React.useState<Order>('asc');
  const [orderBy, setOrderBy] = React.useState<keyof any>('calories');

  let updateClock: string = '**:**';
  let updateDate: string = '*/*/****';

  const [favoriteCurrenciesState, setFavoriteCurrenciesState] = useState<any>({});
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const itemsPerPage = 10;

  useEffect(() => {
    mode === 'dashboard' || mode === 'exchange'
      ? (rows =
          (mode === 'dashboard' ? currenciesDashResult : exchangeCurrenciesInfoResult?.ExchangeCurrenciesInfoResp)
            ?.currencies &&
          (mode === 'dashboard'
            ? currenciesDashResult
            : exchangeCurrenciesInfoResult?.ExchangeCurrenciesInfoResp
          )?.currencies.map((item: DashboardCurrency) => createData(item)))
      : (rows = userFinanceResult && userFinanceResult.wallets.map((item: DashboardCurrency) => createData(item)));
    setCurrenciesResultState(rows);
    const favoriteCurrenciesStatusArray = rows?.reduce((accumulator, value: any) => {
      return { ...accumulator, [value.iso_code]: value.favorite };
    }, {});
    setFavoriteCurrenciesState(favoriteCurrenciesStatusArray);
    !!rows?.length && setTotalPages(Math.ceil(+rows?.length / itemsPerPage));
    updateDate = (
      mode === 'dashboard'
        ? currenciesDashResult
        : mode === 'exchange'
        ? exchangeCurrenciesInfoResult?.ExchangeCurrenciesInfoResp
        : undefined
    )?.latest_update.split(' ')[0];
    updateClock = (
      mode === 'dashboard'
        ? currenciesDashResult
        : mode === 'exchange'
        ? exchangeCurrenciesInfoResult?.ExchangeCurrenciesInfoResp
        : undefined
    )?.latest_update.split(' ')[1];
  }, [currenciesDashResult, userFinanceResult, exchangeCurrenciesInfoResult]);

  const handleRequestSort = (event: React.MouseEvent<unknown>, property: keyof any) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  useEffect(() => {
    if (mode === 'exchange') {
      stableSort(currenciesResultState, getComparator(order, orderBy));
    }
  }, [orderBy, order]);

  function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
    if (Number(b[orderBy]) < Number(a[orderBy])) {
      return -1;
    }
    if (Number(b[orderBy]) > Number(a[orderBy])) {
      return 1;
    }
    return 0;
  }

  type Order = 'asc' | 'desc';

  function getComparator<Key extends keyof any>(
    order: Order,
    orderBy: Key,
  ): (a: { [key in Key]: number | string }, b: { [key in Key]: number | string }) => number {
    return order === 'desc'
      ? (a, b) => {
          return descendingComparator(a, b, orderBy);
        }
      : (a, b) => {
          return -descendingComparator(a, b, orderBy);
        };
  }

  function stableSort<T>(array: readonly T[], comparator: (a: T, b: T) => number) {
    const stabilizedThis = array?.map((el, index) => [el, index] as [T, number]);
    stabilizedThis?.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) {
        return order;
      }
      return a[1] - b[1];
    });
    // return stabilizedThis.map((el) => el[0]);
    setCurrenciesResultState(stabilizedThis?.map((el) => el[0]));
  }

  function createData(object: DashboardCurrency) {
    if (mode === 'dashboard' || mode === 'exchange') {
      const nameIcon = { title: object.title, image: object.image };
      let clone = Object.assign({}, { nameIcon, ...object });
      // @ts-ignore
      delete clone.image;
      // @ts-ignore
      delete clone.title;
      return { ...clone };
    } else if (mode === 'wallet') {
      const nameIcon = { title: object.title, image: object.wallet_logo };
      let clone = Object.assign({}, { nameIcon, ...object });
      // @ts-ignore
      delete clone.wallet_logo;
      // @ts-ignore
      delete clone.title;
      return { ...clone };
    }
  }

  const dashboardColumns = [
    { type: 'name-icon', id: 'nameIcon', label: '', width: '50px', maxWidth: '16%', align: 'center' },
    { type: 'change-icon', id: 'change_percent', label: '', width: '30px', maxWidth: '19%', align: 'center' },
    { type: 'chart', id: 'price_history', label: '', width: '60px', maxWidth: '25%', align: 'center' },
    { type: 'text', id: 'buy_price', label: t('my_assets:buy'), width: '40px', maxWidth: '20%', align: 'center' },
    { type: 'text', id: 'sell_price', label: t('my_assets:sell'), width: '40px', maxWidth: '20%', align: 'center' },
  ];

  const walletColumns = [
    { type: 'name-icon', id: 'nameIcon', label: '', width: '150px', maxWidth: '40%', align: 'center' },
    {
      type: 'text',
      id: 'benefit',
      label: `${t('my_assets:profit')} / ${t('my_assets:loss')}`,
      width: '60px',
      maxWidth: '20%',
      align: 'center',
    },
    {
      type: 'text',
      id: 'balance',
      label: `${t('my_assets:balance')} (${t('my_assets:currency')})`,
      width: '85px',
      maxWidth: '20%',
      align: 'center',
    },
    {
      type: 'text',
      id: 'rial_balance',
      label: `${t('my_assets:balance')} (${t('my_assets:rial')})`,
      width: '85px',
      maxWidth: '20%',
      align: 'center',
    },
  ];

  const exchangeColumns = [
    {
      type: 'favorite-sign',
      id: 'favorite',
      label: '',
      width: '6%',
      maxWidth: '6%',
      align: 'left',
      isSortable: false,
    },
    {
      type: 'name-icon',
      id: 'nameIcon',
      label: t('exchange:type'),
      width: '15%',
      maxWidth: '15%',
      align: 'left',
      isSortable: false,
    },
    {
      type: 'text',
      id: 'buy_price',
      label: t('exchange:buy_price'),
      width: '15.5%',
      maxWidth: '15.5%',
      align: 'left',
      isSortable: true,
    },
    {
      type: 'text',
      id: 'sell_price',
      label: t('exchange:sell_price'),
      width: '15.5%',
      maxWidth: '15.5%',
      align: 'left',
      isSortable: true,
    },
    {
      type: 'change-icon',
      id: 'change_percent',
      label: t('exchange:change'),
      width: '12.5%',
      maxWidth: '12.5%',
      align: 'left',
      isSortable: true,
    },
    {
      type: 'text',
      id: 'change',
      label: t('exchange:change_percent'),
      width: '14.5%',
      maxWidth: '14.5%',
      align: 'left',
      isSortable: true,
    },
    {
      type: 'chart',
      id: 'price_history',
      label: t('exchange:change_chart'),
      width: '20%',
      maxWidth: '20%',
      align: 'center',
      isSortable: false,
    },
    {
      type: 'details-sign',
      id: 'details',
      label: ``,
      width: '1%',
      maxWidth: '1%',
      align: 'center',
      isSortable: false,
    },
  ];

  const handleChangePage = (event: any, page: number) => {
    setCurrentPage(page);
  };

  let desiredHeader: ColumnsSample[] | any[] = [];
  mode === 'wallet'
    ? (desiredHeader = [...walletColumns])
    : mode === 'dashboard'
    ? (desiredHeader = [...dashboardColumns])
    : (desiredHeader = [...exchangeColumns]);

  const handleAddCurrencyToFavorite = (isFavorite: boolean, iso_code: string) => {
    setFavoriteCurrenciesState((prevState: any) => ({
      ...favoriteCurrenciesState,
      [iso_code]: favoriteCurrenciesState[iso_code] ? false : true,
    }));
    const data = {
      iso_code: `${iso_code}`,
    };

    if (isFavorite) {
      dispatch(fetchRemoveCurrencyFavorite(data));
    } else {
      dispatch(fetchAddCurrencyFavorite(data));
    }
  };

  return (
    <Box
      sx={{
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      }}
    >
      <TableContainer
        component={Paper}
        sx={{ boxShadow: 'none', display: 'flex', flexDirection: 'column', alignItems: 'center' }}
      >
        <Table aria-label="simple table">
          <TableHead>
            <TableRow>
              {desiredHeader.map((column: ColumnsSample, index) => {
                return !!column.isSortable ? (
                  <TableCell
                    key={column.id}
                    align={column.align as 'inherit'}
                    sx={{
                      display: {
                        xs:
                          mode === 'exchange' &&
                          (column.id === 'change_percent' || column.id === 'change' || column.id === 'price_history')
                            ? 'none'
                            : 'table-cell',
                        sm: 'table-cell',
                      },
                      width: column.width,
                      maxWidth: column.maxWidth,
                      borderBottom: mode === 'dashboard' ? 0 : '',
                      fontSize:
                        mode === 'dashboard'
                          ? '12px'
                          : {
                              xs: '10px',
                              lg: '14px',
                            },
                      // fontWeight: mode === 'exchange' ? '400' : '600',
                      fontFamily: mode === 'exchange' ? 'IRANSansXFaNum-Regular' : 'IRANSansXFaNum-DemiBold',

                      color: mode === 'exchange' ? 'text.secondary' : 'text.primary',
                    }}
                  >
                    <TableSortLabel
                      active={orderBy === column.id}
                      direction={orderBy === column.id ? order : 'asc'}
                      IconComponent={ArrowDropDownIcon}
                      hideSortIcon={true}
                      onClick={(event) => handleRequestSort(event, column.id)}
                    >
                      {column.label}
                      {orderBy === column.id ? (
                        <Box component="span" sx={visuallyHidden}>
                          {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                        </Box>
                      ) : (
                        <Box
                          component={'img'}
                          src={upDownSortIcon.src}
                          alt={'backward icon'}
                          sx={{
                            width: '9px',
                            height: '14px',
                            ml: '7px',
                            transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                          }}
                        />
                      )}
                    </TableSortLabel>
                  </TableCell>
                ) : (
                  <TableCell
                    key={column.id}
                    align={column.align as 'inherit'}
                    sx={{
                      display: {
                        xs:
                          mode === 'exchange' &&
                          (column.id === 'change_percent' || column.id === 'change' || column.id === 'price_history')
                            ? 'none'
                            : 'table-cell',
                        sm: 'table-cell',
                      },
                      width: column.width,
                      maxWidth: column.maxWidth,
                      borderBottom: mode === 'dashboard' ? 0 : '',
                      fontSize:
                        mode === 'dashboard'
                          ? '12px'
                          : {
                              xs: '10px',
                              lg: '14px',
                            },
                      // fontWeight: mode === 'exchange' ? '400' : '600',
                      fontFamily: mode === 'exchange' ? 'IRANSansXFaNum-Regular' : 'IRANSansXFaNum-DemiBold',

                      color: mode === 'exchange' ? 'text.secondary' : 'text.primary',
                    }}
                  >
                    {column.label}
                  </TableCell>
                );
              })}
            </TableRow>
          </TableHead>
          <TableBody>
            {(mode === 'exchange'
              ? (!!searchedText
                  ? AllFilteredResultsArray?.map((item: DashboardCurrency) => createData(item))
                  : currenciesResultState
                )?.slice((currentPage - 1) * itemsPerPage, (currentPage - 1) * itemsPerPage + itemsPerPage)
              : currenciesResultState
            )?.map((row: any, index: number) => (
              <TableRow
                key={index}
                sx={{
                  '& th, & td': {
                    borderColor: 'secondary.light',
                    px: { xs: mode === 'exchange' ? '0' : 'inherit' },
                    py: { xl: '20px' },
                  },
                  '&:last-child th, &:last-child td': {
                    borderBottom: 0,
                    borderColor: 'secondary.light',
                  },
                }}
              >
                {desiredHeader.map((column: ColumnsSample) => {
                  const value = row[column.id];
                  return (
                    <TableCell
                      key={index}
                      component="th"
                      scope="row"
                      align={column.align as 'inherit'}
                      sx={{
                        display: {
                          xs:
                            mode === 'exchange' &&
                            (column.id === 'change_percent' || column.id === 'change' || column.id === 'price_history')
                              ? 'none'
                              : 'table-cell',
                          sm: 'table-cell',
                        },
                        width: column.width,
                        maxWidth: column.maxWidth,
                        '&:last-child, &:last-child':
                          mode === 'dashboard'
                            ? {
                                borderLeft: '1px solid',
                                borderColor: 'secondary.light',
                              }
                            : {},
                      }}
                    >
                      {column.type.includes('sign') ? (
                        column.type.includes('favorite') ? (
                          <Box
                            component={'img'}
                            // src={row.favorite ? goldenStarIcon.src : starIcon.src}
                            src={favoriteCurrenciesState[row?.iso_code] ? goldenStarIcon.src : starIcon.src}
                            sx={{
                              width: column.type.includes('favorite')
                                ? {
                                    xs: '16px',
                                    sm: '24px',
                                  }
                                : '4px',
                              height: column.type.includes('favorite')
                                ? {
                                    xs: '16px',
                                    sm: '24px',
                                  }
                                : '16px',
                              mt: '8px',
                              cursor: 'pointer',
                            }}
                            onClick={() => handleAddCurrencyToFavorite(row.favorite, row.iso_code)}
                          />
                        ) : (
                          <IconButton
                            color="inherit"
                            aria-label="prev button"
                            edge="start"
                            // onClick={() => router.push(`/${PATHS.USER_DASHBOARD}`)}
                            sx={{
                              width: { xs: '20px', sm: '30px' },
                              height: { xs: '20px', sm: '30px' },
                              display: 'flex',
                              justifySelf: 'flex-end',
                              padding: '0',
                              borderRadius: '5px',
                              backgroundColor: 'transparent',
                              '&:hover': {
                                bgcolor: 'secondary.light',
                              },
                            }}
                          >
                            <Box
                              component={'img'}
                              src={detailIcon.src}
                              alt={'backward icon'}
                              sx={{
                                width: '5px',
                                height: '16px',
                                transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                              }}
                            />
                          </IconButton>
                        )
                      ) : column.type.includes('icon') ? (
                        <Box
                          sx={{
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: column.type.includes('name') ? 'flex-start' : 'flex-end',
                            flexDirection: column.type.includes('name') ? 'row' : 'row-reverse',
                          }}
                        >
                          <Box
                            component={'img'}
                            src={
                              column.type.includes('name')
                                ? mode === 'dashboard' || mode === 'exchange'
                                  ? value.image
                                  : value.image
                                : +value > 0
                                ? increaseIcon.src
                                : +value < 0 && decreaseIcon.src
                            }
                            alt={
                              column.type.includes('name')
                                ? `${value.title} icon`
                                : `${+value > 0 ? 'increase icon' : +value < 0 && 'decrease icon'}`
                            }
                            sx={{
                              width: column.type.includes('name')
                                ? mode === 'dashboard'
                                  ? { xs: '24px' }
                                  : mode === 'exchange'
                                  ? {
                                      xs: '16px',
                                      sm: '24px',
                                      lg: '32px',
                                    }
                                  : {
                                      xs: '29px',
                                      sm: '35px',
                                    }
                                : '12px',
                              height: column.type.includes('name')
                                ? mode === 'dashboard'
                                  ? { xs: '24px' }
                                  : mode === 'exchange'
                                  ? {
                                      xs: '16px',
                                      sm: '24px',
                                      lg: '32px',
                                    }
                                  : '22px'
                                : '12px',

                              mr: column.type.includes('name')
                                ? mode === 'dashboard' || mode === 'exchange'
                                  ? {
                                      xs: '8px',
                                      xl: '16px',
                                    }
                                  : {
                                      xs: '6px',
                                      lg: '16px',
                                    }
                                : '0',
                              ml: !column.type.includes('name')
                                ? mode === 'dashboard' || mode === 'exchange'
                                  ? '8px'
                                  : {
                                      xs: '6px',
                                      lg: '16px',
                                    }
                                : '0',
                              display: +value === 0 ? 'none' : 'block',
                            }}
                          />
                          {mode === 'dashboard' || mode === 'wallet' ? (
                            <Typography
                              dir={'ltr'}
                              variant="inherit"
                              component="span"
                              color={+value > 0 ? 'success.main' : +value < 0 ? 'error.main' : 'text.secondary'}
                              fontFamily={pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Demibold'}
                              sx={{
                                // fontWeight: (mode === 'dashboard') ? column.type === 'name-icon' ? '600' : '400' : {
                                //     xs: '600',
                                //     lg: '400'
                                // },
                                fontFamily:
                                  mode === 'dashboard'
                                    ? column.type === 'name-icon'
                                      ? 'IRANSansXFaNum-DemiBold'
                                      : 'IRANSansXFaNum-Regular'
                                    : {
                                        xs: 'IRANSansXFaNum-DemiBold',
                                        lg: 'IRANSansXFaNum-Regular',
                                      },

                                fontSize:
                                  mode === 'dashboard'
                                    ? {
                                        xs: '12px',
                                        xl: '16px',
                                      }
                                    : {
                                        xs: '12px',
                                        lg: '16px',
                                      },
                              }}
                            >
                              {column.type.includes('name')
                                ? value.title
                                : +value < 1 && +value > 0
                                ? `${value.toFixed(1)}%`
                                : `${Math.trunc(value)}%`}
                            </Typography>
                          ) : (
                            <Box
                              sx={{
                                display: 'flex',
                                flexDirection: 'column',
                                alignItems: 'flex-start',
                              }}
                            >
                              <Typography
                                dir={'ltr'}
                                variant="inherit"
                                component="span"
                                color={+value > 0 ? 'success.main' : +value < 0 ? 'error.main' : 'text.primary'}
                                fontFamily={pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Demibold'}
                                sx={{
                                  fontWeight: '400',
                                  fontSize: {
                                    xs: '10px',
                                    sm: '12px',
                                    lg: '16px',
                                  },
                                }}
                              >
                                {column.type.includes('name')
                                  ? value.title
                                  : +value < 1 && +value > 0
                                  ? `${value.toFixed(1)}%`
                                  : `${Math.trunc(value)}%`}
                              </Typography>
                              {column.type.includes('name') && (
                                <Typography
                                  dir={'ltr'}
                                  variant="inherit"
                                  component="span"
                                  color={+value > 0 ? 'success.main' : +value < 0 ? 'error.main' : 'text.secondary'}
                                  fontFamily={pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Demibold'}
                                  sx={{
                                    fontWeight: '400',
                                    fontSize: '12px',
                                    display: {
                                      xs: 'none',
                                      sm: 'block',
                                    },
                                    mt: '0px',
                                  }}
                                >
                                  {row.iso_code}
                                </Typography>
                              )}
                            </Box>
                          )}
                        </Box>
                      ) : column.type === 'chart' ? (
                        <Box
                          sx={{
                            width: '100%',
                            display: 'flex',
                            justifyContent: 'center',
                            pr: { xs: 0, lg: '13px' },
                          }}
                        >
                          <Chart
                            mode={`line-${mode}`}
                            chartData={value}
                            chartWidth={xl ? 80 : 50}
                            chartHeight={27}
                            color={row.color}
                          />
                        </Box>
                      ) : (
                        <Typography
                          dir={'ltr'}
                          variant="inherit"
                          component="span"
                          color={
                            mode === 'dashboard'
                              ? 'text.primary'
                              : (mode === 'wallet' && column.id === 'benefit') ||
                                (mode === 'exchange' && column.id === 'change')
                              ? value > 0
                                ? 'success.main'
                                : value < 0
                                ? 'error.main'
                                : 'text.primary'
                              : 'text.primary'
                          }
                          fontFamily={pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Bold'}
                          sx={{
                            fontWeight: '400',
                            fontSize:
                              mode === 'dashboard'
                                ? {
                                    xs: '12px',
                                    xl: '16px',
                                  }
                                : mode === 'exchange'
                                ? {
                                    xs: '10px',
                                    sm: '14px',
                                    lg: '16px',
                                  }
                                : {
                                    xs: '12px',
                                    lg: '16px',
                                  },
                          }}
                        >
                          {Number(Math.trunc(value)).toLocaleString('en-US')}
                          {/*{row['iso_code'] === 'IRR' ? Number(value).toLocaleString('en-US') : Number(Number(value).toFixed(2)).toLocaleString('en-US')}*/}
                        </Typography>
                      )}
                    </TableCell>
                  );
                })}
              </TableRow>
            ))}
          </TableBody>
        </Table>
        {mode === 'exchange' && totalPages > 1 && (
          <Box
            sx={{
              width: 'auto',
              height: 'auto',
              borderRadius: '14px',
              backgroundColor: 'white',
              padding: '0 24px',
              mt: '24px',
            }}
          >
            <Pagination
              count={totalPages}
              page={currentPage}
              // defaultPage={1}
              shape="rounded"
              dir={'ltr'}
              onChange={handleChangePage}
            />
          </Box>
        )}
      </TableContainer>
      {mode === 'dashboard' && (
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            mt: '50px',
          }}
        >
          <Typography
            variant="inherit"
            component="span"
            color={'text.secondary'}
            sx={{
              fontWeight: '400',
              fontSize: '14px',
            }}
          >
            {t('my_assets:prices_are_rials')}
          </Typography>
          <Typography
            variant="inherit"
            component="span"
            color={'text.disabled'}
            fontFamily={pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Regular'}
            sx={{
              fontWeight: '400',
              fontSize: '12px',
              unicodeBidi: 'embed',
              direction: 'ltr',
            }}
          >
            <Trans
              i18nKey="my_assets:last_update"
              values={{
                date: currenciesDashResult?.latest_update.split(' ')[0],
                clock: currenciesDashResult?.latest_update.split(' ')[1],
              }}
            >
              آخرین به روز رسانی در{' '}
              <bdi
                dir={'ltr'}
                style={{ fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Regular' }}
              ></bdi>{' '}
              ساعت
            </Trans>
          </Typography>
        </Box>
      )}
    </Box>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export { CurrencyTable };
