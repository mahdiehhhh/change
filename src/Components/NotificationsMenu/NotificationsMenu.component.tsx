import * as React from 'react';
import {
  Box,
  Menu,
  Divider,
  IconButton,
  Typography,
  Tooltip,
  Badge,
  Link,
  ListItem,
  Button,
  ListItemButton,
  List,
  Pagination,
  Skeleton,
} from '@mui/material';
import bell from '/public/assets/images/bell.svg';
import bulletPoint from '/public/assets/images/BulletPoint.svg';
import notifFilterIcon from '/public/assets/images/notif-filter-icon.svg';
import readNotifIcon from '/public/assets/images/read-notif-icon.svg';
import unreadNotifIcon from '/public/assets/images/unread-notif-icon.svg';
import backArrow from '/public/assets/images/Back-arrow.svg';
import { CounterBadge } from 'Components';
import { notifications } from 'utils/Data/data.utils';
import jmoment from 'jalali-moment';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { useEffect, useState } from 'react';
import { fetchUserUnreadNotifications } from '../../store/slices/user_unread_notifications.slice';
import { useI18nTrDir } from '../../utils/CustomHooks/i18nTrDir';
import { localesNS } from '../../utils/Data/locales_NameSpace.utils';
import { PATHS } from '../../configs/routes.config';
import { fetchUserAllNotifications } from '../../store/slices/user_all_notifications.slice';
import NextLink from 'next/link';

interface Props {
  mode: string;
}

interface NotificationsSample {
  id: string;
  type: string;
  message: string;
  url: string;
  is_read?: number;
  date: string;
  time: string;
}

const NotificationsMenu = ({ mode }: Props) => {
  const { t, pageDirection } = useI18nTrDir();

  const [anchorEl, setAnchorEl] = useState(null);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [todayNotificationsState, setTodayNotificationsState] = useState<NotificationsSample[]>([]);
  const [yesterdayNotificationsState, setYesterdayNotificationsState] = useState<NotificationsSample[]>([]);
  const [archiveNotificationsState, setArchiveNotificationsState] = useState<NotificationsSample[]>([]);
  const [unreadNotificationsNum, setUnreadNotificationsNum] = useState('');
  const [pageReady, setPageReady] = useState(false);

  const itemsPerPage = 10;

  const dispatch = useAppDispatch();
  const userUnreadNotificationsResult = useAppSelector((state) => state.UserUnreadNotifications);
  const userAllNotificationsResult = useAppSelector((state) => state.UserAllNotifications);

  let todayNotifications: NotificationsSample[] = [...todayNotificationsState];
  let yesterdayNotifications: NotificationsSample[] = [...yesterdayNotificationsState];
  let archiveNotifications: NotificationsSample[] = [...archiveNotificationsState];

  useEffect(() => {
    if (mode === 'menu') {
      dispatch(fetchUserUnreadNotifications());
    }
    if (mode === 'total') {
      dispatch(fetchUserAllNotifications(currentPage));
    }
  }, []);

  useEffect(() => {
    if (!userAllNotificationsResult.loading && !!userAllNotificationsResult.userAllNotificationsResp) {
      setPageReady(true);
    } else {
      setPageReady(false);
    }
  }, [userAllNotificationsResult]);

  useEffect(() => {
    if (!!userAllNotificationsResult.userAllNotificationsResp?.notifications.length) {
      todayNotifications = [];
      yesterdayNotifications = [];
      archiveNotifications = [];
      userAllNotificationsResult.userAllNotificationsResp?.notifications.forEach((item: NotificationsSample) => {
        item.date === todayDate
          ? todayNotifications.push(item)
          : item.date === yesterdayDate
          ? yesterdayNotifications.push(item)
          : archiveNotifications.push(item);
      });
      setTodayNotificationsState(todayNotifications);
      setYesterdayNotificationsState(yesterdayNotifications);
      setArchiveNotificationsState(archiveNotifications);
      setTotalPages(Math.ceil(+userAllNotificationsResult.userAllNotificationsResp?.total_messages / itemsPerPage));
      setUnreadNotificationsNum(userAllNotificationsResult.userAllNotificationsResp?.unread_messages);
    }
  }, [userAllNotificationsResult]);

  const open = Boolean(anchorEl);

  const handleClick = (event: any) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  let todayDate = jmoment(new Date(), 'YYYY/MM/DD').locale('fa').format('YYYY/MM/DD');
  let yesterdayDate = jmoment(new Date(new Date().setDate(new Date().getDate() - 1)), 'YYYY/MM/DD')
    .locale('fa')
    .format('YYYY/MM/DD');

  // notifications.data.notifications.forEach(item => {
  //     item.date === todayDate ? todayNotifications.push(item) : item.date === yesterdayDate ? yesterdayNotifications.push(item) : archiveNotifications.push(item)
  // })

  const notificationsList = [
    { date: t('common:today'), data: todayNotificationsState },
    { date: t('common:yesterday'), data: yesterdayNotificationsState },
    { date: t('common:archive'), data: archiveNotificationsState },
  ];

  const handleChangePage = (event: any, page: number) => {
    setCurrentPage(page);
  };

  useEffect(() => {
    dispatch(fetchUserAllNotifications(currentPage));
  }, [currentPage]);

  return (
    <>
      {mode === 'menu' && (
        <Box sx={{ display: { xs: 'none', sm: 'flex' }, alignItems: 'center', textAlign: 'center' }}>
          <Tooltip title={t('notifications_menu:notifications_fa')}>
            <IconButton
              disableRipple={true}
              onClick={(event) => handleClick(event)}
              size="small"
              sx={{
                pr: 0,
                mr: '5px',
                '&:hover': {
                  '.muirtl-i4bv87-MuiSvgIcon-root': {
                    borderRadius: '50px',
                    backgroundColor: 'rgba(0,0,0,0.1)',
                  },
                },
              }}
              aria-controls={open ? 'account-menu' : undefined}
              aria-haspopup="true"
              aria-expanded={open ? 'true' : undefined}
            >
              <Box
                sx={{
                  width: '32px',
                  height: '32px',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderRadius: '50px',
                  backgroundColor: '#ffffff',
                  ml: '20px',
                }}
              >
                <Badge
                  color="error"
                  variant={true ? 'dot' : undefined}
                  anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
                >
                  <Box
                    component={'img'}
                    src={bell.src}
                    alt={'question-mark'}
                    sx={{
                      width: '17px',
                      height: '20px',
                    }}
                  />
                </Badge>
              </Box>
            </IconButton>
          </Tooltip>
        </Box>
      )}

      {mode === 'menu' ? (
        <Menu
          anchorEl={anchorEl}
          id="account-menu"
          open={open}
          onClose={handleClose}
          onClick={handleClose}
          PaperProps={{
            // elevation: 0,
            sx: {
              boxShadow: '0px 4px 38px rgba(68, 68, 68, 0.1)',

              width: '420px',
              height: 'auto',
              padding: '10px 15px',
              overflow: 'visible',
              overflowX: 'hidden',
              borderRadius: '24px',
              mt: 1.5,
              '& .MuiAvatar-root': {
                width: 32,
                height: 32,
                ml: -0.5,
                mr: 1,
              },
            },
          }}
          transformOrigin={{ horizontal: 'left', vertical: 'top' }}
          anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
        >
          <Box
            sx={{
              width: '100%',
              height: 'auto',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'space-between',
              mb: '10px',
              px: '10px',
            }}
          >
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <Box
                component={'img'}
                src={bulletPoint.src}
                alt={'bullet-point'}
                sx={{
                  width: '24px',
                  height: '24px',
                  transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                }}
              />
              <Typography
                variant="inherit"
                component="span"
                color={'text.primary'}
                sx={{
                  // fontWeight: {xs: '600'},
                  fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                  fontSize: { xs: '16px', xl: '16px' },
                  ml: '10px',
                }}
              >
                {t('notifications_menu:notifications_ar')}
              </Typography>
            </Box>
            <CounterBadge
              value={userUnreadNotificationsResult?.userUnreadNotificationsResp?.count}
              width={'40px'}
              height={'24px'}
              bgColor={'error.light'}
              textColor={'error.main'}
            />
          </Box>

          {userUnreadNotificationsResult?.userUnreadNotificationsResp?.notifications
            ?.slice(-3)
            .map((item: NotificationsSample, index: number) => {
              return (
                <Box key={item.id}>
                  <List sx={{ p: '0', my: '0' }}>
                    <ListItem key={index} disablePadding>
                      <ListItemButton
                        sx={{
                          '&:hover': {
                            bgcolor: 'secondary.main',
                          },
                          borderRadius: '16px',
                          padding: '16px 11px',
                          position: 'relative',
                        }}
                      >
                        <Box
                          sx={{
                            display: 'grid',
                            gridTemplateColumns: '12px 95%',
                            width: '100%',
                            height: 'auto',
                          }}
                        >
                          <Box
                            sx={{
                              width: '11px',
                              height: '11px',
                              backgroundColor: '#EB5757',
                              borderRadius: '50%',
                              alignSelf: 'flex-start',
                              mt: '6px',
                            }}
                          ></Box>
                          <Box
                            sx={{
                              display: 'flex',
                              flexDirection: 'column',
                            }}
                          >
                            <Typography
                              variant="inherit"
                              component="span"
                              color={'text.primary'}
                              sx={{
                                fontSize: '14px',
                                fontWeight: '400',
                                ml: '10px',
                                alignSelf: 'flex-start',
                                textAlign: 'justify',
                              }}
                            >
                              {item.message}
                            </Typography>
                            <Box>
                              <Typography
                                variant="inherit"
                                component="span"
                                color={'text.secondary'}
                                sx={{
                                  fontSize: '12px',
                                  fontWeight: '400',
                                  ml: '10px',
                                  alignSelf: 'flex-start',
                                  textAlign: 'justify',
                                }}
                              >
                                {`${item.date} ${t('notifications_menu:clock')} ${item.time}`}
                              </Typography>
                            </Box>
                          </Box>
                        </Box>
                      </ListItemButton>
                    </ListItem>
                  </List>
                  {notifications.data.notifications?.slice(-3).length - 1 !== index && (
                    <Divider variant={'fullWidth'} sx={{ mx: '18px', borderColor: 'secondary.main' }} />
                  )}
                </Box>
              );
            })}

          <Box>
            <Divider
              variant={'fullWidth'}
              sx={{
                my: '16px',
              }}
            />

            <NextLink href={`/${PATHS.USER_NOTIFICATIONS}`} passHref>
              <Link href="#" underline="none">
                <Typography
                  variant="inherit"
                  component="span"
                  color={'primary.main'}
                  sx={{
                    fontSize: { xs: '12px', sm: '14px' },
                    fontWeight: '400',
                    px: '10px',
                    cursor: 'pointer',
                  }}
                >
                  {t('notifications_menu:check_all_notifications')}
                </Typography>
              </Link>
            </NextLink>
          </Box>
        </Menu>
      ) : !pageReady ? (
        <Box
          sx={{
            width: '100%',
            // height: '100%',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Box
            sx={{
              width: '100%',
              height: 'auto',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'space-between',
              mb: '10px',
              // px: '10px'
            }}
          >
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <Skeleton
                animation="wave"
                sx={{
                  width: '180px',
                  height: '30px',
                  borderRadius: '12px',
                }}
              />
            </Box>

            <Skeleton
              animation="wave"
              variant="rectangular"
              sx={{
                width: { xs: '32px', sm: '48px' },
                height: { xs: '32px', sm: '40px' },
                borderRadius: '12px',
              }}
            />
          </Box>

          <Box
            sx={{
              width: '100%',
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'flex-start',
              gap: '30px',
            }}
          >
            {Array.from(Array(12).keys()).map((item, index) => (
              <Skeleton
                key={index}
                animation="wave"
                sx={{
                  width: '100%',
                  height: '30px',
                  borderRadius: '12px',
                }}
              />
            ))}
          </Box>

          {/*<Box*/}
          {/*    sx={{*/}
          {/*        width: 'auto',*/}
          {/*        height: 'auto',*/}
          {/*        borderRadius: '14px',*/}
          {/*        backgroundColor: 'white',*/}
          {/*        padding: '12px 24px',*/}
          {/*        mt: '26px'*/}
          {/*    }}*/}
          {/*>*/}
          {/*    <Pagination*/}
          {/*        count={totalPages}*/}
          {/*        page={currentPage}*/}
          {/*        // defaultPage={1}*/}
          {/*        shape="rounded"*/}
          {/*        dir={'ltr'}*/}
          {/*        onChange={handleChangePage}*/}
          {/*    />*/}
          {/*</Box>*/}
        </Box>
      ) : (
        <Box
          sx={{
            width: '100%',
            // height: '100%',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Box
            sx={{
              width: '100%',
              height: 'auto',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'space-between',
              mb: '10px',
              // px: '10px'
            }}
          >
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <Box
                component={'img'}
                src={bulletPoint.src}
                alt={'bullet-point'}
                sx={{
                  width: '24px',
                  height: '24px',
                }}
              />

              <Typography
                variant="inherit"
                component="span"
                color={'text.primary'}
                sx={{
                  // fontWeight: {xs: '600'},
                  fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                  fontSize: { xs: '16px', xl: '16px' },
                  ml: '10px',
                  mr: '30px',
                }}
              >
                {t('notifications_menu:notifications_ar')}
              </Typography>
              <CounterBadge
                value={unreadNotificationsNum}
                width={'40px'}
                height={'24px'}
                bgColor={'error.light'}
                textColor={'error.main'}
              />
            </Box>

            <Button
              variant="outlined"
              sx={{
                width: '48px',
                height: '40px',
                borderRadius: '11px',
                border: 'none',
                backgroundColor: 'white',
                '&: hover': { border: 'none' },
              }}
            >
              <Box
                component={'img'}
                src={notifFilterIcon.src}
                alt={'notification-filter-icon'}
                sx={{
                  width: 'fit-content',
                  height: 'fit-content',
                }}
              />
            </Button>
          </Box>

          {notificationsList.map((item, index) => {
            return (
              !!item.data.length && (
                <Box sx={{ width: 'calc(100%)' }}>
                  <Typography
                    variant="inherit"
                    component="span"
                    color={'text.primary'}
                    sx={{
                      fontSize: '16px',
                      // fontWeight: '600',
                      fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                      lineHeight: '3',
                    }}
                  >
                    {item.date}
                  </Typography>
                  {item.data.map((notification: NotificationsSample) => {
                    return (
                      // <h1 key={notification.id}>eee</h1>
                      <Box sx={{ width: '100%' }} key={notification.id}>
                        <List sx={{ p: '0', my: '10px' }}>
                          <ListItem key={index} disablePadding>
                            <ListItemButton
                              sx={{
                                borderRadius: '16px',
                                padding: '16px 11px',
                                position: 'relative',
                                backgroundColor: 'white',
                              }}
                            >
                              <Box
                                sx={{
                                  display: 'grid',
                                  gridTemplateColumns: '24px calc(100% - 30px)',
                                  // gap: '10px',
                                  width: '100%',
                                  height: 'auto',
                                }}
                              >
                                <Box
                                  component={'img'}
                                  src={notification.is_read ? readNotifIcon.src : unreadNotifIcon.src}
                                  sx={{
                                    width: '18px',
                                    height: '18px',
                                    mt: '2px',
                                  }}
                                ></Box>
                                <Box
                                  sx={{
                                    display: 'flex',
                                    alignItems: 'center',
                                    justifyContent: 'space-between',
                                  }}
                                >
                                  <Typography
                                    variant="inherit"
                                    component="span"
                                    color={'text.primary'}
                                    sx={{
                                      fontSize: '14px',
                                      fontWeight: '400',
                                      ml: '10px',
                                      alignSelf: 'flex-start',
                                      textAlign: 'justify',
                                    }}
                                  >
                                    {notification.message}
                                  </Typography>
                                  <Box
                                    sx={{
                                      display: 'flex',
                                      alignItems: 'center',
                                    }}
                                  >
                                    <Typography
                                      variant="inherit"
                                      component="span"
                                      color={'text.secondary'}
                                      sx={{
                                        fontSize: '12px',
                                        fontWeight: '400',
                                        mx: '16px',
                                        alignSelf: 'flex-start',
                                        textAlign: 'justify',
                                      }}
                                    >
                                      {`${notification.date} ${t('notifications_menu:clock')} ${notification.time}`}
                                    </Typography>

                                    {notification.url && (
                                      <Box
                                        component={'img'}
                                        src={backArrow.src}
                                        alt={'see-more'}
                                        sx={{
                                          width: '7px',
                                          height: '14px',
                                        }}
                                      />
                                    )}
                                  </Box>
                                </Box>
                              </Box>
                            </ListItemButton>
                          </ListItem>
                        </List>
                      </Box>
                    );
                  })}
                </Box>
              )
            );
          })}

          <Box
            sx={{
              width: 'auto',
              height: 'auto',
              borderRadius: '14px',
              backgroundColor: 'white',
              padding: '12px 24px',
              mt: '26px',
            }}
          >
            <Pagination
              count={totalPages}
              page={currentPage}
              // defaultPage={1}
              shape="rounded"
              dir={'ltr'}
              onChange={handleChangePage}
            />
          </Box>
        </Box>
      )}
    </>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export { NotificationsMenu };
