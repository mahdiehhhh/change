import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import { Skeleton, Typography } from '@mui/material';
import Box from '@mui/material/Box';
import hamburgerMenu from '*.svg';
import IconButton from '@mui/material/IconButton';
import Divider from '@mui/material/Divider';
import backwardArrow from '/public/assets/images/backwardArrowDeposit.svg';
import depositRial from '/public/assets/images/deposit-rial-icon.svg';
import depositCash from '/public/assets/images/deposit-cash-icon.svg';
import depositPaypal from '/public/assets/images/deposit-paypal-icon.svg';
import depositBankFish from '/public/assets/images/deposit-bankfish-icon.svg';
import depositBitcoin from '/public/assets/images/deposit-bitcoin-icon.svg';
import depositGiftCard from '/public/assets/images/deposit-giftcard-icon.svg';
import { PATHS } from '../../configs/routes.config';
import { useRouter } from 'next/router';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { useEffect, useState } from 'react';
import { fetchWalletTransactionTypes } from '../../store/slices/wallet_transaction_types.slice';
import { useI18nTrDir } from '../../utils/CustomHooks/i18nTrDir';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { localesNS } from '../../utils/Data/locales_NameSpace.utils';
import { fetchCheckWalletTransfer } from '../../store/slices/wallet_transfer_info_check.slice';
import { fetchConfirmWalletTransfer } from '../../store/slices/wallet_transfer_info_confirm.slice';
import { fetchFindTransferDestUser } from '../../store/slices/transfer_destination_user.slice';
import { fetchFindTransferDestWallet } from '../../store/slices/transfer_destination_wallet.slice';
import forwardArrow from '/public/assets/images/forward-arrow.svg';
import { fetchWalletDepositAmount } from '../../store/slices/wallet_deposit_amount.slice';

interface Props {
  operation: string;
  open: boolean;
  close: Function;
  activeUid: string;
}

const DialogBox = ({ operation, open, close, activeUid }: Props) => {
  const { t, pageDirection } = useI18nTrDir();

  const router = useRouter();

  const dispatch = useAppDispatch();
  const walletTransactionTypesResult = useAppSelector((state) => state.WalletTransactionTypes);

  const [pageReady, setPageReady] = useState(false);

  useEffect(() => {
    dispatch(fetchWalletTransactionTypes(activeUid, operation));
    dispatch(fetchCheckWalletTransfer('reset'));
    dispatch(fetchConfirmWalletTransfer('reset'));
    dispatch(fetchFindTransferDestUser('reset'));
    dispatch(fetchFindTransferDestWallet('reset'));
    dispatch(fetchWalletDepositAmount('reset'));
  }, []);

  useEffect(() => {
    if (!walletTransactionTypesResult.loading) {
      setPageReady(true);
    } else {
      setPageReady(false);
    }
  }, [walletTransactionTypesResult]);

  const handleRedirectPage = (operationType: string) => {
    if (operationType) {
      if (operation === 'deposit') {
        if (operationType === 'rial') {
          typeof window !== 'undefined' && router.push(`/${PATHS.USER_WALLET}/${activeUid}/${PATHS.RIAL_DEPOSIT}`);
        } else if (operationType === 'cash') {
          typeof window !== 'undefined' && router.push(`/${PATHS.USER_WALLET}/${activeUid}/${PATHS.CASH_DEPOSIT}`);
        }
      } else if (operation === 'withdraw') {
        if (operationType === 'rial') {
          typeof window !== 'undefined' && router.push(`/${PATHS.USER_WALLET}/${activeUid}/${PATHS.RIAL_WITHDRAW}`);
        } else if (operationType === 'cash') {
          typeof window !== 'undefined' && router.push(`/${PATHS.USER_WALLET}/${activeUid}/${PATHS.CASH_WITHDRAW}`);
        }
      } else if (operation === 'transfer') {
        if (operationType === 'rial') {
          typeof window !== 'undefined' && router.push(`/${PATHS.USER_WALLET}/${activeUid}/${PATHS.TRANSFER_INTERNAL}`);
        } else if (operationType === 'cash') {
          typeof window !== 'undefined' && router.push(`/${PATHS.USER_WALLET}/${activeUid}/${PATHS.TRANSFER_OTHERS}`);
        }
      }
    }
  };

  return (
    <>
      {!pageReady ? (
        <Dialog
          fullScreen={true}
          open={open}
          onClose={() => close(operation)}
          aria-labelledby="responsive-dialog-title"
          sx={{
            backdropFilter: 'blur(4px)',
          }}
          PaperProps={{
            elevation: 0,
            sx: {
              width:
                operation === 'deposit' || operation === 'withdraw'
                  ? {
                      xs: 'auto',
                      sm: 'auto',
                      lg: 'auto',
                    }
                  : 'auto',
              maxWidth:
                operation === 'deposit' || operation === 'withdraw'
                  ? {
                      xs: '327px',
                      sm: '386px',
                      lg: '833px',
                    }
                  : operation === 'transfer'
                  ? { xs: '327px', sm: '454px' }
                  : 'auto',
              minWidth:
                operation === 'deposit' || operation === 'withdraw'
                  ? {
                      xs: '337px',
                      sm: '396px',
                    }
                  : operation === 'transfer'
                  ? { xs: '327px', sm: '454px' }
                  : 'auto',
              height: 'auto',
              minHeight: '140px',
              // backgroundColor: 'teal',
              borderRadius: '22px',
              // maxWidth: "720px!important",
              padding: '24px 24px 12px 24px',
            },
          }}
        >
          <DialogTitle
            id="responsive-dialog-title"
            sx={{
              width: '100%',
              height: 'auto',
              display: 'grid',
              gridTemplateColumns: '33.3% 33.3% 33.3%',
              // backgroundColor: 'teal',
              justifyItems: 'center',
              alignItems: 'center',
              mb: '20px',
              padding: 0,
            }}
          >
            <Box></Box>
            <Skeleton
              animation="wave"
              sx={{
                width: '141px',
                height: '30px',
                borderRadius: '12px',
                my: '2px',
              }}
            />
            <Box></Box>
          </DialogTitle>
          <DialogContent
            sx={{
              padding: 0,
            }}
          >
            <DialogContentText
              sx={{
                mb: '20px',
              }}
            >
              <Skeleton
                animation="wave"
                sx={{
                  width: '50%',
                  height: '30px',
                  borderRadius: '12px',
                  my: '2px',
                }}
              />
            </DialogContentText>
            <Divider variant={'fullWidth'} sx={{ my: '10px', borderColor: 'secondary.light' }} />
          </DialogContent>
          <DialogActions
            disableSpacing={true}
            sx={{
              width: '100%',
              display: 'flex',
              alignItems: 'flex-start',
              justifyContent: 'space-between',
              flexWrap: 'wrap',
              gap: '12px',
              mb: '20px',
              // px: '15px'
            }}
          >
            {Array.from(Array(3).keys()).map((item, index) => (
              <Skeleton
                key={index}
                animation="wave"
                variant="rectangular"
                sx={{
                  width: '80px',
                  height: '113px',
                  borderRadius: '15px',
                }}
              />
            ))}
          </DialogActions>
        </Dialog>
      ) : (
        <Dialog
          fullScreen={true}
          open={open}
          onClose={() => close(operation)}
          aria-labelledby="responsive-dialog-title"
          sx={{
            backdropFilter: 'blur(4px)',
          }}
          PaperProps={{
            elevation: 0,
            sx: {
              width:
                operation === 'deposit' || operation === 'withdraw'
                  ? {
                      xs: 'auto',
                      sm: 'auto',
                      lg: 'auto',
                    }
                  : 'auto',
              maxWidth:
                operation === 'deposit' || operation === 'withdraw'
                  ? {
                      xs: '327px',
                      sm: '386px',
                      lg: '833px',
                    }
                  : operation === 'transfer'
                  ? { xs: '327px', sm: '454px' }
                  : 'auto',
              minWidth:
                operation === 'deposit' || operation === 'withdraw'
                  ? {
                      xs: '337px',
                      sm: '396px',
                    }
                  : operation === 'transfer'
                  ? { xs: '327px', sm: '454px' }
                  : 'auto',
              height: 'auto',
              minHeight: '140px',
              // backgroundColor: 'teal',
              borderRadius: '22px',
              // maxWidth: "720px!important",
              padding: '24px 24px 12px 24px',
            },
          }}
        >
          <DialogTitle
            id="responsive-dialog-title"
            sx={{
              width: '100%',
              height: 'auto',
              display: 'grid',
              gridTemplateColumns: '33.3% 33.3% 33.3%',
              // backgroundColor: 'teal',
              justifyItems: 'center',
              alignItems: 'center',
              mb: '20px',
              padding: 0,
            }}
          >
            <IconButton
              color="inherit"
              aria-label="prev button"
              edge="start"
              onClick={() => close(operation)}
              sx={{
                width: '30px',
                height: '30px',
                display: 'flex',
                justifySelf: 'flex-start',
                padding: '0',
                backgroundColor: '#D9D9D9',
              }}
            >
              <Box
                component={'img'}
                src={forwardArrow.src}
                alt={'backward icon'}
                sx={{
                  width: '8px',
                  height: '15px',
                  transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                }}
              />
            </IconButton>
            <Typography
              variant="inherit"
              component="span"
              color={'text.primary'}
              sx={{
                // fontWeight: {sm: '600'},
                fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                fontSize: { xs: '16px', sm: '20px' },
                display: 'flex',
                justifySelf: 'center',
              }}
            >
              {operation === 'deposit'
                ? `${t('dialog_box:deposit')}/${t('dialog_box:charge')}`
                : operation === 'withdraw'
                ? t('dialog_box:withdraw')
                : t('dialog_box:transfer')}
            </Typography>
            <Box></Box>
          </DialogTitle>
          <DialogContent
            sx={{
              padding: 0,
            }}
          >
            <DialogContentText
              sx={{
                mb: '20px',
              }}
            >
              {(operation === 'deposit' || operation === 'withdraw') && (
                <Typography
                  variant="inherit"
                  component="span"
                  color={'text.primary'}
                  sx={{ fontWeight: '400', fontSize: { xs: '14px', sm: '16px' } }}
                >
                  {t('dialog_box:which_operation_to_do', {
                    operation: operation === 'deposit' ? t('dialog_box:a_deposit') : t('dialog_box:a_withdraw'),
                  })}
                </Typography>
              )}

              {operation === 'transfer' && (
                <Typography
                  variant="inherit"
                  component="span"
                  color={'text.primary'}
                  sx={{ fontWeight: '400', fontSize: { xs: '14px', sm: '16px' } }}
                >
                  {t('dialog_box:wanna_transfer_to')}
                </Typography>
              )}
            </DialogContentText>
            <Divider variant={'fullWidth'} sx={{ my: '10px', borderColor: 'secondary.light' }} />
          </DialogContent>
          <DialogActions
            disableSpacing={true}
            sx={{
              width: '100%',
              display: 'flex',
              alignItems: 'flex-start',
              justifyContent: 'space-between',
              flexWrap: 'wrap',
              gap: '12px',
              mb: '20px',
              // px: '15px'
            }}
          >
            {walletTransactionTypesResult?.walletTransactionTypesResp?.transaction_types_list?.map((feature: any) => {
              return (
                <Box
                  key={feature.title}
                  sx={{
                    width: 'auto',
                    height: 'auto',
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                  }}
                >
                  <Button
                    variant="contained"
                    disableRipple={true}
                    disableElevation={true}
                    sx={{
                      width: 'auto',
                      height: 'auto',
                      '&:hover': {
                        backgroundColor: '#F2F5FF',
                        // boxShadow: 'none',
                        '& .MuiTypography-root': {
                          fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                        },
                      },
                      boxShadow: 'none',
                      backgroundColor: 'transparent',
                      borderRadius: '15px',
                      // fontWeight: '600',
                      fontSize: '14px',
                      padding: '10px',
                    }}
                    onClick={() => {
                      handleRedirectPage(feature.delivery_type);
                      close(operation);
                    }}
                  >
                    <Box
                      sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                      }}
                    >
                      <Box
                        sx={{
                          width: '47px',
                          height: '47px',
                          borderRadius: '50%',
                          display: 'flex',
                          alignItems: 'center',
                          justifyContent: 'center',
                          backgroundColor: '#D1E7FF',
                          mb: '16px',
                        }}
                      >
                        <Box
                          component={'img'}
                          src={feature.icon}
                          // alt={feature.title}
                          sx={{
                            width: '24px',
                            height: '24px',
                          }}
                        />
                      </Box>
                      <Typography
                        variant="inherit"
                        component="span"
                        color={'text.primary'}
                        sx={{
                          // fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Regular',
                          fontSize: { xs: '12px', sm: '14px' },
                          lineHeight: '1.5',
                        }}
                      >
                        {feature.title}
                      </Typography>
                      <Typography
                        variant="inherit"
                        component="span"
                        color={'text.disabled'}
                        sx={{
                          fontWeight: '400',
                          fontSize: { xs: '12px', sm: '14px' },
                        }}
                      >
                        {feature.description}
                      </Typography>
                    </Box>
                  </Button>
                  {/*<Typography variant="inherit" component="span" color={'text.disabled'}*/}
                  {/*            sx={{fontWeight: '400', fontSize: '16px', mt: '0px'}}>*/}
                  {/*    {feature.description}*/}
                  {/*</Typography>*/}
                </Box>
              );
            })}
          </DialogActions>
        </Dialog>
      )}
    </>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export { DialogBox };
