import React, { useEffect, useRef, useState } from 'react';
import {
  Chart as ChartJS,
  ArcElement,
  Tooltip,
  Legend,
  CategoryScale,
  LinearScale,
  PointElement,
  LineController,
  LineElement,
  Title,
} from 'chart.js';
import { Doughnut, Line } from 'react-chartjs-2';
import { borderColor, Box } from '@mui/system';
import {
  dashboard_doughnut_options,
  wallet_doughnut_options,
  dashboard_line_options,
  wallet_line_options,
  empty_doughnut_options,
} from './Chart.config';
import { useAppSelector } from '../../store/hooks';
import increaseIcon from '/public/assets/images/Increase-icon.svg';
import Typography from '@mui/material/Typography';
import { useTranslation } from 'next-i18next';
import { useI18nTrDir } from '../../utils/CustomHooks/i18nTrDir';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';

interface ChartProps {
  mode: string;
  chartWidth: number;
  chartHeight: number;
  chartData?: any;
  color?: string;
}

interface LineDailySample {
  day: string;
  average_sell_price: string;
  average_buy_price: string;
}

interface RenderDataSample {
  labels: any[];
  datasets: any[];
}

interface FinanceWalletSample {
  title: string;
  currency: string;
  iso_code: string;
  wallet_logo: string;
  color: string;
  balance: number;
  rial_balance: number;
  benefit: number;
}

ChartJS.register(
  ArcElement,
  Tooltip,
  Legend,
  CategoryScale,
  LinearScale,
  PointElement,
  LineController,
  LineElement,
  Title,
);

const Chart = ({ mode, chartHeight, chartWidth, chartData, color }: ChartProps) => {
  const { t, pageDirection } = useI18nTrDir();
  const theme = useTheme();
  const xl = useMediaQuery(theme.breakpoints.up('xl'));

  let renderData: RenderDataSample = {
    labels: [],
    datasets: [],
  };

  const userFinanceResult = useAppSelector((state) => state.UserFinance?.userFinanceResp);

  let activeLegendsValSum = userFinanceResult?.total_rial_balance;

  const handleCheckBalance = () => {
    // userFinanceResult && userFinanceResult.total_rial_balance !==0 && userFinanceResult?.wallets.map((wallet: FinanceWalletSample) => wallet.rial_balance)
    if (userFinanceResult && userFinanceResult.total_rial_balance !== 0) {
      renderData = {
        labels: userFinanceResult && userFinanceResult?.wallets.map((wallet: FinanceWalletSample) => wallet.currency),
        datasets: [
          {
            label: userFinanceResult && userFinanceResult?.wallets.map((wallet: FinanceWalletSample) => wallet.balance),
            data:
              userFinanceResult && userFinanceResult?.wallets.map((wallet: FinanceWalletSample) => wallet.rial_balance),
            backgroundColor:
              userFinanceResult && userFinanceResult?.wallets.map((wallet: FinanceWalletSample) => wallet.color),
            hoverOffset: 4,
          },
        ],
      };
    } else {
      renderData = {
        labels: userFinanceResult && userFinanceResult?.wallets.map((wallet: FinanceWalletSample) => wallet.currency),
        datasets: [
          {
            label: userFinanceResult && userFinanceResult?.wallets.map((wallet: FinanceWalletSample) => wallet.balance),
            data: [],
            backgroundColor:
              userFinanceResult && userFinanceResult?.wallets.map((wallet: FinanceWalletSample) => '#D9D9D9'),
            hoverOffset: 4,
          },
        ],
      };
    }
    // const sum = userFinanceResult && userFinanceResult?.wallets.reduce((accumulator: number, wallet: FinanceWalletSample) => {
    //     return accumulator + wallet?.rial_balance;
    // }, 0);
    return renderData;
  };

  if (mode.includes('doughnut')) {
    renderData = handleCheckBalance();
  } else if (mode.includes('line')) {
    if (mode.includes('dashboard') || mode.includes('exchange')) {
      renderData = {
        labels: chartData?.map((item: LineDailySample) => ''),
        datasets: [
          {
            label: '',
            data: chartData?.map((item: LineDailySample) => item.average_sell_price),
            fill: true,
            backgroundColor: 'rgba(75,192,192,0.2)',
            borderColor: color,
            borderWidth: 2,
            lineTension: 0.5,
          },
        ],
      };
    } else {
      renderData = chartData;
    }
  }

  const customLegendStyles = {
    legendItem: {
      width: '50%',
      display: 'flex',
      alignItems: 'flex-start',
      padding: xl ? '5px 5px 15px 5px' : '5px',
      flex: '0 0 50%',
    },
    currencyPoint: {
      minWidth: '12px',
      minHeight: '12px',
      borderRadius: '50%',
      margin: pageDirection === 'rtl' ? '3px 0px 0 12px' : '3px 12px 0 0px',
    },
    titleValue: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',
    },
    currencyTitle: {
      fontSize: xl ? '14px' : '12px',
      // fontWeight: '600',
      fontFamily: 'IRANSansXFaNum-DemiBold',
      color: '#262626',
      lineHeight: '18px',
      marginBottom: xl ? '6px' : '2px',
    },
    currencyValue: {
      fontSize: xl ? '14px' : '12px',
      fontWeight: '400',
      color: '#262626',
      lineHeight: '18px',
      fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Regular',
    },
  };

  const adjustFontSize = (number: number | string) => {
    if (number.toString().length > 0 && number.toString().length <= 6) {
      return 20;
    }
    if (number.toString().length > 6 && number.toString().length <= 10) {
      return 18;
    }
    if (number.toString().length > 10) {
      return 16;
    }
  };

  const computeTotBalanceForActiveLegends = (balanceArr: number[], legendsInfo: []) => {
    let activeLegendsBalanceArr: number[] = [];
    legendsInfo.forEach((legend: any) => {
      if (!legend.hidden) {
        activeLegendsBalanceArr.push(balanceArr[legend.index]);
      }
    });
    activeLegendsValSum = activeLegendsBalanceArr.reduce((partialSum, a) => partialSum + a, 0);
  };

  const plugins = [
    {
      id: 'emptyDoughnut',
      afterDraw(chart: any, args: any, options: any) {
        if (userFinanceResult.total_rial_balance === 0) {
          const { datasets } = chart.data;
          const { color, width, radiusDecrease } = options;
          let hasData = false;

          for (let i = 0; i < datasets.length; i += 1) {
            const dataset = datasets[i];
            // @ts-ignore
            hasData |= dataset.data.length > 0;
          }

          if (!hasData) {
            const {
              chartArea: { left, top, right, bottom },
              ctx,
            } = chart;
            const centerX = (left + right) / 2;
            const centerY = (top + bottom) / 2;
            const r = Math.min(right - left, bottom - top) / 2;

            ctx.beginPath();
            ctx.lineWidth = width || 2;
            ctx.strokeStyle = color || 'rgba(255, 128, 0, 0.5)';
            ctx.arc(centerX, centerY, r - radiusDecrease || 0, 0, 2 * Math.PI);
            ctx.stroke();
          }
        }
      },
    },
    {
      id: 'set text inside doughnut chart',
      beforeDraw: function (chart: any) {
        const {
          ctx,
          chartArea: { top, right, bottom, left, width, height },
        } = chart;
        let chart_width = width / 2,
          chart_height = mode.includes('wallet') ? height / 2 : top + height / 2;

        let text1 = `${t('my_assets:total_assets')} (${t('my_assets:IRR')})`,
          text1X = Math.round(chart_width),
          text1Y = mode.includes('wallet')
            ? chart_height - (pageDirection === 'rtl' ? 25 : 20)
            : chart_height - (pageDirection === 'rtl' ? 15 : 12);
        let fontSize1 = (chart_height / 110).toFixed(2);
        ctx.font = fontSize1 + `em ${pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Regular'}`;
        ctx.textAlign = 'center';
        ctx.fillStyle = '#606060';
        ctx.fillText(text1, text1X, text1Y);
        ctx.save();

        let text2 = Number(activeLegendsValSum).toLocaleString('en-US'),
          text2X = Math.round(chart_width),
          text2Y = mode.includes('wallet')
            ? chart_height + (pageDirection === 'rtl' ? 4 : 4)
            : chart_height + (pageDirection === 'rtl' ? 15 : 17);
        // let fontSize2 = (chart_height / ((10 - (userFinanceResult?.total_rial_balance.toString().length)) * Math.round(Math.round(ctx.measureText(text2).width)))).toFixed(2);
        let fontSize2 = adjustFontSize(userFinanceResult?.total_rial_balance);
        ctx.font = fontSize2 + `px ${pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold'}`;
        ctx.textAlign = 'center';
        ctx.fillStyle = userFinanceResult?.total_rial_balance === 0 ? '#606060' : '#262626';
        ctx.fillText(text2, text2X, text2Y);
        ctx.save();

        if (mode.includes('wallet')) {
          let text3 = `${t('my_assets:profit')} - ${t('my_assets:loss')}`,
            text3X = Math.round(chart_width),
            text3Y = chart_height + (pageDirection === 'rtl' ? 28 : 28);
          let fontSize3 = (chart_height / (1.5 * Math.round(Math.round(ctx.measureText(text3).width)))).toFixed(2);
          ctx.font = fontSize3 + `em ${pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Regular'}`;
          ctx.textAlign = 'center';
          ctx.fillStyle = '#606060';
          ctx.fillText(text3, text3X, text3Y);
          ctx.save();

          let text4 = Number(Math.trunc(userFinanceResult?.total_benefit)).toLocaleString('en-US'),
            text4X = Math.round(chart_width - (userFinanceResult?.total_benefit === 0 ? 0 : 10)),
            text4Y = chart_height + (pageDirection === 'rtl' ? 48 : 45);
          let fontSize4 = chart_height / 130;
          ctx.font = fontSize4 + `em ${pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Regular'}`;
          ctx.textAlign = 'center';
          ctx.fillStyle = userFinanceResult?.total_benefit === 0 ? '#606060' : '#27AE60';
          ctx.fillText(text4, text4X, text4Y);
          ctx.save();

          let image = new Image();
          image.src =
            userFinanceResult?.total_benefit === 0
              ? ''
              : 'https://www.accesshq.com/workspace/images/icons/about-us/arrow-up.svg';
          let imageSize = 20;
          ctx.drawImage(
            image,
            width / 2 + Math.round(Math.round(ctx.measureText(text4).width)) / 2 - 10,
            text4Y - imageSize / 1.6,
            imageSize,
            imageSize,
          );
          ctx.restore();
          ctx.save();
        }
      },
    },
    {
      id: 'htmlLegend',
      afterUpdate(chart: any) {
        if (mode.includes('dashboard')) {
          const generateCustomLegend = () => {
            const legendBox = document.querySelector('#chart-legend');
            // @ts-ignore
            legendBox.innerHTML = '';
            let values: number[] = [];
            let currencyRialBalance: number[] = [];

            chart.data.datasets[0].label.map((item: number) => values.push(item));
            chart.data.datasets[0].data.forEach((item: any) => currencyRialBalance.push(item));

            chart.legend.legendItems.forEach((dataset: any, index: number) => {
              const text = dataset.text;
              const datasetIndex = dataset.index;

              const BgColor = dataset.fillStyle;
              // const borderColor = dataset.strokeStyle;

              const legendItem = document.createElement('div');
              legendItem.setAttribute('class', 'legend-item');
              legendItem.style.width = customLegendStyles.legendItem.width;
              legendItem.style.display = customLegendStyles.legendItem.display;
              legendItem.style.alignItems = customLegendStyles.legendItem.alignItems;
              legendItem.style.padding = customLegendStyles.legendItem.padding;
              legendItem.style.flex = customLegendStyles.legendItem.flex;
              legendItem.style.cursor = 'pointer';
              // legendItem.style.textDecoration = dataset.hidden ? 'line-through' : '';

              legendItem.onclick = () => {
                chart.toggleDataVisibility(datasetIndex);
                chart.update();
                computeTotBalanceForActiveLegends(currencyRialBalance, chart.legend.legendItems);
              };

              const currencyPoint = document.createElement('div');
              // span.style.borderColor = borderColor;
              currencyPoint.style.minWidth = customLegendStyles.currencyPoint.minWidth;
              currencyPoint.style.minHeight = customLegendStyles.currencyPoint.minHeight;
              currencyPoint.style.borderRadius = customLegendStyles.currencyPoint.borderRadius;
              currencyPoint.style.margin = customLegendStyles.currencyPoint.margin;
              currencyPoint.style.backgroundColor = BgColor;
              currencyPoint.style.opacity = dataset.hidden ? '0.5' : '1';

              const titleValue = document.createElement('div');
              titleValue.setAttribute('id', 'title-value');
              titleValue.style.display = customLegendStyles.titleValue.display;
              titleValue.style.flexDirection = customLegendStyles.titleValue.flexDirection;
              titleValue.style.alignItems = customLegendStyles.titleValue.alignItems;

              const currencyTitle = document.createElement('span');
              currencyTitle.setAttribute('id', 'currency-title');
              currencyTitle.style.fontSize = customLegendStyles.currencyTitle.fontSize;
              currencyTitle.style.fontFamily = customLegendStyles.currencyTitle.fontFamily;
              currencyTitle.style.color = customLegendStyles.currencyTitle.color;
              currencyTitle.style.opacity = dataset.hidden ? '0.5' : '1';
              currencyTitle.style.lineHeight = customLegendStyles.currencyTitle.lineHeight;
              currencyTitle.style.marginBottom = customLegendStyles.currencyTitle.marginBottom;
              currencyTitle.innerText = text;

              const currencyValue = document.createElement('span');
              currencyValue.setAttribute('id', 'currency-value');
              currencyValue.style.fontSize = customLegendStyles.currencyValue.fontSize;
              currencyValue.style.fontWeight = customLegendStyles.currencyValue.fontWeight;
              currencyValue.style.fontFamily = customLegendStyles.currencyValue.fontFamily;
              currencyValue.style.color = customLegendStyles.currencyValue.color;
              currencyValue.style.opacity = dataset.hidden ? '0.5' : '1';
              currencyValue.style.lineHeight = customLegendStyles.currencyValue.lineHeight;
              currencyValue.innerText = String(Number(Number(values[index]).toFixed(2)).toLocaleString('en-US'));

              legendItem.appendChild(currencyPoint);
              legendItem.appendChild(titleValue);
              titleValue.appendChild(currencyTitle);
              titleValue.appendChild(currencyValue);
              // @ts-ignore
              legendBox.appendChild(legendItem);
            });
          };
          generateCustomLegend();
        }
      },
    },
  ];

  return (
    <Box
      sx={{
        width: chartWidth === 1 ? `100%` : `${chartWidth}px`,
        height: 'auto',
      }}
    >
      {mode.includes('doughnut') && userFinanceResult?.wallets ? (
        <Box
          sx={{
            width: 'auto',
            height: 'auto',
            // backgroundColor: 'red'
          }}
        >
          <Doughnut
            data={renderData}
            options={
              userFinanceResult && userFinanceResult?.total_rial_balance !== 0
                ? mode.includes('dashboard')
                  ? dashboard_doughnut_options
                  : wallet_doughnut_options
                : empty_doughnut_options
            }
            plugins={plugins}
          />
          {mode.includes('dashboard') && (
            <Box
              id={'chart-legend'}
              sx={{
                width: '100%',
                height: 'auto',
                display: 'flex',
                flexWrap: 'wrap',
                alignItems: 'center',
                justifyItems: 'center',

                justifyContent: 'space-between',
                mt: '10px',
                // backgroundColor: 'yellow',
              }}
            ></Box>
          )}
        </Box>
      ) : (
        <Box
          sx={{
            width: chartWidth === 1 ? `100%` : `${chartWidth}px`,
            height: chartHeight === 1 ? `100%` : `${chartHeight}px`,
          }}
        >
          <Line
            data={renderData}
            options={
              mode.includes('dashboard') || mode.includes('exchange') ? dashboard_line_options : wallet_line_options
            }
          />
        </Box>
      )}
    </Box>
  );
};
export { Chart };
