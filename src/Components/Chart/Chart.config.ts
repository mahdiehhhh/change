export const dashboard_doughnut_options = {
  responsive: true,
  cutout: '70%',
  plugins: {
    legend: {
      display: false,
      position: 'bottom' as const,
      align: 'center' as const,
      maxWidth: 260,
      rtl: true,
      labels: {
        usePointStyle: true,
        pointStyle: 'circle' as const,
        textAlign: 'center' as const,
        boxWidth: 160,
        padding: 20,
        // generateLabels(chart: any) {
        //     const data = chart.data;
        //     if (data.labels.length && data.datasets.length) {
        //         const {labels: {pointStyle}} = chart.legend.options;
        //
        //         return data.labels.map((label: any, i: number) => {
        //             const meta = chart.getDatasetMeta(0);
        //             const style = meta.controller.getStyle(i);
        //
        //             return {
        //                 text: `${label}: ${Number(data['datasets'][0].label[i]).toLocaleString('en-US')}`,
        //                 fillStyle: style.backgroundColor,
        //                 strokeStyle: style.borderColor,
        //                 lineWidth: style.borderWidth,
        //                 pointStyle: pointStyle,
        //                 hidden: !chart.getDataVisibility(i),
        //                 width: 100,
        //
        //                 // Extra data used for toggling the correct item
        //                 index: i
        //             };
        //         });
        //     }
        //     return [];
        // },
        // This more specific font property overrides the global property
        font: {
          size: 14,
          family: 'IRANSansXFaNum-Regular',
        },
      },
    },
  },
};

export const wallet_doughnut_options = {
  responsive: true,
  cutout: '70%',
  plugins: {
    legend: {
      display: false,
    },
  },
};

export const empty_doughnut_options = {
  responsive: true,
  cutout: '70%',
  plugins: {
    legend: {
      display: false,
    },
    emptyDoughnut: {
      color: '#D9D9D9',
      width: 30,
      radiusDecrease: 18,
    },
  },
};

export const dashboard_line_options = {
  responsive: true,
  maintainAspectRatio: false,

  elements: {
    point: {
      radius: 0,
    },
  },
  plugins: {
    legend: {
      display: false,
    },
    tooltip: {
      enabled: false,
    },
  },

  // layout: {
  //     padding: '1px',
  // },

  scales: {
    x: {
      display: false,
      ticks: {
        display: false,
      },
    },
    y: {
      display: false,
      ticks: {
        display: false,
      },
    },
  },
};

export const wallet_line_options = {
  responsive: true,
  elements: {
    point: {
      radius: 0,
    },
  },

  // layout: {
  //     padding: '1px',
  // },

  scales: {
    x: {
      grid: {
        display: false,
      },
    },
    y: {
      grid: {
        display: false,
      },
    },
  },
  plugins: {
    legend: {
      align: 'center' as const,
      rtl: true,
      labels: {
        usePointStyle: true,
        pointStyle: 'circle' as const,
        textAlign: 'center' as const,
        boxWidth: 5,
        boxHeight: 5,
        font: {
          size: 14,
          family: 'IRANSansXFaNum-Regular',
        },
      },
    },
  },
};
