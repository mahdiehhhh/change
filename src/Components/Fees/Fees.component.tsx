import React, { useEffect, useState, ChangeEvent, ReactNode } from 'react';
import {
  Box,
  ButtonGroup,
  Button,
  InputAdornment,
  IconButton,
  Select,
  MenuItem,
  TextField,
  InputLabel,
  Tabs,
  Tab,
  Chip,
  FormHelperText,
  FormControl,
  SelectChangeEvent,
  Skeleton,
} from '@mui/material';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useRouter } from 'next/router';
import { PATHS } from '../../configs/routes.config';
import starIcon from '/public/assets/images/Star-icon.svg';
import filterIcon from '/public/assets/images/notif-filter-icon.svg';
import searchIconActive from '/public/assets/images/Search-icon-active.svg';
import coloredStarIcon from '/public/assets/images/colored-star-icon.svg';
import coloredPlusIcon from '/public/assets/images/colored_plus_icon.svg';
import Typography from '@mui/material/Typography';
import { SearchBar } from '../SearchBar/SearchBar.component';
import FlexibleSearchBarComponent from '../FlexibleSearchBar/FlexibleSearchBar.component';
import FlexibleSearchBar from '../FlexibleSearchBar/FlexibleSearchBar.component';
import { dispatch } from 'jest-circus/build/state';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { fetchExchangeCurrenciesInfo } from '../../store/slices/currencies_exchange_infos.slice';
import { CurrencyTable } from '../CurrencyTable/CurrencyTable.component';
import { Trans } from 'next-i18next';
import { useI18nTrDir } from '../../utils/CustomHooks/i18nTrDir';
import { localesNS } from '../../utils/Data/locales_NameSpace.utils';
import headphoneIcon from '/public/assets/images/headphone-icon.svg';
import { TextInfoTable } from '../TextInfoTable/TextInfoTable.component';
import { fetchUserTickets } from '../../store/slices/user_tickets.slice';
import noTransactionSmileIcon from '/public/assets/images/no-transaction-smile.svg';
import AlertDialogBox from '../AlertDialogBox/AlertDialogBox.component';
import bulletPoint from '/public/assets/images/BulletPoint.svg';
import dangerCirclePurple from '/public/assets/images/Danger-Circle-purple-icon.svg';
import { fetchCurrenciesDashboard } from '../../store/slices/currencies_dashboard.slice';
import dangerCircleGreenIcon from '*.svg';
import { fetchFeesList } from '../../store/slices/fees_list.slice';

interface DashboardCurrency {
  title: string;
  type: string;
  symbol: string;
  iso_code: string;
  image: string;
  color: string;
  sell_price: string;
  buy_price: string;
  id: number;
  price_history: PriceHistorySample[];
  change_percent: string;
  change: number;
}

interface PriceHistorySample {
  average_buy_price: string;
  average_sell_price: string;
  day: string;
}

const Fees = () => {
  const router = useRouter();
  const UrlPath = router.pathname;
  const { t, pageDirection } = useI18nTrDir();

  let titleFilteredArray: any = [];
  let linkFilteredArray: any = [];
  let AllFilteredResultsArray: any = [];

  const dispatch = useAppDispatch();
  const currenciesDashResult = useAppSelector((state) => state.CurrenciesDashboard);
  const feesListResult = useAppSelector((state) => state.FeesList);

  const [feesMode, setFeesMode] = useState('fees_rate');
  const [pageReady, setPageReady] = useState(false);
  const [searchedResults, setSearchedResults] = useState<[]>([]);
  const [searchedText, setSearchedText] = useState('');
  const [currencyListState, setCurrencyListState] = useState([]);
  const [currencyTypeState, setCurrencyTypeState] = useState<DashboardCurrency>({
    title: '',
    type: '',
    symbol: '',
    iso_code: '',
    image: '',
    color: '',
    sell_price: '',
    buy_price: '',
    id: 0,
    price_history: [],
    change_percent: '',
    change: 0,
  });

  const [openCallUsDialogBox, setOpenCallUsDialogBox] = React.useState(false);
  const [operationResultState, setOperationResultState] = useState<object>({
    status: '',
    messages: {},
  });

  const handleOpenCallUsDB = () => {
    setOpenCallUsDialogBox(true);
  };

  const handleCloseCallUsDB = () => {
    setOpenCallUsDialogBox(false);
  };

  const currencyTransactionRateTable = [
    {
      id: 1,
      title: t('exchange:dynamic_rate', { item: t('my_assets:buy') }),
      value: currencyTypeState?.buy_price && Number(currencyTypeState?.buy_price).toLocaleString('en-US'),
    },
    {
      id: 2,
      title: t('exchange:dynamic_rate', { item: t('my_assets:sell') }),
      value: currencyTypeState?.sell_price && Number(currencyTypeState?.sell_price).toLocaleString('en-US'),
    },
  ];

  const feesTabs = [
    {
      id: 1,
      key: 'fees_rate',
      title: t('exchange:dynamic_rate', { item: t('exchange:wages') }),
      link: `${PATHS.USER_EXCHANGE}/${PATHS.FEE}/${PATHS.RATE}`,
    },
    {
      id: 2,
      key: 'open_account_rate',
      title: t('exchange:dynamic_rate', {
        item: t('useful_services:create_account'),
      }),
      link: `${PATHS.USER_EXCHANGE}/${PATHS.FEE}/${PATHS.OPEN_ACCOUNT}`,
    },
  ];
  const [activeFeesTab, setActiveFeesTab] = useState(feesTabs[0]);

  useEffect(() => {
    setCurrencyListState(currenciesDashResult.currenciesDashResp?.currencies);
    const defaultCurrency = currenciesDashResult.currenciesDashResp?.currencies.find(
      (currency: any) => currency.iso_code.toUpperCase() === 'USD',
    );
    setCurrencyTypeState(defaultCurrency);
  }, [currenciesDashResult]);

  useEffect(() => {
    if (
      !currenciesDashResult.loading &&
      !feesListResult.loading &&
      (currenciesDashResult.currenciesDashResp || currenciesDashResult.error) &&
      (feesListResult.feesListResp || feesListResult.error)
    ) {
      setPageReady(true);
    } else {
      setPageReady(false);
    }
  }, [currenciesDashResult, feesListResult]);

  useEffect(() => {
    dispatch(fetchCurrenciesDashboard());
    dispatch(fetchFeesList());
  }, []);

  useEffect(() => {
    const selectedFeesTab = feesTabs.find((tab) => `/${tab.link}` === UrlPath);
    // @ts-ignore
    setActiveFeesTab(selectedFeesTab);
    // @ts-ignore
    setFeesMode(selectedFeesTab?.key);
  }, [UrlPath]);

  const handleChangeCurrencyType = (event: SelectChangeEvent<any>, child: ReactNode): void => {
    setCurrencyTypeState(event.target.value);
  };

  const handleSearchForResult = (event: any) => {
    setSearchedText(event.target.value);
    if (!!feesListResult.feesListResp) {
      titleFilteredArray = (
        feesMode === 'fees_rate'
          ? feesListResult.feesListResp?.exchange_fee_list
          : feesListResult.feesListResp?.open_accounts_fee
      )?.filter((item: any) => {
        return feesMode === 'fees_rate'
          ? item.website_name.toLowerCase().includes(event.target.value.toLowerCase())
          : item.account_title.toLowerCase().includes(event.target.value.toLowerCase());
      });
      linkFilteredArray = (feesMode === 'fees_rate' ? feesListResult.feesListResp?.exchange_fee_list : [])?.filter(
        (item: any) => {
          return item.website_link.toLowerCase().includes(event.target.value.toLowerCase());
        },
      );
    }
    AllFilteredResultsArray = titleFilteredArray.concat(linkFilteredArray);
    // @ts-ignore
    const uniqueFilteredResults = [...new Set(AllFilteredResultsArray)];
    // @ts-ignore
    setSearchedResults(uniqueFilteredResults);
  };

  const selectNewIconSimple = (props: any) => (
    <svg {...props} width="16" height="9" viewBox="0 0 16 9" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M15 1L8 8L1 1" stroke="#606060" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
    </svg>
  );

  return (
    <>
      {!pageReady ? (
        <>
          <Box
            sx={{
              width: '100%',
              display: 'flex',
              justifyContent: 'center',
            }}
          >
            <Box
              sx={{
                width: { xs: '100%', xl: '1100px' },
                height: 'auto',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'flex-start',
                justifyContent: 'flex-start',
              }}
            >
              <Skeleton
                animation="wave"
                sx={{
                  width: '108px',
                  height: '30px',
                  borderRadius: '12px',
                }}
              />

              <Box
                sx={{
                  width: '100%',
                  height: 'auto',
                  display: 'flex',
                  flexDirection: { xs: 'column', md: 'row' },
                  // alignItems: 'center',
                  mt: '30px',
                }}
              >
                <Box
                  sx={{
                    width: { xs: '100%', md: '49%' },
                    height: 'auto',
                    bgcolor: 'white',
                    borderRadius: '16px',
                    overflow: 'hidden',
                    display: 'flex',
                    pt: '24px',
                    mr: '42px',
                  }}
                >
                  <Box
                    sx={{
                      width: '100%',
                      padding: { xs: '12px', sm: '16px' },
                      bgcolor: 'white',
                      display: 'flex',
                      alignItems: 'flex-end',
                      justifyContent: 'space-between',
                    }}
                  >
                    <Box
                      sx={{
                        width: { xs: '50%', sm: '40%' },
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'flex-start',
                        // bgcolor: 'red'
                      }}
                    >
                      <Skeleton
                        animation="wave"
                        sx={{
                          width: '40%',
                          height: '30px',
                          borderRadius: '12px',
                        }}
                      />
                      <Skeleton
                        animation="wave"
                        variant="rectangular"
                        sx={{
                          width: '90%',
                          height: '48px',
                          borderRadius: '12px',
                        }}
                      />
                    </Box>

                    <Box
                      sx={{
                        width: { xs: '55%', sm: '45%' },
                        height: '52px',
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'flex-start',
                        justifyContent: 'space-between',
                      }}
                    >
                      <Skeleton
                        animation="wave"
                        sx={{
                          width: '90%',
                          height: '30px',
                          borderRadius: '12px',
                        }}
                      />
                      <Skeleton
                        animation="wave"
                        sx={{
                          width: '90%',
                          height: '30px',
                          borderRadius: '12px',
                        }}
                      />
                    </Box>
                  </Box>
                </Box>

                <Box
                  sx={{
                    width: { xs: '100%', md: '49%' },
                    display: 'flex',
                    alignItems: 'center',
                    // justifyContent: 'center',
                    bgcolor: 'white',
                    padding: '16px',
                    borderRadius: '16px',
                    borderColor: 'success.dark',
                    mt: { xs: '16px', md: '0' },
                  }}
                >
                  <Box
                    sx={{
                      width: '100%',
                      display: 'flex',
                      alignItems: 'flex-start',
                      flexWrap: 'wrap',
                    }}
                  >
                    <Skeleton
                      animation="wave"
                      sx={{
                        width: '100%',
                        height: '30px',
                        borderRadius: '12px',
                        my: '12px',
                      }}
                    />
                    <Skeleton
                      animation="wave"
                      sx={{
                        width: '60%',
                        height: '30px',
                        borderRadius: '12px',
                        my: '12px',
                      }}
                    />
                  </Box>
                </Box>
              </Box>

              <Box
                sx={{
                  width: '100%',
                  bgcolor: 'white',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  flexWrap: 'wrap',
                  mt: '48px',
                  padding: '24px',
                  borderRadius: '24px',
                }}
              >
                {Array.from(Array(12).keys()).map((item) => (
                  <Box
                    key={item}
                    sx={{
                      width: '30%',
                      display: 'flex',
                      flexDirection: 'column',
                      my: '30px',
                    }}
                  >
                    <Skeleton
                      animation="wave"
                      sx={{
                        width: '40%',
                        height: '30px',
                        borderRadius: '12px',
                        my: '2px',
                      }}
                    />
                    <Skeleton
                      animation="wave"
                      sx={{
                        width: '100%',
                        height: '30px',
                        borderRadius: '12px',
                        my: '2px',
                      }}
                    />
                  </Box>
                ))}
              </Box>
            </Box>
          </Box>
        </>
      ) : (
        <Box
          sx={{
            width: '100%',
            display: 'flex',
            justifyContent: 'center',
          }}
        >
          <Box
            sx={{
              width: { xs: '100%', xl: '1100px' },
              height: 'auto',
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'flex-start',
              justifyContent: 'flex-start',
            }}
          >
            <Box
              sx={{
                width: 'auto',
                display: 'flex',
                alignItems: 'center',
              }}
            >
              <Box
                component={'img'}
                src={bulletPoint.src}
                alt={'bullet-point'}
                sx={{
                  width: '22px',
                  height: '18px',
                  mr: '9px',
                  transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                }}
              />
              <Typography
                variant="inherit"
                component="span"
                color={'text.primary'}
                sx={{
                  fontWeight: { xs: '600' },
                  fontSize: { xs: '16px', xl: '16px' },
                }}
              >
                {t('exchange:wages')}
              </Typography>
            </Box>

            <Box
              sx={{
                width: '100%',
                height: 'auto',
                display: 'flex',
                flexDirection: { xs: 'column', md: 'row' },
                // alignItems: 'center',
                mt: '40px',
              }}
            >
              <Box
                sx={{
                  width: { xs: '100%', md: '49%' },
                  height: 'auto',
                  bgcolor: '#6FCF97',
                  borderRadius: '16px',
                  overflow: 'hidden',
                  display: 'flex',
                  pt: '24px',
                  mr: '42px',
                }}
              >
                <Box
                  sx={{
                    width: '100%',
                    padding: { xs: '12px', sm: '16px' },
                    bgcolor: 'white',
                    display: 'flex',
                    alignItems: 'flex-end',
                    justifyContent: 'space-between',
                  }}
                >
                  <Box
                    sx={{
                      width: { xs: '50%', sm: '40%' },
                      display: 'flex',
                      flexDirection: 'column',
                      alignItems: 'flex-start',
                    }}
                  >
                    <InputLabel
                      htmlFor={'currency_type'}
                      sx={{
                        fontSize: '17px',
                        fontWeight: '400',
                        color: 'text.primary',
                      }}
                    >
                      {t('bank_accounts:ex_type')}
                    </InputLabel>
                    <Select
                      // dir={'rtl'}
                      value={currencyTypeState}
                      onChange={handleChangeCurrencyType}
                      name={'currency_type'}
                      displayEmpty={true}
                      placeholder={t('bank_accounts:choose')}
                      IconComponent={selectNewIconSimple}
                      renderValue={(value: any) => {
                        return (
                          <Box
                            sx={{
                              display: currencyTypeState ? 'flex' : 'none',
                              alignItems: 'flex-end',
                              mr: '14px',
                            }}
                          >
                            <Typography
                              variant="inherit"
                              component="span"
                              color={'text.secondary'}
                              sx={{
                                fontSize: '14px',
                                fontWeight: '600',
                                mr: '16px',
                              }}
                            >
                              {currencyTypeState?.title}
                            </Typography>
                            <Typography
                              variant="inherit"
                              component="span"
                              color={'text.secondary'}
                              sx={{
                                fontSize: '14px',
                                fontWeight: '600',
                                mr: '10px',
                                lineHeight: '20px',
                              }}
                            >
                              {currencyTypeState?.iso_code}
                            </Typography>
                            <Box
                              component={'img'}
                              src={currencyTypeState?.image}
                              // alt={`${value.title} icon`}
                              sx={{
                                width: '24px',
                                height: '24px',
                              }}
                            />
                          </Box>
                        );
                      }}
                      MenuProps={{
                        anchorOrigin: {
                          vertical: 'bottom',
                          horizontal: 'left',
                        },
                        transformOrigin: {
                          vertical: 'top',
                          horizontal: 'left',
                        },
                        PaperProps: {
                          sx: {
                            mt: '8px',
                            boxShadow: '0px 4px 38px rgba(68, 68, 68, 0.1)',
                            height: 'auto',
                            borderRadius: '10px',
                            overflowY: 'auto',
                          },
                        },
                      }}
                      sx={{
                        width: '100%',
                        mt: '8px',
                        bgcolor: currencyTypeState ? 'white' : 'secondary.main',
                        borderRadius: '12px',
                        '& .MuiSelect-icon': { top: '40%', mx: '5px' },
                        height: '100%',
                        '.MuiInputBase-input': {
                          py: '14px',
                        },
                        '& fieldset': {
                          border: currencyTypeState ? '1px solid' : 'none',
                          borderColor: 'primary.main',
                        },
                      }}
                    >
                      {currencyListState?.map((item: any) => {
                        return (
                          <MenuItem
                            value={item}
                            key={item.id}
                            sx={{
                              width: '100%',
                              px: '8px',
                              my: '5px',
                            }}
                          >
                            <Box
                              sx={{
                                width: '100%',
                                display: 'flex',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                                ml: '8px',
                              }}
                            >
                              <Typography
                                variant="inherit"
                                component="span"
                                color={'text.primary'}
                                sx={{
                                  fontSize: '14px',
                                  fontWeight: '400',
                                }}
                              >
                                {item.title}
                              </Typography>

                              <Typography
                                variant="inherit"
                                component="span"
                                color={'text.primary'}
                                sx={{
                                  fontSize: '12px',
                                  fontWeight: '600',
                                }}
                              >
                                {item.iso_code}
                              </Typography>
                              <Box
                                component={'img'}
                                src={item.image}
                                // alt={`${value.title} icon`}
                                sx={{
                                  width: '24px',
                                  height: '24px',
                                }}
                              />
                            </Box>
                          </MenuItem>
                        );
                      })}
                    </Select>
                  </Box>

                  <Box
                    sx={{
                      width: { xs: '55%', sm: '45%' },
                      height: '52px',
                      display: 'flex',
                      flexDirection: 'column',
                      alignItems: 'flex-start',
                      justifyContent: 'space-between',
                    }}
                  >
                    {currencyTransactionRateTable.map((rate) => (
                      <Box
                        key={rate.id}
                        sx={{
                          width: '100%',
                          display: 'flex',
                          alignItems: 'center',
                          justifyContent: { xs: 'center', md: 'space-between' },

                          // py: '10px'
                        }}
                      >
                        <Box
                          sx={{
                            width: { xs: '100%', md: '100%' },
                            maxWidth: { xs: '135px', sm: '180px', md: '100%' },
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'space-between',
                          }}
                        >
                          <Box
                            sx={{
                              display: 'flex',
                              alignItems: 'center',
                            }}
                          >
                            <Chip
                              sx={{
                                display: { xs: 'none', sm: 'block' },
                                width: '5px',
                                height: '5px',
                                borderRadius: '50%',
                                bgcolor: 'text.secondary',
                                mr: '10px',
                                '& .MuiChip-label': {
                                  padding: '0',
                                  margin: '0',
                                  display: 'flex',
                                  alignItems: 'center',
                                  justifyContent: 'center',
                                },
                              }}
                            />
                            <Typography
                              variant="inherit"
                              component="span"
                              color={'text.secondary'}
                              sx={{
                                fontSize: { xs: '12px', sm: '14px' },
                                fontWeight: '400',
                                mr: '8px',
                              }}
                            >
                              {rate.title}
                            </Typography>
                            <Typography
                              variant="inherit"
                              component="span"
                              color={'text.primary'}
                              sx={{
                                fontSize: '14px',
                                // fontWeight: '600',
                                fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                              }}
                            >
                              {currencyTypeState?.title}
                            </Typography>
                          </Box>

                          <Typography
                            variant="inherit"
                            component="span"
                            color={'primary.main'}
                            sx={{
                              fontSize: { xs: '12px', sm: '14px' },
                              // fontWeight: '600',
                              fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                            }}
                          >
                            {rate.value}
                          </Typography>
                        </Box>
                      </Box>
                    ))}
                  </Box>
                </Box>
              </Box>

              <Box
                sx={{
                  width: { xs: '100%', md: '49%' },
                  display: 'flex',
                  alignItems: 'center',
                  // justifyContent: 'center',
                  bgcolor: '#F2DCF6',
                  padding: '16px',
                  borderRadius: '16px',
                  borderColor: 'success.dark',
                  mt: { xs: '16px', md: '0' },
                }}
              >
                <Box
                  sx={{
                    display: 'flex',
                    alignItems: 'flex-start',
                  }}
                >
                  <Box
                    component={'img'}
                    src={dangerCirclePurple.src}
                    alt={'bullet-point'}
                    sx={{
                      width: '24px',
                      height: '24px',
                      mt: '2px',
                      mr: '9px',
                      transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                    }}
                  />
                  <Typography
                    variant="inherit"
                    component="span"
                    color={'info.main'}
                    sx={{
                      // width: {lg: '444px'},
                      fontSize: '14px',
                      fontWeight: '400',
                      lineHeight: { sm: '25px', md: '30px' },
                    }}
                  >
                    {t('exchange:fee_description')}
                  </Typography>
                </Box>
              </Box>
            </Box>

            <Box
              sx={{
                width: '100%',
                display: 'grid',
                gridAutoColumns: 'row',
                gridTemplateColumns: {
                  xs: '0fr 3fr 0fr',
                  sm: '0.5fr 2fr 0.5fr',
                },
                mt: '48px',
              }}
            >
              <Box></Box>

              <Box
                sx={{
                  height: 'auto',
                  display: 'flex',
                  justifyContent: 'center',
                }}
              >
                <Tabs
                  value={+activeFeesTab?.id - 1}
                  // onChange={handleChangeTabs}
                  aria-label="nav tabs example"
                  TabIndicatorProps={{ style: { display: 'none' } }}
                >
                  {feesTabs.map((tab) => {
                    return (
                      <Tab
                        key={tab.id}
                        label={tab.title}
                        disableRipple={true}
                        sx={{
                          width: 'auto',
                          px: { xs: '35px', lg: '50px' },
                          position: 'relative',
                          fontSize: { xs: '14px', sm: '16px' },
                          fontWeight: +activeFeesTab?.id === +tab.id ? '600' : '400',
                          borderRadius: '12px',
                          pb: '28px',
                          pt: '18px',
                          // bgcolor: +activeFeesTab?.id === +tab.id ? 'white' : 'transparent',
                          bgcolor: 'transparent',
                          zIndex: 0,
                          '&::before': {
                            content: '" "',
                            display: 'block',
                            background: +activeFeesTab?.id === +tab.id ? 'white' : 'transparent',
                            width: 'calc(100% - 40%)',
                            height: '80px',
                            position: 'absolute',
                            top: 0,
                            right: pageDirection === 'rtl' ? '10px' : 'auto',
                            left: pageDirection === 'rtl' ? 'auto' : '10px',
                            transform: 'skew(-17deg)',
                            borderRadius: { xs: '15px', lg: '20px' },
                            zIndex: -1,
                          },
                          '&::after': {
                            content: '" "',
                            display: 'block',
                            background: +activeFeesTab?.id === +tab.id ? 'white' : 'transparent',
                            width: 'calc(100% - 40%)',
                            height: '80px',
                            position: 'absolute',
                            top: 0,
                            left: pageDirection === 'rtl' ? '10px' : 'auto',
                            right: pageDirection === 'rtl' ? 'auto' : '10px',
                            transform: 'skew(17deg)',
                            borderRadius: { xs: '15px', lg: '20px' },
                            zIndex: -1,
                          },
                        }}
                        component="a"
                        onClick={(event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
                          event.preventDefault();
                          router.push(`/${tab.link}`);
                        }}
                        // href={tab.link}
                      />
                    );
                  })}
                </Tabs>
              </Box>

              <Box
                sx={{
                  height: 'auto',
                  display: { xs: 'none', sm: 'flex' },
                  alignItems: 'flex-start',
                  justifyContent: 'flex-end',
                }}
              >
                <FlexibleSearchBar handleSearchForResult={handleSearchForResult} />
              </Box>
            </Box>
            <Box
              sx={{
                width: '100%',
                height: 'auto',
                padding: { xs: '16px', sm: '24px' },
                bgcolor: 'white',
                mt: '-10px',
                zIndex: '2',
                borderRadius: '24px',
              }}
            >
              <TextInfoTable mode={feesMode} AllFilteredResultsArray={searchedResults} searchedText={searchedText} />
            </Box>
          </Box>
        </Box>
      )}

      {openCallUsDialogBox && (
        <AlertDialogBox
          mode={'call_us'}
          data={operationResultState}
          open={openCallUsDialogBox}
          close={handleCloseCallUsDB}
        />
      )}
    </>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      hasLayout: true,
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default Fees;
