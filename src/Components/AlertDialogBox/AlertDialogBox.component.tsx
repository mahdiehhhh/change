import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { Box } from '@mui/system';
import successTransactionIcon from '/public/assets/images/transaction-success.svg';
import errorTransactionIcon from '/public/assets/images/transaction-error.svg';
import { Divider, Typography } from '@mui/material';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { localesNS } from '../../utils/Data/locales_NameSpace.utils';
import { useI18nTrDir } from '../../utils/CustomHooks/i18nTrDir';

interface AlertDialogBoxPropsSample {
  mode: string;
  data: any;
  open: boolean;
  close: Function;
}

const AlertDialogBox = ({ mode, data, open, close }: AlertDialogBoxPropsSample) => {
  console.log('this is alert dialog box data', data);
  const { t, pageDirection } = useI18nTrDir();

  return (
    <>
      {mode === 'result' ? (
        <Dialog
          fullScreen={true}
          open={open}
          onClose={() => close()}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          sx={{
            backdropFilter: 'blur(4px)',
          }}
          PaperProps={{
            elevation: 0,
            sx: {
              width: { xs: '327px', sm: '586px', lg: '724px' },
              height: { xs: '283px', sm: '502px', lg: '502px' },
              // minHeight: '140px',
              // backgroundColor: 'teal',
              borderRadius: '35px',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            },
          }}
        >
          {/*<DialogTitle id="alert-dialog-title">*/}
          {/*</DialogTitle>*/}

          <DialogContent
            sx={{
              width: '100%',
              height: '100%',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <Box
                sx={{
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                <Box
                  component={'img'}
                  src={successTransactionIcon.src}
                  alt={'backward icon'}
                  sx={{
                    width: { xs: '85px', sm: '145px' },
                    height: { xs: '55px', sm: '115px' },
                  }}
                />

                <Typography
                  variant="inherit"
                  component="span"
                  color={'text.primary'}
                  sx={{
                    fontWeight: '400',
                    fontSize: { xs: '14px', sm: '24px' },
                    mt: { xs: '16px', sm: '32px' },
                    mb: { xs: '8px', sm: '16px' },
                  }}
                >
                  {data.messages?.line_1}
                </Typography>

                <Typography
                  variant="inherit"
                  component="span"
                  color={'text.primary'}
                  sx={{ fontWeight: '600', fontSize: { xs: '16px', sm: '24px' } }}
                >
                  {data.messages?.line_2}
                </Typography>

                <Typography
                  variant="inherit"
                  component="span"
                  color={'text.secondary'}
                  sx={{
                    fontWeight: '400',
                    fontSize: { xs: '14px', sm: '20px' },
                    mt: { xs: '8px', sm: '20px' },
                    mb: { xs: '26px', sm: '55px' },
                  }}
                >
                  {data.messages?.line_3}
                </Typography>
              </Box>

              <Button
                variant="contained"
                disableRipple={true}
                sx={{
                  width: { xs: '187px', sm: '236px' },
                  height: '40px',
                  '&:hover': {
                    backgroundColor: 'primary.main',
                    boxShadow: 'none',
                  },
                  boxShadow: 'none',
                  backgroundColor: 'primary.main',
                  borderRadius: '12px',
                  fontWeight: '600',
                  fontSize: '14px',
                }}
                onClick={() => close()}
              >
                {t('dialog_box:close')}
              </Button>
            </Box>
          </DialogContent>

          {/*<DialogActions>*/}
          {/*    */}
          {/*</DialogActions>*/}
        </Dialog>
      ) : (
        mode === 'call_us' && (
          <Dialog
            fullScreen={true}
            open={open}
            onClose={() => close()}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
            sx={{
              backdropFilter: 'blur(4px)',
            }}
            PaperProps={{
              elevation: 0,
              sx: {
                width: { xs: '327px', sm: '450px', lg: '450px' },
                height: { xs: '281px', sm: '314px', lg: '314px' },
                // minHeight: '140px',
                // backgroundColor: 'teal',
                borderRadius: '22px',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
              },
            }}
          >
            {/*<DialogTitle id="alert-dialog-title">*/}
            {/*</DialogTitle>*/}

            <DialogContent
              sx={{
                width: '100%',
                height: '100%',
                display: 'flex',
                alignItems: 'flex-start',
                justifyContent: 'center',
                mt: '12px',
              }}
            >
              <Box
                sx={{
                  width: '100%',
                  height: '100%',
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}
              >
                <Box
                  sx={{
                    width: '100%',
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                >
                  <Box
                    sx={{
                      width: '100%',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                    }}
                  >
                    <Typography
                      variant="inherit"
                      component="span"
                      color={'text.primary'}
                      sx={{
                        fontWeight: '400',
                        fontSize: { xs: '12px', sm: '16px' },
                      }}
                    >
                      {t('dialog_box:contact_support')}
                    </Typography>

                    <Typography
                      variant="inherit"
                      component="span"
                      color={'text.primary'}
                      sx={{
                        fontWeight: '400',
                        fontSize: { xs: '12px', sm: '16px' },
                        fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Regular',
                      }}
                    >
                      09123456789
                    </Typography>
                  </Box>

                  <Divider
                    orientation="horizontal"
                    variant="fullWidth"
                    flexItem
                    sx={{
                      my: '16px',
                      borderColor: 'secondary.light',
                    }}
                  />

                  <Box
                    sx={{
                      width: '100%',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                    }}
                  >
                    <Typography
                      variant="inherit"
                      component="span"
                      color={'text.primary'}
                      sx={{
                        fontWeight: '400',
                        fontSize: { xs: '12px', sm: '16px' },
                      }}
                    >
                      {t('dialog_box:contact_manager')}
                    </Typography>

                    <Typography
                      variant="inherit"
                      component="span"
                      color={'text.primary'}
                      sx={{
                        fontWeight: '400',
                        fontSize: { xs: '12px', sm: '16px' },
                        fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Regular',
                      }}
                    >
                      09123456789
                    </Typography>
                  </Box>

                  <Divider
                    orientation="horizontal"
                    variant="fullWidth"
                    flexItem
                    sx={{
                      my: '16px',
                      borderColor: 'secondary.light',
                    }}
                  />

                  <Box
                    sx={{
                      width: '100%',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                    }}
                  >
                    <Typography
                      variant="inherit"
                      component="span"
                      color={'text.primary'}
                      sx={{
                        fontWeight: '400',
                        fontSize: { xs: '12px', sm: '16px' },
                      }}
                    >
                      {t('dialog_box:support_email')}
                    </Typography>

                    <Typography
                      variant="inherit"
                      component="span"
                      color={'text.primary'}
                      sx={{
                        fontWeight: '400',
                        fontSize: { xs: '12px', sm: '16px' },
                        fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Regular',
                      }}
                    >
                      info@zimapay.com
                    </Typography>
                  </Box>
                </Box>

                <Button
                  variant="contained"
                  disableRipple={true}
                  sx={{
                    width: { xs: '187px', sm: '236px' },
                    height: '40px',
                    '&:hover': {
                      backgroundColor: 'primary.main',
                      boxShadow: 'none',
                    },
                    boxShadow: 'none',
                    backgroundColor: 'primary.main',
                    borderRadius: '12px',
                    fontWeight: '600',
                    fontSize: '14px',
                  }}
                  onClick={() => close()}
                >
                  {t('dialog_box:close')}
                </Button>
              </Box>
            </DialogContent>

            {/*<DialogActions>*/}
            {/*    */}
            {/*</DialogActions>*/}
          </Dialog>
        )
      )}
    </>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default AlertDialogBox;
