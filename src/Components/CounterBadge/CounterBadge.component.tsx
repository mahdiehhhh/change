import React from 'react';
import { Box, Chip, Typography } from '@mui/material';
import { useTranslation } from 'next-i18next';
import { useI18nTrDir } from '../../utils/CustomHooks/i18nTrDir';

interface Props {
  value: number | string;
  width: string;
  height: string;
  bgColor: string;
  textColor: string;
}

const CounterBadge = ({ value, width, height, bgColor, textColor }: Props) => {
  const { t, pageDirection } = useI18nTrDir();

  return (
    <Chip
      label={value}
      sx={{
        minWidth: width,
        height: height,
        borderRadius: '11px',
        bgcolor: bgColor,
        '& .MuiChip-label': {
          padding: '0',
          margin: '0',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          color: textColor,
          fontSize: { xs: '14px', sm: '16px' },
          fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
          // fontWeight: '600'
        },
      }}
    />
  );
};

export { CounterBadge };
