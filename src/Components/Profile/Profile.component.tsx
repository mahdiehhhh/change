import React, { useEffect, useState, ChangeEvent, ReactNode } from 'react';
import {
  Box,
  ButtonGroup,
  Button,
  InputAdornment,
  IconButton,
  Select,
  MenuItem,
  TextField,
  InputLabel,
  Tabs,
  Tab,
  Chip,
  FormHelperText,
  FormControl,
} from '@mui/material';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useRouter } from 'next/router';
import { PATHS } from '../../configs/routes.config';
import starIcon from '/public/assets/images/Star-icon.svg';
import searchIconActive from '/public/assets/images/Search-icon-active.svg';
import coloredStarIcon from '/public/assets/images/colored-star-icon.svg';
import coloredPlusIcon from '*.svg';
import Typography from '@mui/material/Typography';
import { SearchBar } from '../SearchBar/SearchBar.component';
import FlexibleSearchBarComponent from '../FlexibleSearchBar/FlexibleSearchBar.component';
import FlexibleSearchBar from '../FlexibleSearchBar/FlexibleSearchBar.component';
import { dispatch } from 'jest-circus/build/state';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { fetchExchangeCurrenciesInfo } from '../../store/slices/currencies_exchange_infos.slice';
import { CurrencyTable } from '../CurrencyTable/CurrencyTable.component';
import { Trans } from 'next-i18next';
import { useI18nTrDir } from '../../utils/CustomHooks/i18nTrDir';
import { localesNS } from '../../utils/Data/locales_NameSpace.utils';
import bulletPoint from '/public/assets/images/BulletPoint.svg';
import garbageIcon from '/public/assets/images/garbage_icon.svg';
import dangerCircleIcon from '/public/assets/images/Danger-Circle-icon.svg';
import backwardArrowIcon from '/public/assets/images/backwardArrowDeposit.svg';
import attachmentIcon from '/public/assets/images/Attachment-icon.svg';
import editProfileIcon from '/public/assets/images/edit-profile-icon.svg';
import passportSampleImg from '/public/assets/images/correct-passport-sample-img.png';
import selfiSampleImg from '/public/assets/images/correct-selfi-sample-img.png';
import StepButton from '@mui/material/StepButton';
import Badge from '@mui/material/Badge';
import Avatar from '@mui/material/Avatar';
import jmoment from 'jalali-moment';

import { styled, useTheme } from '@mui/material/styles';
import Stack from '@mui/material/Stack';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';
import Check from '@mui/icons-material/Check';
import SettingsIcon from '@mui/icons-material/Settings';
import GroupAddIcon from '@mui/icons-material/GroupAdd';
import VideoLabelIcon from '@mui/icons-material/VideoLabel';
import StepConnector, { stepConnectorClasses } from '@mui/material/StepConnector';
import { StepIconProps } from '@mui/material/StepIcon';
import * as yup from 'yup';
import Tooltip from '@mui/material/Tooltip';
import { useFormik, Field, ErrorMessage } from 'formik';

import theme from 'tailwindcss/defaultTheme';
import Dropzone from '../DropZone/DropZone.component';
import { fetchProfileVerifyAuth } from '../../store/slices/profile_verify_authentication.slice';
import AlertDialogBox from '../AlertDialogBox/AlertDialogBox.component';
import { fetchUserProfile } from '../../store/slices/user_profile.slice';
import { calcCurrencyConversionCosts } from '../../utils/functions/calc_currency_conversion';
import useMediaQuery from '@mui/material/useMediaQuery';
import { fetchGetCountries } from '../../store/slices/get_countries.slice';

interface DocsPayloadSample {
  result: string;
  imageFile: any;
}

const Profile = () => {
  const router = useRouter();
  const UrlPath = router.pathname;
  const { t, i18n, pageDirection } = useI18nTrDir();

  const dispatch = useAppDispatch();
  const theme = useTheme();
  const lg = useMediaQuery(theme.breakpoints.up('lg'));

  const ColorlibConnector = styled(StepConnector)(({ theme }) => ({
    [`&.${stepConnectorClasses.alternativeLabel}`]: {
      top: 5,
      [theme.breakpoints.up('sm')]: {
        top: 9,
      },
      left: 'calc(-50% + 22px)',
      right: 'calc(50% + 22px)',
    },
    [`&.${stepConnectorClasses.active}`]: {
      [`& .${stepConnectorClasses.line}`]: {
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.primary.main,
      },
    },
    [`&.${stepConnectorClasses.completed}`]: {
      [`& .${stepConnectorClasses.line}`]: {
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.primary.main,
      },
    },
    [`& .${stepConnectorClasses.line}`]: {
      height: 3,
      border: 0,
      backgroundColor: theme.palette.text.disabled,
      borderRadius: 1,
    },
  }));

  const ColorlibStepIconRoot = styled('div')<{
    ownerState: { completed?: boolean; active?: boolean };
  }>(({ theme, ownerState }) => ({
    backgroundColor: theme.palette.text.disabled,
    zIndex: 1,
    color: '#fff',
    width: 12,
    height: 12,
    [theme.breakpoints.up('sm')]: {
      width: 20,
      height: 20,
    },
    display: 'flex',
    borderRadius: '50%',
    justifyContent: 'center',
    alignItems: 'center',
    ...(ownerState.active && {
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.primary.main,

      [theme.breakpoints.up('sm')]: {
        width: 32,
        height: 32,
      },
      [theme.breakpoints.down('sm')]: {
        width: 24,
        height: 24,
      },
    }),
    ...(ownerState.completed && {
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.primary.main,
      [theme.breakpoints.up('sm')]: {
        width: 32,
        height: 32,
      },
      [theme.breakpoints.down('sm')]: {
        width: 24,
        height: 24,
      },
    }),
  }));

  function ColorlibStepIcon(props: StepIconProps) {
    const { active, completed, className } = props;

    const icons: { [index: string]: React.ReactElement } = {
      1: (
        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
          <g id="Iconly/Light/Profile">
            <g id="Profile">
              <path
                id="Stroke 1"
                fillRule="evenodd"
                clipRule="evenodd"
                d="M7.99125 10.2305C5.41284 10.2305 3.21094 10.6203 3.21094 12.1816C3.21094 13.7428 5.39887 14.1467 7.99125 14.1467C10.5697 14.1467 12.7709 13.7562 12.7709 12.1955C12.7709 10.6349 10.5836 10.2305 7.99125 10.2305Z"
                stroke="white"
                strokeWidth="1.5"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
              <path
                id="Stroke 3"
                fillRule="evenodd"
                clipRule="evenodd"
                d="M7.99383 8.00262C9.6859 8.00262 11.0573 6.63056 11.0573 4.93849C11.0573 3.24643 9.6859 1.875 7.99383 1.875C6.30177 1.875 4.92971 3.24643 4.92971 4.93849C4.92399 6.62484 6.28653 7.9969 7.97225 8.00262H7.99383Z"
                stroke="white"
                strokeWidth="1.42857"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </g>
          </g>
        </svg>
      ),
      2: (
        <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
          <g id="Iconly/Light/Paper">
            <g id="Paper">
              <path
                id="Stroke 1"
                fillRule="evenodd"
                clipRule="evenodd"
                d="M11.0534 2.07033H6.06369C4.50369 2.06433 3.22494 3.30783 3.18819 4.86708V12.9018C3.15369 14.4866 4.40994 15.7998 5.99469 15.8351C6.01794 15.8351 6.04044 15.8358 6.06369 15.8351H12.0554C13.6259 15.7713 14.8634 14.4738 14.8522 12.9018V6.02733L11.0534 2.07033Z"
                stroke="white"
                strokeWidth="1.5"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
              <path
                id="Stroke 3"
                d="M10.8594 2.0625V4.24425C10.8594 5.30925 11.7204 6.1725 12.7854 6.1755H14.8516"
                stroke="white"
                strokeWidth="1.5"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
              <path
                id="Stroke 5"
                d="M10.7141 11.5195H6.66406"
                stroke="white"
                strokeWidth="1.5"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
              <path
                id="Stroke 7"
                d="M9.18106 8.70312H6.66406"
                stroke="white"
                strokeWidth="1.5"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </g>
          </g>
        </svg>
      ),
      3: (
        <svg width="16" height="18" viewBox="0 0 16 18" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M11 7.65595C11 6.08092 9.65692 4.80469 8.00061 4.80469C6.34308 4.80469 5 6.08092 5 7.65595C5 9.22981 6.34308 10.506 8.00061 10.506C9.65692 10.506 11 9.22981 11 7.65595Z"
            stroke="white"
            strokeWidth="1.5"
            strokeLinecap="round"
            strokeLinejoin="round"
          />
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M7.99954 17.1538C6.88097 17.1538 1 12.5755 1 7.78757C1 4.03929 4.13329 1 7.99954 1C11.8658 1 15 4.03929 15 7.78757C15 12.5755 9.11812 17.1538 7.99954 17.1538Z"
            stroke="white"
            strokeWidth="1.5"
            strokeLinecap="round"
            strokeLinejoin="round"
          />
        </svg>
      ),
    };

    return (
      <ColorlibStepIconRoot ownerState={{ completed, active }} className={className}>
        {completed ? (
          icons[String(props.icon)]
        ) : active ? (
          icons[String(props.icon)]
        ) : (
          <Box
            sx={{
              width: '20px',
              height: '20px',
              borderRadius: '50%',
            }}
          ></Box>
        )}
      </ColorlibStepIconRoot>
    );
  }

  const userProfileResult = useAppSelector((state) => state.UserProfile);
  const profileVerifyAuthResult = useAppSelector((state) => state.ProfileVerifyAuthentication);
  const getCountriesResult = useAppSelector((state) => state.GetCountries);

  const profileTabs = [
    {
      id: 1,
      title: t('profile:my_info'),
      link: `${PATHS.USER_PROFILE}`,
    },
    {
      id: 2,
      title: t('profile:my_bank_accounts'),
      link: `${PATHS.USER_PROFILE}/${PATHS.BANK_ACCOUNTS}`,
    },
    {
      id: 3,
      title: t('profile:authentication'),
      link: `${PATHS.USER_PROFILE}/${PATHS.VERIFY}`,
    },
  ];

  const steps = [t('profile:personal_info'), t('profile:documents_img'), t('profile:location_info')];

  const verificationOptions = {
    id: 1,
    // urlKey: PATHS.IRANIAN,
    formTitle: t('profile:authentication'),
    formOptions: [
      {
        id: 'first_name',
        api_id: 'first_name',
        title: t('profile:name'),
        placeholder: t('profile:enter_name'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'false',
        width: '50%',
      },
      {
        id: 'last_name',
        api_id: 'last_name',
        title: t('profile:family_name'),
        placeholder: t('profile:enter_family_name'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'false',
        width: '50%',
      },
      {
        id: 'nationality',
        api_id: 'nationality',
        title: t('profile:nationality'),
        placeholder: t('profile:choose'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'true-simple',
        width: '50%',
      },
      {
        id: 'identity_code',
        api_id: 'identity_code',
        title: `${t('profile:national_code')}/${t('profile:passport_number')}`,
        placeholder: t('profile:national_code'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'false',
        width: '50%',
      },
      {
        id: 'birth_date',
        api_id: 'birth_date',
        title: t('profile:birthday'),
        subInput: [
          {
            id: 'birthday_day',
            api_id: 'birthday_day',
            title: ``,
            placeholder: t('profile:day'),
            validation: yup
              .string()
              .required(t('bank_accounts:required_text'))
              .matches(/(?=.*?\d)^\$?(([1-9]\d{0,2}(,\d{3})*)|\d+)?(\.\d{1,2})?$/, t('bank_accounts:number_text'))
              .test('max', 'مقدار مجاز بین 1 تا 31 میباشد', function (value: string | undefined) {
                // @ts-ignore
                return +value < 32 && +value > 0;
              }),
            hasSelect: 'false',
            width: '30%',
          },
          {
            id: 'birthday_month',
            api_id: 'birthday_month',
            title: ``,
            placeholder: t('profile:month'),
            validation: yup
              .string()
              .required(t('bank_accounts:required_text'))
              .matches(/(?=.*?\d)^\$?(([1-9]\d{0,2}(,\d{3})*)|\d+)?(\.\d{1,2})?$/, t('bank_accounts:number_text'))
              .test('max', 'مقدار مجاز بین 1 تا 12 میباشد', function (value: string | undefined) {
                // @ts-ignore
                return +value < 13 && +value > 0;
              }),
            hasSelect: 'false',
            width: '30%',
          },
          {
            id: 'birthday_year',
            api_id: 'birthday_year',
            title: ``,
            placeholder: t('profile:year'),
            validation: yup
              .string()
              .required(t('bank_accounts:required_text'))
              .matches(/(?=.*?\d)^\$?(([1-9]\d{0,2}(,\d{3})*)|\d+)?(\.\d{1,2})?$/, t('bank_accounts:number_text'))
              .test('max', 'مقدار معتبر نیست', function (value: string | undefined) {
                // @ts-ignore
                if (i18n.language === 'fa') {
                  // @ts-ignore
                  return +value <= jmoment(new Date()).locale('fa').format('YYYY');
                }
                // @ts-ignore
                return +value <= new Date().getFullYear();
              }),
            hasSelect: 'false',
            width: '30%',
          },
        ],
        // validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'false',
        width: '50%',
      },
    ],
  };

  const docsImgOptions = {
    id: 2,
    // urlKey: PATHS.IRANIAN,
    formTitle: t('profile:upload_docs'),
    formOptions: [
      {
        id: 'profile_image',
        api_id: 'profile_image',
        title: '',
        placeholder: '',
        // validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'false',
        width: '100%',
        tip: [],
      },
      {
        id: 'id_card_image',
        api_id: 'id_card_image',
        title: t('profile:passport_img'),
        placeholder: '',
        // validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'false',
        width: '100%',
      },
      {
        id: 'id_card_selfie',
        api_id: 'id_card_selfie',
        title: t('profile:self_img_with_docs'),
        placeholder: '',
        // validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'false',
        width: '100%',
        tip: [t('profile:correct_take_img')],
      },
    ],
  };

  const locationsInfoOptions = {
    id: 3,
    // urlKey: PATHS.IRANIAN,
    formTitle: t('profile:specs'),
    formOptions: [
      {
        id: 'email',
        api_id: 'email',
        title: t('profile:email'),
        placeholder: t('profile:enter_email'),
        validation: yup.string().required(t('bank_accounts:required_text')).email(t('bank_accounts:invalid_email')),
        hasSelect: 'false',
        width: '50%',
      },
      {
        id: 'land_line',
        api_id: 'phone',
        title: t('profile:land_line_num'),
        placeholder: t('profile:enter_land_line'),
        validation: yup
          .string()
          .required(t('bank_accounts:required_text'))
          .matches(/(?=.*?\d)^\$?(([1-9]\d{0,2}(,\d{3})*)|\d+)?(\.\d{1,2})?$/, t('bank_accounts:number_text')),
        hasSelect: 'false',
        width: '50%',
      },
      {
        id: 'citizenship',
        api_id: 'citizenship',
        title: t('profile:country'),
        placeholder: t('profile:choose'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'true-simple',
        width: '50%',
      },
      {
        id: 'state',
        api_id: 'state',
        title: t('profile:province'),
        placeholder: t('profile:enter_province'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'false',
        width: '50%',
      },
      {
        id: 'city',
        api_id: 'city',
        title: t('profile:city'),
        placeholder: t('profile:enter_city'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'false',
        width: '50%',
      },
      {
        id: 'address',
        api_id: 'address',
        title: t('profile:address_details'),
        placeholder: t('profile:enter_address_details'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'false',
        width: '50%',
      },
      {
        id: 'postal_code',
        api_id: 'postal_code',
        title: t('profile:postal_code'),
        placeholder: t('profile:enter_postal_code'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'false',
        width: '50%',
      },
    ],
  };
  // const [activeProfileTab, setActiveProfileTab] = useState(profileTabs[0])

  const authenticationStepsOptions = [verificationOptions, docsImgOptions, locationsInfoOptions];
  const [activeAuthStep, setActiveAuthStep] = useState(authenticationStepsOptions[0].id);
  const [activeAuthStepOptions, setActiveAuthStepOptions] = useState(verificationOptions);
  const [completedAuthStep, setCompletedAuthStep] = useState(authenticationStepsOptions[0].id);
  const [passImgIsValid, setPassImgIsValid] = useState('');
  const [selfiImgIsValid, setSelfiImgIsValid] = useState('');
  const [profileImgIsValid, setProfileImgIsValid] = useState('');
  const docsImgTips = {
    passport: {
      tips: [t('profile:clear_img'), t('common:limited_max_vol', { vol: '1', unit: t('common:MB') })],
      image: passportSampleImg,
    },
    selfi: {
      tips: [t('profile:correct_take_img')],
      image: selfiSampleImg,
    },
  };
  const [docsImgValidation, setDocsImgValidation] = useState({ id_card_image: null, id_card_selfie: null });
  const [totalAuthData, setTotalAuthData] = useState<any>({});
  const [openResultDialogBox, setOpenResultDialogBox] = React.useState(false);
  const [operationResultState, setOperationResultState] = useState<object>({
    status: '',
    messages: {},
  });
  const [inputValues, setInputValues] = useState<any>({});
  const [personalInfosInputValuesState, setPersonalInfosInputValuesState] = useState<any>({});
  const [locationInfosInputValuesState, setLocationInfosInputValuesState] = useState<any>({});
  const [nationalityState, setNationalityState] = useState(undefined);
  const [countryState, setCountryState] = useState(undefined);
  const [profileImageFile, setProfileImageFile] = useState<any>();
  const [passImageFile, setPassImageFile] = useState<any>(null);
  const [selfieImageFile, setSelfieImageFile] = useState<any>(null);
  const [imagesInvalidFileType, setImagesInvalidFileType] = useState<any>({
    profile_image: '',
    id_card_image: '',
    id_card_selfie: '',
  });

  const [preview, setPreview] = useState<string>();
  const [passPreview, setPassPreview] = useState<string>();
  const [selfiePreview, setSelfiePreview] = useState<string>();
  const [showEmailTooltip, setShowEmailTooltip] = useState(false);

  const handleCloseResultDB = () => {
    setOpenResultDialogBox(false);
  };

  useEffect(() => {
    // if (!imageFile) {
    //     setPreview(undefined)
    //     return
    // }

    const objectUrl = profileImageFile && URL.createObjectURL(profileImageFile);
    setPreview(objectUrl);

    // free memory when ever this component is unmounted
    return () => URL.revokeObjectURL(objectUrl as string);
  }, [profileImageFile]);

  useEffect(() => {
    // if (!imageFile) {
    //     setPreview(undefined)
    //     return
    // }

    const objectUrl = passImageFile && URL.createObjectURL(passImageFile);
    setPassPreview(objectUrl);

    // free memory when ever this component is unmounted
    return () => URL.revokeObjectURL(objectUrl as string);
  }, [passImageFile]);

  useEffect(() => {
    // if (!imageFile) {
    //     setPreview(undefined)
    //     return
    // }

    const objectUrl = selfieImageFile && URL.createObjectURL(selfieImageFile);
    setSelfiePreview(objectUrl);

    // free memory when ever this component is unmounted
    return () => URL.revokeObjectURL(objectUrl as string);
  }, [selfieImageFile]);

  const bankAccountTabs = [
    {
      id: 1,
      title: t('profile:my_info'),
      link: `${PATHS.USER_PROFILE}`,
    },
    {
      id: 2,
      title: t('profile:my_bank_accounts'),
      link: `${PATHS.USER_PROFILE}/${PATHS.BANK_ACCOUNTS}`,
    },
    {
      id: 3,
      title: t('profile:authentication'),
      link: `${PATHS.USER_PROFILE}/${PATHS.VERIFY}`,
    },
  ];

  const [activeAccountTab, setActiveAccountTab] = useState(bankAccountTabs[0]);
  const [countryList, setCountrylist] = useState([]);

  const nationalityList = [
    {
      id: 1,
      name: 'ایرانی',
    },
    {
      id: 2,
      name: 'افغانی',
    },
    {
      id: 3,
      name: 'جیبوتیایی',
    },
    {
      id: 4,
      name: 'گینه ای',
    },
    {
      id: 5,
      name: 'ونزوئلایی',
    },
  ];

  useEffect(() => {
    const selectedTab = bankAccountTabs.find((tab) => `/${tab.link}` === UrlPath);
    // @ts-ignore
    setActiveAccountTab(selectedTab);
  }, [UrlPath]);

  useEffect(() => {
    CreateStepsInitialInputValues();
  }, []);

  useEffect(() => {
    setCountrylist(getCountriesResult?.CountriesResp?.country_list);
  }, [getCountriesResult]);

  useEffect(() => {
    if (userProfileResult.userProfileResp?.user_type_key === 'validated') {
      router.push(`/${PATHS.USER_PROFILE}`);
    } else {
      dispatch(fetchGetCountries());
    }
  }, [userProfileResult]);

  useEffect(() => {
    if (
      userProfileResult.userProfileResp?.user_type_key !== 'validated' &&
      !!profileVerifyAuthResult.profileVerifyAuthResp?.data &&
      !profileVerifyAuthResult.error
    ) {
      setOpenResultDialogBox(true);
      setOperationResultState({
        status: 'success',
        messages: profileVerifyAuthResult.profileVerifyAuthResp?.message,
      });
    }
  }, [profileVerifyAuthResult]);

  const CreateStepsInitialInputValues = () => {
    authenticationStepsOptions.forEach((item) => {
      // @ts-ignore
      const stepInitialInputsValueObj = item.formOptions.reduce((accumulator, value) => {
        let subInputValidations;
        let withoutSubInputValidations = { ...accumulator };
        if (!!value.subInput) {
          // @ts-ignore
          subInputValidations = value.subInput.reduce((accumulator, value) => {
            let subSchemaAcc = { ...accumulator };
            if (!!value.validation) {
              subSchemaAcc = {
                ...subSchemaAcc,
                [value.api_id]:
                  item.id === 1
                    ? personalInfosInputValuesState[value.api_id]
                      ? personalInfosInputValuesState[value.api_id]
                      : ''
                    : item.id === 3 && locationInfosInputValuesState[value.api_id]
                    ? locationInfosInputValuesState[value.api_id]
                    : '',
              };
            }
            return { ...subSchemaAcc };
          }, {});
        } else {
          // @ts-ignore
          withoutSubInputValidations = {
            ...withoutSubInputValidations,
            [value.api_id]:
              item.id === 1
                ? personalInfosInputValuesState[value.api_id]
                  ? personalInfosInputValuesState[value.api_id]
                  : ''
                : item.id === 3 && locationInfosInputValuesState[value.api_id]
                ? locationInfosInputValuesState[value.api_id]
                : '',
          };
        }
        return {
          ...subInputValidations,
          ...withoutSubInputValidations,
        };
      }, {});

      if (item.id === 1) {
        setPersonalInfosInputValuesState(stepInitialInputsValueObj);
      } else if (item.id === 3) {
        setLocationInfosInputValuesState(stepInitialInputsValueObj);
      }
    });
  };

  const selectNewIconSimple = (props: any) => (
    <svg {...props} width="16" height="9" viewBox="0 0 16 9" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M15 1L8 8L1 1" stroke="#606060" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
    </svg>
  );

  const coloredNewIcon = (props: any) => (
    <svg {...props} width="9" height="9" viewBox="0 0 9 9" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M3.65175 7.85617C3.60825 7.81177 3.42225 7.64414 3.26925 7.488C2.307 6.57254 0.732 4.1844 0.25125 2.93445C0.174 2.74462 0.0105 2.2647 0 2.00828C0 1.76258 0.054 1.52836 0.1635 1.30485C0.3165 1.02623 0.55725 0.802727 0.8415 0.680258C1.03875 0.601419 1.629 0.47895 1.6395 0.47895C2.28525 0.356481 3.3345 0.289124 4.494 0.289124C5.59875 0.289124 6.60525 0.356481 7.26075 0.456753C7.27125 0.468234 8.00475 0.590703 8.256 0.724653C8.715 0.970357 9 1.45028 9 1.96389V2.00828C8.98875 2.34277 8.70375 3.0462 8.69325 3.0462C8.21175 4.22879 6.714 6.56183 5.71875 7.49948C5.71875 7.49948 5.463 7.76355 5.30325 7.87837C5.07375 8.05748 4.7895 8.14627 4.50525 8.14627C4.188 8.14627 3.8925 8.04599 3.65175 7.85617Z"
        fill="#298FFE"
      />
    </svg>
  );

  const handleChangeSelectValues = (event: any, input: string) => {
    if (input === 'nationality') {
      setNationalityState(event.target.value);
      setPersonalInfosInputValuesState((prevState: any) => ({
        ...prevState,
        [input]: JSON.parse(event.target.value).id,
      }));
      // setBankNameState(event.target.value)
      // setInputValues((prevState: any) => ({...prevState, [input]: JSON.parse(event.target.value).id}))
    } else if (input === 'citizenship') {
      setCountryState(event.target.value);
      setLocationInfosInputValuesState((prevState: any) => ({
        ...prevState,
        [input]: JSON.parse(event.target.value).id,
      }));
    }
  };

  const handleGetImageFile = (e: ChangeEvent<HTMLInputElement>, id: string) => {
    // @ts-ignore
    const selectedFile = e.target.files[0];
    if (selectedFile) {
      const SUPPORTED_FORMATS = ['image/jpg', 'image/jpeg', 'image/png'];
      if (!SUPPORTED_FORMATS.includes(selectedFile.type)) {
        setImagesInvalidFileType((prevState: any) => ({
          ...prevState,
          [id]: t('common:allowed_jpg_png'),
        }));
      } else if (SUPPORTED_FORMATS.includes(selectedFile.type)) {
        setImagesInvalidFileType((prevState: any) => ({
          ...prevState,
          [id]: '',
        }));
      } else if (+selectedFile.size > (id === 'profile_image' ? 1 : 8) * 1024 * 1024) {
        setImagesInvalidFileType((prevState: any) => ({
          ...prevState,
          [id]:
            id === 'profile_image'
              ? t('common:limited_max_vol', {
                  vol: '1',
                  unit: t('common:MB'),
                })
              : t('common:limited_max_vol', { vol: '8', unit: t('common:MB') }),
        }));
      } else if (+selectedFile.size < (id === 'profile_image' ? 1 : 8) * 1024 * 1024) {
        setImagesInvalidFileType((prevState: any) => ({
          ...prevState,
          [id]: '',
        }));
      }

      id === 'profile_image'
        ? setProfileImageFile(selectedFile)
        : id === 'id_card_image'
        ? setPassImageFile(selectedFile)
        : setSelfieImageFile(selectedFile);
      // @ts-ignore
      // formik.handleChange('profile_image')(selectedFile.name)
    }
  };

  const handleSetInputValues = (event: any, inputValue: string) => {
    if (activeAuthStep === 1) {
      setPersonalInfosInputValuesState((prevState: any) => ({ ...prevState, [inputValue]: event.target.value }));
    } else if (activeAuthStep === 3) {
      setLocationInfosInputValuesState((prevState: any) => ({ ...prevState, [inputValue]: event.target.value }));
    }
    // setInputValues((prevState: any) => ({...prevState, [inputValue]: event.target.value}))
  };

  const createBankAccountYupSchema = () => {
    const specificStepYupSchema = authenticationStepsOptions.map((item) => {
      // @ts-ignore
      const stepsYapSchema = item.formOptions.reduce((accumulator, value) => {
        let schemaAcc = { ...accumulator };
        let subInputValidations;
        if (!!value.validation) {
          schemaAcc = {
            ...schemaAcc,
            [value.api_id]: value.validation,
          };
        }
        if (!!value.subInput) {
          // @ts-ignore
          subInputValidations = value.subInput.reduce((accumulator, value) => {
            let subSchemaAcc = { ...accumulator };
            if (!!value.validation) {
              subSchemaAcc = {
                ...subSchemaAcc,
                [value.api_id]: value.validation,
              };
            }
            return { ...subSchemaAcc };
          }, {});
        }
        return { ...schemaAcc, ...subInputValidations };
      }, {});
      return { ...stepsYapSchema };
    });
    return specificStepYupSchema[activeAuthStep - 1];
  };

  let bankAccountSchemaDeclaration: object = createBankAccountYupSchema();
  const bankAccountValidationSchema = yup.object({ ...bankAccountSchemaDeclaration });

  const formik = useFormik({
    enableReinitialize: true,
    // initialValues: selectInitialValueByType(),
    initialValues:
      activeAuthStep === 1 ? personalInfosInputValuesState : activeAuthStep === 3 ? locationInfosInputValuesState : {},
    validationSchema: bankAccountValidationSchema,
    validateOnChange: true,
    validateOnBlur: false,
    onSubmit: async (values) => {
      handleSaveCompletedStepsData();

      if (activeAuthStep < authenticationStepsOptions.length) {
        if (activeAuthStep < completedAuthStep) {
          activeAuthStep === 2
            ? handleDocsImgWarning() && setActiveAuthStep(activeAuthStep + 1)
            : setActiveAuthStep(activeAuthStep + 1);
        }
        if (completedAuthStep === activeAuthStep) {
          activeAuthStep === 2
            ? handleDocsImgWarning() && setCompletedAuthStep(completedAuthStep + 1)
            : setCompletedAuthStep(completedAuthStep + 1);
          activeAuthStep === 2
            ? handleDocsImgWarning() && setActiveAuthStep(activeAuthStep + 1)
            : setActiveAuthStep(activeAuthStep + 1);
        }
      }
    },
  });

  useEffect(() => {
    if (activeAuthStep === 3) {
      let completeData = { ...totalAuthData };
      completeData['profile_image'] = profileImageFile;
      completeData['id_card_image'] = docsImgValidation['id_card_image'];
      completeData['id_card_selfie'] = docsImgValidation['id_card_selfie'];
      dispatch(fetchProfileVerifyAuth(completeData));
    }
  }, [totalAuthData]);

  const handleSaveCompletedStepsData = () => {
    if (activeAuthStep === 1) {
      const { birthday_day, birthday_month, birthday_year, ...others } = personalInfosInputValuesState;
      others[
        'birth_date'
      ] = `${personalInfosInputValuesState['birthday_year']}/${personalInfosInputValuesState['birthday_month']}/${personalInfosInputValuesState['birthday_day']}`;
      setTotalAuthData({ ...totalAuthData, ...others });
    }
    if (activeAuthStep === 3) {
      setTotalAuthData({ ...totalAuthData, ...locationInfosInputValuesState });
    }
  };

  const handleDocsImgWarning = (): boolean => {
    const passDocValid = docsImgValidation['id_card_image'];
    const selfiDocValid = docsImgValidation['id_card_selfie'];
    if (lg ? passDocValid : passImageFile) {
      setPassImgIsValid('');
    }
    if ((lg ? passDocValid : passImageFile) === null) {
      setPassImgIsValid(t('bank_accounts:required_text'));
    }
    if (lg ? selfiDocValid : selfieImageFile) {
      setSelfiImgIsValid('');
    }
    if ((lg ? selfiDocValid : selfieImageFile) === null) {
      setSelfiImgIsValid(t('bank_accounts:required_text'));
    }
    if (preview) {
      setProfileImgIsValid('');
    } else {
      setProfileImgIsValid(t('bank_accounts:required_text'));
    }
    if (!!(lg ? passDocValid : passImageFile) && !!(lg ? selfiDocValid : selfieImageFile) && !!preview) {
      return true;
    } else {
      return false;
    }
  };

  const handleDeleteImage = (id: string) => {
    setImagesInvalidFileType((prevState: any) => ({
      ...prevState,
      [id]: '',
    }));
    if (id === 'id_card_image') {
      setPassPreview(undefined);
      setPassImageFile(null);
    } else {
      setSelfiePreview(undefined);
      setSelfieImageFile(null);
    }
  };

  const handleStep = (step: number) => () => {
    setActiveAuthStep(step);
  };

  useEffect(() => {
    const activeStepOptions = authenticationStepsOptions.find((stepOption) => stepOption.id === activeAuthStep);
    // @ts-ignore
    setActiveAuthStepOptions(activeStepOptions);
    CreateStepsInitialInputValues();
  }, [activeAuthStep]);

  const handleSetDocsImgValidation = (payload: DocsPayloadSample) => {
    const key = payload.result.split('/')[1];
    const validation = payload.result.split('/')[0];
    if (JSON.parse(validation)) {
      const imageFile = payload.imageFile;
      setDocsImgValidation({ ...docsImgValidation, [key]: imageFile });
    } else {
      setDocsImgValidation({ ...docsImgValidation, [key]: JSON.parse(validation) });
    }
  };

  // @ts-ignore
  return (
    <>
      <Box
        sx={{
          width: '100%',
          height: 'auto',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'flex-start',
          justifyContent: 'flex-start',
        }}
      >
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            mb: { xs: '24px', sm: '64px' },
            mt: { xs: '15px', sm: 0 },
          }}
        >
          <Tabs
            value={+activeAccountTab?.id - 1}
            // onChange={handleChangeTabs}
            aria-label="nav tabs example"
            TabIndicatorProps={{ style: { display: 'none' } }}
          >
            {profileTabs.map((tab) => {
              return (
                <Tab
                  key={tab.id}
                  label={tab.title}
                  disableRipple={true}
                  sx={{
                    fontSize: { xs: '14px', sm: '16px' },
                    // fontWeight: +activeAccountTab?.id === +tab.id ? '600' : '400',
                    fontFamily:
                      +activeAccountTab?.id === +tab.id ? 'IRANSansXFaNum-DemiBold' : 'IRANSansXFaNum-Regular',
                    borderRadius: '12px',
                    // transform: 'perspective(10px) rotateX(5deg)',
                    // transformOrigin: 'bottom',
                    bgcolor: +activeAccountTab?.id === +tab.id ? 'white' : 'transparent',
                  }}
                  component="a"
                  onClick={(event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
                    event.preventDefault();
                    router.push(`/${tab.link}`);
                  }}
                  // href={tab.link}
                />
              );
            })}
          </Tabs>
        </Box>

        <Box
          sx={{
            width: '100%',
            display: 'flex',
            justifyContent: 'center',
          }}
        >
          <Box
            sx={{
              // width: {sm: '664px', lg: activeAuthStep === 2 ? '774px' : '664px'},
              maxWidth: { xs: '100%', sm: '664px', lg: activeAuthStep === 2 ? '774px' : '664px' },
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <Stack sx={{ width: { xs: '100%', md: '122%' } }} spacing={4}>
              <Stepper
                alternativeLabel
                activeStep={completedAuthStep - 1}
                sx={{ alignItems: 'center' }}
                connector={<ColorlibConnector />}
              >
                {steps.map((label, index) => (
                  <Step
                    key={label}
                    sx={{
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                      px: '0',
                    }}
                  >
                    <StepButton
                      color="inherit"
                      disableRipple={true}
                      sx={{
                        '& .MuiStepLabel-alternativeLabel, .MuiStepLabel-label': {
                          fontSize: { xs: '10px', sm: '17px' },
                          fontWeight: '400',
                          color: 'text.disabled',
                        },
                        '& .Mui-active, .Mui-completed, .MuiStepLabel-label.Mui-active, .MuiStepLabel-label.Mui-completed':
                          {
                            fontSize: { xs: '10px', sm: '17px' },
                            // fontWeight: '600',
                            fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                            color: 'primary.main',
                          },
                      }}
                      onClick={handleStep(index + 1)}
                    >
                      <StepLabel StepIconComponent={ColorlibStepIcon}>{label}</StepLabel>
                    </StepButton>
                  </Step>
                ))}
              </Stepper>
            </Stack>

            <Box
              sx={{
                minWidth: { xs: '100%', lg: '100%' },
                flex: 1,
                padding: { xs: '16px 12px', sm: '19px 24px' },
                borderRadius: '22px',
                bgcolor: 'white',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'flex-start',
                mt: '24px',
              }}
              component={'form'}
              onSubmit={formik.handleSubmit}
            >
              <Box
                sx={{
                  width: '100%',
                  display: 'flex',
                  flexDirection: { xs: 'column', sm: 'row' },
                  alignItems: { xs: 'flex-start', sm: 'center' },
                  // justifyContent: 'flex-start',
                }}
              >
                <Box
                  sx={{
                    width: 'auto',
                    display: 'flex',
                    alignItems: 'center',
                    mr: '30px',
                  }}
                >
                  <Box
                    component={'img'}
                    src={bulletPoint.src}
                    alt={'bullet-point'}
                    sx={{
                      width: '22px',
                      height: '18px',
                      mr: '9px',
                      transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                    }}
                  />
                  <Typography
                    variant="inherit"
                    component="span"
                    color={'text.primary'}
                    sx={{
                      // fontWeight: {xs: '600'},
                      fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                      fontSize: { xs: '16px', xl: '16px' },
                    }}
                  >
                    {activeAuthStepOptions.formTitle}
                  </Typography>
                </Box>

                {activeAuthStep === 1 && (
                  <Box
                    sx={{
                      width: 'auto',
                      display: 'flex',
                      alignItems: 'center',
                      mt: { xs: '20px', sm: '0' },
                    }}
                  >
                    <Box
                      component={'img'}
                      src={dangerCircleIcon.src}
                      alt={'bullet-point'}
                      sx={{
                        width: '22px',
                        height: '18px',
                        mr: '9px',
                        transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                      }}
                    />
                    <Typography
                      variant="inherit"
                      component="span"
                      color={'error.main'}
                      sx={{
                        fontWeight: { xs: '400' },
                        fontSize: { xs: '12px', sm: '14px' },
                      }}
                    >
                      {t('profile:not_verify_as_real_user')}
                    </Typography>
                  </Box>
                )}
              </Box>

              <Box
                sx={{
                  width: '100%',
                  height: 'auto',
                  display: 'flex',
                  alignItems: 'flex-start',
                  justifyContent: 'space-between',
                  flexWrap: 'wrap',
                  mt: '32px',
                  // bgcolor: "red"
                }}
              >
                {activeAuthStepOptions.formOptions.map((option) => {
                  return activeAuthStepOptions.id === 1 || activeAuthStepOptions.id === 3 ? (
                    <Box
                      key={option.id}
                      sx={{
                        width: { xs: '100%', sm: '45%' },
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'flex-start',
                        mb: { xs: '40px', sm: '48px' },
                      }}
                    >
                      <Box
                        sx={{
                          display: 'flex',
                          alignItems: 'center',
                        }}
                      >
                        <InputLabel
                          htmlFor={option.id}
                          sx={{
                            fontSize: {
                              xs: '17px',
                              sm: option.api_id === 'document_image' ? '14px' : '17px',
                              lg: '17px',
                            },
                            fontWeight: '400',
                            color: 'text.primary',
                          }}
                        >
                          {option.title}
                        </InputLabel>

                        {option.id === 'email' && (
                          <Tooltip
                            title={t('profile:email_tooltip')}
                            placement={pageDirection === 'rtl' ? 'left' : 'right'}
                            arrow={true}
                            // disableInteractive={true}
                            open={showEmailTooltip}
                            PopperProps={{
                              sx: {
                                '& .MuiTooltip-tooltip': {
                                  padding: '15px',
                                  borderRadius: '16px',
                                  border: '1px solid',
                                  borderColor: 'error.main',
                                  bgcolor: 'error.light',
                                  mx: '12px',
                                  color: 'text.primary',
                                  fontSize: { xs: '10px', sm: '14px' },
                                  fontWeight: '400',
                                },

                                '& .MuiTooltip-arrow': {
                                  width: '52px',
                                  height: '52px',
                                  color: 'error.light',
                                  // bgcolor: "blue",
                                  // right: 'auto',
                                  left: '-10px',
                                  right: 'auto',
                                  '&::before': {
                                    // borderLeft: '20px solid transparent',
                                    // borderRight: '20px solid transparent',
                                    // borderBottom: 'calc(2 * 20px * 0.866) solid green',
                                    // borderTop: '20px solid transparent',
                                    // display: 'inline-block',
                                    width: { xs: '7px', sm: '11px' },
                                    height: { xs: '7px', sm: '11px' },

                                    // backgroundColor: "blue",
                                    border: '1px solid',
                                    borderColor: 'error.main',
                                    overflow: 'hidden',
                                  },
                                },
                              },
                            }}
                          >
                            <Box
                              component={'img'}
                              src={dangerCircleIcon.src}
                              // alt={'backward icon'}
                              sx={{
                                width: '18.5px',
                                height: '18.5px',
                                ml: '11px',
                              }}
                            />
                          </Tooltip>
                        )}
                      </Box>

                      {option.hasSelect.includes('false') ? (
                        <Box
                          sx={{
                            width: '100%',
                            display: 'flex',
                            alignItems: 'flex-start',
                            justifyContent: 'space-between',
                            mt: '8px',
                          }}
                        >
                          {!!option.subInput ? (
                            option.subInput.map((subField) => (
                              <TextField
                                key={subField.id}
                                value={formik['values'][subField.api_id]}
                                onChange={(event) => {
                                  formik.handleChange(subField.api_id)(event);
                                  handleSetInputValues(event, subField.api_id);
                                }}
                                error={formik['touched'][subField.api_id] && Boolean(formik['errors'][subField.api_id])}
                                helperText={
                                  formik['touched'][subField.api_id] && (formik['errors'][subField.api_id] as ReactNode)
                                }
                                name={subField.api_id}
                                fullWidth
                                id={subField.api_id}
                                placeholder={subField.placeholder}
                                type={'text'}
                                autoComplete="off"
                                sx={{
                                  '& input::placeholder': {
                                    fontSize: '14px',
                                    color: 'text.secondary',
                                  },
                                  width: subField.width,
                                  // flexGrow: '1',
                                  height: '100%',
                                  // bgcolor: option.id === 'passport_image' ? preview ? 'white' : 'secondary.main' : inputValues[option.api_id] ? 'white' : 'secondary.main',
                                  borderRadius: '12px',
                                  '& .MuiInputAdornment-root': {
                                    ml: '8px',
                                    height: '40px',
                                    maxHeight: '40px',
                                    backgroundColor:
                                      subField.id === 'passport_image'
                                        ? preview
                                          ? 'white'
                                          : 'secondary.main'
                                        : 'secondary.main',
                                    borderRadius: '12px',
                                    // border: option.id === 'passport_image' ? '1px dash' : 'none'
                                  },
                                  '& legend': { display: 'none' },
                                  '& fieldset': {
                                    top: 0,
                                    borderRadius: '12px',
                                    border:
                                      subField.id === 'passport_image' ? (preview ? '1px solid' : '1px dashed') : '',
                                    borderColor:
                                      activeAuthStep === 1
                                        ? personalInfosInputValuesState[subField.api_id]
                                          ? 'primary.main'
                                          : 'secondary.main'
                                        : activeAuthStep === 3
                                        ? locationInfosInputValuesState[subField.api_id]
                                          ? 'primary.main'
                                          : 'secondary.main'
                                        : 'secondary.main',
                                  },
                                  '& .MuiOutlinedInput-root:hover': {
                                    '& > fieldset': {
                                      border: subField.id === 'passport_image' ? '1px dash' : '',
                                      borderColor: inputValues[subField.api_id]
                                        ? 'primary.main'
                                        : subField.id === 'passport_image'
                                        ? preview
                                          ? 'primary.main'
                                          : 'text.disabled'
                                        : 'secondary.main',
                                    },
                                  },
                                  // '& fieldset:hover': {top: 0, borderRadius: '12px', border: '1px solid red'},
                                  '& .MuiOutlinedInput-root': {
                                    padding: 0,
                                    borderRadius: '12px',
                                    overflow: 'hidden',
                                    height: '100%',
                                    bgcolor:
                                      option.id === 'passport_image'
                                        ? preview
                                          ? 'white'
                                          : 'secondary.main'
                                        : inputValues[option.api_id]
                                        ? 'white'
                                        : 'secondary.main',
                                    // "& > fieldset": {
                                    //     border: '1.5px solid green',
                                    // },
                                    // bgcolor: 'secondary.main',
                                    // backgroundColor: '#ffffff',
                                    '&.Mui-focused > fieldset': {
                                      // border: '1px solid',
                                      borderColor: 'primary.main',
                                    },
                                    '&:hover:before fieldset': {
                                      borderColor: 'primary.main',
                                    },
                                    color: 'text.primary',
                                  },
                                  '.MuiOutlinedInput-root :focus': {
                                    backgroundColor: 'white',
                                  },
                                  '.MuiInputBase-input': {
                                    padding: '14px',
                                  },
                                  '.MuiOutlinedInput-input': {
                                    bgcolor: personalInfosInputValuesState[subField.id] ? 'white' : 'secondary.main',
                                  },
                                  zIndex: 1,
                                }}
                              />
                            ))
                          ) : (
                            <TextField
                              value={formik['values'][option.api_id]}
                              onChange={(event) => {
                                formik.handleChange(option.api_id)(event);
                                handleSetInputValues(event, option.api_id);
                              }}
                              error={formik['touched'][option.api_id] && Boolean(formik['errors'][option.api_id])}
                              helperText={
                                formik['touched'][option.api_id] && (formik['errors'][option.api_id] as ReactNode)
                              }
                              name={option.api_id}
                              fullWidth
                              id={option.api_id}
                              placeholder={option.placeholder}
                              onBlur={
                                option.id === 'email'
                                  ? () => setShowEmailTooltip(true)
                                  : option.id === 'land_line'
                                  ? () => setShowEmailTooltip(false)
                                  : undefined
                              }
                              type={'text'}
                              autoComplete="off"
                              sx={{
                                '& input::placeholder': {
                                  fontSize: '14px',
                                  color: 'text.secondary',
                                },
                                width: '100%',
                                // flexGrow: '1',
                                height: '100%',
                                // bgcolor: option.id === 'passport_image' ? preview ? 'white' : 'secondary.main' : inputValues[option.api_id] ? 'white' : 'secondary.main',
                                borderRadius: '12px',
                                '& .MuiInputAdornment-root': {
                                  ml: '8px',
                                  height: '40px',
                                  maxHeight: '40px',
                                  backgroundColor:
                                    option.id === 'passport_image'
                                      ? preview
                                        ? 'white'
                                        : 'secondary.main'
                                      : 'secondary.main',
                                  borderRadius: '12px',
                                  // border: option.id === 'passport_image' ? '1px dash' : 'none'
                                },
                                '& legend': { display: 'none' },
                                '& fieldset': {
                                  top: 0,
                                  borderRadius: '12px',
                                  border: option.id === 'passport_image' ? (preview ? '1px solid' : '1px dashed') : '',
                                  borderColor:
                                    activeAuthStep === 1
                                      ? personalInfosInputValuesState[option.api_id]
                                        ? 'primary.main'
                                        : 'secondary.main'
                                      : activeAuthStep === 3
                                      ? locationInfosInputValuesState[option.api_id]
                                        ? 'primary.main'
                                        : 'secondary.main'
                                      : 'secondary.main',
                                },
                                '& .MuiOutlinedInput-root:hover': {
                                  '& > fieldset': {
                                    border: option.id === 'passport_image' ? '1px dash' : '',
                                    borderColor: inputValues[option.api_id]
                                      ? 'primary.main'
                                      : option.id === 'passport_image'
                                      ? preview
                                        ? 'primary.main'
                                        : 'text.disabled'
                                      : 'secondary.main',
                                  },
                                },
                                // '& fieldset:hover': {top: 0, borderRadius: '12px', border: '1px solid red'},
                                '& .MuiOutlinedInput-root': {
                                  padding: 0,
                                  borderRadius: '12px',
                                  overflow: 'hidden',
                                  height: '100%',
                                  bgcolor:
                                    option.id === 'passport_image'
                                      ? preview
                                        ? 'white'
                                        : 'secondary.main'
                                      : inputValues[option.api_id]
                                      ? 'white'
                                      : 'secondary.main',
                                  // "& > fieldset": {
                                  //     border: '1.5px solid green',
                                  // },
                                  // bgcolor: 'secondary.main',
                                  // backgroundColor: '#ffffff',
                                  '&.Mui-focused > fieldset': {
                                    // border: '1px solid',
                                    borderColor: 'primary.main',
                                  },
                                  '&:hover:before fieldset': {
                                    borderColor: 'primary.main',
                                  },
                                  color: 'text.primary',
                                },
                                '.MuiOutlinedInput-root :focus': {
                                  backgroundColor: 'white',
                                },
                                '.MuiInputBase-input': {
                                  padding: '14px',
                                },
                                '.MuiOutlinedInput-input': {
                                  bgcolor:
                                    activeAuthStep === 1
                                      ? personalInfosInputValuesState[option.api_id]
                                        ? 'white'
                                        : 'secondary.main'
                                      : activeAuthStep === 3
                                      ? locationInfosInputValuesState[option.api_id]
                                        ? 'white'
                                        : 'secondary.main'
                                      : 'secondary.main',
                                },
                                zIndex: 1,
                              }}
                            />
                          )}
                        </Box>
                      ) : (
                        option.hasSelect.includes('simple') && (
                          <FormControl fullWidth>
                            <Select
                              // dir={'rtl'}
                              value={formik['values'][option.api_id]}
                              // value={option.id === 'passport_image' ? preview && imageFile && imageFile.name : formik["values"][option.api_id]}
                              onChange={(event) => {
                                formik.handleChange(option.api_id)(
                                  JSON.parse(event.target.value)[
                                    option.api_id === 'bank_id' ? 'id' : 'name'
                                  ].toString(),
                                );
                                handleChangeSelectValues(
                                  event,
                                  option.id === 'nationality' ? 'nationality' : 'citizenship',
                                );
                              }}
                              error={formik['touched'][option.api_id] && Boolean(formik['errors'][option.api_id])}
                              // helperText={(formik["touched"][option.api_id]) && (formik["errors"][option.api_id])}
                              name={option.api_id}
                              displayEmpty={true}
                              placeholder={option.placeholder}
                              IconComponent={selectNewIconSimple}
                              renderValue={(value: string) => {
                                return (
                                  <Typography
                                    variant="inherit"
                                    component="span"
                                    color={'text.secondary'}
                                    sx={{
                                      fontSize: '14px',
                                      fontWeight: '400',
                                      opacity: (option.id === 'nationality' ? !!nationalityState : !!countryState)
                                        ? 1
                                        : '0.4',
                                    }}
                                  >
                                    {option.id === 'nationality' && !!nationalityState
                                      ? JSON.parse(nationalityState).name
                                      : option.id === 'citizenship' && !!countryState
                                      ? JSON.parse(countryState).name
                                      : option.placeholder}
                                  </Typography>
                                );
                              }}
                              MenuProps={{
                                anchorOrigin: {
                                  vertical: 'bottom',
                                  horizontal: 'left',
                                },
                                transformOrigin: {
                                  vertical: 'top',
                                  horizontal: 'left',
                                },
                                PaperProps: {
                                  sx: {
                                    mt: '8px',
                                    boxShadow: '0px 4px 38px rgba(68, 68, 68, 0.1)',
                                    maxHeight: '200px',
                                    borderRadius: '10px',
                                    overflowY: 'auto',
                                  },
                                },
                              }}
                              sx={{
                                width: '100%',
                                mt: '8px',
                                bgcolor: (option.id === 'nationality' ? nationalityState : countryState)
                                  ? 'white'
                                  : 'secondary.main',
                                borderRadius: '12px',
                                '& .MuiSelect-icon': { top: '40%', mx: '5px' },
                                height: '100%',
                                '.MuiInputBase-input': {
                                  py: '14px',
                                },
                                '& fieldset': {
                                  border:
                                    (option.id === 'nationality' ? nationalityState : countryState) ||
                                    formik['touched'][option.api_id]
                                      ? '1px solid'
                                      : 'none',
                                  borderColor: 'primary.main',
                                },
                              }}
                            >
                              {(option.id === 'nationality' ? nationalityList : countryList)?.map(
                                (item: any, index: number) => {
                                  return (
                                    <MenuItem
                                      value={JSON.stringify(item)}
                                      key={item.id}
                                      sx={{
                                        width: '100%',
                                        px: '8px',
                                        my: '5px',
                                      }}
                                    >
                                      <Typography
                                        variant="inherit"
                                        component="span"
                                        color={'text.secondary'}
                                        sx={{
                                          fontSize: '14px',
                                          fontWeight: '400',
                                        }}
                                      >
                                        {option.id === 'card_type' ? item.name : item.name}
                                      </Typography>
                                    </MenuItem>
                                  );
                                },
                              )}
                            </Select>
                            {formik['touched'][option.api_id] && (
                              <FormHelperText error={true}>
                                {formik['errors'][option.api_id] as ReactNode}
                              </FormHelperText>
                            )}
                          </FormControl>
                        )
                      )}
                    </Box>
                  ) : option.id === 'profile_image' ? (
                    <Box
                      sx={{
                        width: '100%',
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        justifyContent: 'center',
                        mb: '32px',
                      }}
                    >
                      <>
                        <Badge
                          overlap="circular"
                          anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
                          badgeContent={
                            <IconButton
                              color="primary"
                              aria-label="upload picture"
                              component="label"
                              disableRipple={true}
                              sx={{
                                width: '24px',
                                height: '24px',
                                display: 'flex',
                                justifySelf: 'flex-end',
                                padding: '0',
                                backgroundColor: '#6FCF97',
                                mt: '-12px',
                                ml: '0px',
                                '&:hover': {
                                  bgcolor: 'success.dark',
                                },
                              }}
                            >
                              <input
                                hidden
                                accept="image/png, image/jpg, image/jpeg"
                                type="file"
                                onChange={(event) => handleGetImageFile(event, option.id)}
                              />
                              <Box
                                component={'img'}
                                src={editProfileIcon.src}
                                alt={'backward icon'}
                                sx={{
                                  width: '14px',
                                  height: '14px',
                                  transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                                }}
                              />
                            </IconButton>
                          }
                        >
                          <Avatar alt="Travis Howard" src={preview} sx={{ width: 96, height: 96 }} />
                        </Badge>

                        {profileImgIsValid && (
                          <Box
                            sx={{
                              display: 'flex',
                              alignItems: 'center',
                              mt: { xs: '8px', sm: '16px' },
                            }}
                          >
                            <Box
                              component={'img'}
                              src={dangerCircleIcon.src}
                              alt={'bullet-point'}
                              sx={{
                                width: '19px',
                                height: '19px',
                                mr: '8px',
                                transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                              }}
                            />
                            <FormHelperText error={true}>{profileImgIsValid}</FormHelperText>
                          </Box>
                        )}
                      </>
                    </Box>
                  ) : (
                    <>
                      {option.id === 'id_card_selfie' && (
                        <Box
                          sx={{
                            display: { xs: 'none', sm: 'block' },
                            width: '2px',
                            height: '300px',
                            // position: 'absolute',
                            // top: '205px',
                            backgroundImage: 'linear-gradient(to bottom, #C3C3C3 45%, rgba(255,255,255,0) 30%)',
                            backgroundPosition: 'top',
                            backgroundSize: '1px 20px',
                            backgroundRepeat: 'repeat-y',
                          }}
                        ></Box>
                      )}

                      <Box
                        sx={{
                          width: { xs: '100%', sm: '45%', lg: '49%' },

                          display: 'flex',
                          flexDirection: 'column',
                          // bgcolor: 'yellow',
                          px: { xs: 0, lg: '40px' },
                        }}
                      >
                        <Box
                          sx={{
                            width: 'auto',
                            display: 'flex',
                            alignItems: 'center',
                            mr: '30px',
                          }}
                        >
                          <Box
                            component={'img'}
                            src={bulletPoint.src}
                            alt={'bullet-point'}
                            sx={{
                              display: { xs: 'none', lg: 'block' },
                              width: '22px',
                              height: '18px',
                              mr: '9px',
                              transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                            }}
                          />
                          <Typography
                            variant="inherit"
                            component="span"
                            color={'text.primary'}
                            sx={{
                              // fontWeight: {xs: '600'},
                              fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                              fontSize: { xs: '16px', xl: '16px' },
                            }}
                          >
                            {option.title}
                          </Typography>
                        </Box>
                        <Dropzone
                          id={option.api_id}
                          handleSetDocsImgValidation={handleSetDocsImgValidation}
                          images={docsImgValidation}
                        />

                        <Box
                          sx={{
                            width: '100%',
                            display: { xs: 'flex', lg: 'none' },
                            alignItems: 'center',
                            mt: '8px',
                            // pb: '40px'
                          }}
                        >
                          <TextField
                            value={
                              option.id === 'id_card_image'
                                ? passPreview
                                  ? passImageFile?.name
                                  : ''
                                : selfiePreview
                                ? selfieImageFile?.name
                                : ''
                            }
                            onChange={(event) => {
                              formik.handleChange(option.api_id)(event);
                              handleSetInputValues(event, option.api_id);
                            }}
                            error={formik['touched'][option.api_id] && Boolean(formik['errors'][option.api_id])}
                            helperText={
                              formik['touched'][option.api_id] && (formik['errors'][option.api_id] as ReactNode)
                            }
                            name={option.api_id}
                            fullWidth
                            id={option.api_id}
                            placeholder={option.placeholder}
                            type={'text'}
                            autoComplete="off"
                            // onChange={(event) => handleSetInputValues(event, option.api_id)}
                            sx={{
                              '& input::placeholder': {
                                fontSize: '14px',
                                color: 'text.secondary',
                              },
                              width: '100%',
                              // flexGrow: '1',
                              height: '100%',
                              // bgcolor: option.id === 'passport_image' ? preview ? 'white' : 'secondary.main' : inputValues[option.api_id] ? 'white' : 'secondary.main',
                              borderRadius: '12px',
                              '& .MuiInputAdornment-root': {
                                ml: '8px',
                                height: '40px',
                                maxHeight: '40px',
                                backgroundColor: (option.id === 'id_card_image' ? passPreview : selfiePreview)
                                  ? 'white'
                                  : 'secondary.main',
                                borderRadius: '12px',
                                // border: option.id === 'passport_image' ? '1px dash' : 'none'
                              },
                              '& legend': { display: 'none' },
                              '& fieldset': {
                                top: 0,
                                borderRadius: '12px',
                                border: (option.id === 'id_card_image' ? passPreview : selfiePreview)
                                  ? '1px solid'
                                  : '1px dashed',
                                borderColor: (option.id === 'id_card_image' ? passPreview : selfiePreview)
                                  ? 'primary.main'
                                  : 'text.disabled',
                              },
                              '& .MuiOutlinedInput-root:hover': {
                                '& > fieldset': {
                                  border: option.id === 'passport_image' ? '1px dash' : '',
                                  borderColor: inputValues[option.api_id]
                                    ? 'primary.main'
                                    : option.id === 'passport_image'
                                    ? preview
                                      ? 'primary.main'
                                      : 'text.disabled'
                                    : 'secondary.main',
                                },
                              },
                              // '& fieldset:hover': {top: 0, borderRadius: '12px', border: '1px solid red'},
                              '& .MuiOutlinedInput-root': {
                                padding: 0,
                                borderRadius: '12px',
                                overflow: 'hidden',
                                height: '100%',
                                bgcolor: (option.id === 'id_card_image' ? passPreview : selfiePreview)
                                  ? 'white'
                                  : 'secondary.main',
                                // "& > fieldset": {
                                //     border: '1.5px solid green',
                                // },
                                // bgcolor: 'secondary.main',
                                // backgroundColor: '#ffffff',
                                '&.Mui-focused > fieldset': {
                                  // border: '1px solid',
                                  borderColor: 'primary.main',
                                },
                                '&:hover:before fieldset': {
                                  borderColor: 'primary.main',
                                },
                                color: 'text.primary',
                              },
                              '.MuiOutlinedInput-root :focus': {
                                backgroundColor:
                                  option.id === 'passport_image'
                                    ? preview
                                      ? 'white'
                                      : 'secondary.main'
                                    : 'secondary.main',
                              },
                              '.MuiInputBase-input': {
                                padding: '14px',
                              },
                              '.MuiOutlinedInput-input': {
                                bgcolor: (option.id === 'id_card_image' ? passPreview : selfiePreview)
                                  ? 'white'
                                  : 'secondary.main',
                              },
                              zIndex: 1,
                            }}
                            InputProps={{
                              readOnly: true,
                              autoComplete: 'off',
                              startAdornment: (option.id === 'id_card_image' ? passPreview : selfiePreview) && (
                                <InputAdornment
                                  position="end"
                                  sx={{
                                    minWidth: '36px',
                                    height: '40px',
                                    borderRadius: '11px',
                                  }}
                                >
                                  <Box
                                    component={'img'}
                                    src={option.id === 'id_card_image' ? passPreview : selfiePreview}
                                    sx={{
                                      width: '36px',
                                      height: '40px',
                                      borderRadius: '11px',
                                      objectFit: 'fill',
                                    }}
                                  />
                                </InputAdornment>
                              ),
                              endAdornment: (option.id === 'id_card_image' ? passPreview : selfiePreview) && (
                                <InputAdornment
                                  position="start"
                                  sx={{
                                    width: 'fit-content',
                                    height: '40px',
                                    backgroundColor: 'transparent',
                                    '&:hover': {
                                      cursor: 'pointer',
                                    },
                                  }}
                                  onClick={() => handleDeleteImage(option.id)}
                                >
                                  <Box
                                    component={'img'}
                                    src={garbageIcon.src}
                                    sx={{
                                      width: '24px',
                                      height: '24px',
                                      objectFit: 'fill',
                                    }}
                                  />
                                </InputAdornment>
                              ),
                            }}
                          />

                          {
                            <Button
                              variant="contained"
                              component="label"
                              disableRipple
                              sx={{
                                width: 'auto',
                                minWidth: '45px',
                                px: '10px',
                                height: '51px',
                                flexGrow: '1',
                                borderRadius: '12px',
                                ml: '8px',
                                boxShadow: 'none',
                                '&:hover': {
                                  boxShadow: 'none',
                                },
                              }}
                            >
                              <Box
                                component={'img'}
                                src={attachmentIcon.src}
                                sx={{
                                  width: '24px',
                                  height: '24px',
                                }}
                              />
                              <input
                                hidden
                                type="file"
                                accept="image/png, image/bmp, image/jpeg"
                                onChange={(event) => handleGetImageFile(event, option.id)}
                              />
                            </Button>
                          }
                        </Box>

                        {imagesInvalidFileType[option.id] && (
                          <Box
                            sx={{
                              display: 'flex',
                              alignItems: 'center',
                              mt: { xs: '8px', sm: '16px' },
                            }}
                          >
                            <Box
                              component={'img'}
                              src={dangerCircleIcon.src}
                              alt={'bullet-point'}
                              sx={{
                                width: '19px',
                                height: '19px',
                                mr: '8px',
                                transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                              }}
                            />
                            <FormHelperText error={true}>{imagesInvalidFileType[option.id]}</FormHelperText>
                          </Box>
                        )}

                        {option.id === 'id_card_selfie' && !!selfiImgIsValid && (
                          <Box
                            sx={{
                              display: 'flex',
                              alignItems: 'center',
                              mt: { xs: '8px', sm: '16px' },
                            }}
                          >
                            <Box
                              component={'img'}
                              src={dangerCircleIcon.src}
                              alt={'bullet-point'}
                              sx={{
                                width: '19px',
                                height: '19px',
                                mr: '8px',
                                transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                              }}
                            />
                            <FormHelperText error={true}>{selfiImgIsValid}</FormHelperText>
                          </Box>
                        )}

                        {option.id === 'id_card_image' && !!passImgIsValid && (
                          <Box
                            sx={{
                              display: 'flex',
                              alignItems: 'center',
                              mt: { xs: '8px', sm: '16px' },
                            }}
                          >
                            <Box
                              component={'img'}
                              src={dangerCircleIcon.src}
                              alt={'bullet-point'}
                              sx={{
                                width: '19px',
                                height: '19px',
                                mr: '8px',
                                transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                              }}
                            />
                            <FormHelperText error={true}>{passImgIsValid}</FormHelperText>
                          </Box>
                        )}

                        <Box
                          sx={{
                            width: '100%',
                            display: 'flex',
                            flexDirection: { xs: 'row-reverse', sm: 'column' },
                            alignItems: 'center',
                            // mb: '80px'
                            mt: { xs: '40px', sm: '20px' },
                          }}
                        >
                          <Box
                            sx={{
                              width: '100%',
                              display: 'flex',
                              flexDirection: 'column',
                              alignItems: 'flex-start',
                              my: '16px',
                            }}
                          >
                            {(option.api_id === 'id_card_image'
                              ? docsImgTips.passport.tips
                              : docsImgTips.selfi.tips
                            )?.map((tip) => (
                              <Box
                                key={tip}
                                sx={{
                                  display: 'flex',
                                  alignItems: 'center',
                                }}
                              >
                                <Chip
                                  label=""
                                  sx={{
                                    direction: 'rtl',
                                    width: '3px',
                                    height: '3px',
                                    minWidth: '2px',
                                    minHeight: '2px',
                                    borderRadius: '50%',
                                    bgcolor: 'text.primary',
                                    mr: '8px',
                                  }}
                                />
                                <Typography
                                  variant="inherit"
                                  component="span"
                                  color={'text.primary'}
                                  sx={{
                                    fontWeight: '400',
                                    fontSize: {
                                      xs: '10px',
                                      sm: '14px',
                                    },
                                  }}
                                >
                                  {tip}
                                </Typography>
                              </Box>
                            ))}
                          </Box>

                          <Box
                            component={'img'}
                            src={
                              option.api_id === 'id_card_image'
                                ? docsImgTips.passport.image.src
                                : docsImgTips.selfi.image.src
                            }
                            alt={'bullet-point'}
                            sx={{
                              width: { xs: '105px', sm: '190px' },
                              height: { xs: '69px', sm: '118px' },
                              mr: '8px',
                              transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                            }}
                          />
                        </Box>
                      </Box>
                    </>
                  );
                })}
              </Box>

              <Box
                sx={{
                  width: '100%',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  mb: '8px',
                  mt: '80px',
                }}
              >
                {activeAuthStep > 1 && (
                  <Button
                    variant="contained"
                    disableRipple={true}
                    sx={{
                      width: { xs: '47%', sm: '236px' },
                      height: { xs: '40px', sm: '40px' },
                      '&:hover': {
                        backgroundColor: 'white',
                        color: 'primary.main',
                        boxShadow: 'none',
                      },
                      bgcolor: 'white',
                      border: '1px solid',
                      borderColor: 'primary.main',
                      color: 'primary.main',
                      boxShadow: 'none',
                      // fontWeight: '600',
                      borderRadius: '12px',
                      fontSize: { xs: '12px', sm: '14px' },
                      mr: '16px',
                    }}
                    onClick={() => setActiveAuthStep((prevState) => prevState - 1)}
                  >
                    {t('profile:prev')}
                  </Button>
                )}

                <Button
                  variant="contained"
                  type={'submit'}
                  disableRipple={true}
                  sx={{
                    width: { xs: '47%', sm: '236px' },
                    height: { xs: '40px', sm: '40px' },
                    '&:hover': {
                      boxShadow: 'none',
                    },
                    boxShadow: 'none',
                    // fontWeight: '600',
                    borderRadius: '12px',
                    fontSize: { xs: '12px', sm: '14px' },
                  }}
                >
                  {activeAuthStep === 3 ? t('profile:authentication') : t('profile:next')}
                </Button>
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
      {openResultDialogBox && (
        <AlertDialogBox
          mode={'result'}
          data={operationResultState}
          open={openResultDialogBox}
          close={handleCloseResultDB}
        />
      )}
    </>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      hasLayout: true,
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default Profile;
