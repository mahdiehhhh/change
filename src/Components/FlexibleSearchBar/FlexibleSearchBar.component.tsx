import React from 'react';
import { styled, alpha } from '@mui/material/styles';
import Box from '@mui/material/Box';
import InputBase from '@mui/material/InputBase';

import searchIconActive from '/public/assets/images/Search-icon-active.svg';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import { useAppSelector } from '../../store/hooks';
import { toUpperCase } from 'uri-js/dist/esnext/util';
import { localesNS } from '../../utils/Data/locales_NameSpace.utils';

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: '11px',
  backgroundColor: 'white',
  '&:hover': {
    backgroundColor: 'white',
  },
  marginLeft: 0,
  width: '100%',
  height: '40px',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(1),
    width: 'auto',
  },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 1),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(0, 0, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(3)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    height: '40px',
    [theme.breakpoints.up('sm')]: {
      width: '0px',
      '&:focus': {
        width: '60px',
      },
    },
    [theme.breakpoints.up('md')]: {
      width: '0px',
      '&:focus': {
        width: '160px',
      },
    },
    [theme.breakpoints.up('lg')]: {
      width: '0px',
      '&:focus': {
        width: '242px',
      },
    },
  },
}));

interface Props {
  handleSearchForResult: Function;
}

const FlexibleSearchBar = ({ handleSearchForResult }: Props) => {
  const { t } = useTranslation();
  const exchangeCurrenciesInfoResult = useAppSelector((state) => state.ExchangeCurrenciesInfo);

  return (
    <Search>
      <SearchIconWrapper>
        <Box
          component={'img'}
          src={searchIconActive.src}
          alt={'search-icon'}
          sx={{
            width: '24px',
            height: '24px',
          }}
        />
      </SearchIconWrapper>
      <StyledInputBase
        placeholder={t('common:search')}
        inputProps={{ 'aria-label': 'search' }}
        onChange={(event) => handleSearchForResult(event)}
      />
    </Search>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default FlexibleSearchBar;
