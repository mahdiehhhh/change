import React, { useEffect } from 'react';
import { Box, Link, Skeleton } from '@mui/material';
import bulletPoint from '/public/assets/images/BulletPoint.svg';
import Typography from '@mui/material/Typography';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';

import creditCard from '/public/assets/images/CreditCard-icon.svg';
import transferIcon from '/public/assets/images/Transfer-icon.svg';
import paypalIcon from '/public/assets/images/paypal-icon.svg';
import exchangePaymentIcon from '/public/assets/images/exchange-payment-icon.svg';
import createAccountIcon from '/public/assets/images/create-account-icon.svg';
import cashIncomeIcon from '/public/assets/images/cash-income-icon.svg';
import usefulCardsCurve from '/public/assets/images/useful-cards-curve.svg';
import usefulCardsCurveTablet from '/public/assets/images/useful-cards-curve-tablet.svg';
import depositUseful from '/public/assets/images/deposit-in-useful-icon.svg';
import paypalDouble from '/public/assets/images/paypal-double-p-icon.svg';
import holeCash from '/public/assets/images/Cash-has-hole-icon.svg';
import { useI18nTrDir } from '../../utils/CustomHooks/i18nTrDir';
import NextLink from 'next/link';
import { localesNS } from '../../utils/Data/locales_NameSpace.utils';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import { PATHS } from '../../configs/routes.config';

interface Props {
  mode: string;
  loading: boolean;
}

interface DashboardServicesCardTemplate {
  id: number;
  icon: any;
  title: string;
  color: string;
  link: string;
}

interface WalletServicesCardTemplate {
  id: number;
  icon: any;
  title: string;
  color: string;
  link: string;
  curveColor: string;
}

const UsefulServices = ({ mode, loading }: Props) => {
  const { t, pageDirection } = useI18nTrDir();

  const theme = useTheme();
  const lg = useMediaQuery(theme.breakpoints.up('lg'));
  const md = useMediaQuery(theme.breakpoints.up('md'));

  const dashboardServicesCard: DashboardServicesCardTemplate[] = [
    {
      id: 1,
      icon: creditCard,
      title: t('useful_services:credit_card'),
      color: '#FFA36E',
      link: `https://my.zimapay.com/orders/category/credit-card-services`,
    },
    {
      id: 2,
      icon: transferIcon,
      title: t('useful_services:exchange_transfer'),
      color: '#A582EF',
      link: `https://my.zimapay.com/orders/category/money-transfer`,
    },
    {
      id: 3,
      icon: paypalIcon,
      title: t('useful_services:paypal'),
      color: '#88A7F6',
      link: `https://my.zimapay.com/orders/category/paypal`,
    },
    {
      id: 4,
      icon: exchangePaymentIcon,
      title: t('useful_services:exchange_payment'),
      color: '#68C2D7',
      link: `https://my.zimapay.com/orders/category/website-payment`,
    },
    {
      id: 5,
      icon: createAccountIcon,
      title: t('useful_services:create_account'),
      color: '#FFC452',
      link: '/',
    },
    {
      id: 6,
      icon: cashIncomeIcon,
      title: t('useful_services:cash_income'),
      color: '#6FCF97',
      link: `https://my.zimapay.com/orders/category/cash-revenue`,
    },
  ];

  const walletServicesCard: WalletServicesCardTemplate[] = [
    {
      id: 1,
      icon: paypalDouble,
      title: t('useful_services:paypal_charge'),
      color: '#298FFE',
      curveColor: '#D1E7FF',
      link: '/',
    },
    {
      id: 2,
      icon: holeCash,
      title: t('useful_services:cash'),
      color: '#FF55BB',
      curveColor: '#F2DCF6',
      link: '/',
    },
    {
      id: 3,
      icon: depositUseful,
      title: t('useful_services:deposit_to_wallet'),
      color: '#3AE5B2',
      curveColor: '#E1FFEE',
      link: '/',
    },
    {
      id: 4,
      icon: depositUseful,
      title: t('useful_services:deposit_to_wallet'),
      color: '#3AE5B2',
      curveColor: '#E1FFEE',
      link: '/',
    },
  ];

  return (
    <>
      {!loading ? (
        <Box
          sx={{
            width: '100%',
            // height: mode === 'wallet' ? {xs: 'auto', sm: '435px', md: '438px', lg: '352px', xl: '270px'} : {
            //     xs: 'auto',
            //     xl: '245px'
            // },
            backgroundColor: 'white',
            borderRadius: '22px',
            padding: { xs: '16px 16px 24px 16px', sm: '18px 25px 24px 25px' },
            display: 'flex',
            flexDirection: 'column',
            flex: '1 1 auto',
          }}
        >
          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'space-between',
              mb: '25px',
            }}
          >
            <Skeleton
              animation="wave"
              sx={{
                width: '156px',
                height: '30px',
                borderRadius: '12px',
              }}
            />
          </Box>

          <Box
            sx={{
              width: '100%',
              height: '100%',
              px: { xs: '0', lg: mode === 'dashboard' ? { xs: '70px', xl: '0' } : '0' },
              display: 'flex',
              flexWrap: 'wrap',
              // display: 'grid',
              // gridTemplateColumns: 'auto auto auto',
              gap: { xs: '8px', sm: '16px', xl: '10px' },
              justifyContent: 'space-around',
              mt: mode === 'wallet' ? { xl: '25px' } : '0',
            }}
          >
            {mode === 'dashboard'
              ? dashboardServicesCard.map((card, index) => (
                  <Skeleton
                    key={index}
                    animation="wave"
                    variant="rectangular"
                    sx={{
                      minWidth: { xs: '140px', sm: '130px', lg: '168px', xl: '80px' },
                      maxWidth: { xs: '25%', xl: '15%' },
                      height: '115px',
                      flex: { xs: '0 0 25%', xl: '1 1 15%' },
                      display: 'flex',
                      flexDirection: 'column',
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderRadius: '15px',
                      boxShadow: '0px 4px 38px rgba(68, 68, 68, 0.1)',
                      cursor: 'pointer',
                    }}
                  />
                ))
              : walletServicesCard.map((card, index) => (
                  <Skeleton
                    key={index}
                    animation="wave"
                    variant="rectangular"
                    sx={{
                      minWidth: { xs: '100px', sm: '120px', md: '50px', lg: '150px', xl: '155px' },
                      maxWidth: { xs: '45%', sm: '45%', md: '22%', lg: '45%', xl: '23%' },
                      height: md && !lg ? '80px' : '115px',
                      flex: { xs: '0 0 45%', sm: '0 0 45%', md: '0 0 22%', lg: '1 1 45%', xl: '0 0 23%' },
                      position: 'relative',
                      display: 'flex',
                      flexDirection: 'column',
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderRadius: md && !lg ? '14px' : '22px',
                      cursor: 'pointer',
                    }}
                  />
                ))}
          </Box>
        </Box>
      ) : (
        <Box
          sx={{
            width: '100%',
            // height: mode === 'wallet' ? {xs: 'auto', sm: '435px', md: '438px', lg: '352px', xl: '270px'} : {
            //     xs: 'auto',
            //     xl: '245px'
            // },
            backgroundColor: 'white',
            borderRadius: '22px',
            padding: { xs: '16px 16px 24px 16px', sm: '18px 25px 24px 25px' },
            display: 'flex',
            flexDirection: 'column',
            flex: '1 1 auto',
          }}
        >
          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'space-between',
              mb: '25px',
            }}
          >
            <Box
              sx={{
                display: 'flex',
                alignItems: 'center',
              }}
            >
              <Box
                component={'img'}
                src={bulletPoint.src}
                alt={'bullet-point'}
                sx={{
                  width: '22px',
                  height: '18px',
                  mr: '9px',
                  transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                }}
              />
              <Typography
                variant="inherit"
                component="span"
                color={'text.primary'}
                sx={{
                  // fontWeight: {xs: '600'},
                  fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                  fontSize: { xs: '16px', xl: '16px' },
                }}
              >
                {mode === 'dashboard' ? t('useful_services:services') : t('useful_services:useful')}
              </Typography>
            </Box>

            {mode === 'dashboard' && (
              <NextLink href={`https://zimapay.com/services/`} passHref>
                <Link
                  href="#"
                  underline="none"
                  color={'text.secondary'}
                  sx={{ fontSize: { xs: '12px', sm: '14px' }, fontWeight: '400' }}
                >
                  {t('useful_services:see_all')}
                </Link>
              </NextLink>
            )}
          </Box>

          <Box
            sx={{
              width: '100%',
              height: '100%',
              px: { xs: '0', lg: mode === 'dashboard' ? { xs: '70px', xl: '0' } : '0' },
              display: 'flex',
              flexWrap: 'wrap',
              // display: 'grid',
              // gridTemplateColumns: 'auto auto auto',
              gap: { xs: '8px', sm: '16px', xl: '10px' },
              justifyContent: 'space-around',
              mt: mode === 'wallet' ? { xl: '25px' } : '0',
            }}
          >
            {mode === 'dashboard'
              ? dashboardServicesCard.map((card, index) => (
                  <NextLink key={card.id} href={card.link} passHref>
                    <Link
                      href="#"
                      underline="none"
                      sx={{
                        minWidth: { xs: '140px', sm: '130px', lg: '168px', xl: '80px' },
                        maxWidth: { xs: '25%', xl: '15%' },
                        height: '115px',
                        flex: { xs: '0 0 25%', xl: '1 1 15%' },
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: 'white',
                        borderRadius: '15px',
                        boxShadow: '0px 4px 38px rgba(68, 68, 68, 0.1)',
                        cursor: 'pointer',
                      }}
                    >
                      {/*<Box*/}
                      {/*    // key={index}*/}
                      {/*    sx={{*/}
                      {/*        minWidth: {xs: '140px', sm: '130px', lg: '168px', xl: '80px'},*/}
                      {/*        maxWidth: {xs: '25%', xl: '15%'},*/}
                      {/*        height: '115px',*/}
                      {/*        flex: {xs: '0 0 25%', xl: '1 1 15%'},*/}
                      {/*        display: 'flex',*/}
                      {/*        flexDirection: 'column',*/}
                      {/*        alignItems: 'center',*/}
                      {/*        justifyContent: 'center',*/}
                      {/*        backgroundColor: 'white',*/}
                      {/*        borderRadius: '15px',*/}
                      {/*        boxShadow: '0px 4px 38px rgba(68, 68, 68, 0.1)',*/}
                      {/*        cursor: 'pointer'*/}
                      {/*    }}*/}
                      {/*>*/}
                      <Box
                        component={'img'}
                        src={card.icon.src}
                        alt={'credit card'}
                        sx={{
                          width: 'fit-content',
                          height: 'fit-content',
                          mb: '10px',
                        }}
                      />
                      <Typography
                        variant="inherit"
                        component="span"
                        color={card.color}
                        sx={{
                          // fontWeight: '400',
                          fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                          fontSize: { xs: '14px', sm: '16px' },
                          textAlign: 'center',
                        }}
                      >
                        {card.title}
                      </Typography>
                      {/*</Box>*/}
                    </Link>
                  </NextLink>
                ))
              : walletServicesCard.map((card, index) => (
                  <Box
                    key={index}
                    sx={{
                      minWidth: { xs: '100px', sm: '120px', md: '50px', lg: '150px', xl: '155px' },
                      maxWidth: { xs: '45%', sm: '45%', md: '22%', lg: '45%', xl: '23%' },
                      height: md && !lg ? '80px' : '115px',
                      flex: { xs: '0 0 45%', sm: '0 0 45%', md: '0 0 22%', lg: '1 1 45%', xl: '0 0 23%' },
                      position: 'relative',
                      display: 'flex',
                      flexDirection: 'column',
                      alignItems: 'center',
                      justifyContent: 'center',
                      backgroundColor: card.color,
                      borderRadius: md && !lg ? '14px' : '22px',
                      cursor: 'pointer',
                    }}
                  >
                    <Box
                      sx={{
                        width: 'auto',
                        height: 'auto',
                        position: 'absolute',
                        top: '0',
                        left: pageDirection === 'rtl' ? '0' : 'auto',
                        right: pageDirection === 'rtl' ? 'auto' : '0',
                      }}
                    >
                      {md && !lg ? (
                        <svg width="57" height="41" viewBox="0 0 57 41" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path
                            d="M56.9962 40.25C45.4177 13.125 35.9444 14.875 0.15625 0H45.2962C51.758 0 56.9962 5.23827 56.9962 11.7V40.25Z"
                            fill={card.curveColor}
                          />
                        </svg>
                      ) : (
                        <svg
                          width={'120'}
                          height={'83'}
                          viewBox="0 0 120 83"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M120.001 82.6562C95.6854 26.9531 75.7907 30.5469 0.632812 0H99.0012C110.599 0 120.001 9.40202 120.001 21V82.6562Z"
                            fill={card.curveColor}
                          />
                        </svg>
                      )}
                    </Box>

                    <Box
                      component={'img'}
                      src={card.icon.src}
                      alt={'credit card'}
                      sx={{
                        width: 'fit-content',
                        height: 'fit-content',
                        mb: md && !lg ? '8px' : '18px',
                      }}
                    />
                    <Typography
                      variant="inherit"
                      component="span"
                      color={'white'}
                      sx={{
                        // fontWeight: '400',
                        fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                        fontSize: md && !lg ? '12px' : '16px',
                      }}
                    >
                      {card.title}
                    </Typography>
                  </Box>
                ))}
          </Box>
        </Box>
      )}
    </>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export { UsefulServices };
