import React, { useEffect, useState, ChangeEvent, ReactNode } from 'react';
import {
  Box,
  ButtonGroup,
  Button,
  InputAdornment,
  IconButton,
  Select,
  MenuItem,
  TextField,
  InputLabel,
  Tabs,
  Tab,
  Chip,
  FormHelperText,
  FormControl,
  Skeleton,
} from '@mui/material';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useI18nTrDir } from '../../utils/CustomHooks/i18nTrDir';
import bulletPoint from '/public/assets/images/BulletPoint.svg';
import backwardArrowIcon from '/public/assets/images/backwardArrowDeposit.svg';
import coloredPlusIcon from '/public/assets/images/colored_plus_icon.svg';
import IRRAccountIcon from '/public/assets/images/IRR-account-circled-icon.svg';
import IntAccountIcon from '/public/assets/images/Int-account-circled-icon.svg';
import editIcon from '/public/assets/images/Edit-icon.svg';
import noTransactionIcon from '/public/assets/images/no-transaction.svg';

import Typography from '@mui/material/Typography';
import bell from '*.svg';
import Divider from '@mui/material/Divider';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { fetchUserBankAccounts } from '../../store/slices/user_bank_accounts.slice';
import { router } from 'next/client';
import { PATHS } from '../../configs/routes.config';
import { useRouter } from 'next/router';
import { localesNS } from '../../utils/Data/locales_NameSpace.utils';

const AccountsInfoCards = () => {
  const { t, pageDirection } = useI18nTrDir();
  const router = useRouter();
  const [pageReady, setPageReady] = useState(false);

  const dispatch = useAppDispatch();
  const userBankAccountsResult = useAppSelector((state) => state.UserBankAccounts);

  useEffect(() => {
    dispatch(fetchUserBankAccounts('all'));
  }, []);

  useEffect(() => {
    if (!userBankAccountsResult.loading && !!userBankAccountsResult.userBankAccountsResp) {
      setPageReady(true);
    } else {
      setPageReady(false);
    }
  }, [userBankAccountsResult]);

  const addSpaceForCardNumber = (number: string) => {
    return number.replace(/.{4}/g, '$& ');
  };

  return (
    <>
      {!pageReady ? (
        <Box
          sx={{
            width: '100%',
            height: 'auto',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'flex-start',
          }}
        >
          <Box
            sx={{
              width: '100%',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'space-between',
              mb: '40px',
            }}
          >
            <Skeleton
              animation="wave"
              sx={{
                width: '20%',
                height: '30px',
                borderRadius: '12px',
              }}
            />
          </Box>

          {Array.from(Array(4).keys())?.map((box, boxIndex) => (
            <Box
              key={boxIndex}
              sx={{
                width: '100%',
                height: 'auto',
                bgcolor: 'white',
                borderRadius: { xs: '16px', sm: '24px' },
                padding: '14px 16px',
                mb: '24px',
              }}
            >
              <Box
                sx={{
                  width: '100%',
                  height: 'auto',
                  bgcolor: 'transparent',
                  display: { xs: 'none', sm: 'block' },
                }}
              >
                <Box
                  sx={{
                    width: '100%',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}
                >
                  <Box
                    sx={{
                      width: '100%',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                  >
                    <Skeleton
                      animation="wave"
                      sx={{
                        width: '80%',
                        height: '30px',
                        borderRadius: '12px',
                      }}
                    />
                  </Box>

                  <Box
                    sx={{
                      width: '100%',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                  >
                    <Skeleton
                      animation="wave"
                      sx={{
                        width: '80%',
                        height: '30px',
                        borderRadius: '12px',
                      }}
                    />
                  </Box>

                  <Box
                    sx={{
                      width: '100%',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                  >
                    <Skeleton
                      animation="wave"
                      sx={{
                        width: '80%',
                        height: '30px',
                        borderRadius: '12px',
                      }}
                    />
                  </Box>
                </Box>
                <Box
                  sx={{
                    width: '100%',
                    height: '2px',
                    backgroundImage: 'linear-gradient(to right, #C3C3C3 50%, rgba(255,255,255,0) 0%)',
                    backgroundPosition: 'bottom',
                    backgroundSize: '16px 1px',
                    backgroundRepeat: 'repeat-x',
                    margin: '24px 0',
                  }}
                ></Box>

                <Box
                  sx={{
                    width: '100%',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}
                >
                  <Box
                    sx={{
                      width: '100%',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                  >
                    <Skeleton
                      animation="wave"
                      sx={{
                        width: '80%',
                        height: '30px',
                        borderRadius: '12px',
                      }}
                    />
                  </Box>

                  <Box
                    sx={{
                      width: '100%',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                  >
                    <Skeleton
                      animation="wave"
                      sx={{
                        width: '80%',
                        height: '30px',
                        borderRadius: '12px',
                      }}
                    />
                  </Box>

                  <Box
                    sx={{
                      width: '100%',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                  >
                    <Skeleton
                      animation="wave"
                      sx={{
                        width: '80%',
                        height: '30px',
                        borderRadius: '12px',
                      }}
                    />
                  </Box>
                </Box>

                {/*{*/}
                {/*    !account.is_active &&*/}
                {/*    <Box*/}
                {/*        sx={{*/}
                {/*            width: '100%',*/}
                {/*            display: 'flex',*/}
                {/*            alignItems: 'center',*/}
                {/*            justifyContent: 'flex-end',*/}
                {/*            mt: '5px'*/}
                {/*        }}*/}
                {/*    >*/}
                {/*        <Button*/}
                {/*            variant="contained"*/}
                {/*            // type={'submit'}*/}
                {/*            disableRipple*/}
                {/*            sx={{*/}
                {/*                '&:hover': {*/}
                {/*                    bgcolor: 'transparent',*/}
                {/*                    boxShadow: 'none',*/}
                {/*                },*/}
                {/*                '&:active': {*/}
                {/*                    transition: 'none',*/}
                {/*                    color: 'primary.main',*/}
                {/*                    '& .MuiButton-endIcon': {*/}
                {/*                        filter: 'invert(42%) sepia(49%) saturate(2461%) hue-rotate(195deg) brightness(103%) contrast(99%)'*/}
                {/*                    }*/}
                {/*                },*/}
                {/*                padding: '0',*/}
                {/*                bgcolor: 'transparent',*/}
                {/*                color: 'text.secondary',*/}
                {/*                boxShadow: 'none',*/}
                {/*                // fontWeight: '600',*/}
                {/*                borderRadius: '12px',*/}
                {/*                height: '40px',*/}
                {/*                fontSize: {xs: '10px', sm: '14px'},*/}
                {/*                minWidth: '45px',*/}
                {/*            }}*/}
                {/*            endIcon={*/}
                {/*                <Box*/}
                {/*                    component={'img'}*/}
                {/*                    src={editIcon.src}*/}
                {/*                    alt={'bullet-point'}*/}
                {/*                    sx={{*/}
                {/*                        width: {xs: '16px', sm: '24px'},*/}
                {/*                        height: {xs: '16px', sm: '24px'},*/}
                {/*                        mx: 0*/}
                {/*                    }}*/}
                {/*                />*/}
                {/*            }*/}
                {/*        >*/}
                {/*            {t('bank_accounts:edit')}*/}
                {/*        </Button>*/}
                {/*    </Box>*/}

                {/*}*/}
              </Box>

              <Box
                sx={{
                  width: '100%',
                  height: 'auto',
                  bgcolor: 'transparent',
                  display: { xs: 'block', sm: 'none' },
                }}
              >
                <Box
                  sx={{
                    width: '100%',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}
                >
                  <Box
                    sx={{
                      width: '100%',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                  >
                    <Skeleton
                      animation="wave"
                      sx={{
                        width: '80%',
                        height: '30px',
                        borderRadius: '12px',
                      }}
                    />
                  </Box>

                  <Box
                    sx={{
                      width: '100%',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                  >
                    <Skeleton
                      animation="wave"
                      sx={{
                        width: '80%',
                        height: '30px',
                        borderRadius: '12px',
                      }}
                    />
                  </Box>
                </Box>

                <Box
                  sx={{
                    width: '100%',
                    height: '2px',
                    backgroundImage: 'linear-gradient(to right, #C3C3C3 50%, rgba(255,255,255,0) 0%)',
                    backgroundPosition: 'bottom',
                    backgroundSize: '16px 1px',
                    backgroundRepeat: 'repeat-x',
                    margin: '14px 0',
                  }}
                ></Box>

                <Box
                  sx={{
                    width: '100%',
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'flex-start',
                  }}
                >
                  <Box
                    sx={{
                      width: '100%',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                      mb: '12px',
                    }}
                  >
                    <Skeleton
                      animation="wave"
                      sx={{
                        width: '80%',
                        height: '30px',
                        borderRadius: '12px',
                      }}
                    />
                  </Box>

                  <Box
                    sx={{
                      width: '100%',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                  >
                    <Skeleton
                      animation="wave"
                      sx={{
                        width: '80%',
                        height: '30px',
                        borderRadius: '12px',
                      }}
                    />
                  </Box>
                </Box>

                <Box
                  sx={{
                    width: '100%',
                    height: '2px',
                    backgroundImage: 'linear-gradient(to right, #C3C3C3 50%, rgba(255,255,255,0) 0%)',
                    backgroundPosition: 'bottom',
                    backgroundSize: '16px 1px',
                    backgroundRepeat: 'repeat-x',
                    margin: '14px 0',
                  }}
                ></Box>

                <Box
                  sx={{
                    width: '100%',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}
                >
                  <Box
                    sx={{
                      width: '100%',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                  >
                    <Skeleton
                      animation="wave"
                      sx={{
                        width: '80%',
                        height: '30px',
                        borderRadius: '12px',
                      }}
                    />
                  </Box>

                  <Box
                    sx={{
                      width: '100%',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                  >
                    <Skeleton
                      animation="wave"
                      sx={{
                        width: '80%',
                        height: '30px',
                        borderRadius: '12px',
                      }}
                    />
                  </Box>
                </Box>

                {/*{*/}
                {/*    !account.is_active &&*/}
                {/*    <Box*/}
                {/*        sx={{*/}
                {/*            width: '100%',*/}
                {/*            display: 'flex',*/}
                {/*            alignItems: 'center',*/}
                {/*            justifyContent: 'flex-end',*/}
                {/*            mt: {sm: '5px'}*/}
                {/*        }}*/}
                {/*    >*/}
                {/*        <Button*/}
                {/*            variant="contained"*/}
                {/*            // type={'submit'}*/}
                {/*            disableRipple*/}
                {/*            sx={{*/}
                {/*                '&:hover': {*/}
                {/*                    bgcolor: 'transparent',*/}
                {/*                    boxShadow: 'none',*/}
                {/*                },*/}
                {/*                '&:active': {*/}
                {/*                    transition: 'none',*/}
                {/*                    color: 'primary.main',*/}
                {/*                    '& .MuiButton-endIcon': {*/}
                {/*                        filter: 'invert(42%) sepia(49%) saturate(2461%) hue-rotate(195deg) brightness(103%) contrast(99%)'*/}
                {/*                    }*/}
                {/*                },*/}
                {/*                padding: '0',*/}
                {/*                bgcolor: 'transparent',*/}
                {/*                color: 'text.secondary',*/}
                {/*                boxShadow: 'none',*/}
                {/*                // fontWeight: '600',*/}
                {/*                borderRadius: '12px',*/}
                {/*                height: '40px',*/}
                {/*                fontSize: {xs: '10px', sm: '14px'},*/}
                {/*                minWidth: '45px',*/}
                {/*            }}*/}
                {/*            endIcon={*/}
                {/*                <Box*/}
                {/*                    component={'img'}*/}
                {/*                    src={editIcon.src}*/}
                {/*                    alt={'bullet-point'}*/}
                {/*                    sx={{*/}
                {/*                        width: {xs: '16px', sm: '24px'},*/}
                {/*                        height: {xs: '16px', sm: '24px'},*/}
                {/*                        mx: 0*/}
                {/*                    }}*/}
                {/*                />*/}
                {/*            }*/}
                {/*        >*/}
                {/*            {t('bank_accounts:edit')}*/}
                {/*        </Button>*/}
                {/*    </Box>*/}

                {/*}*/}
              </Box>
            </Box>
          ))}
        </Box>
      ) : (
        <Box
          sx={{
            width: '100%',
            height: 'auto',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Box
            sx={{
              width: '100%',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'space-between',
              mb: '40px',
            }}
          >
            <Box
              sx={{
                width: 'auto',
                display: 'flex',
                alignItems: 'center',
              }}
            >
              <Box
                component={'img'}
                src={bulletPoint.src}
                alt={'bullet-point'}
                sx={{
                  width: '22px',
                  height: '18px',
                  mr: '9px',
                  transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                }}
              />
              <Typography
                variant="inherit"
                component="span"
                color={'text.primary'}
                sx={{
                  // fontWeight: {xs: '600'},
                  fontFamily: 'IRANSansXFaNum-DemiBold',
                  fontSize: { xs: '16px', xl: '16px' },
                }}
              >
                {t('bank_accounts:accounts')}
              </Typography>
            </Box>

            <Box
              sx={{
                display: 'flex',
                alignItems: 'center',
              }}
            >
              {!!userBankAccountsResult.userBankAccountsResp?.bank_accounts_list.length && (
                <Button
                  variant="contained"
                  // type={'submit'}
                  fullWidth
                  disableRipple
                  sx={{
                    '&:hover': {
                      boxShadow: 'none',
                    },
                    bgcolor: '#D1E7FF',
                    color: 'text.primary',
                    boxShadow: 'none',
                    // fontWeight: '600',
                    borderRadius: '12px',
                    height: '40px',
                    fontSize: '14px',
                    minWidth: '45px',
                    padding: { xs: '10px 12px' },
                    '& .MuiButton-startIcon': {
                      mx: '0',
                      mr: { xs: '0', sm: '9px' },
                    },
                  }}
                  startIcon={
                    <Box
                      component={'img'}
                      src={coloredPlusIcon.src}
                      alt={'bullet-point'}
                      sx={{
                        width: '20px',
                        height: '20px',
                        mx: 0,
                      }}
                    />
                  }
                  onClick={() => router.push(`/${PATHS.USER_BANK_ACCOUNT}/${PATHS.IRANIAN}`)}
                >
                  <Typography
                    variant="inherit"
                    component="span"
                    color={'text.primary'}
                    sx={{
                      fontFamily: 'IRANSansXFaNum-DemiBold',
                      display: { xs: 'none', sm: 'block' },
                      // fontWeight: '600',
                      fontSize: '14px',
                    }}
                  >
                    {t('bank_accounts:add_new_account')}
                  </Typography>
                </Button>
              )}

              <IconButton
                color="inherit"
                aria-label="prev button"
                edge="start"
                // onClick={() => router.push(`/${PATHS.USER_DASHBOARD}`)}
                sx={{
                  minWidth: '24px',
                  minHeight: '24px',
                  display: 'flex',
                  padding: '0',
                  ml: '40px',
                  backgroundColor: '#D9D9D9',
                }}
              >
                <Box
                  component={'img'}
                  src={backwardArrowIcon.src}
                  alt={'backward icon'}
                  sx={{
                    width: '6px',
                    height: '12px',
                    transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                  }}
                />
              </IconButton>
            </Box>
          </Box>

          {!!userBankAccountsResult.userBankAccountsResp?.bank_accounts_list.length
            ? userBankAccountsResult.userBankAccountsResp?.bank_accounts_list.map((account: any) => {
                return (
                  <Box
                    key={account.id}
                    sx={{
                      width: '100%',
                      height: 'auto',
                      bgcolor: 'white',
                      borderRadius: { xs: '16px', sm: '24px' },
                      padding: !account.is_active ? '14px 16px 5px 16px' : '14px 16px',
                      mb: '24px',
                    }}
                  >
                    <Box
                      sx={{
                        width: '100%',
                        height: 'auto',
                        bgcolor: 'transparent',
                        display: { xs: 'none', sm: 'block' },
                      }}
                    >
                      <Box
                        sx={{
                          width: '100%',
                          display: 'flex',
                          alignItems: 'center',
                          justifyContent: 'space-between',
                        }}
                      >
                        <Box
                          sx={{
                            display: 'flex',
                            alignItems: 'center',
                          }}
                        >
                          <Typography
                            variant="inherit"
                            component="span"
                            color={'text.secondary'}
                            sx={{
                              fontWeight: '400',
                              fontSize: { xs: '10px', sm: '14px' },
                              mr: '8px',
                            }}
                          >
                            {`${t('bank_accounts:acc_title')}:`}
                          </Typography>
                          <Typography
                            variant="inherit"
                            component="span"
                            color={'text.primary'}
                            sx={{
                              // fontWeight: '600',
                              fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                              fontSize: { xs: '12px', sm: '16px', lg: '17px' },
                            }}
                          >
                            {account.bank_name}
                          </Typography>
                        </Box>

                        <Box
                          sx={{
                            display: 'flex',
                            alignItems: 'center',
                          }}
                        >
                          <Typography
                            variant="inherit"
                            component="span"
                            color={'text.secondary'}
                            sx={{
                              fontWeight: '400',
                              fontSize: { xs: '10px', sm: '14px' },
                            }}
                          >
                            {`${t('bank_accounts:choose_account_type')}:`}
                          </Typography>

                          <Box
                            sx={{
                              display: 'flex',
                              alignItems: 'center',
                            }}
                          >
                            <Box
                              component={'img'}
                              src={account.accountable_type === 'iran' ? IRRAccountIcon.src : IntAccountIcon.src}
                              alt={'backward icon'}
                              sx={{
                                width: '24px',
                                height: '24px',
                                mx: '8px',
                              }}
                            />
                            <Typography
                              variant="inherit"
                              component="span"
                              color={'text.primary'}
                              sx={{
                                // fontWeight: '600',
                                fontFamily: 'IRANSansXFaNum-DemiBold',
                                fontSize: { xs: '12px', sm: '16px', lg: '17px' },
                              }}
                            >
                              {account.accountable_type === 'iran'
                                ? t('bank_accounts:ir_acc')
                                : account.accountable_type === 'international'
                                ? t('bank_accounts:international_account')
                                : account.accountable_type === 'credits'
                                ? t('bank_accounts:ex_card')
                                : ''}
                            </Typography>
                          </Box>
                        </Box>

                        <Box
                          sx={{
                            display: 'flex',
                            alignItems: 'center',
                          }}
                        >
                          <Typography
                            variant="inherit"
                            component="span"
                            color={'text.secondary'}
                            sx={{
                              fontWeight: '400',
                              fontSize: { xs: '10px', sm: '14px' },
                              mr: '8px',
                            }}
                          >
                            {`${t('bank_accounts:situation')}:`}
                          </Typography>
                          <Chip
                            label={account.status}
                            sx={{
                              minWidth: { xs: '56px', sm: '112px' },
                              height: '24px',
                              borderRadius: { xs: '30px', sm: '11px' },
                              bgcolor: 'success.light',
                              '& .MuiChip-label': {
                                padding: '0',
                                margin: '0',
                                display: 'flex',
                                alignItems: 'center',
                                justifyContent: 'center',
                                color: 'success.dark',
                                fontSize: { xs: '10px', sm: '12px' },
                                // fontWeight: '600'
                                fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                              },
                            }}
                          />
                        </Box>
                      </Box>
                      <Box
                        sx={{
                          width: '100%',
                          height: '2px',
                          backgroundImage: 'linear-gradient(to right, #C3C3C3 50%, rgba(255,255,255,0) 0%)',
                          backgroundPosition: 'bottom',
                          backgroundSize: '16px 1px',
                          backgroundRepeat: 'repeat-x',
                          margin: '24px 0',
                        }}
                      ></Box>

                      <Box
                        sx={{
                          width: '100%',
                          display: 'flex',
                          alignItems: 'center',
                          justifyContent: 'space-between',
                        }}
                      >
                        <Box
                          sx={{
                            display: 'flex',
                            alignItems: 'center',
                          }}
                        >
                          <Typography
                            variant="inherit"
                            component="span"
                            color={'text.secondary'}
                            sx={{
                              fontWeight: '400',
                              fontSize: { xs: '10px', sm: '14px' },
                              mr: '8px',
                            }}
                          >
                            {`${t('bank_accounts:register_date')}:`}
                          </Typography>
                          <Typography
                            variant="inherit"
                            component="span"
                            color={'text.primary'}
                            // fontFamily={pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold'}
                            sx={{
                              // fontWeight: '600',
                              fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                              fontSize: { xs: '12px', sm: '16px', lg: '17px' },
                            }}
                          >
                            {account.created_at}
                          </Typography>
                        </Box>

                        <Box
                          sx={{
                            display: 'flex',
                            alignItems: 'center',
                          }}
                        >
                          <Typography
                            variant="inherit"
                            component="span"
                            color={'text.secondary'}
                            sx={{
                              fontWeight: '400',
                              fontSize: { xs: '10px', sm: '14px' },
                              mr: '8px',
                            }}
                          >
                            {account.accountable_type === 'iran'
                              ? `${t('bank_accounts:shaba_num')}:`
                              : account.accountable_type === 'international'
                              ? `${t('bank_accounts:swift_code')}:`
                              : account.accountable_type === 'credits'
                              ? `${t('bank_accounts:card_type')}:`
                              : ''}
                          </Typography>
                          <Typography
                            variant="inherit"
                            component="span"
                            color={'primary.main'}
                            sx={{
                              // fontWeight: '600',
                              fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                              fontSize: { xs: '12px', lg: '14px' },
                            }}
                          >
                            {account.code}
                          </Typography>
                        </Box>

                        <Box
                          sx={{
                            display: 'flex',
                            alignItems: 'center',
                          }}
                        >
                          <Typography
                            variant="inherit"
                            component="span"
                            color={'text.secondary'}
                            sx={{
                              fontWeight: '400',
                              fontSize: '14px',
                              mr: '8px',
                            }}
                          >
                            {account.accountable_type === 'iran' || account.accountable_type === 'credits'
                              ? `${t('bank_accounts:card_num')}:`
                              : account.accountable_type === 'international'
                              ? `${t('bank_accounts:acc_num')}:`
                              : ''}
                          </Typography>
                          <Typography
                            variant="inherit"
                            component="span"
                            color={'primary.main'}
                            sx={{
                              // fontWeight: '600',
                              fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                              fontSize: { xs: '12px', lg: '14px' },
                            }}
                          >
                            {account.accountable_type === 'iran' || account.accountable_type === 'credits'
                              ? addSpaceForCardNumber(account.card_number)
                              : account.accountable_type === 'international'
                              ? addSpaceForCardNumber(account.ban)
                              : ''}
                          </Typography>
                        </Box>
                      </Box>

                      {!account.is_active && (
                        <Box
                          sx={{
                            width: '100%',
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'flex-end',
                            mt: '5px',
                          }}
                        >
                          <Button
                            variant="contained"
                            // type={'submit'}
                            disableRipple
                            sx={{
                              '&:hover': {
                                bgcolor: 'transparent',
                                boxShadow: 'none',
                              },
                              '&:active': {
                                transition: 'none',
                                color: 'primary.main',
                                '& .MuiButton-endIcon': {
                                  filter:
                                    'invert(42%) sepia(49%) saturate(2461%) hue-rotate(195deg) brightness(103%) contrast(99%)',
                                },
                              },
                              padding: '0',
                              bgcolor: 'transparent',
                              color: 'text.secondary',
                              boxShadow: 'none',
                              // fontWeight: '600',
                              borderRadius: '12px',
                              height: '40px',
                              fontSize: { xs: '10px', sm: '14px' },
                              minWidth: '45px',
                            }}
                            endIcon={
                              <Box
                                component={'img'}
                                src={editIcon.src}
                                alt={'bullet-point'}
                                sx={{
                                  width: { xs: '16px', sm: '24px' },
                                  height: { xs: '16px', sm: '24px' },
                                  mx: 0,
                                }}
                              />
                            }
                          >
                            {t('bank_accounts:edit')}
                          </Button>
                        </Box>
                      )}
                    </Box>

                    <Box
                      sx={{
                        width: '100%',
                        height: 'auto',
                        bgcolor: 'transparent',
                        display: { xs: 'block', sm: 'none' },
                      }}
                    >
                      <Box
                        sx={{
                          width: '100%',
                          display: 'flex',
                          alignItems: 'center',
                          justifyContent: 'space-between',
                        }}
                      >
                        <Box
                          sx={{
                            display: 'flex',
                            alignItems: 'center',
                          }}
                        >
                          <Typography
                            variant="inherit"
                            component="span"
                            color={'text.secondary'}
                            sx={{
                              fontWeight: '400',
                              fontSize: { xs: '10px', sm: '14px' },
                              mr: '4px',
                            }}
                          >
                            {`${t('bank_accounts:acc_title')}:`}
                          </Typography>
                          <Typography
                            variant="inherit"
                            component="span"
                            color={'text.primary'}
                            sx={{
                              // fontWeight: '600',
                              fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                              fontSize: { xs: '12px', sm: '16px', lg: '17px' },
                            }}
                          >
                            {account.bank_name}
                          </Typography>
                        </Box>

                        <Box
                          sx={{
                            display: 'flex',
                            alignItems: 'center',
                          }}
                        >
                          <Typography
                            variant="inherit"
                            component="span"
                            color={'text.secondary'}
                            sx={{
                              fontWeight: '400',
                              fontSize: { xs: '10px', sm: '14px' },
                            }}
                          >
                            {`${t('bank_accounts:choose_account_type')}:`}
                          </Typography>

                          <Box
                            sx={{
                              display: 'flex',
                              alignItems: 'center',
                            }}
                          >
                            <Box
                              component={'img'}
                              src={account.accountable_type === 'iran' ? IRRAccountIcon.src : IntAccountIcon.src}
                              alt={'backward icon'}
                              sx={{
                                width: '16px',
                                height: '16px',
                                mx: '4px',
                              }}
                            />
                            <Typography
                              variant="inherit"
                              component="span"
                              color={'text.primary'}
                              sx={{
                                // fontWeight: '600',
                                fontFamily: 'IRANSansXFaNum-DemiBold',
                                fontSize: { xs: '12px', sm: '16px', lg: '17px' },
                              }}
                            >
                              {account.accountable_type === 'iran'
                                ? t('bank_accounts:ir_acc')
                                : account.accountable_type === 'international'
                                ? t('bank_accounts:international_account')
                                : account.accountable_type === 'credits'
                                ? t('bank_accounts:ex_card')
                                : ''}
                            </Typography>
                          </Box>
                        </Box>
                      </Box>

                      <Box
                        sx={{
                          width: '100%',
                          height: '2px',
                          backgroundImage: 'linear-gradient(to right, #C3C3C3 50%, rgba(255,255,255,0) 0%)',
                          backgroundPosition: 'bottom',
                          backgroundSize: '16px 1px',
                          backgroundRepeat: 'repeat-x',
                          margin: '14px 0',
                        }}
                      ></Box>

                      <Box
                        sx={{
                          display: 'flex',
                          flexDirection: 'column',
                          alignItems: 'flex-start',
                        }}
                      >
                        <Box
                          sx={{
                            display: 'flex',
                            alignItems: 'center',
                            mb: '12px',
                          }}
                        >
                          <Typography
                            variant="inherit"
                            component="span"
                            color={'text.secondary'}
                            sx={{
                              fontWeight: '400',
                              fontSize: { xs: '10px', sm: '14px' },
                              mr: '4px',
                            }}
                          >
                            {account.accountable_type === 'iran'
                              ? `${t('bank_accounts:shaba_num')}:`
                              : account.accountable_type === 'international'
                              ? `${t('bank_accounts:swift_code')}:`
                              : account.accountable_type === 'credits'
                              ? `${t('bank_accounts:card_type')}:`
                              : ''}
                          </Typography>
                          <Typography
                            variant="inherit"
                            component="span"
                            color={'primary.main'}
                            sx={{
                              // fontWeight: '600',
                              fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                              fontSize: { xs: '12px', lg: '14px' },
                            }}
                          >
                            {account.code}
                          </Typography>
                        </Box>

                        <Box
                          sx={{
                            display: 'flex',
                            alignItems: 'center',
                          }}
                        >
                          <Typography
                            variant="inherit"
                            component="span"
                            color={'text.secondary'}
                            sx={{
                              fontWeight: '400',
                              fontSize: { xs: '10px', sm: '14px' },
                              mr: '4px',
                            }}
                          >
                            {account.accountable_type === 'iran' || account.accountable_type === 'credits'
                              ? `${t('bank_accounts:card_num')}:`
                              : account.accountable_type === 'international'
                              ? `${t('bank_accounts:acc_num')}:`
                              : ''}
                          </Typography>
                          <Typography
                            variant="inherit"
                            component="span"
                            color={'primary.main'}
                            sx={{
                              // fontWeight: '600',
                              fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                              fontSize: { xs: '12px', lg: '14px' },
                            }}
                          >
                            {account.accountable_type === 'iran' || account.accountable_type === 'credits'
                              ? addSpaceForCardNumber(account.card_number)
                              : account.accountable_type === 'international'
                              ? addSpaceForCardNumber(account.ban)
                              : ''}
                          </Typography>
                        </Box>
                      </Box>

                      <Box
                        sx={{
                          width: '100%',
                          height: '2px',
                          backgroundImage: 'linear-gradient(to right, #C3C3C3 50%, rgba(255,255,255,0) 0%)',
                          backgroundPosition: 'bottom',
                          backgroundSize: '16px 1px',
                          backgroundRepeat: 'repeat-x',
                          margin: '14px 0',
                        }}
                      ></Box>

                      <Box
                        sx={{
                          width: '100%',
                          display: 'flex',
                          alignItems: 'center',
                          justifyContent: 'space-between',
                        }}
                      >
                        <Box
                          sx={{
                            display: 'flex',
                            alignItems: 'center',
                          }}
                        >
                          <Typography
                            variant="inherit"
                            component="span"
                            color={'text.secondary'}
                            sx={{
                              fontWeight: '400',
                              fontSize: { xs: '10px', sm: '14px' },
                              mr: '4px',
                            }}
                          >
                            {`:${t('bank_accounts:register_date')}`}
                          </Typography>
                          <Typography
                            variant="inherit"
                            component="span"
                            color={'text.primary'}
                            sx={{
                              // fontWeight: '600',
                              fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                              fontSize: { xs: '12px', sm: '16px', lg: '17px' },
                            }}
                          >
                            {account.created_at}
                          </Typography>
                        </Box>

                        <Box
                          sx={{
                            display: 'flex',
                            alignItems: 'center',
                          }}
                        >
                          <Typography
                            variant="inherit"
                            component="span"
                            color={'text.secondary'}
                            sx={{
                              fontWeight: '400',
                              fontSize: { xs: '10px', sm: '14px' },
                              mr: '4px',
                            }}
                          >
                            {`${t('bank_accounts:situation')}:`}
                          </Typography>
                          <Chip
                            label={account.status}
                            sx={{
                              minWidth: { xs: '56px', sm: '112px' },
                              height: '24px',
                              borderRadius: { xs: '30px', sm: '11px' },
                              bgcolor: 'success.light',
                              '& .MuiChip-label': {
                                padding: '0',
                                margin: '0',
                                display: 'flex',
                                alignItems: 'center',
                                justifyContent: 'center',
                                color: 'success.dark',
                                fontSize: { xs: '10px', sm: '12px' },
                                // fontWeight: '600',
                                fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                              },
                            }}
                          />
                        </Box>
                      </Box>

                      {!account.is_active && (
                        <Box
                          sx={{
                            width: '100%',
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'flex-end',
                            mt: { sm: '5px' },
                          }}
                        >
                          <Button
                            variant="contained"
                            // type={'submit'}
                            disableRipple
                            sx={{
                              '&:hover': {
                                bgcolor: 'transparent',
                                boxShadow: 'none',
                              },
                              '&:active': {
                                transition: 'none',
                                color: 'primary.main',
                                '& .MuiButton-endIcon': {
                                  filter:
                                    'invert(42%) sepia(49%) saturate(2461%) hue-rotate(195deg) brightness(103%) contrast(99%)',
                                },
                              },
                              padding: '0',
                              bgcolor: 'transparent',
                              color: 'text.secondary',
                              boxShadow: 'none',
                              // fontWeight: '600',
                              borderRadius: '12px',
                              height: '40px',
                              fontSize: { xs: '10px', sm: '14px' },
                              minWidth: '45px',
                            }}
                            endIcon={
                              <Box
                                component={'img'}
                                src={editIcon.src}
                                alt={'bullet-point'}
                                sx={{
                                  width: { xs: '16px', sm: '24px' },
                                  height: { xs: '16px', sm: '24px' },
                                  mx: 0,
                                }}
                              />
                            }
                          >
                            {t('bank_accounts:edit')}
                          </Button>
                        </Box>
                      )}
                    </Box>
                  </Box>
                );
              })
            : !userBankAccountsResult.loading && (
                <Box
                  sx={{
                    width: '100%',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius: '22px',
                    bgcolor: 'white',
                  }}
                >
                  <Box
                    sx={{
                      display: 'flex',
                      flexDirection: 'column',
                      alignItems: 'center',
                      justifyContent: 'center',
                      mt: { xs: '95px', sm: '130px' },
                      mb: { xs: '160px', sm: '400px', lg: '195px' },
                    }}
                  >
                    <Box
                      component={'img'}
                      src={noTransactionIcon.src}
                      alt={'spotify icon'}
                      sx={{
                        width: { xs: '149px', sm: '208px' },
                        height: { xs: '153px', sm: '214px' },
                        mb: '16px',
                      }}
                    />
                    <Typography
                      variant="inherit"
                      component="span"
                      color={'text.primary'}
                      sx={{
                        fontWeight: { xs: '400' },
                        fontSize: { xs: '12px', sm: '20px' },
                        textAlign: 'center',
                      }}
                    >
                      {t('bank_accounts:no_accounts_recorded')}
                    </Typography>
                    <Typography
                      variant="inherit"
                      component="span"
                      color={'text.secondary'}
                      sx={{
                        fontWeight: '400',
                        fontSize: { xs: '10px', sm: '16px' },
                        textAlign: 'center',
                        mt: '8px',
                        mb: '48px',
                      }}
                    >
                      {t('bank_accounts:register_your_new_account')}
                    </Typography>
                    <Button
                      variant="contained"
                      disableRipple={true}
                      sx={{
                        width: '236px',
                        height: '40px',
                        '&:hover': {
                          boxShadow: 'none',
                        },
                        boxShadow: 'none',
                        borderRadius: '12px',
                        // fontWeight: '600',
                        fontSize: '14px',
                      }}
                      onClick={() => router.push(`/${PATHS.USER_BANK_ACCOUNT}/${PATHS.IRANIAN}`)}
                    >
                      {t('bank_accounts:introduce_account')}
                    </Button>
                  </Box>
                </Box>
              )}
        </Box>
      )}
    </>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default AccountsInfoCards;
