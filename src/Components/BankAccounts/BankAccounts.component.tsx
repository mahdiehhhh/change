import React, { useEffect, useState, ChangeEvent, ReactNode } from 'react';
import {
  Box,
  ButtonGroup,
  Button,
  InputAdornment,
  IconButton,
  Select,
  MenuItem,
  TextField,
  InputLabel,
  Tabs,
  Tab,
  Chip,
  FormHelperText,
  FormControl,
} from '@mui/material';
import bulletPoint from '/public/assets/images/BulletPoint.svg';
import attachmentIcon from '/public/assets/images/Attachment-icon.svg';
import documentIcon from '/public/assets/images/Document-icon.svg';
import deleteIcon from '/public/assets/images/delete_icon_hover.svg';
import garbageIcon from '/public/assets/images/garbage_icon.svg';
import dangerCircleIcon from '/public/assets/images/Danger-Circle-icon.svg';
import backwardArrowIcon from '/public/assets/images/backwardArrowDeposit.svg';
import Typography from '@mui/material/Typography';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import visibleEye from '*.svg';
import visibleEyeDisabled from '*.svg';
import invisibleEye from '*.svg';
import invisibleEyeDisabled from '*.svg';
import { useI18nTrDir } from 'utils/CustomHooks/i18nTrDir';
import { prev } from 'dom7';
import Head from 'next/head';
import { fetchUserBankAccounts } from 'store/slices/user_bank_accounts.slice';
import { useAppDispatch, useAppSelector } from 'store/hooks';
import { fetchCurrenciesList } from 'store/slices/currencies_list.slice';
import { useFormik } from 'formik';
import * as yup from 'yup';

import { fetchCreateIranBankAccount } from 'store/slices/create_iran_bank_account.slice';
import { fetchCreateIntBankAccount } from 'store/slices/create_int_bank_account.slice';
import { fetchCreateCreditBankAccount } from 'store/slices/create_credit_bank_account.slice';
import bell from '*.svg';
import Tooltip from '@mui/material/Tooltip';
import { useRouter } from 'next/router';
import { PATHS } from '../../configs/routes.config';
import backwardArrowDeposit from '*.svg';
import Divider from '@mui/material/Divider';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import { localesNS } from '../../utils/Data/locales_NameSpace.utils';
import { fetchBanksLists } from '../../store/slices/banks_lists.slice';
import AlertDialogBox from '../AlertDialogBox/AlertDialogBox.component';

interface AccountTypeSample {
  id: number;
  title: string;
}

interface CurrenciesListSample {
  id: number;
  title: string;
  symbol: string;
  iso_code: string;
  image: string;
  colors: {
    one: string;
    two: string;
    five: string;
    four: string;
    three: string;
  };
}

interface BankAccountsKeysSample {
  title?: string;
  ban?: string;
  bank_id?: number;
  currency_id?: number;
  card_title?: string;
  card_number?: string;
  bank_name?: string;
  holder_name?: string;
  code?: string;
  swift_code?: string;
  bank_phone?: string | number;
  document_image?: any;
  bank_branch_code?: string;
  bank_address?: string;
  name?: string;
  type?: string;
}

interface BankAccountSample {
  id: number;
  title: string;
  ban: string;
  accountable_type: string;
  code: string;
  card_number: string;
  is_active: boolean;
  status: string;
  bank_name: string;
  bank_logo: any;
  created_at: string;
}

interface BanksListsSample {
  id: number;
  name: string;
  logo: object;
}

interface CurrencyTypeSample {
  id: string;
  name: string;
}

interface CardTypeSample {
  id: string;
  name: string;
}

// interface BankAccountSchema {
//     phone?: string | number
//     password?: string | number
//     repeatPassword?: string | number
// }

const BankAccounts = () => {
  const router = useRouter();
  const UrlPath = router.pathname;

  const theme = useTheme();
  const lg = useMediaQuery(theme.breakpoints.up('lg'));

  const { t, pageDirection } = useI18nTrDir();
  const dispatch = useAppDispatch();
  const userBankAccountsResult = useAppSelector((state) => state.UserBankAccounts);
  const banksListsResult = useAppSelector((state) => state.BanksLists);
  const currenciesListResult = useAppSelector((state) => state.CurrenciesList);
  const createIranBankAccountResult = useAppSelector((state) => state.CreateIranBankAccount);
  const createIntBankAccountResult = useAppSelector((state) => state.CreateInternationalBankAccount);
  const createCreditBankAccountResult = useAppSelector((state) => state.CreateCreditBankAccount);

  const [inputValues, setInputValues] = useState<any>({});
  const [bankNameState, setBankNameState] = useState(undefined);
  const [currencyTypeState, setCurrencyTypeState] = useState(undefined);
  const [cardTypeState, setCardTypeState] = useState(undefined);
  const [imageFile, setImageFile] = useState<File>();
  const [preview, setPreview] = useState<string>();
  const [desiredCurrenciesListState, setDesiredCurrenciesListState] = useState<CurrenciesListSample[]>();
  const [monetaryUnit, setMonetaryUnit] = useState<CurrenciesListSample>();
  const [invalidFileType, setInvalidFileType] = useState(false);
  const [activeTabApiErrorsState, setActiveTabApiErrorsState] = useState<any>({});
  const [activeBankAccounts, setActiveBankAccounts] = useState([]);

  const [openResultDialogBox, setOpenResultDialogBox] = React.useState(false);
  const [operationResultState, setOperationResultState] = useState<object>({
    status: '',
    messages: {},
  });

  const handleOpenResultDB = () => {
    setOpenResultDialogBox(true);
  };

  const handleCloseResultDB = () => {
    setOpenResultDialogBox(false);
  };

  useEffect(() => {
    dispatch(fetchCurrenciesList());
    // dispatch(fetchUserBankAccounts('iran'))
    dispatch(fetchBanksLists());
  }, []);

  useEffect(() => {
    // if (!imageFile) {
    //     setPreview(undefined)
    //     return
    // }

    const objectUrl = imageFile && URL.createObjectURL(imageFile);
    console.log('what is object uielsdlf', objectUrl);
    setPreview(objectUrl);

    // free memory when ever this component is unmounted
    return () => URL.revokeObjectURL(objectUrl as string);
  }, [imageFile]);

  useEffect(() => {
    if (activeAccountTab.id === 1) {
      if (Object.keys(createIranBankAccountResult?.error)?.length !== 0) {
        setActiveTabApiErrorsState(createIranBankAccountResult?.error);
      } else {
        !!createIranBankAccountResult.createIranBankAccResp?.message && setOpenResultDialogBox(true);
        setOperationResultState({
          status: 'success',
          messages: createIranBankAccountResult.createIranBankAccResp?.message,
        });
        setActiveTabApiErrorsState([]);
      }
    }
    if (activeAccountTab.id === 2) {
      if (Object.keys(createIntBankAccountResult?.error)?.length !== 0) {
        setActiveTabApiErrorsState(createIntBankAccountResult?.error);
      } else {
        !!createIntBankAccountResult.createIntBankAccResp?.message && setOpenResultDialogBox(true);
        setOperationResultState({
          status: 'success',
          messages: createIntBankAccountResult.createIntBankAccResp?.message,
        });
        setActiveTabApiErrorsState([]);
      }
    }
    if (activeAccountTab.id === 3) {
      if (Object.keys(createCreditBankAccountResult?.error)?.length !== 0) {
        setActiveTabApiErrorsState(createCreditBankAccountResult?.error);
      } else {
        !!createCreditBankAccountResult.createCreditBankAccResp?.message && setOpenResultDialogBox(true);
        setOperationResultState({
          status: 'success',
          messages: createCreditBankAccountResult.createCreditBankAccResp?.message,
        });
        setActiveTabApiErrorsState([]);
      }
    }
  }, [createIranBankAccountResult, createIntBankAccountResult, createCreditBankAccountResult]);

  const bankAccountTabs = [
    {
      id: 1,
      title: t('bank_accounts:ir_acc'),
      link: `${PATHS.USER_BANK_ACCOUNT}/${PATHS.IRANIAN}`,
    },
    {
      id: 2,
      title: t('bank_accounts:ex_acc'),
      link: `${PATHS.USER_BANK_ACCOUNT}/${PATHS.INTERNATIONAL}`,
    },
    {
      id: 3,
      title: t('bank_accounts:ex_card'),
      link: `${PATHS.USER_BANK_ACCOUNT}/${PATHS.CARD}`,
    },
    {
      id: 4,
      title: t('bank_accounts:crypto_acc'),
      link: `${PATHS.USER_BANK_ACCOUNT}/${PATHS.CRYPTO}`,
    },
  ];

  const iranianAccountOptions = {
    id: 1,
    urlKey: PATHS.IRANIAN,
    formTitle: t('bank_accounts:introduce_ir_acc'),
    formOptions: [
      {
        id: 'account_title',
        api_id: 'title',
        title: t('bank_accounts:acc_title'),
        placeholder: t('bank_accounts:enter_acc_title'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'false',
        width: '50%',
      },
      {
        id: 'bank_name',
        api_id: 'bank_id',
        title: t('bank_accounts:bank_name'),
        placeholder: t('bank_accounts:choose'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'true-simple',
        width: '50%',
      },
      {
        id: 'monetary_unit',
        api_id: 'currency_id',
        title: t('bank_accounts:monetary_unit'),
        placeholder: t('bank_accounts:choose'),
        validation: false,
        hasSelect: 'true-colored',
        width: '50%',
      },
      {
        id: 'card_owner_name',
        api_id: 'card_title',
        title: t('bank_accounts:card_owner_name'),
        placeholder: t('bank_accounts:enter_name'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'false',
        width: '50%',
      },
      {
        id: 'card_number',
        api_id: 'card_number',
        title: t('bank_accounts:card_num'),
        placeholder: t('bank_accounts:enter_card_num'),
        validation: yup
          .string()
          .required(t('bank_accounts:required_text'))
          .matches(/(?=.*?\d)^\$?(([1-9]\d{0,2}(,\d{3})*)|\d+)?(\.\d{1,2})?$/, t('bank_accounts:number_text'))
          .min(16, t('bank_accounts:16_digit_text'))
          .max(16, t('bank_accounts:16_digit_text')),
        hasSelect: 'false',
        width: '50%',
      },
      {
        id: 'shaba_number',
        api_id: 'iban',
        title: t('bank_accounts:shaba_num'),
        placeholder: t('bank_accounts:enter_shaba_num'),
        validation: yup
          .string()
          .required(t('bank_accounts:required_text'))
          .matches(/(?=.*?\d)^\$?(([1-9]\d{0,2}(,\d{3})*)|\d+)?(\.\d{1,2})?$/, t('bank_accounts:number_text'))
          .min(24, t('bank_accounts:24_digit_text'))
          .max(24, t('bank_accounts:24_digit_text')),
        hasSelect: 'false',
        width: '50%',
      },
      {
        id: 'account_number',
        api_id: 'ban',
        title: t('bank_accounts:acc_num'),
        placeholder: t('bank_accounts:enter_acc_num'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'false',
        width: '50%',
      },
    ],
  };

  const [activeAccountTab, setActiveAccountTab] = useState(bankAccountTabs[0]);

  const [activeAccountTabOptions, setActiveAccountTabOptions] = useState(iranianAccountOptions);

  const currencyAccountOptions = {
    id: 2,
    urlKey: PATHS.INTERNATIONAL,
    formTitle: t('bank_accounts:introduce_ex_acc'),
    formOptions: [
      {
        id: 'account_title',
        api_id: 'title',
        title: t('bank_accounts:acc_title'),
        placeholder: t('bank_accounts:enter_acc_title'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'false',
        width: '50%',
      },
      {
        id: 'bank_name',
        api_id: 'bank_name',
        title: t('bank_accounts:bank_name'),
        placeholder: t('bank_accounts:choose'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'false',
        width: '50%',
      },
      {
        id: 'monetary_unit',
        api_id: 'currency_id',
        title: t('bank_accounts:monetary_unit'),
        placeholder: t('bank_accounts:choose'),
        validation: false,
        hasSelect: 'true-colored',
        width: '50%',
      },
      {
        id: 'account_owner_name',
        api_id: 'holder_name',
        title: t('bank_accounts:acc_owner_name'),
        placeholder: t('bank_accounts:enter_name'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'false',
        width: '50%',
      },
      {
        id: 'account_code',
        api_id: 'code',
        title: t('bank_accounts:acc_code'),
        placeholder: t('bank_accounts:enter_acc_code'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'false',
        width: '50%',
      },
      {
        id: 'swift_code',
        api_id: 'swift_code',
        title: t('bank_accounts:swift_code'),
        placeholder: t('bank_accounts:enter_swift_code'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'false',
        width: '50%',
      },
      {
        id: 'tel_bank',
        api_id: 'bank_phone',
        title: t('bank_accounts:bank_tel'),
        placeholder: t('bank_accounts:enter_bank_tel'),
        validation: yup
          .string()
          .required(t('bank_accounts:required_text'))
          .matches(/(?=.*?\d)^\$?(([1-9]\d{0,2}(,\d{3})*)|\d+)?(\.\d{1,2})?$/, t('bank_accounts:number_text')),
        hasSelect: 'false',
        width: '50%',
      },
      {
        id: 'passport_image',
        api_id: 'document_image',
        title: t('bank_accounts:acc_owner_passport_img'),
        placeholder: t('bank_accounts:upload_your_file'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'false',
        width: '50%',
      },
      {
        id: 'account_number',
        api_id: 'ban',
        title: t('bank_accounts:acc_num'),
        placeholder: t('bank_accounts:enter_acc_num'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'false',
        width: '50%',
      },
      {
        id: 'branch_code',
        api_id: 'bank_branch_code',
        title: t('bank_accounts:branch_code'),
        placeholder: t('bank_accounts:enter_branch_code'),
        validation: yup
          .string()
          .required(t('bank_accounts:required_text'))
          .matches(/(?=.*?\d)^\$?(([1-9]\d{0,2}(,\d{3})*)|\d+)?(\.\d{1,2})?$/, t('bank_accounts:number_text')),
        hasSelect: 'false',
        width: '50%',
      },
      {
        id: 'bank_address',
        api_id: 'bank_address',
        title: t('bank_accounts:bank_address'),
        placeholder: t('bank_accounts:enter_address'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'false',
        width: '100%',
      },
    ],
  };

  const currencyCardOptions = {
    id: 3,
    urlKey: PATHS.CARD,
    formTitle: t('bank_accounts:introduce_ex_card'),
    formOptions: [
      {
        id: 'account_title',
        api_id: 'title',
        title: t('bank_accounts:acc_title'),
        placeholder: t('bank_accounts:enter_acc_title'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'false',
        width: '50%',
      },
      {
        id: 'card_type',
        api_id: 'type',
        title: t('bank_accounts:card_type'),
        placeholder: t('bank_accounts:choose'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'true-simple',
        width: '50%',
      },
      {
        id: 'card_name',
        api_id: 'name',
        title: t('bank_accounts:card_name'),
        placeholder: t('bank_accounts:enter_name'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'false',
        width: '50%',
      },
      {
        id: 'monetary_unit',
        api_id: 'currency_id',
        title: t('bank_accounts:monetary_unit'),
        placeholder: t('bank_accounts:choose'),
        validation: false,
        hasSelect: 'true-colored',
        width: '50%',
      },
      {
        id: 'account_number',
        api_id: 'card_number',
        title: t('bank_accounts:acc_num'),
        placeholder: t('bank_accounts:enter_acc_num'),
        validation: yup.string().required(t('bank_accounts:required_text')),
        hasSelect: 'false',
        width: '50%',
      },
    ],
  };

  const cryptoAccountOptions = {
    id: 4,
    urlKey: PATHS.CRYPTO,
    formTitle: t('bank_accounts:introduce_crypto_acc'),
    formOptions: [
      {
        id: 'account_title',
        api_id: 'title',
        title: t('bank_accounts:acc_title'),
        placeholder: t('bank_accounts:enter_acc_title'),
        validation: false,
        hasSelect: 'false',
        width: '50%',
      },
      {
        id: 'currency_type',
        api_id: 'currency_type',
        title: t('bank_accounts:ex_type'),
        placeholder: t('bank_accounts:choose'),
        validation: false,
        hasSelect: 'true-simple',
        width: '50%',
      },
      {
        id: 'network_type',
        api_id: 'network_type',
        title: t('bank_accounts:network_type'),
        placeholder: '',
        validation: false,
        hasSelect: 'false',
        width: '50%',
      },
      {
        id: 'address',
        api_id: 'address',
        title: t('bank_accounts:address'),
        placeholder: t('bank_accounts:enter_address'),
        validation: false,
        hasSelect: 'false',
        width: '50%',
      },
    ],
  };

  const importantPoints = [
    {
      id: 1,
      title: t('bank_accounts:withdraw_possible_for_acc_owner'),
    },
    {
      id: 2,
      title: t('bank_accounts:register_swift_by_others_name'),
    },
    {
      id: 3,
      title: t('bank_accounts:ensure_bank_info'),
    },
    {
      id: 4,
      title: t('bank_accounts:discrepancy_not_confirmed'),
    },
  ];

  const currencyTypeList = [
    {
      id: 1,
      name: t('bank_accounts:etherium'),
    },
    {
      id: 2,
      name: t('bank_accounts:bitcoin'),
    },
  ];

  const cardTypeList = [
    {
      id: 1,
      name: 'VISA',
    },
    {
      id: 2,
      name: 'MASTER',
    },
  ];

  const fileTypeWarningText = [t('bank_accounts:file_format_type'), t('bank_accounts:file_max_vol')];

  useEffect(() => {
    const selectedTab = bankAccountTabs.find((tab) => `/${tab.link}` === UrlPath);
    // @ts-ignore
    setActiveAccountTab(selectedTab);
  }, [UrlPath]);

  const handleChangeAccountForMobile = (event: any) => {
    router.push(`/${event.target.value}`);
  };

  // useEffect(() => {
  //     const filteredActiveBankAccounts = userBankAccountsResult?.userBankAccountsResp?.bank_accounts_list.filter((bank: BankAccountSample) => bank.is_active)
  //     setActiveBankAccounts(filteredActiveBankAccounts)
  // }, [userBankAccountsResult])

  useEffect(() => {
    // const filteredActiveBankAccounts = userBankAccountsResult?.userBankAccountsResp?.bank_accounts_list.filter((bank: BankAccountSample) => bank.is_active)
    // console.log('this is banks lists', banksListsResult.banksListsResp?.banks_list)
    if (!!banksListsResult.banksListsResp?.banks_list) {
      setActiveBankAccounts(banksListsResult.banksListsResp?.banks_list);
    }
  }, [banksListsResult]);

  // const handleChangeTabs = (event: React.SyntheticEvent, newValue: number) => {
  //     const selectedTab = bankAccountTabs.find(tab => tab.id === newValue + 1)
  //     // @ts-ignore
  //     setActiveAccountTab(selectedTab)
  // }

  const tabOptions = [iranianAccountOptions, currencyAccountOptions, currencyCardOptions, cryptoAccountOptions];

  useEffect(() => {
    const { selectedTabOption, activeTabInitialInputsValueObj } = createActiveTabInitialInputsValue();
    setInputValues(activeTabInitialInputsValueObj);
    // @ts-ignore
    setActiveAccountTabOptions(selectedTabOption);

    // @ts-ignore
    // if (selectedTabOption.id === 1) {
    //     dispatch(fetchUserBankAccounts('iran'))
    // }
    // @ts-ignore
    // if(selectedTabOption.id !== 4){
    //     dispatch(fetchCurrenciesList())
    // }

    handleSetDesiredCurrenciesListArray();

    setBankNameState(undefined);
    setCurrencyTypeState(undefined);
    setCardTypeState(undefined);
    handleDeleteImage();
    setActiveTabApiErrorsState([]);
    setOpenResultDialogBox(false);
  }, [activeAccountTab]);

  const createActiveTabInitialInputsValue = () => {
    const selectedTabOption = tabOptions.find((tab) => tab.id === activeAccountTab.id);
    // @ts-ignore
    const activeTabInitialInputsValueObj = selectedTabOption.formOptions.reduce((accumulator, value) => {
      return { ...accumulator, [value.api_id]: '' };
    }, {});
    return { selectedTabOption, activeTabInitialInputsValueObj };
  };

  const handleSetDesiredCurrenciesListArray = () => {
    if (activeAccountTab.id === 2 || activeAccountTab.id === 3) {
      const desiredCurrenciesListArray = currenciesListResult?.currenciesListResp?.currency_list?.filter(
        (currency: CurrenciesListSample) => currency.iso_code !== 'IRR',
      );
      setDesiredCurrenciesListState(desiredCurrenciesListArray);
      desiredCurrenciesListArray && setMonetaryUnit(desiredCurrenciesListArray[0]);
      setInputValues((prevState: any) => ({
        ...prevState,
        ['currency_id']: desiredCurrenciesListArray && desiredCurrenciesListArray[0]?.id,
      }));
    } else {
      setDesiredCurrenciesListState(currenciesListResult?.currenciesListResp?.currency_list);
      setMonetaryUnit(currenciesListResult?.currenciesListResp?.currency_list[0]);
      setInputValues((prevState: any) => ({
        ...prevState,
        ['currency_id']: currenciesListResult?.currenciesListResp?.currency_list[0].id,
      }));
    }
  };

  useEffect(() => {
    handleSetDesiredCurrenciesListArray();
  }, [currenciesListResult]);

  const selectNewIconSimple = (props: any) => (
    <svg {...props} width="16" height="9" viewBox="0 0 16 9" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M15 1L8 8L1 1" stroke="#606060" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
    </svg>
  );

  const coloredNewIcon = (props: any) => (
    <svg {...props} width="9" height="9" viewBox="0 0 9 9" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M3.65175 7.85617C3.60825 7.81177 3.42225 7.64414 3.26925 7.488C2.307 6.57254 0.732 4.1844 0.25125 2.93445C0.174 2.74462 0.0105 2.2647 0 2.00828C0 1.76258 0.054 1.52836 0.1635 1.30485C0.3165 1.02623 0.55725 0.802727 0.8415 0.680258C1.03875 0.601419 1.629 0.47895 1.6395 0.47895C2.28525 0.356481 3.3345 0.289124 4.494 0.289124C5.59875 0.289124 6.60525 0.356481 7.26075 0.456753C7.27125 0.468234 8.00475 0.590703 8.256 0.724653C8.715 0.970357 9 1.45028 9 1.96389V2.00828C8.98875 2.34277 8.70375 3.0462 8.69325 3.0462C8.21175 4.22879 6.714 6.56183 5.71875 7.49948C5.71875 7.49948 5.463 7.76355 5.30325 7.87837C5.07375 8.05748 4.7895 8.14627 4.50525 8.14627C4.188 8.14627 3.8925 8.04599 3.65175 7.85617Z"
        fill="#298FFE"
      />
    </svg>
  );

  const handleChangeSelectValues = (event: any, input: string) => {
    if (input === 'bank_id') {
      setBankNameState(event.target.value);
      setInputValues((prevState: any) => ({ ...prevState, [input]: JSON.parse(event.target.value).id }));
    } else if (input === 'currency_id') {
      setInputValues((prevState: any) => ({ ...prevState, [input]: JSON.parse(event.target.value).id }));
      setMonetaryUnit(JSON.parse(event.target.value));
    } else if (input === 'type') {
      setCardTypeState(event.target.value);
      setInputValues((prevState: any) => ({ ...prevState, [input]: JSON.parse(event.target.value).name }));
    } else if (input === 'currency_type') {
      setCurrencyTypeState(event.target.value);
    }
  };

  const handleGetImageFile = (e: ChangeEvent<HTMLInputElement>) => {
    // @ts-ignore
    const selectedFile = e.target.files[0];
    if (selectedFile) {
      const allowedFileFormats = /(\.jpg|\.jpeg|\.png|\.bmp)$/i;
      if (!allowedFileFormats.exec(selectedFile.name)) {
        setInvalidFileType(true);
      } else {
        setInvalidFileType(false);
      }
      setImageFile(selectedFile);
      // @ts-ignore
      formik.handleChange('document_image')(selectedFile.name);
    }
  };

  const handleSetInputValues = (event: any, inputValue: string) => {
    setInputValues((prevState: any) => ({ ...prevState, [inputValue]: event.target.value }));
  };

  const selectInitialValueByType = (): any => {
    const { selectedTabOption, activeTabInitialInputsValueObj } = createActiveTabInitialInputsValue();
    // @ts-ignore
    selectedTabOption.formOptions.forEach((item) => {
      if (!item.validation) {
        // @ts-ignore
        delete activeTabInitialInputsValueObj[item.api_id];
      }
    });
    // let bankAccountInitialObject = activeTabInitialInputsValueObj;
    return activeTabInitialInputsValueObj;
  };

  const createBankAccountYupSchema = () => {
    const { selectedTabOption } = createActiveTabInitialInputsValue();
    // @ts-ignore
    const activeTabYupSchema = selectedTabOption.formOptions.reduce((accumulator, value) => {
      let schemaAcc = { ...accumulator };
      if (!!value.validation) {
        schemaAcc = { ...schemaAcc, [value.api_id]: value.validation };
      }
      return schemaAcc;
    }, {});

    return activeTabYupSchema;
  };

  let bankAccountSchemaDeclaration: object = createBankAccountYupSchema();
  const bankAccountValidationSchema = yup.object({ ...bankAccountSchemaDeclaration });

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: selectInitialValueByType(),
    validationSchema: bankAccountValidationSchema,
    validateOnChange: true,
    validateOnBlur: false,
    onSubmit: async (values) => {
      const completeData = {
        ...values,
        ...(activeAccountTab.id === 1 && {
          bank_id: Number(values['bank_id']),
          currency_id: inputValues.currency_id,
          iban: `IR${inputValues.iban}`,
        }),
        ...(activeAccountTab.id === 2 && { currency_id: inputValues.currency_id, document_image: imageFile }),
        ...(activeAccountTab.id === 3 && { currency_id: inputValues.currency_id }),
      };
      activeAccountTab.id === 1
        ? dispatch(fetchCreateIranBankAccount(completeData))
        : activeAccountTab.id === 2
        ? !invalidFileType && dispatch(fetchCreateIntBankAccount(completeData))
        : activeAccountTab.id === 3 && dispatch(fetchCreateCreditBankAccount(completeData));
    },
  });

  const handleDeleteImage = () => {
    setPreview(undefined);
    setImageFile(undefined);
    formik.handleChange('document_image')('');
    setInvalidFileType(false);
  };

  return (
    <>
      <Head>
        <title>{`${t('layout:zimapay')} | ${t('layout:bank_account')}`}</title>
      </Head>

      <Box
        sx={{
          width: '100%',
          height: 'auto',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'flex-start',
          justifyContent: 'flex-start',
        }}
      >
        <Box
          sx={{
            display: { xs: 'none', sm: 'flex' },
            flexDirection: 'row',
            alignItems: 'center',
            mb: '32px',
          }}
        >
          <Tabs
            value={+activeAccountTab.id - 1}
            // onChange={handleChangeTabs}
            aria-label="nav tabs example"
            TabIndicatorProps={{ style: { display: 'none' } }}
          >
            {bankAccountTabs.map((tab) => {
              return (
                <Tab
                  key={tab.id}
                  label={tab.title}
                  disableRipple={true}
                  sx={{
                    fontSize: '16px',
                    // fontWeight: +activeAccountTab.id === +tab.id ? '600' : '400',
                    fontFamily: +activeAccountTab.id === +tab.id ? 'IRANSansXFaNum-DemiBold' : 'IRANSansXFaNum-Regular',
                    borderRadius: '12px',
                    bgcolor: +activeAccountTab.id === +tab.id ? 'white' : 'transparent',
                  }}
                  component="a"
                  onClick={(event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
                    event.preventDefault();
                    router.push(`/${tab.link}`);
                  }}
                  // href={tab.link}
                />
              );
            })}
          </Tabs>
        </Box>
        <Box
          sx={{
            width: '100%',
            display: 'flex',
            flexDirection: { xs: 'column', lg: 'row' },
            alignItems: 'flex-start',
            justifyContent: 'space-between',
            gap: '16px',
          }}
        >
          <Box
            sx={{
              minWidth: { xs: '100%', lg: '664px' },
              flex: 1,
              padding: { xs: '16px 12px', sm: '19px 24px' },
              borderRadius: '22px',
              bgcolor: 'white',
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'flex-start',
            }}
            component={'form'}
            onSubmit={formik.handleSubmit}
          >
            <Box
              sx={{
                width: '100%',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
                mb: '32px',
              }}
            >
              <Box
                sx={{
                  width: 'auto',
                  display: 'flex',
                  alignItems: 'center',
                }}
              >
                <Box
                  component={'img'}
                  src={bulletPoint.src}
                  alt={'bullet-point'}
                  sx={{
                    width: '22px',
                    height: '18px',
                    mr: '9px',
                    transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                  }}
                />
                <Typography
                  variant="inherit"
                  component="span"
                  color={'text.primary'}
                  sx={{
                    // fontWeight: {xs: '600'},
                    fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                    fontSize: { xs: '16px', xl: '16px' },
                  }}
                >
                  {activeAccountTabOptions.formTitle}
                </Typography>
              </Box>
              <IconButton
                color="inherit"
                aria-label="prev button"
                edge="start"
                // onClick={() => router.push(`/${PATHS.USER_DASHBOARD}`)}
                sx={{
                  width: '24px',
                  height: '24px',
                  display: { xs: 'flex', sm: 'none' },
                  justifySelf: 'flex-end',
                  padding: '0',
                  backgroundColor: '#D9D9D9',
                }}
              >
                <Box
                  component={'img'}
                  src={backwardArrowIcon.src}
                  alt={'backward icon'}
                  sx={{
                    width: '6px',
                    height: '12px',
                    transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                  }}
                />
              </IconButton>
            </Box>

            <Box
              sx={{
                width: '100%',
                height: 'auto',
                display: 'flex',
                alignItems: 'flex-start',
                justifyContent: 'space-between',
                flexWrap: 'wrap',
              }}
            >
              <Box
                sx={{
                  width: { xs: '100%' },
                  display: { xs: 'flex', sm: 'none' },
                  flexDirection: 'column',
                  alignItems: 'flex-start',
                  mb: { xs: '40px', sm: '48px' },
                }}
              >
                <InputLabel
                  htmlFor={'account_type_selection'}
                  sx={{
                    fontSize: '17px',
                    fontWeight: '400',
                    color: 'text.primary',
                  }}
                >
                  {t('bank_accounts:choose_account_type')}
                </InputLabel>
                <Select
                  // dir={'rtl'}
                  value={activeAccountTab.link}
                  onChange={handleChangeAccountForMobile}
                  name={'account_type_selection'}
                  displayEmpty={true}
                  placeholder={t('bank_accounts:choose')}
                  IconComponent={selectNewIconSimple}
                  renderValue={(value: string) => {
                    return (
                      <Typography
                        variant="inherit"
                        component="span"
                        color={'text.secondary'}
                        sx={{
                          fontSize: '14px',
                          fontWeight: '400',
                        }}
                      >
                        {activeAccountTab.title}
                      </Typography>
                    );
                  }}
                  MenuProps={{
                    anchorOrigin: {
                      vertical: 'bottom',
                      horizontal: 'left',
                    },
                    transformOrigin: {
                      vertical: 'top',
                      horizontal: 'left',
                    },
                    PaperProps: {
                      sx: {
                        mt: '8px',
                        boxShadow: '0px 4px 38px rgba(68, 68, 68, 0.1)',
                        height: 'auto',
                        borderRadius: '10px',
                        overflowY: 'auto',
                      },
                    },
                  }}
                  sx={{
                    width: '100%',
                    mt: '8px',
                    bgcolor: activeAccountTab ? 'white' : 'secondary.main',
                    borderRadius: '12px',
                    '& .MuiSelect-icon': { top: '40%', mx: '5px' },
                    height: '100%',
                    '.MuiInputBase-input': {
                      py: '14px',
                    },
                    '& fieldset': {
                      border: activeAccountTab ? '1px solid' : 'none',
                      borderColor: 'primary.main',
                    },
                  }}
                >
                  {bankAccountTabs?.map((item: any) => {
                    return (
                      <MenuItem
                        value={item.link}
                        key={item.id}
                        sx={{
                          width: '100%',
                          px: '8px',
                          my: '5px',
                        }}
                      >
                        <Typography
                          variant="inherit"
                          component="span"
                          color={'text.secondary'}
                          sx={{
                            fontSize: '14px',
                            fontWeight: '400',
                          }}
                        >
                          {item.title}
                        </Typography>
                      </MenuItem>
                    );
                  })}
                </Select>
              </Box>

              {activeAccountTabOptions.formOptions.map((option) => {
                return (
                  <Box
                    key={option.id}
                    sx={{
                      width: { xs: '100%', sm: '46%' },
                      display: 'flex',
                      flexDirection: 'column',
                      alignItems: 'flex-start',
                      mb: { xs: '40px', sm: '48px' },
                    }}
                  >
                    <Box
                      sx={{
                        display: 'flex',
                        alignItems: 'center',
                      }}
                    >
                      <InputLabel
                        htmlFor={option.id}
                        sx={{
                          fontSize: {
                            xs: '17px',
                            sm: option.api_id === 'document_image' ? '14px' : '17px',
                            lg: '17px',
                          },
                          fontWeight: '400',
                          color: 'text.primary',
                        }}
                      >
                        {option.title}
                      </InputLabel>

                      {option.id === 'passport_image' && (
                        <Tooltip
                          title={
                            <Box
                              sx={{
                                display: 'flex',
                                flexDirection: 'column',
                                alignItems: 'flex-start',
                              }}
                            >
                              {fileTypeWarningText.map((warning) => {
                                return (
                                  <>
                                    <Box
                                      sx={{
                                        display: 'flex',
                                        alignItems: 'center',
                                      }}
                                    >
                                      <Chip
                                        sx={{
                                          width: '5px',
                                          height: '5px',
                                          borderRadius: '50%',
                                          bgcolor: 'error.main',
                                          mr: '10px',
                                          '& .MuiChip-label': {
                                            padding: '0',
                                            margin: '0',
                                            display: 'flex',
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                          },
                                        }}
                                      />
                                      <Typography
                                        variant="inherit"
                                        component="span"
                                        color={'error.main'}
                                        sx={{
                                          fontWeight: '400',
                                          fontSize: '12px',
                                          lineHeight: '18px',
                                        }}
                                      >
                                        {warning}
                                      </Typography>
                                    </Box>
                                  </>
                                );
                              })}
                            </Box>
                          }
                          placement={pageDirection === 'rtl' ? (!lg ? 'right' : 'left') : !lg ? 'left' : 'right'}
                          arrow={lg}
                          // disableInteractive={true}
                          open={invalidFileType}
                          PopperProps={{
                            sx: {
                              '& .MuiTooltip-tooltip': {
                                padding: '10px',
                                borderRadius: '16px',
                                border: '1px solid',
                                borderColor: 'error.main',
                                bgcolor: 'error.light',
                                mx: '12px',
                                color: 'text.primary',
                                fontSize: { xs: '10px', sm: '14px' },
                                fontWeight: '400',
                              },

                              '& .MuiTooltip-arrow': {
                                width: '52px',
                                height: '52px',
                                color: 'error.light',
                                // bgcolor: "blue",
                                // right: 'auto',
                                left: { xs: 'auto', sm: 'auto', lg: '-10px' },
                                right: { xs: '-7px', sm: '-10px', lg: 'auto' },
                                '&::before': {
                                  // borderLeft: '20px solid transparent',
                                  // borderRight: '20px solid transparent',
                                  // borderBottom: 'calc(2 * 20px * 0.866) solid green',
                                  // borderTop: '20px solid transparent',
                                  // display: 'inline-block',
                                  width: { xs: '7px', sm: '11px' },
                                  height: { xs: '7px', sm: '11px' },

                                  // backgroundColor: "blue",
                                  border: '1px solid',
                                  borderColor: 'error.main',
                                  overflow: 'hidden',
                                },
                              },
                            },
                          }}
                        >
                          <Box
                            component={'img'}
                            src={dangerCircleIcon.src}
                            // alt={'backward icon'}
                            sx={{
                              width: '18.5px',
                              height: '18.5px',
                              ml: '11px',
                              mb: '2px',
                            }}
                          />
                        </Tooltip>
                      )}
                    </Box>

                    {option.hasSelect.includes('false') ? (
                      <Box
                        sx={{
                          width: '100%',
                          display: 'flex',
                          alignItems: 'flex-start',
                          justifyContent: 'space-between',
                          mt: '8px',
                        }}
                      >
                        <FormControl fullWidth error>
                          <TextField
                            // value={option.id === 'passport_image' ? preview && imageFile ? imageFile.name : '' : formik["values"][option.api_id]}
                            value={formik['values'][option.api_id]}
                            onChange={(event) => {
                              formik.handleChange(option.api_id)(event);
                              handleSetInputValues(event, option.api_id);
                            }}
                            error={
                              (formik['touched'][option.api_id] && Boolean(formik['errors'][option.api_id])) ||
                              (!!Object.keys(activeTabApiErrorsState).length &&
                                Object.keys(activeTabApiErrorsState).includes(option.api_id))
                            }
                            helperText={
                              formik['touched'][option.api_id] && (formik['errors'][option.api_id] as ReactNode)
                            }
                            name={option.api_id}
                            fullWidth
                            id={option.api_id}
                            placeholder={option.placeholder}
                            type={'text'}
                            autoComplete="off"
                            // onChange={(event) => handleSetInputValues(event, option.api_id)}
                            sx={{
                              '& input::placeholder': {
                                fontSize: '14px',
                                color: 'text.secondary',
                              },
                              width: '100%',
                              // flexGrow: '1',
                              height: '100%',
                              // bgcolor: option.id === 'passport_image' ? preview ? 'white' : 'secondary.main' : inputValues[option.api_id] ? 'white' : 'secondary.main',
                              borderRadius: '12px',
                              '& .MuiInputAdornment-root': {
                                ml: '8px',
                                height: '40px',
                                maxHeight: '40px',
                                backgroundColor:
                                  option.id === 'passport_image'
                                    ? preview
                                      ? 'white'
                                      : 'secondary.main'
                                    : 'secondary.main',
                                borderRadius: '12px',
                                // border: option.id === 'passport_image' ? '1px dash' : 'none'
                              },
                              '& legend': { display: 'none' },
                              '& fieldset': {
                                top: 0,
                                borderRadius: '12px',
                                border: option.id === 'passport_image' ? (preview ? '1px solid' : '1px dashed') : '',
                                borderColor: inputValues[option.api_id]
                                  ? 'primary.main'
                                  : option.id === 'passport_image'
                                  ? preview
                                    ? 'primary.main'
                                    : 'text.disabled'
                                  : 'secondary.main',
                              },
                              '& .MuiOutlinedInput-root:hover': {
                                '& > fieldset': {
                                  border: option.id === 'passport_image' ? '1px dash' : '',
                                  borderColor: inputValues[option.api_id]
                                    ? 'primary.main'
                                    : option.id === 'passport_image'
                                    ? preview
                                      ? 'primary.main'
                                      : 'text.disabled'
                                    : 'secondary.main',
                                },
                              },
                              // '& fieldset:hover': {top: 0, borderRadius: '12px', border: '1px solid red'},
                              '& .MuiOutlinedInput-root': {
                                padding: 0,
                                borderRadius: '12px',
                                overflow: 'hidden',
                                height: '100%',
                                bgcolor:
                                  option.id === 'passport_image'
                                    ? preview
                                      ? 'white'
                                      : 'secondary.main'
                                    : inputValues[option.api_id]
                                    ? 'white'
                                    : 'secondary.main',
                                // "& > fieldset": {
                                //     border: '1.5px solid green',
                                // },
                                // bgcolor: 'secondary.main',
                                // backgroundColor: '#ffffff',
                                '&.Mui-focused > fieldset': {
                                  // border: '1px solid',
                                  borderColor: 'primary.main',
                                },
                                '&:hover:before fieldset': {
                                  borderColor: 'primary.main',
                                },
                                color: 'text.primary',
                              },
                              '.MuiOutlinedInput-root :focus': {
                                backgroundColor:
                                  option.id === 'passport_image' ? (preview ? 'white' : 'secondary.main') : 'white',
                              },
                              '.MuiInputBase-input': {
                                padding: '14px',
                              },
                              '.MuiOutlinedInput-input': {
                                bgcolor:
                                  option.id === 'passport_image'
                                    ? preview
                                      ? 'white'
                                      : 'secondary.main'
                                    : inputValues[option.api_id]
                                    ? 'white'
                                    : 'secondary.main',
                              },
                              zIndex: 1,
                            }}
                            InputProps={{
                              readOnly: option.id === 'passport_image',
                              autoComplete: 'off',
                              startAdornment: option.id === 'passport_image' && preview && (
                                <InputAdornment
                                  position="end"
                                  sx={{
                                    minWidth: '36px',
                                    height: '40px',
                                    borderRadius: '11px',
                                  }}
                                >
                                  <Box
                                    component={'img'}
                                    src={preview}
                                    sx={{
                                      width: '36px',
                                      height: '40px',
                                      borderRadius: '11px',
                                      objectFit: 'fill',
                                    }}
                                  />
                                </InputAdornment>
                              ),
                              endAdornment: option.id === 'passport_image' && preview && (
                                <InputAdornment
                                  position="start"
                                  sx={{
                                    width: 'fit-content',
                                    height: '40px',
                                    backgroundColor: 'transparent',
                                    '&:hover': {
                                      cursor: 'pointer',
                                    },
                                  }}
                                  onClick={handleDeleteImage}
                                >
                                  <Box
                                    component={'img'}
                                    src={garbageIcon.src}
                                    sx={{
                                      width: '24px',
                                      height: '24px',
                                      objectFit: 'fill',
                                    }}
                                  />
                                </InputAdornment>
                              ),
                            }}
                          />

                          {Object.keys(activeTabApiErrorsState).includes(option.api_id) && (
                            <FormHelperText error={true}>{activeTabApiErrorsState[option.api_id]}</FormHelperText>
                          )}
                        </FormControl>
                        {option.id === 'shaba_number' ? (
                          <Chip
                            label="IR /"
                            sx={{
                              direction: 'rtl',
                              minWidth: 'fit-content',
                              flexGrow: 1,
                              height: '51px',
                              borderRadius: '12px',
                              color: 'text.secondary',
                              fontSize: '14px',
                              // fontWeight: '600',
                              fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                              bgcolor: 'secondary.main',
                              ml: '8px',
                            }}
                          />
                        ) : (
                          option.id === 'passport_image' &&
                          !preview && (
                            <Button
                              variant="contained"
                              component="label"
                              disableRipple
                              sx={{
                                width: 'auto',
                                minWidth: '45px',
                                px: '10px',
                                height: '51px',
                                flexGrow: '1',
                                borderRadius: '12px',
                                ml: '8px',
                                boxShadow: 'none',
                                '&:hover': {
                                  boxShadow: 'none',
                                },
                              }}
                            >
                              <Box
                                component={'img'}
                                src={attachmentIcon.src}
                                sx={{
                                  width: '24px',
                                  height: '24px',
                                }}
                              />
                              <input
                                hidden
                                type="file"
                                accept="image/png, image/bmp, image/jpeg"
                                onChange={handleGetImageFile}
                              />
                            </Button>
                          )
                        )}
                      </Box>
                    ) : option.hasSelect.includes('simple') ? (
                      <FormControl fullWidth>
                        <Select
                          // dir={'rtl'}
                          value={formik['values'][option.api_id]}
                          // value={option.id === 'passport_image' ? preview && imageFile && imageFile.name : formik["values"][option.api_id]}
                          onChange={(event) => {
                            formik.handleChange(option.api_id)(
                              JSON.parse(event.target.value)[option.api_id === 'bank_id' ? 'id' : 'name'].toString(),
                            );
                            handleChangeSelectValues(
                              event,
                              option.id === 'bank_name'
                                ? 'bank_id'
                                : option.id === 'card_type'
                                ? 'type'
                                : 'currency_type',
                            );
                          }}
                          error={formik['touched'][option.api_id] && Boolean(formik['errors'][option.api_id])}
                          // helperText={(formik["touched"][option.api_id]) && (formik["errors"][option.api_id])}
                          name={option.api_id}
                          // onChange={(event) => handleChangeSelectValues(event, option.id === 'bank_name' ? 'bank_id' : option.id === 'card_type' ? 'type' : 'currency_type')}
                          displayEmpty={true}
                          placeholder={option.placeholder}
                          IconComponent={selectNewIconSimple}
                          renderValue={(value: string) => {
                            return (
                              <Typography
                                variant="inherit"
                                component="span"
                                color={'text.secondary'}
                                sx={{
                                  fontSize: '14px',
                                  fontWeight: '400',
                                  opacity: (
                                    option.id === 'bank_name'
                                      ? !!bankNameState
                                      : option.id === 'card_type'
                                      ? !!cardTypeState
                                      : !!currencyTypeState
                                  )
                                    ? 1
                                    : '0.4',
                                }}
                              >
                                {option.id === 'bank_name' && !!bankNameState
                                  ? JSON.parse(bankNameState).name
                                  : option.id === 'card_type' && !!cardTypeState
                                  ? JSON.parse(cardTypeState).name
                                  : option.id === 'currency_type' && !!currencyTypeState
                                  ? JSON.parse(currencyTypeState).name
                                  : option.placeholder}
                              </Typography>
                            );
                          }}
                          MenuProps={{
                            anchorOrigin: {
                              vertical: 'bottom',
                              horizontal: 'left',
                            },
                            transformOrigin: {
                              vertical: 'top',
                              horizontal: 'left',
                            },
                            PaperProps: {
                              sx: {
                                mt: '8px',
                                boxShadow: '0px 4px 38px rgba(68, 68, 68, 0.1)',
                                maxHeight: '200px',
                                borderRadius: '10px',
                                overflowY: 'auto',
                              },
                            },
                          }}
                          sx={{
                            width: '100%',
                            mt: '8px',
                            bgcolor: (
                              option.id === 'bank_name'
                                ? bankNameState
                                : option.id === 'card_type'
                                ? cardTypeState
                                : currencyTypeState
                            )
                              ? 'white'
                              : 'secondary.main',
                            borderRadius: '12px',
                            '& .MuiSelect-icon': { top: '40%', mx: '5px' },
                            height: '100%',
                            '.MuiInputBase-input': {
                              py: '14px',
                            },
                            '& fieldset': {
                              border:
                                (option.id === 'bank_name'
                                  ? bankNameState
                                  : option.id === 'card_type'
                                  ? cardTypeState
                                  : currencyTypeState) || formik['touched'][option.api_id]
                                  ? '1px solid'
                                  : 'none',
                              borderColor: 'primary.main',
                            },
                          }}
                        >
                          {(option.id === 'bank_name'
                            ? activeBankAccounts
                            : option.id === 'card_type'
                            ? cardTypeList
                            : currencyTypeList
                          )?.map((item: any, index: number) => {
                            return (
                              <MenuItem
                                value={JSON.stringify(item)}
                                key={item.id}
                                sx={{
                                  width: '100%',
                                  px: '8px',
                                  my: '5px',
                                }}
                              >
                                <Typography
                                  variant="inherit"
                                  component="span"
                                  color={'text.secondary'}
                                  sx={{
                                    fontSize: '14px',
                                    fontWeight: '400',
                                  }}
                                >
                                  {option.id === 'bank_name'
                                    ? item.name
                                    : option.id === 'card_type'
                                    ? item.name
                                    : item.name}
                                </Typography>
                              </MenuItem>
                            );
                          })}
                        </Select>
                        {formik['touched'][option.api_id] && (
                          <FormHelperText error={true}>{formik['errors'][option.api_id] as ReactNode}</FormHelperText>
                        )}
                      </FormControl>
                    ) : (
                      <TextField
                        // inputRef={focusInput-index}
                        value={monetaryUnit?.title}
                        // onChange={}
                        // error={}
                        // helperText={}

                        // color={'primary.main'}
                        fullWidth
                        name={'variable'}
                        placeholder={option.placeholder}
                        type={'text'}
                        autoComplete="off"
                        sx={{
                          '& input::placeholder': {
                            fontSize: '14px',
                            color: 'text.secondary',
                          },
                          width: '100%',
                          height: '100%',
                          bgcolor: 'secondary.main',
                          borderRadius: '12px',
                          mt: '8px',
                          '& .MuiInputAdornment-root': {
                            ml: '8px',
                            height: '36px',
                            maxHeight: '40px',
                            backgroundColor: 'white',
                            borderRadius: '12px',
                            border: 'none',
                          },
                          '& legend': { display: 'none' },
                          '& fieldset': {
                            top: 0,
                            borderRadius: '12px',
                            borderColor: monetaryUnit ? 'primary.main' : 'secondary.main',
                          },
                          '& .MuiOutlinedInput-root:hover': {
                            '& > fieldset': {
                              borderColor: monetaryUnit ? 'primary.main' : 'secondary.main',
                            },
                          },
                          // '& fieldset:hover': {top: 0, borderRadius: '12px', border: '1px solid red'},
                          '& .MuiOutlinedInput-root': {
                            padding: 0,
                            borderRadius: '12px',
                            overflow: 'hidden',
                            height: '100%',
                            // "& > fieldset": {
                            //     border: '1.5px solid green',
                            // },
                            // bgcolor: 'secondary.main',
                            // backgroundColor: '#ffffff',
                            '&.Mui-focused > fieldset': {
                              // border: '1px solid',
                              borderColor: 'primary.main',
                            },
                            '&:hover:before fieldset': {
                              borderColor: 'primary.main',
                            },
                            color: 'text.primary',
                            // fontWeight: '600'
                            fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                          },
                          '.MuiOutlinedInput-root :focus': {
                            backgroundColor: 'white',
                          },
                          '.MuiInputBase-input': {
                            fontFamily: pageDirection === 'ltr' ? 'IRANSansX-Regular' : 'IRANSansXFaNum-Regular',
                            padding: '14px',
                          },
                          '.MuiOutlinedInput-input': {
                            bgcolor: monetaryUnit ? 'white' : 'secondary.main',
                          },
                          zIndex: 1,
                        }}
                        InputProps={{
                          autoComplete: 'off',
                          endAdornment: (
                            <InputAdornment position="start">
                              <Select
                                // dir={'rtl'}
                                value={monetaryUnit}
                                onChange={(event) => handleChangeSelectValues(event, 'currency_id')}
                                displayEmpty
                                IconComponent={coloredNewIcon}
                                renderValue={(value) => {
                                  return (
                                    <Box
                                      // dir={'ltr'}
                                      sx={{
                                        width: '100%',
                                        display: 'flex',
                                        alignItems: 'center',
                                        justifyContent: 'space-between',
                                      }}
                                    >
                                      <Typography
                                        variant="inherit"
                                        component="span"
                                        color={'text.secondary'}
                                        sx={{
                                          mt: '4px',
                                          fontSize: '12px',
                                          // fontWeight: '600',
                                          fontFamily:
                                            pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                                          mr: '8px',
                                        }}
                                      >
                                        {monetaryUnit?.iso_code}
                                      </Typography>
                                      <Box
                                        component={'img'}
                                        src={monetaryUnit?.image}
                                        // alt={`${value.title} icon`}
                                        sx={{
                                          width: '24px',
                                          height: '24px',
                                        }}
                                      />
                                    </Box>
                                  );
                                }}
                                MenuProps={{
                                  anchorOrigin: {
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                  },
                                  transformOrigin: {
                                    vertical: 'top',
                                    horizontal: 'left',
                                  },
                                  PaperProps: {
                                    sx: {
                                      mt: '8px',
                                      boxShadow: '0px 4px 38px rgba(68, 68, 68, 0.1)',
                                      width: '120px',
                                      height: 'auto',
                                      // maxHeight: '176px',
                                      borderRadius: '10px',
                                      overflowY: 'auto',
                                    },
                                  },
                                }}
                                sx={{
                                  // minWidth: '110px',
                                  '& .MuiSelect-icon': {
                                    top: '40%',
                                    mx: '5px',
                                  },
                                  height: '100%',
                                  // ml: '8px',
                                  '& .muirtl-bpeome-MuiSvgIcon-root-MuiSelect-icon': { left: 'auto' },
                                  '& .muirtl-hfutr2-MuiSvgIcon-root-MuiSelect-icon': { left: 'auto' },
                                  '& .muirtl-ittuaa-MuiInputAdornment-root': { margin: '0' },
                                  '& .muirtl-keuqm-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input.muirtl-keuqm-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input.muirtl-keuqm-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input':
                                    {
                                      // px: '12px',
                                      pl: '12px',
                                      pr: '35px',
                                      width: 'fit-content',
                                    },
                                  '& fieldset': { border: 'none' },
                                }}
                              >
                                {desiredCurrenciesListState?.map((item: CurrenciesListSample, index: number) => {
                                  return (
                                    <MenuItem
                                      value={JSON.stringify(item)}
                                      key={item.id}
                                      sx={{
                                        width: '100%',
                                        px: '8px',
                                        my: '5px',
                                      }}
                                    >
                                      <Box
                                        // dir={'ltr'}
                                        sx={{
                                          width: '100%',
                                          display: 'flex',
                                          alignItems: 'center',
                                          justifyContent: 'space-between',
                                        }}
                                      >
                                        <Box
                                          component={'img'}
                                          src={item.image}
                                          // alt={`${item.currency} icon`}
                                          sx={{
                                            width: '24px',
                                            height: '24px',
                                          }}
                                        />
                                        <Box
                                          sx={{
                                            width: 'auto',
                                            height: 'auto',
                                            flexGrow: 1,
                                            display: 'flex',
                                            alignItems: 'center',
                                            justifyContent: 'space-between',
                                            mx: '8px',
                                          }}
                                        >
                                          <Typography
                                            variant="inherit"
                                            component="span"
                                            color={'text.secondary'}
                                            sx={{
                                              fontSize: '12px',
                                              // fontWeight: '600',
                                              fontFamily:
                                                pageDirection === 'rtl'
                                                  ? 'IRANSansXFaNum-DemiBold'
                                                  : 'IRANSansX-DemiBold',
                                            }}
                                          >
                                            {item.iso_code}
                                          </Typography>
                                          <Typography
                                            variant="inherit"
                                            component="span"
                                            color={'text.secondary'}
                                            sx={{
                                              fontSize: '14px',
                                              fontWeight: '400',
                                            }}
                                          >
                                            {item.title}
                                          </Typography>
                                        </Box>
                                      </Box>
                                    </MenuItem>
                                  );
                                })}
                              </Select>
                            </InputAdornment>
                          ),
                        }}
                      />
                    )}
                  </Box>
                );
              })}
            </Box>
            <Box
              sx={{
                width: '100%',
                height: '2px',
                // position: 'absolute',
                // top: '205px',
                backgroundImage: 'linear-gradient(to right, #C3C3C3 45%, rgba(255,255,255,0) 0%)',
                backgroundPosition: 'bottom',
                backgroundSize: '20px 1px',
                backgroundRepeat: 'repeat-x',
              }}
            ></Box>

            <Box
              sx={{
                width: '100%',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <Button
                variant="contained"
                type={'submit'}
                disableRipple={true}
                sx={{
                  width: { xs: '236px', sm: '236px' },
                  height: { xs: '40px', sm: '40px' },
                  '&:hover': {
                    boxShadow: 'none',
                  },
                  boxShadow: 'none',
                  fontWeight: '500',
                  borderRadius: '12px',
                  fontSize: { xs: '12px', sm: '14px' },
                  mt: '32px',
                  mb: '13px',
                }}
              >
                {t('bank_accounts:entry')}
              </Button>
            </Box>
          </Box>

          <Box
            sx={{
              width: { xs: '100%', lg: '344px' },
              flexShrink: 0,
              height: 'auto',
              display: 'flex',
              gap: '16px',
              flexDirection: { xs: 'column', sm: 'row', lg: 'column' },
              alignItems: 'flex-start',
            }}
          >
            <Box
              sx={{
                width: '100%',
                height: 'auto',
                borderRadius: '22px',
                padding: '20px',
                bgcolor: 'white',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'flex-start',
              }}
            >
              <Box
                sx={{
                  width: 'auto',
                  display: 'flex',
                  alignItems: 'center',
                  mb: '16px',
                }}
              >
                <Box
                  component={'img'}
                  src={bulletPoint.src}
                  alt={'bullet-point'}
                  sx={{
                    width: '22px',
                    height: '18px',
                    mr: '9px',
                    transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                  }}
                />
                <Typography
                  variant="inherit"
                  component="span"
                  color={'text.primary'}
                  sx={{
                    // fontWeight: '600',
                    fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                    fontSize: { xs: '16px', xl: '16px' },
                  }}
                >
                  {t('bank_accounts:description')}
                </Typography>
              </Box>
              <Typography
                variant="inherit"
                component="span"
                color={'text.primary'}
                sx={{
                  fontWeight: '400',
                  fontSize: '17px',
                  lineHeight: '28px',
                  textAlign: 'justify',
                }}
              >
                {t('bank_accounts:description_text')}
              </Typography>
              <Divider
                orientation="horizontal"
                variant="fullWidth"
                flexItem
                sx={{
                  mt: '16px',
                  mb: '24px',
                  display: { xs: 'block', lg: 'none' },
                  borderColor: 'secondary.light',
                }}
              />
              <Box
                sx={{
                  width: '100%',
                  height: 'auto',
                  display: { xs: 'flex', sm: 'none' },
                  flexDirection: 'column',
                  alignItems: 'flex-start',
                }}
              >
                <Box
                  sx={{
                    width: 'auto',
                    display: 'flex',
                    alignItems: 'center',
                    mb: '16px',
                  }}
                >
                  <Box
                    component={'img'}
                    src={bulletPoint.src}
                    alt={'bullet-point'}
                    sx={{
                      width: '22px',
                      height: '18px',
                      mr: '9px',
                      transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                    }}
                  />
                  <Typography
                    variant="inherit"
                    component="span"
                    color={'text.primary'}
                    sx={{
                      // fontWeight: {xs: '600'},
                      fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                      fontSize: { xs: '16px', xl: '16px' },
                    }}
                  >
                    {t('bank_accounts:important_points')}
                  </Typography>
                </Box>

                {importantPoints.map((point) => {
                  return (
                    <Box
                      key={point.id}
                      sx={{
                        width: '100%',
                        display: 'flex',
                        alignItems: 'flex-start',
                        mb: '24px',
                      }}
                    >
                      <Box
                        sx={{
                          minWidth: '6px',
                          minHeight: '6px',
                          backgroundColor: '#EB5757',
                          borderRadius: '50%',
                          alignSelf: 'flex-start',
                          margin: '10px 12px 0 8px',
                        }}
                      ></Box>
                      <Typography
                        variant="inherit"
                        component="span"
                        color={'text.primary'}
                        sx={{
                          fontWeight: '400',
                          fontSize: '17px',
                          lineHeight: '28px',
                          textAlign: 'justify',
                        }}
                      >
                        {point.title}
                      </Typography>
                    </Box>
                  );
                })}
              </Box>
            </Box>

            <Box
              sx={{
                width: '100%',
                height: 'auto',
                borderRadius: '22px',
                padding: '20px',
                bgcolor: 'white',
                display: { xs: 'none', sm: 'flex' },
                flexDirection: 'column',
                alignItems: 'flex-start',
              }}
            >
              <Box
                sx={{
                  width: 'auto',
                  display: 'flex',
                  alignItems: 'center',
                  mb: '16px',
                }}
              >
                <Box
                  component={'img'}
                  src={bulletPoint.src}
                  alt={'bullet-point'}
                  sx={{
                    width: '22px',
                    height: '18px',
                    mr: '9px',
                    transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                  }}
                />
                <Typography
                  variant="inherit"
                  component="span"
                  color={'text.primary'}
                  sx={{
                    // fontWeight: {xs: '600'},
                    fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                    fontSize: { xs: '16px', xl: '16px' },
                  }}
                >
                  {t('bank_accounts:important_points')}
                </Typography>
              </Box>

              {importantPoints.map((point) => {
                return (
                  <Box
                    key={point.id}
                    sx={{
                      width: '100%',
                      display: 'flex',
                      alignItems: 'flex-start',
                      mb: '24px',
                    }}
                  >
                    <Box
                      sx={{
                        minWidth: '6px',
                        minHeight: '6px',
                        backgroundColor: '#EB5757',
                        borderRadius: '50%',
                        alignSelf: 'flex-start',
                        margin: '10px 12px 0 8px',
                      }}
                    ></Box>
                    <Typography
                      variant="inherit"
                      component="span"
                      color={'text.primary'}
                      sx={{
                        fontWeight: '400',
                        fontSize: '17px',
                        lineHeight: '28px',
                        textAlign: 'justify',
                      }}
                    >
                      {point.title}
                    </Typography>
                  </Box>
                );
              })}
            </Box>
          </Box>
        </Box>
      </Box>
      {openResultDialogBox && (
        <AlertDialogBox
          mode={'result'}
          data={operationResultState}
          open={openResultDialogBox}
          close={handleCloseResultDB}
        />
      )}
    </>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      hasLayout: true,
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default BankAccounts;
