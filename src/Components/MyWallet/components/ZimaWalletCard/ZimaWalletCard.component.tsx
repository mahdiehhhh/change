import React, { useEffect } from 'react';
import { Box, Typography } from '@mui/material';
import rawCard from '/public/assets/images/raw-card.svg';
import middleCircle from '/public/assets/images/raw-middle-circle.svg';
import bigCircle from '/public/assets/images/raw-big-circle.svg';
import littleCircle from '/public/assets/images/raw-little-circle.svg';
import cardFoldersIcon from '/public/assets/images/raw-folders-icon.svg';
import cardZimapayLogo from '/public/assets/images/raw-zimapay-logo.svg';
import { useAppSelector } from '../../../../store/hooks';
import { useTranslation } from 'next-i18next';
import { useI18nTrDir } from '../../../../utils/CustomHooks/i18nTrDir';

interface WalletSample {
  balance: number;
  benefit: number;
  code: string;
  colors: any;
  currency: string;
  iso_code: string;
  rial_balance: number;
  title: string;
  symbol: string;
  uid: string;
}

interface Props {
  data: WalletSample;
}

const ZimaWalletCard = ({ data }: Props) => {
  const { t, pageDirection } = useI18nTrDir();

  const { colors, balance, benefit, code, currency, iso_code, rial_balance, title, uid } = data;

  return (
    <Box
      sx={{
        width: { xs: '279px', sm: '303px' },
        height: { xs: '165px', sm: '180px' },
        borderRadius: '22px',
        overflow: 'hidden',
        position: 'relative',
      }}
    >
      <Box
        sx={{
          position: 'relative',
        }}
      >
        <svg
          id="untitled"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 303 180"
          shapeRendering="geometricPrecision"
          textRendering="geometricPrecision"
          width="303"
          height="180"
        >
          <rect
            id="untitled-s-rect1"
            style={{ isolation: 'isolate' }}
            width="303"
            height="180"
            rx="22"
            ry="22"
            transform="matrix(.999995 0 0 0.999998 0.000757 0.00018)"
            paintOrder="markers stroke fill"
            fill={colors.three}
            strokeWidth="0"
          />
        </svg>
      </Box>

      <Box
        sx={{
          width: 'fit-content',
          height: 'fit-content',
          position: 'absolute',
          zIndex: '2',
          top: '-14.29px',
          right: pageDirection === 'rtl' ? '-83px' : 'auto',
          left: pageDirection === 'rtl' ? 'auto' : '-83px',
        }}
      >
        <svg
          id="untitled"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 244 244"
          shapeRendering="geometricPrecision"
          textRendering="geometricPrecision"
          width="244"
          height="244"
        >
          <ellipse
            id="untitled-s-ellipse1"
            rx="122"
            ry="122"
            transform="translate(122 122)"
            fill={colors.one}
            strokeWidth="0"
          />
        </svg>
      </Box>

      <Box
        sx={{
          width: 'fit-content',
          height: 'fit-content',
          position: 'absolute',
          zIndex: '1',
          top: '-34.5px',
          right: pageDirection === 'rtl' ? '-77px' : 'auto',
          left: pageDirection === 'rtl' ? 'auto' : '-77px',
        }}
      >
        <svg
          id="untitled"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 282 282"
          shapeRendering="geometricPrecision"
          textRendering="geometricPrecision"
          width="282"
          height="282"
        >
          <ellipse
            id="untitled-s-ellipse1"
            rx="141"
            ry="141"
            transform="translate(141 141)"
            fill={colors.two}
            strokeWidth="0"
          />
        </svg>
      </Box>

      <Box
        sx={{
          width: 'fit-content',
          height: 'fit-content',
          position: 'absolute',
          zIndex: '3',
          top: '-115px',
          left: pageDirection === 'rtl' ? '-124px' : 'auto',
          right: pageDirection === 'rtl' ? 'auto' : '-124px',
        }}
      >
        <svg
          id="untitled"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 213 213"
          shapeRendering="geometricPrecision"
          textRendering="geometricPrecision"
          width="213"
          height="213"
        >
          <ellipse
            id="untitled-s-ellipse1"
            rx="106.5"
            ry="106.5"
            transform="translate(106.5 106.5)"
            fill={colors.one}
            strokeWidth="0"
          />
        </svg>
      </Box>

      <Box
        sx={{
          width: '200px',
          display: 'flex',
          flexDirection: pageDirection === 'rtl' ? 'row-reverse' : 'reverse',
          alignItems: 'center',
          position: 'absolute',
          top: { xs: '59px', sm: '65px' },
          right: pageDirection === 'rtl' ? '24px' : 'auto',
          left: pageDirection === 'rtl' ? 'auto' : '24px',
          zIndex: '3',
        }}
      >
        <Box
          component={'img'}
          src={cardFoldersIcon.src}
          sx={{
            width: '16px',
            height: '20px',
            ml: pageDirection === 'rtl' ? '16px' : '0',
            mr: pageDirection === 'rtl' ? '0' : '16px',
          }}
        />
        <Typography
          variant="inherit"
          component="span"
          color={'white'}
          fontFamily={pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Regular'}
          sx={{
            fontSize: '14px',
            fontWeight: '400',
          }}
        >
          {code}
        </Typography>
      </Box>

      <Typography
        variant="inherit"
        component="span"
        color={'white'}
        fontFamily={pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Regular'}
        sx={{
          fontSize: '20px',
          fontWeight: '400',
          position: 'absolute',
          top: { xs: '88px', sm: '94px' },
          right: pageDirection === 'rtl' ? '24px' : 'auto',
          left: pageDirection === 'rtl' ? 'auto' : '24px',
          zIndex: '3',
        }}
      >
        {iso_code !== 'IRR' && iso_code} {iso_code !== 'IRR' && Number(balance.toFixed(2)).toLocaleString('en-US')}
      </Typography>

      <Typography
        variant="inherit"
        component="span"
        color={'white'}
        fontFamily={pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Regular'}
        sx={{
          fontSize: iso_code === 'IRR' ? '20px' : '14px',
          fontWeight: '400',
          position: 'absolute',
          top: { xs: '120px', sm: '126px' },
          right: pageDirection === 'rtl' ? '24px' : 'auto',
          left: pageDirection === 'rtl' ? 'auto' : '24px',
          zIndex: '3',
        }}
      >
        IRRI {rial_balance.toLocaleString('en-US')}
      </Typography>

      <Box
        component={'img'}
        src={cardZimapayLogo.src}
        sx={{
          width: '24px',
          height: '23px',
          position: 'absolute',
          top: '16px',
          right: pageDirection === 'rtl' ? '24px' : 'auto',
          left: pageDirection === 'rtl' ? 'auto' : '24px',
          zIndex: '3',
        }}
      />
      <Typography
        variant="inherit"
        component="span"
        color={'white'}
        fontFamily={pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Regular'}
        sx={{
          fontSize: '16px',
          // fontWeight: '600',
          fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
          position: 'absolute',
          top: '16px',
          left: pageDirection === 'rtl' ? '24px' : 'auto',
          right: pageDirection === 'rtl' ? 'auto' : '24px',
          zIndex: '4',
        }}
      >
        {currency}
      </Typography>
    </Box>
  );
};

export { ZimaWalletCard };
