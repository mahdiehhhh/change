import React, { useRef, useEffect } from 'react';
import { Box, Button, Divider, IconButton } from '@mui/material';
import Typography from '@mui/material/Typography';
import dollarIcon from '/public/assets/images/dollar-icon.svg';
import decreaseIcon from '/public/assets/images/decrease-icon.svg';
import increaseIcon from '/public/assets/images/Increase-icon.svg';

import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import { useI18nTrDir } from '../../../../utils/CustomHooks/i18nTrDir';
import { localesNS } from '../../../../utils/Data/locales_NameSpace.utils';
import { PATHS } from '../../../../configs/routes.config';
import { useRouter } from 'next/router';

interface WalletSample {
  balance: number;
  benefit: number;
  code: string;
  colors: any;
  currency: string;
  iso_code: string;
  rial_balance: number;
  title: string;
  symbol: string;
  uid: string;
}

interface Props {
  data: WalletSample;
  handleOpenDialogBox: Function;
}

const WalletFeatures = ({ data, handleOpenDialogBox }: Props) => {
  const { colors, balance, benefit, code, currency, iso_code, rial_balance, title, uid, symbol } = data;

  const router = useRouter();
  const { t, pageDirection } = useI18nTrDir();

  const prevCountRef = useRef();

  return (
    <>
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          margin: '5px 15px',
        }}
      >
        <Typography
          variant="inherit"
          component="span"
          color={'text.primary'}
          sx={{
            fontWeight: '400',
            fontSize: '14px',
          }}
        >
          {t('my_wallet:Your_dollar_assets', { currency: currency })}
        </Typography>

        <Box
          sx={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}
        >
          <Box>
            {/*<Box*/}
            {/*    component={'img'}*/}
            {/*    src={symbol}*/}
            {/*    alt={'dollar-icon'}*/}
            {/*    sx={{*/}
            {/*        width: '6px',*/}
            {/*        height: '12px',*/}
            {/*        mr: '5px'*/}
            {/*    }}*/}
            {/*/>*/}
            <Typography
              variant="inherit"
              component="span"
              color={'text.primary'}
              // fontFamily={pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Bold'}
              sx={{
                // fontWeight: '600',
                fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                fontSize: '16px',
              }}
            >
              {symbol}{' '}
              {t(`my_wallet:currency_amount`, {
                amount:
                  currency === 'IRR'
                    ? Number(rial_balance).toLocaleString('en-US')
                    : Number(Number(balance).toFixed(2)).toLocaleString('en-US'),
                currency: currency,
              })}
            </Typography>
          </Box>

          <Box>
            {+benefit !== 0 && (
              <Box
                component={'img'}
                src={benefit > 0 ? increaseIcon.src : decreaseIcon.src}
                alt={benefit > 0 ? 'increaseIcon' : 'decrease-icon'}
                sx={{
                  width: '9px',
                  height: '5px',
                  mr: '10px',
                }}
              />
            )}

            <Typography
              dir={'ltr'}
              variant="inherit"
              component="span"
              color={benefit > 0 ? 'green' : benefit < 0 ? 'error.main' : 'text.primary'}
              // fontFamily={pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Bold'}
              sx={{
                // fontWeight: '600',
                fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                fontSize: '14px',
              }}
            >
              {currency === 'IRR'
                ? Number(benefit).toLocaleString('en-US')
                : Number(Number(benefit).toFixed(2)).toLocaleString('en-US')}
            </Typography>
          </Box>
        </Box>
      </Box>
      <Divider variant={'middle'} sx={{ borderColor: 'secondary.light' }} />
      <Box
        sx={{
          width: '100%',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          padding: '26px 15px',
        }}
      >
        <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
          <IconButton
            aria-label="deposit button"
            component="label"
            sx={{
              width: '32px',
              height: '32px',
              borderRadius: '6px',
              '&:hover': {
                bgcolor: 'secondary.main',
              },
              padding: '4px',
              mb: '4px',
            }}
            onClick={() => handleOpenDialogBox('deposit', data.uid)}
          >
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                opacity="0.4"
                d="M17.554 7.29639C20.005 7.29639 22 9.35618 22 11.8878V16.9201C22 19.4456 20.01 21.5002 17.564 21.5002L6.448 21.5002C3.996 21.5002 2 19.4414 2 16.9098V11.8775C2 9.35205 3.991 7.29639 6.438 7.29639H7.378L17.554 7.29639Z"
                fill={colors.one}
              />
              <path
                d="M12.5459 16.0374L15.4549 13.0695C15.7549 12.7627 15.7549 12.2691 15.4529 11.9634C15.1509 11.6587 14.6639 11.6597 14.3639 11.9654L12.7709 13.5905L12.7709 3.2821C12.7709 2.85042 12.4259 2.5 11.9999 2.5C11.5749 2.5 11.2309 2.85042 11.2309 3.2821L11.2309 13.5905L9.63694 11.9654C9.33694 11.6597 8.84994 11.6587 8.54794 11.9634C8.39693 12.1168 8.32093 12.3168 8.32093 12.518C8.32093 12.717 8.39694 12.9171 8.54594 13.0695L11.4549 16.0374C11.5999 16.1847 11.7959 16.268 11.9999 16.268C12.2049 16.268 12.4009 16.1847 12.5459 16.0374Z"
                fill={colors.one}
              />
            </svg>
          </IconButton>
          <Typography
            variant="inherit"
            component="span"
            color={'text.secondary'}
            // fontFamily={"IRANSansXFaNum-DemiBold"}
            sx={{
              // fontWeight: '600',
              fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
              fontSize: '12px',
            }}
          >
            {t('my_wallet:deposit_money')}
          </Typography>
        </Box>
        <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
          <IconButton
            aria-label="withdraw button"
            component="label"
            sx={{
              width: '32px',
              height: '32px',
              borderRadius: '6px',
              '&:hover': {
                bgcolor: 'secondary.main',
              },
              padding: '4px',
              mb: '4px',
            }}
            onClick={() => handleOpenDialogBox('withdraw', data.uid)}
          >
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                opacity="0.4"
                d="M6.447 22C3.996 22 2 19.9698 2 17.4755V12.5144C2 10.0252 3.99 8 6.437 8L17.553 8C20.005 8 22 10.0302 22 12.5256V17.4846C22 19.9748 20.01 22 17.563 22H16.623H6.447Z"
                fill={colors.one}
              />
              <path
                d="M11.4541 2.22128L8.54506 5.06706C8.24506 5.36119 8.24506 5.83451 8.54706 6.12766C8.84906 6.41984 9.33606 6.41886 9.63606 6.12571L11.2291 4.56647V6.06144V14.4517C11.2291 14.8657 11.5741 15.2017 11.9991 15.2017C12.4251 15.2017 12.7691 14.8657 12.7691 14.4517V4.56647L14.3621 6.12571C14.6621 6.41886 15.1491 6.41984 15.4511 6.12766C15.6021 5.9806 15.6781 5.78874 15.6781 5.5959C15.6781 5.40501 15.6021 5.21315 15.4531 5.06706L12.5451 2.22128C12.4001 2.08006 12.2041 2.0002 11.9991 2.0002C11.7951 2.0002 11.5991 2.08006 11.4541 2.22128Z"
                fill={colors.one}
              />
            </svg>
          </IconButton>
          <Typography
            variant="inherit"
            component="span"
            color={'text.secondary'}
            // fontFamily={"IRANSansXFaNum-DemiBold"}
            sx={{
              // fontWeight: '600',
              fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
              fontSize: '12px',
            }}
          >
            {t('my_wallet:withdrawal_money')}
          </Typography>
        </Box>

        <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
          <IconButton
            aria-label="transfer button"
            component="label"
            sx={{
              width: '32px',
              height: '32px',
              borderRadius: '6px',
              '&:hover': {
                bgcolor: 'secondary.main',
              },
              padding: '4px',
              mb: '4px',
            }}
            onClick={() => handleOpenDialogBox('transfer', data.uid)}
          >
            <svg width="21" height="22" viewBox="0 0 21 22" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M18.749 2.3636C18.3115 1.89519 17.664 1.72194 17.0602 1.90527L2.98149 6.16685C2.34449 6.35202 1.89299 6.88094 1.77137 7.55194C1.64712 8.23577 2.08112 9.10477 2.64812 9.46777L7.05024 12.2838C7.50174 12.5734 8.08449 12.501 8.45812 12.1087L13.499 6.82777C13.7527 6.55185 14.1727 6.55185 14.4265 6.82777C14.6802 7.09269 14.6802 7.52444 14.4265 7.79944L9.37687 13.0804C9.00237 13.4727 8.93237 14.0814 9.20799 14.5553L11.8977 19.1844C12.2127 19.7335 12.7552 20.0461 13.3502 20.0461C13.4202 20.0461 13.499 20.0461 13.569 20.036C14.2515 19.9453 14.794 19.4585 14.9952 18.771L19.169 4.13277C19.3527 3.50944 19.1865 2.8311 18.749 2.3636Z"
                fill={colors.one}
              />
              <path
                opacity="0.4"
                fillRule="evenodd"
                clipRule="evenodd"
                d="M2.63259 15.4075C2.46459 15.4075 2.29659 15.3406 2.16884 15.2058C1.91247 14.9372 1.91247 14.5027 2.16884 14.2342L3.36322 12.982C3.61959 12.7143 4.03522 12.7143 4.29159 12.982C4.54709 13.2506 4.54709 13.686 4.29159 13.9546L3.09634 15.2058C2.96859 15.3406 2.80059 15.4075 2.63259 15.4075ZM5.92364 16.5005C5.75564 16.5005 5.58764 16.4336 5.45989 16.2989C5.20352 16.0303 5.20352 15.5958 5.45989 15.3272L6.65427 14.075C6.91064 13.8074 7.32627 13.8074 7.58264 14.075C7.83814 14.3436 7.83814 14.779 7.58264 15.0476L6.38739 16.2989C6.25964 16.4336 6.09164 16.5005 5.92364 16.5005ZM6.14563 19.7712C6.27338 19.9059 6.44138 19.9729 6.60938 19.9729C6.77738 19.9729 6.94538 19.9059 7.07313 19.7712L8.26838 18.5199C8.52388 18.2514 8.52388 17.8159 8.26838 17.5474C8.01201 17.2797 7.59638 17.2797 7.34001 17.5474L6.14563 18.7995C5.88926 19.0681 5.88926 19.5026 6.14563 19.7712Z"
                fill={colors.one}
              />
            </svg>
          </IconButton>
          <Typography
            variant="inherit"
            component="span"
            color={'text.secondary'}
            // fontFamily={"IRANSansXFaNum-DemiBold"}
            sx={{
              // fontWeight: '600',
              fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
              fontSize: '12px',
            }}
          >
            {t('my_wallet:transfer')}
          </Typography>
        </Box>

        <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
          <IconButton
            aria-label="conversion button"
            component="label"
            sx={{
              width: '32px',
              height: '32px',
              borderRadius: '6px',
              '&:hover': {
                bgcolor: 'secondary.main',
              },
              padding: '4px',
              mb: '4px',
            }}
            onClick={() => router.push(`/${PATHS.USER_WALLET}/${data.uid}/${PATHS.TRANSFER_INTERNAL}`)}
          >
            <svg width="24" height="23" viewBox="0 0 24 23" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                opacity="0.4"
                d="M6.70702 12.3535C6.1909 12.3535 5.7731 12.7599 5.7731 13.2618L5.51562 17.6498C5.51562 18.2895 6.04929 18.8074 6.70702 18.8074C7.36474 18.8074 7.89724 18.2895 7.89724 17.6498L7.64094 13.2618C7.64094 12.7599 7.22313 12.3535 6.70702 12.3535Z"
                fill={colors.one}
              />
              <path
                d="M7.98037 3.52039C7.98037 3.52039 7.71236 3.25631 7.54618 3.14135C7.30509 2.96378 7.00783 2.875 6.71173 2.875C6.37936 2.875 6.07039 2.97517 5.81877 3.16412C5.77313 3.20851 5.57886 3.37583 5.41852 3.53177C4.41204 4.44351 2.76539 6.82358 2.26215 8.06997C2.18257 8.25892 2.01053 8.73698 2 8.99309C2 9.23667 2.05618 9.47115 2.17087 9.69311C2.3312 9.97084 2.58282 10.1939 2.88009 10.3157C3.08606 10.3943 3.70282 10.5161 3.71453 10.5161C4.38981 10.639 5.48757 10.705 6.70003 10.705C7.85514 10.705 8.90727 10.639 9.59308 10.5388C9.60478 10.5274 10.3702 10.4057 10.6335 10.2713C11.1133 10.0266 11.4118 9.54855 11.4118 9.03748V8.99309C11.4001 8.65958 11.1016 7.95842 11.0911 7.95842C10.5879 6.77919 9.02079 4.45489 7.98037 3.52039Z"
                fill={colors.one}
              />
              <path
                opacity="0.4"
                d="M17.293 10.6469C17.8091 10.6469 18.2269 10.2405 18.2269 9.73854L18.4832 5.35049C18.4832 4.71079 17.9507 4.19287 17.293 4.19287C16.6352 4.19287 16.1016 4.71079 16.1016 5.35049L16.359 9.73854C16.359 10.2405 16.7768 10.6469 17.293 10.6469Z"
                fill={colors.one}
              />
              <path
                d="M21.8307 13.3068C21.6704 13.0291 21.4188 12.8071 21.1215 12.6842C20.9155 12.6057 20.2976 12.4839 20.2871 12.4839C19.6118 12.3609 18.514 12.2949 17.3016 12.2949C16.1465 12.2949 15.0943 12.3609 14.4085 12.4611C14.3968 12.4725 13.6314 12.5954 13.3681 12.7286C12.8871 12.9733 12.5898 13.4514 12.5898 13.9636V14.008C12.6015 14.3415 12.8988 15.0416 12.9105 15.0416C13.4138 16.2208 14.9797 18.5463 16.0212 19.4797C16.0212 19.4797 16.2892 19.7438 16.4554 19.8576C16.6953 20.0363 16.9926 20.1251 17.291 20.1251C17.6222 20.1251 17.93 20.0249 18.1828 19.836C18.2285 19.7916 18.4228 19.6243 18.5831 19.4695C19.5884 18.5566 21.2362 16.1764 21.7383 14.9312C21.819 14.7422 21.9911 14.263 22.0016 14.008C22.0016 13.7633 21.9454 13.5288 21.8307 13.3068Z"
                fill={colors.one}
              />
            </svg>
          </IconButton>
          <Typography
            variant="inherit"
            component="span"
            color={'text.secondary'}
            // fontFamily={"IRANSansXFaNum-DemiBold"}
            sx={{
              // fontWeight: '600',
              fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
              fontSize: '12px',
            }}
          >
            {t('my_wallet:conversion_currency')}
          </Typography>
        </Box>
      </Box>

      <Button
        variant="contained"
        // type={'submit'}
        fullWidth
        disableRipple
        sx={{
          '&:hover': {
            boxShadow: 'none',
            color: 'white',
          },
          bgcolor: '#D1E7FF',
          color: 'text.primary',
          boxShadow: 'none',
          fontWeight: '400',
          borderRadius: '12px',
          height: '48px',
          fontSize: '14px',
        }}
        startIcon={
          <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M13.9061 1.6665H6.09654C3.37432 1.6665 1.66797 3.59324 1.66797 6.3208V13.6789C1.66797 16.4064 3.36638 18.3332 6.09654 18.3332H13.9061C16.6362 18.3332 18.3346 16.4064 18.3346 13.6789V6.3208C18.3346 3.59324 16.6362 1.6665 13.9061 1.6665Z"
              fill="#298FFE"
              stroke="#298FFE"
              strokeWidth="1.5"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
            <path
              d="M13.0564 9.99215H6.94531"
              stroke="white"
              strokeWidth="1.5"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
            <path
              d="M9.99839 6.93945V13.0447"
              stroke="white"
              strokeWidth="1.5"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
          </svg>
        }
      >
        {t('my_wallet:add_new_wallet')}
      </Button>
    </>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default WalletFeatures;
