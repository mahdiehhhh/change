import React from 'react';
import { Box, Button, Divider, Link } from '@mui/material';
import bulletPoint from '*.svg';
import Typography from '@mui/material/Typography';
import noTransactionIcon from '/public/assets/images/no-transaction.svg';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import { useI18nTrDir } from '../../../../utils/CustomHooks/i18nTrDir';
import { localesNS } from '../../../../utils/Data/locales_NameSpace.utils';

interface TransactionSample {
  action: string;
  amount: string;
  date: string;
  description: string;
  icon: string;
  id: string;
  symbol: string;
  title: string;
}

interface Props {
  data?: TransactionSample;
  noTransaction: boolean;
  currency?: string;
  loading: boolean;
}

const LatestTransactions = ({ data, noTransaction, currency, loading }: Props) => {
  const { t, pageDirection } = useI18nTrDir();

  return (
    <>
      {loading ? (
        <></>
      ) : !noTransaction ? (
        <>
          <Box
            sx={{
              width: '100%',
              display: 'flex',
              alignItems: 'flex-start',
              justifyContent: 'space-between',
            }}
          >
            <Box sx={{ width: '100%', display: 'flex', alignItems: 'flex-start' }}>
              {/*<Box*/}
              {/*    component={'img'}*/}
              {/*    src={icon}*/}
              {/*    alt={`${data?.title} icon`}*/}
              {/*    sx={{*/}
              {/*        width: '24px',*/}
              {/*        height: '24px',*/}
              {/*        mr: '8px'*/}
              {/*    }}*/}
              {/*/>*/}
              <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                <Typography
                  variant="inherit"
                  component="span"
                  color={'text.primary'}
                  sx={{
                    fontWeight: '400',
                    fontSize: { xs: '14px', sm: '16px' },
                  }}
                >
                  {data?.title}
                </Typography>
                <Typography
                  variant="inherit"
                  component="span"
                  color={'text.disabled'}
                  sx={{
                    fontWeight: '400',
                    fontSize: { xs: '12px', sm: '14px' },
                  }}
                >
                  {data?.description}
                </Typography>
              </Box>
            </Box>

            <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-end', ml: '10px' }}>
              <Typography
                variant="inherit"
                component="span"
                color={'text.primary'}
                fontFamily={pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Bold'}
                sx={{
                  fontWeight: '400',
                  fontSize: '16px',
                }}
              >
                {currency === 'IRR'
                  ? Number(data?.amount).toLocaleString('en-US')
                  : Number(Number(data?.amount).toFixed(2)).toLocaleString('en-US')}
              </Typography>
              <Typography
                variant="inherit"
                component="span"
                color={'text.disabled'}
                fontFamily={pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Regular'}
                sx={{
                  fontWeight: '400',
                  fontSize: { xs: '12px', sm: '14px' },
                }}
              >
                {data?.date}
              </Typography>
            </Box>
          </Box>

          {/*{*/}
          {/*    lastTransactions.slice(-4).length - 1 !== index &&*/}
          {/*    <Divider variant={'fullWidth'}*/}
          {/*             sx={{mt: '24px', mb: '18px', bgcolor: "secondary.light"}}/>*/}
          {/*}*/}
        </>
      ) : (
        <Box
          sx={{
            width: '100%',
            height: 'auto',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            mt: '30px',
          }}
        >
          <Box
            component={'img'}
            src={noTransactionIcon.src}
            alt={'spotify icon'}
            sx={{
              width: '123px',
              height: '120px',
              mt: '20px',
              mb: '16px',
            }}
          />
          <Typography
            variant="inherit"
            component="span"
            color={'text.primary'}
            sx={{
              fontWeight: '400',
              fontSize: '17px',
              textAlign: 'center',
            }}
          >
            {t('my_wallet:no_orders_recorded')}
          </Typography>
          <Typography
            variant="inherit"
            component="span"
            color={'text.secondary'}
            sx={{
              fontWeight: '400',
              fontSize: '14px',
              textAlign: 'center',
              mt: '8px',
              mb: '16px',
            }}
          >
            {t('my_wallet:register_your_new_record')}
          </Typography>
          <Button
            variant="contained"
            disableRipple={true}
            sx={{
              width: '236px',
              height: '40px',
              '&:hover': {
                boxShadow: 'none',
              },
              boxShadow: 'none',
              borderRadius: '12px',
              // fontWeight: '600',
              fontSize: '14px',
            }}
          >
            {t('my_wallet:set_order')}
          </Button>
        </Box>
      )}
    </>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default LatestTransactions;
