import React, { useEffect, useState } from 'react';
import { Box, Button, IconButton, Divider, Link, Skeleton } from '@mui/material';
import Typography from '@mui/material/Typography';
import TablePagination from '@mui/material/TablePagination';

import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import { ZimaWalletCard } from './components';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import NavigateBeforeIcon from '@mui/icons-material/NavigateBefore';
import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/effect-cards';

// import "./styles.css";

// import required modules
import { EffectCards } from 'swiper';

import decreaseIcon from '/public/assets/images/decrease-icon.svg';
import dollarIcon from '/public/assets/images/dollar-icon.svg';
import bulletPoint from '/public/assets/images/BulletPoint.svg';
import spotify from '/public/assets/images/Spotify-icon.svg';
import netflix from '/public/assets/images/Netflix-icon.svg';
import noTransaction from '/public/assets/images/no-transaction.svg';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { fetchUserActiveWallets } from '../../store/slices/user_active_wallets.slice';
import WalletFeatures from './components/WalletFeatures/WalletFeatures.component';
import { fetchWalletLatestTransaction } from '../../store/slices/wallet_latest_transaction.slice';
import LatestTransactions from './components/LatestTransactions/LatestTransactions.component';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import { DialogBox } from '../DialogBox/DialogBox.component';
import { useI18nTrDir } from '../../utils/CustomHooks/i18nTrDir';
import { localesNS } from '../../utils/Data/locales_NameSpace.utils';
import { PATHS } from '../../configs/routes.config';
import NextLink from 'next/link';

interface WalletSample {
  balance: number;
  benefit: number;
  code: string;
  colors: any;
  currency: string;
  iso_code: string;
  rial_balance: number;
  title: string;
  symbol: string;
  uid: string;
}

interface TransactionSample {
  action: string;
  amount: string;
  date: string;
  description: string;
  icon: string;
  id: string;
  symbol: string;
  title: string;
}

const MyWallet = () => {
  const { t, pageDirection } = useI18nTrDir();

  let number_of_cards = 0;
  const cardSlideHeight = 250;
  const [page, setPage] = React.useState(0);
  const [swiper, setSwiper] = React.useState<any>(null);

  const [walletReady, setWalletReady] = useState(false);
  const [transactionReady, setTransactionReady] = useState(false);

  const [activeWallet, setActiveWallet] = useState<WalletSample>({
    balance: 0,
    benefit: 0,
    code: '',
    colors: '',
    currency: '',
    iso_code: '',
    rial_balance: 0,
    title: '',
    symbol: '',
    uid: '',
  });
  const dispatch = useAppDispatch();
  const userActiveWalletResult = useAppSelector((state) => state.UserActiveWallets);
  const walletLatestTransResult = useAppSelector((state) => state.WalletLatestTransaction);

  const [dialogBox, setDialogBox] = React.useState({
    deposit: {
      display: false,
    },
    withdraw: {
      display: false,
    },
    transfer: {
      display: false,
    },
  });

  const handleOpenDialogBox = (dialogType: string) => {
    setDialogBox((prevState) => ({ ...prevState, [dialogType]: { display: true } }));
  };

  const handleCloseDialogBox = (dialogType: string) => {
    setDialogBox((prevState) => ({ ...prevState, [dialogType]: { display: false } }));
  };

  useEffect(() => {
    dispatch(fetchUserActiveWallets());
  }, []);

  useEffect(() => {
    if (
      !walletLatestTransResult.loading ||
      walletLatestTransResult.walletLatestTransResp ||
      walletLatestTransResult.error
    ) {
      setTransactionReady(true);
    } else {
      setTransactionReady(false);
    }
  }, [walletLatestTransResult]);

  useEffect(() => {
    console.log('jklsdflkjsdfsdfsdfwerw', transactionReady);
  }, [transactionReady]);

  useEffect(() => {
    if (!userActiveWalletResult.loading) {
      setWalletReady(true);
    } else {
      setWalletReady(false);
    }
  }, [userActiveWalletResult]);

  useEffect(() => {
    userActiveWalletResult?.userActWalletsResp?.length > 0 &&
      setActiveWallet(userActiveWalletResult?.userActWalletsResp[page]);
    userActiveWalletResult?.userActWalletsResp?.length > 0 &&
      dispatch(fetchWalletLatestTransaction(userActiveWalletResult?.userActWalletsResp[page].uid));
  }, [page, userActiveWalletResult]);

  const handleChangePage = (mode: string) => {
    number_of_cards = userActiveWalletResult.userActWalletsResp?.length;
    if (mode === 'next') {
      if (page + 1 >= number_of_cards) {
        swiper.slideTo(0);
        setPage(0);
      } else {
        swiper.slideTo(page + 1);
        setPage(page + 1);
      }
    } else {
      if (page - 1 < 0) {
        swiper.slideTo(number_of_cards - 1);
        setPage(number_of_cards - 1);
      } else {
        swiper.slideTo(page - 1);
        setPage(page - 1);
      }
    }
  };

  return (
    <>
      <Box
        sx={{
          minWidth: { sm: '351px', xs: '100%' },
          height: { xs: 'auto', sm: 'auto' },
          borderRadius: '22px',
          padding: { xs: '10px 12px', lg: '10px 24px' },
          backgroundColor: 'white',
          mr: '24px',
          mt: { xs: '24px', sm: 0 },
          flex: '0 0 auto',
        }}
      >
        {!walletReady ? (
          <Box
            sx={{
              width: '100%',
              display: 'flex',
              flexDirection: 'column',
            }}
          >
            <Box
              sx={{
                width: '100%',
                height: 'auto',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
                mb: '15px',
              }}
            >
              <Skeleton
                animation="wave"
                sx={{
                  width: '60%',
                  height: '30px',
                  borderRadius: '12px',
                }}
              />
            </Box>
            <Skeleton
              animation="wave"
              variant="rectangular"
              sx={{
                width: '100%',
                height: '180px',
                borderRadius: '22px',
                mt: '30px',
              }}
            />

            <Box
              sx={{
                width: '100%',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'flex-start',
                mt: '40px',
              }}
            >
              <Skeleton
                animation="wave"
                sx={{
                  width: '40%',
                  height: '30px',
                  borderRadius: '12px',
                }}
              />
              <Skeleton
                animation="wave"
                sx={{
                  width: '30%',
                  height: '30px',
                  borderRadius: '12px',
                }}
              />
              <Box
                sx={{
                  width: '100%',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  my: '22px',
                }}
              >
                {Array.from(Array(4).keys()).map((item, index) => (
                  <Box
                    key={index}
                    sx={{
                      width: '32px',
                      display: 'flex',
                      flexDirection: 'column',
                    }}
                  >
                    <Skeleton
                      animation="wave"
                      variant="rectangular"
                      sx={{
                        width: '32px',
                        height: '32px',
                        borderRadius: '6px',
                        mb: '8px',
                      }}
                    />
                    <Skeleton
                      animation="wave"
                      sx={{
                        width: '100%',
                        height: '20px',
                        borderRadius: '8px',
                      }}
                    />
                  </Box>
                ))}
              </Box>
              <Skeleton
                animation="wave"
                variant="rectangular"
                sx={{
                  width: '100%',
                  height: '40px',
                  borderRadius: '20px',
                }}
              />
            </Box>
          </Box>
        ) : (
          <Box
            sx={{
              width: '100%',
              display: 'flex',
              flexDirection: 'column',
            }}
          >
            <Box
              sx={{
                width: '100%',
                height: 'auto',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
                mb: '15px',
              }}
            >
              <Box
                sx={{
                  width: 'auto',
                  height: 'auto',
                  display: 'flex',
                  alignItems: 'center',
                }}
              >
                <Box
                  component={'img'}
                  src={bulletPoint.src}
                  alt={'bullet-point'}
                  sx={{
                    width: '22px',
                    height: '18px',
                    mr: '9px',
                    transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                  }}
                />
                <Typography
                  variant="inherit"
                  component="span"
                  color={'text.primary'}
                  sx={{
                    // fontWeight: {xs: '600'},
                    fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                    fontSize: { xs: '16px', xl: '16px' },
                  }}
                >
                  {t('my_wallet:my_wallets')}
                </Typography>
              </Box>

              <Box
                sx={{
                  width: '60px',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}
              >
                <IconButton
                  aria-label="next button"
                  component="label"
                  sx={{
                    width: '25px',
                    height: '25px',
                    bgcolor: 'secondary.main',
                  }}
                  onClick={() => handleChangePage('next')}
                >
                  <NavigateNextIcon
                    fontSize={'small'}
                    sx={{
                      transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                    }}
                  />
                </IconButton>
                <IconButton
                  aria-label="next button"
                  component="label"
                  sx={{
                    width: '25px',
                    height: '25px',
                    bgcolor: 'secondary.main',
                  }}
                  onClick={() => handleChangePage('prev')}
                >
                  <NavigateBeforeIcon
                    fontSize={'small'}
                    sx={{
                      transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                    }}
                  />
                </IconButton>
              </Box>
            </Box>

            <Swiper
              effect={'cards'}
              cardsEffect={{ rotate: false, slideShadows: false }}
              grabCursor={true}
              initialSlide={page - 1}
              modules={[EffectCards]}
              className="mySwiperCard"
              direction={'vertical'}
              loop={false}
              onSlideChange={(slide) => setPage(slide.activeIndex)}
              onSwiper={(s: any) => {
                setSwiper(s);
              }}
            >
              {userActiveWalletResult.userActWalletsResp?.map((wallet: WalletSample, index: number) => {
                return (
                  <SwiperSlide key={index} className={'swiper-slide-card'}>
                    <ZimaWalletCard data={wallet} />
                  </SwiperSlide>
                );
              })}
            </Swiper>

            {dialogBox.deposit.display && (
              <DialogBox
                operation={'deposit'}
                open={dialogBox.deposit.display}
                close={handleCloseDialogBox}
                activeUid={activeWallet.uid}
              />
            )}

            {dialogBox.withdraw.display && (
              <DialogBox
                operation={'withdraw'}
                open={dialogBox.withdraw.display}
                close={handleCloseDialogBox}
                activeUid={activeWallet.uid}
              />
            )}

            {dialogBox.transfer.display && (
              <DialogBox
                operation={'transfer'}
                open={dialogBox.transfer.display}
                close={handleCloseDialogBox}
                activeUid={activeWallet.uid}
              />
            )}

            <WalletFeatures data={activeWallet} handleOpenDialogBox={handleOpenDialogBox} />
          </Box>
        )}

        <Divider variant={'fullWidth'} sx={{ mt: '32px', mb: '18px', borderColor: 'secondary.light' }} />

        {!transactionReady ? (
          <Box
            sx={{
              width: '100%',
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              pb: { xs: '22px', sm: '0' },
            }}
          >
            <Box
              sx={{
                width: '100%',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}
            >
              <Skeleton
                animation="wave"
                sx={{
                  width: '40%',
                  height: '30px',
                  borderRadius: '12px',
                }}
              />
            </Box>

            <Box
              sx={{
                width: '100%',
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                mt: '40px',
                gap: '50px',
              }}
            >
              {Array.from(Array(4).keys()).map((item, index) => (
                <Box
                  key={index}
                  sx={{
                    width: '100%',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}
                >
                  <Box
                    sx={{
                      display: 'flex',
                      alignItems: 'center',
                    }}
                  >
                    <Skeleton
                      animation="wave"
                      variant="rectangular"
                      sx={{
                        width: '32px',
                        height: '32px',
                        borderRadius: '6px',
                        mr: '11px',
                      }}
                    />
                    <Box
                      sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'flex-start',
                      }}
                    >
                      <Skeleton
                        animation="wave"
                        sx={{
                          width: '113px',
                          height: '13px',
                          borderRadius: '12px',
                        }}
                      />
                      <Skeleton
                        animation="wave"
                        sx={{
                          width: '68px',
                          height: '13px',
                          borderRadius: '12px',
                        }}
                      />
                    </Box>
                  </Box>
                  <Box
                    sx={{
                      display: 'flex',
                      flexDirection: 'column',
                      alignItems: 'flex-end',
                    }}
                  >
                    <Skeleton
                      animation="wave"
                      sx={{
                        width: '55px',
                        height: '13px',
                        borderRadius: '12px',
                      }}
                    />
                    <Skeleton
                      animation="wave"
                      sx={{
                        width: '68px',
                        height: '13px',
                        borderRadius: '12px',
                      }}
                    />
                  </Box>
                </Box>
              ))}
            </Box>
          </Box>
        ) : (
          <Box
            sx={{
              width: '100%',
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              pb: { xs: '22px', sm: '0' },
            }}
          >
            <Box
              sx={{
                width: '100%',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}
            >
              <Box sx={{ display: 'flex', alignItems: 'center' }}>
                <Box
                  component={'img'}
                  src={bulletPoint.src}
                  alt={'bullet point'}
                  sx={{
                    width: '22px',
                    height: '18px',
                    mr: '9px',
                    transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                  }}
                />
                <Typography
                  variant="inherit"
                  component="span"
                  color={'text.primary'}
                  sx={{
                    // fontWeight: {xs: '600'},
                    fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                    fontSize: { xs: '16px', xl: '16px' },
                  }}
                >
                  {t('my_wallet:recent_transaction')}
                </Typography>
              </Box>

              {!!walletLatestTransResult?.walletLatestTransResp?.length && (
                <NextLink href={`https://my.zimapay.com/wallets/transactions`} passHref>
                  <Link
                    href="#"
                    underline="none"
                    color={'text.secondary'}
                    sx={{ fontSize: { xs: '12px', sm: '14px' }, fontWeight: '400' }}
                  >
                    {t('my_wallet:see_all')}
                  </Link>
                </NextLink>
              )}
            </Box>

            <Box
              sx={{
                width: '100%',
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                mt: '20px',
              }}
            >
              {walletLatestTransResult?.walletLatestTransResp?.map((transaction: TransactionSample, index: number) => {
                return (
                  <>
                    <LatestTransactions
                      key={index}
                      data={transaction}
                      noTransaction={false}
                      loading={walletLatestTransResult?.loading}
                      currency={
                        userActiveWalletResult?.userActWalletsResp?.length > 0 &&
                        userActiveWalletResult?.userActWalletsResp[page].iso_code
                      }
                    />
                    {walletLatestTransResult?.walletLatestTransResp?.slice(-4).length - 1 !== index && (
                      <Divider
                        variant={'fullWidth'}
                        sx={{
                          mt: '24px',
                          mb: '18px',
                          borderColor: 'secondary.light',
                        }}
                      />
                    )}
                  </>
                );
              })}
            </Box>
          </Box>
        )}
      </Box>
    </>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export { MyWallet };
