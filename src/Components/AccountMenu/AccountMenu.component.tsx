import * as React from 'react';
import Box from '@mui/material/Box';
import Avatar from '@mui/material/Avatar';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Tooltip from '@mui/material/Tooltip';
import PersonAdd from '@mui/icons-material/PersonAdd';
import Settings from '@mui/icons-material/Settings';
import Logout from '@mui/icons-material/Logout';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import profPic from 'assets/images/profpic.jpg';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import { Button, Link } from '@mui/material';
import profile from '/public/assets/images/Profile.svg';
import bankAccount from '/public/assets/images/BankAccount.svg';
import setting from '/public/assets/images/Setting.svg';
import logout from '/public/assets/images/Logout.svg';
import { useAppDispatch, useAppSelector } from 'store/hooks';
import { useEffect } from 'react';
import { fetchUserProfile } from 'store/slices/user_profile.slice';
import { useI18nTrDir } from '../../utils/CustomHooks/i18nTrDir';
import { eraseCookie, handleRemoveTokens } from '../../utils/functions/local';
import { IS_LOGGED_IN } from '../../configs/variables.config';
import { router } from 'next/client';
import { PATHS } from '../../configs/routes.config';
import { useRouter } from 'next/router';
import { handleSetLogOutEvent } from '../../utils/functions/general';
import NextLink from 'next/link';
import { localesNS } from '../../utils/Data/locales_NameSpace.utils';

const AccountMenu = () => {
  const { t, pageDirection } = useI18nTrDir();
  const router = useRouter();
  const { pathname, asPath, query } = router;

  const dispatch = useAppDispatch();
  const userProfileResult = useAppSelector((state) => state.UserProfile);

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: any) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  useEffect(() => {
    dispatch(fetchUserProfile());
  }, []);

  const menuOptions = [
    {
      id: 'profile',
      icon: profile,
      title: t('account_menu:user_info'),
      link: '/',
    },
    {
      id: 'bank-account',
      icon: bankAccount,
      title: t('account_menu:user_bank_account_info'),
      link: `/${PATHS.USER_PROFILE}/${PATHS.BANK_ACCOUNTS}`,
    },
    {
      id: 'setting',
      icon: setting,
      title: t('account_menu:setting'),
      link: `/${PATHS.USER_SETTINGS}`,
    },
    {
      id: 'logout',
      icon: logout,
      title: t('account_menu:log_out'),
      link: `/${PATHS.USER_LOGIN}`,
    },
  ];

  const handleClickAccountMenuOptions = (id: string) => {
    if (id === 'logout') {
      // handleRemoveTokens(IS_LOGGED_IN)
      eraseCookie(IS_LOGGED_IN);
      // typeof window !== 'undefined' && router.push(`/${PATHS.USER_LOGIN}`)
      handleSetLogOutEvent();
    }
  };

  return (
    <React.Fragment>
      <Box sx={{ display: 'flex', alignItems: 'center', textAlign: 'center' }}>
        <Tooltip title={t('account_menu:user_panel')}>
          <IconButton
            disableRipple={true}
            onClick={(event) => handleClick(event)}
            size="small"
            sx={{
              pr: 0,
              ml: 2,
              '&:hover': {
                '.muirtl-i4bv87-MuiSvgIcon-root': {
                  borderRadius: '50px',
                  backgroundColor: 'rgba(0,0,0,0.1)',
                },
              },
            }}
            aria-controls={open ? 'account-menu' : undefined}
            aria-haspopup="true"
            aria-expanded={open ? 'true' : undefined}
          >
            <Avatar
              alt={userProfileResult?.userProfileResp?.name}
              src={userProfileResult?.userProfileResp?.avatar}
              sx={{ width: { xs: '25px', sm: '40px' }, height: { xs: '25px', sm: '40px' } }}
            />

            <KeyboardArrowDownIcon fontSize={'medium'} />
          </IconButton>
        </Tooltip>
      </Box>
      <Menu
        anchorEl={anchorEl}
        id="account-menu"
        open={open}
        onClose={handleClose}
        onClick={handleClose}
        PaperProps={{
          // elevation: 0,
          sx: {
            boxShadow: '0px 4px 38px rgba(68, 68, 68, 0.1)',

            width: '300px',
            height: 'auto',
            padding: '8px 16px',
            overflow: 'visible',
            overflowX: 'hidden',
            borderRadius: '24px',
            // filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
            mt: 1.5,
            '& .MuiAvatar-root': {
              width: 32,
              height: 32,
              ml: -0.5,
              mr: 1,
            },
            // '&:before': {
            //     content: '""',
            //     display: 'block',
            //     position: 'absolute',
            //     top: 0,
            //     right: 28,
            //     width: 10,
            //     height: 10,
            //     bgcolor: 'background.paper',
            //     transform: 'translateY(-50%) rotate(45deg)',
            //     zIndex: 0,
            // },
          },
        }}
        transformOrigin={{ horizontal: 'left', vertical: 'top' }}
        anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
      >
        <Box
          sx={{
            display: 'flex',
            alignItems: 'flex-start',
            justifyContent: 'space-between',
          }}
        >
          <Box
            sx={{
              width: 'fit-content',
              display: 'flex',
              alignItems: 'flex-start',
            }}
          >
            <Avatar
              alt={userProfileResult?.userProfileResp?.name}
              src={userProfileResult?.userProfileResp?.avatar}
              sx={{ width: 40, height: 40 }}
            />

            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'flex-start',
              }}
            >
              <Typography
                variant="inherit"
                component="span"
                color={'text.primary'}
                sx={{
                  fontWeight: '400',
                  fontSize: '16px',
                }}
              >
                {userProfileResult?.userProfileResp?.name}
              </Typography>
              <Typography
                variant="inherit"
                component="span"
                color={'text.disabled'}
                sx={{
                  fontWeight: '400',
                  fontSize: '14px',
                }}
              >
                {userProfileResult?.userProfileResp?.phone}
              </Typography>
            </Box>
          </Box>

          <Button
            variant="contained"
            disableRipple={true}
            sx={{
              padding: '7px 10px',
              minWidth: '85px',
              height: '32px',
              '&:hover': {
                bgcolor: 'error.main',
                color: 'white',
                boxShadow: 'none',
              },
              boxShadow: 'none',
              borderRadius: '12px',
              // fontWeight: '600',
              fontSize: pageDirection === 'rtl' ? '12px' : '8px',
              bgcolor: 'error.light',
              color: 'error.main',
            }}
          >
            {t('account_menu:not_verified')}
          </Button>
        </Box>
        <Divider variant={'fullWidth'} sx={{ my: '10px', borderColor: 'secondary.light' }} />

        {menuOptions.map((option, index) => {
          return (
            <NextLink
              key={option.id}
              href={option.link}
              passHref
              // href={option.href ? option.href : pathname} as={asPath}
            >
              <Link href="#" underline="none">
                <MenuItem
                  sx={{
                    padding: '10px',
                    borderRadius: '11px',
                  }}
                  onClick={() => handleClickAccountMenuOptions(option.id)}
                >
                  <Box
                    sx={{
                      display: 'flex',
                      alignItems: 'center',
                    }}
                  >
                    <Box
                      component={'img'}
                      src={option.icon.src}
                      alt={'profile'}
                      sx={{
                        width: 'fit-content',
                        height: 'fit-content',
                        mr: '15px',
                      }}
                    />
                    <Typography
                      variant="inherit"
                      component="span"
                      color={+menuOptions.length - 1 === +index ? 'error.main' : 'text.secondary'}
                      sx={{
                        fontWeight: '400',
                        fontSize: '14px',
                      }}
                    >
                      {option.title}
                    </Typography>
                  </Box>
                </MenuItem>
                {+menuOptions.length - 2 === +index && (
                  <Divider variant={'fullWidth'} sx={{ my: '10px', borderColor: 'secondary.light' }} />
                )}
              </Link>
            </NextLink>
          );
        })}
      </Menu>
    </React.Fragment>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export { AccountMenu };
