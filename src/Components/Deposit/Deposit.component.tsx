import React, { ReactNode, useEffect, useState } from 'react';
import {
  Box,
  InputAdornment,
  Link,
  MenuItem,
  Select,
  SelectChangeEvent,
  TextField,
  Divider,
  Chip,
  Avatar,
  Button,
} from '@mui/material';
import Typography from '@mui/material/Typography';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import bulletPoint from '/public/assets/images/BulletPoint.svg';
import IconButton from '@mui/material/IconButton';
import backwardArrowDeposit from '/public/assets/images/backwardArrowDeposit.svg';
import dollarWalletIcon from '/public/assets/images/dollar-wallet-icon.svg';
import selectDropdownIcon from '/public/assets/images/select-dropdown-icon.svg';
import { Visibility, VisibilityOff } from '@mui/icons-material';
import RemoveIcon from '@mui/icons-material/Remove';
import ClearIcon from '@mui/icons-material/Clear';
import DragHandleIcon from '@mui/icons-material/DragHandle';
import { useRouter } from 'next/router';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { fetchWalletDepositRules } from '../../store/slices/wallet_deposit_rules.slice';
import { fetchWalletDepositCash } from '../../store/slices/wallet_deposit_cash_rules.slice';
import dangerCircleIcon from '/public/assets/images/Danger-Circle-icon.svg';
import dangerCircleGreenIcon from '/public/assets/images/Danger-Circle-Green-icon.svg';
import rectRialIcon from '/public/assets/images/Rect-rial-icon.svg';
import melliBankLogo from '/public/assets/images/Melli_Bank_Logo.svg';
import { calcCurrencyConversionCosts, calcZimaPayWage } from 'utils/functions/calc_currency_conversion';
import { fetchUserActiveWallets } from '../../store/slices/user_active_wallets.slice';
import { PATHS } from '../../configs/routes.config';
import Tooltip from '@mui/material/Tooltip';
import { fetchWalletWithdrawRules } from '../../store/slices/wallet_withdraw_rules.slice';
import { fetchWalletWithdrawCash } from '../../store/slices/wallet_withdraw_cash_rules.slice';
import { fetchUserBankAccounts } from '../../store/slices/user_bank_accounts.slice';
import { useI18nTrDir } from '../../utils/CustomHooks/i18nTrDir';
import { fetchWalletDepositAmount } from '../../store/slices/wallet_deposit_amount.slice';
import { fetchWalletDepositCashAmount } from '../../store/slices/wallet_deposit_cash_amount.slice';
import { fetchWalletWithdrawCashAmount } from '../../store/slices/wallet_withdraw_cash_amount.slice';
import { fetchWalletWithdrawAmount } from '../../store/slices/wallet_withdraw_amount.slice';
import AlertDialogBox from '../AlertDialogBox/AlertDialogBox.component';
import { localesNS } from '../../utils/Data/locales_NameSpace.utils';

interface Props {
  operationType: string;
}

interface RulesSample {
  action_rules: {
    min_amount: string;
    max_amount: string;
    tax: number;
    zimapay_fee: number;
    exchange_rate: number;
  };
  wallet_information: {
    title: string;
    currency: string;
    iso_code: string;
    balance: number;
    logo: string;
    color: {
      one: string;
      two: string;
      five: string;
      four: string;
      three: string;
    };
  };
}

interface WalletSample {
  balance: number;
  benefit: number;
  code: string;
  colors: any;
  currency: string;
  iso_code: string;
  rial_balance: number;
  title: string;
  symbol: string;
  uid: string;
  currency_logo: string;
}

interface BankAccountSample {
  id: number;
  title: string;
  ban: string;
  accountable_type: string;
  code: string;
  card_number: string;
  is_active: boolean;
  status: string;
  bank_name: string;
  bank_logo: any;
  created_at: string;
}

const Deposit = ({ operationType }: Props) => {
  const { t, pageDirection } = useI18nTrDir();

  const router = useRouter();
  const UrlPath = router.pathname;

  const [openResultDialogBox, setOpenResultDialogBox] = React.useState(false);

  const handleOpenResultDB = () => {
    setOpenResultDialogBox(true);
  };

  const handleCloseResultDB = () => {
    setOpenResultDialogBox(false);
  };

  const [rulesState, setRulesState] = useState<RulesSample>({
    action_rules: {
      min_amount: '',
      max_amount: '',
      tax: 0,
      zimapay_fee: 0,
      exchange_rate: 0,
    },
    wallet_information: {
      title: '',
      currency: '',
      iso_code: '',
      balance: 0,
      logo: '',
      color: {
        one: '',
        two: '',
        five: '',
        four: '',
        three: '',
      },
    },
  });

  const [currency, setCurrency] = useState({
    uid: '',
    title: '',
    code: '',
    currency: '',
    currency_logo: '',
    iso_code: '',
    symbol: '',
    colors: {
      one: '',
      two: '',
      five: '',
      four: '',
      three: '',
    },
    balance: 0,
    rial_balance: 0,
    benefit: 0,
  });

  const [finalCurrency, setFinalCurrency] = useState({
    uid: '',
    title: '',
    code: '',
    currency: '',
    currency_logo: '',
    iso_code: '',
    symbol: '',
    colors: {
      one: '',
      two: '',
      five: '',
      four: '',
      three: '',
    },
    balance: 0,
    rial_balance: 0,
    benefit: 0,
  });

  const [userActiveWalletsState, setUserActiveWalletsState] = useState<WalletSample[]>([]);
  const [amountWarning, setAmountWarning] = useState(false);
  const [invalidAmount, setInvalidAmount] = useState(false);
  const [isNotFiftyMultiple, setIsNotFiftyMultiple] = useState(false);
  const [operationResultState, setOperationResultState] = useState<object>({
    status: '',
    messages: {},
  });

  const [activeCreditCard, setActiveCreditCard] = useState({
    id: 0,
    title: '',
    ban: '',
    accountable_type: '',
    code: '',
    card_number: '',
    is_active: true,
    status: '',
    bank_name: '',
    bank_logo: '',
    created_at: '',
  });
  // const [bankCardNumber, setBankCardNumber] = useState('6038941565452285')
  const [activeBankAccountsState, setActiveBankAccountsState] = useState([]);

  const dispatch = useAppDispatch();
  const walletDepositRulesResult = useAppSelector((state) => state.WalletDepositRules);
  const walletWithdrawRulesResult = useAppSelector((state) => state.WalletWithdrawRules);
  const walletDepositCashResult = useAppSelector((state) => state.WalletDepositCash);
  const walletWithdrawCashResult = useAppSelector((state) => state.WalletWithdrawCash);
  const userActiveWalletResult = useAppSelector((state) => state.UserActiveWallets);
  const userBankAccountsResult = useAppSelector((state) => state.UserBankAccounts);
  const walletDepositAmountResult = useAppSelector((state) => state.WalletDepositAmount);
  const walletDepositCashAmountResult = useAppSelector((state) => state.WalletDepositCashAmount);
  const walletWithdrawAmountResult = useAppSelector((state) => state.WalletWithdrawAmount);
  const walletWithdrawCashAmountResult = useAppSelector((state) => state.WalletWithdrawCashAmount);

  const wallet_uid: any = router.query.wallet_uid;
  const isDeposit = router.pathname.includes('deposit');
  const isWithdraw = router.pathname.includes('withdraw');
  const isRial = router.pathname.includes('rial');
  const isCash = router.pathname.includes('cash');

  useEffect(() => {
    dispatch(fetchUserActiveWallets());
  }, []);

  useEffect(() => {
    isDeposit
      ? isRial
        ? dispatch(fetchWalletDepositRules(wallet_uid))
        : dispatch(fetchWalletDepositCash(wallet_uid))
      : isRial
      ? (dispatch(fetchWalletWithdrawRules(wallet_uid)), dispatch(fetchUserBankAccounts('iran')))
      : dispatch(fetchWalletWithdrawCash(wallet_uid));
  }, [wallet_uid]);

  useEffect(() => {
    if (userActiveWalletResult?.userActWalletsResp) {
      setUserActiveWalletsState(userActiveWalletResult?.userActWalletsResp);
    }
  }, [userActiveWalletResult]);

  useEffect(() => {
    if (userActiveWalletsState.length) {
      // @ts-ignore
      const selectedWalletUid = userActiveWalletsState.find((item) => item.iso_code === currency.iso_code).uid;
      typeof window !== 'undefined' &&
        router.push(
          `/${PATHS.USER_WALLET}/${selectedWalletUid}/${
            isDeposit
              ? isRial
                ? PATHS.RIAL_DEPOSIT
                : PATHS.CASH_DEPOSIT
              : isRial
              ? PATHS.RIAL_WITHDRAW
              : PATHS.CASH_WITHDRAW
          }`,
        );
    }
  }, [currency]);

  useEffect(() => {
    if (isDeposit) {
      if (isRial && walletDepositRulesResult?.walletDepositRulesResp) {
        setRulesState(walletDepositRulesResult?.walletDepositRulesResp);
      } else if (isCash && walletDepositCashResult?.walletDepositCashResp) {
        setRulesState(walletDepositCashResult?.walletDepositCashResp);
      }
    } else {
      if (isRial && walletWithdrawRulesResult?.walletWithdrawRulesResp) {
        setRulesState(walletWithdrawRulesResult?.walletWithdrawRulesResp);
      } else if (isCash && walletWithdrawCashResult?.walletWithdrawCashResp) {
        setRulesState(walletWithdrawCashResult?.walletWithdrawCashResp);
      }
    }
  }, [walletDepositRulesResult, walletDepositCashResult, walletWithdrawRulesResult, walletWithdrawCashResult]);

  useEffect(() => {
    const activeBankAccountsArr = userBankAccountsResult?.userBankAccountsResp?.bank_accounts_list?.filter(
      (bank: any) => bank.is_active,
    );
    setActiveBankAccountsState(activeBankAccountsArr);
  }, [userBankAccountsResult]);

  useEffect(() => {
    activeBankAccountsState && setActiveCreditCard(activeBankAccountsState[0]);
  }, [activeBankAccountsState]);

  // const [initialValue, setInitialValue] = useState('')
  // const [finalValue, setFinalValue] = useState('')
  // const [descriptionValue, setDescriptionValue] = useState('')

  useEffect(() => {
    if (UrlPath.includes(PATHS.CASH_DEPOSIT)) {
      !!walletDepositCashAmountResult.walletDepositCashAmountResp?.message && setOpenResultDialogBox(true);
      walletDepositCashAmountResult.error
        ? setOperationResultState({
            status: 'error',
            messages: {},
          })
        : setOperationResultState({
            status: 'success',
            messages: walletDepositCashAmountResult.walletDepositCashAmountResp?.message,
          });
      console.log('deposit form data1', walletDepositCashAmountResult);
    }
    if (UrlPath.includes(PATHS.RIAL_DEPOSIT)) {
      if (!!walletDepositAmountResult.walletDepositAmountResp && !walletDepositAmountResult.error) {
        typeof window !== 'undefined' &&
          router.push(`${walletDepositAmountResult.walletDepositAmountResp?.bank_form_data?.action}`);
      }

      console.log('deposit form data2', walletDepositAmountResult);
    }
    if (UrlPath.includes(PATHS.CASH_WITHDRAW)) {
      !!walletWithdrawCashAmountResult.walletWithdrawCashAmountResp?.message && setOpenResultDialogBox(true);
      walletWithdrawCashAmountResult.error
        ? setOperationResultState({
            status: 'error',
            messages: {},
          })
        : setOperationResultState({
            status: 'success',
            messages: walletWithdrawCashAmountResult.walletWithdrawCashAmountResp?.message,
          });
      console.log('deposit form data3', walletWithdrawCashAmountResult);
    }
    if (UrlPath.includes(PATHS.RIAL_WITHDRAW)) {
      !!walletWithdrawAmountResult.walletWithdrawAmountResp?.message && setOpenResultDialogBox(true);
      walletWithdrawAmountResult.error
        ? setOperationResultState({
            status: 'error',
            messages: {},
          })
        : setOperationResultState({
            status: 'success',
            messages: walletWithdrawAmountResult.walletWithdrawAmountResp?.message,
          });
      console.log('deposit form data4', walletWithdrawAmountResult);
    }
  }, [
    walletDepositAmountResult,
    walletDepositCashAmountResult,
    walletWithdrawAmountResult,
    walletWithdrawCashAmountResult,
  ]);

  const [inputsValueState, setInputsValueState] = useState({
    initialValue: '',
    finalValue: '',
    descriptionValue: '',
  });

  const BankCardFormatCreator = (cardNumber: string | number) => {
    // return cardNumber.toString().replace(/\d{4}(?=.)/g, '$& ');
    return cardNumber?.toString().replace(/(.{4})/g, '$1 ');
  };

  useEffect(() => {
    if (userActiveWalletsState && rulesState?.wallet_information.iso_code) {
      const selectedCurrency = userActiveWalletsState.find(
        (wallet) => wallet.iso_code === rulesState?.wallet_information.iso_code,
      );
      selectedCurrency && setCurrency(selectedCurrency);
      selectedCurrency && setFinalCurrency(selectedCurrency);
    }
  }, [userActiveWalletsState, rulesState]);

  const calcFinalAmountProcedure = isRial
    ? [
        {
          icon: <RemoveIcon fontSize={'small'} />,
          title: t('deposit:tax'),
          amount:
            inputsValueState.initialValue || inputsValueState.finalValue
              ? `%${rulesState.action_rules.tax}`
              : '...................',
          margin_top: '38px',
        },
        {
          icon: <ClearIcon fontSize={'small'} />,
          title: t('deposit:currency_exchange_rate'),
          amount: Number(rulesState.action_rules.exchange_rate).toLocaleString('en-US'),
          margin_top: '32px',
        },
        {
          icon: <RemoveIcon fontSize={'small'} />,
          title: t('deposit:zimapay_wage', { wage: rulesState.action_rules.zimapay_fee }),
          amount:
            inputsValueState.initialValue || inputsValueState.finalValue
              ? calcZimaPayWage(
                  +inputsValueState.initialValue.replace(/,(?=\d{3})/g, ''),
                  rulesState.action_rules.zimapay_fee,
                )
              : '...................',
          margin_top: '32px',
        },
        {
          icon: <DragHandleIcon fontSize={'small'} />,
          title: t('deposit:final_amount'),
          amount: '',
          margin_top: '65px',
        },
      ]
    : [
        {
          icon: <RemoveIcon fontSize={'small'} />,
          title: t('deposit:tax'),
          amount:
            inputsValueState.initialValue || inputsValueState.finalValue
              ? `%${rulesState.action_rules.tax}`
              : '...................',
          margin_top: '57px',
        },
        {
          icon: <RemoveIcon fontSize={'small'} />,
          title: t('deposit:zimapay_wage', { wage: rulesState.action_rules.zimapay_fee }),
          amount:
            inputsValueState.initialValue || inputsValueState.finalValue
              ? calcZimaPayWage(
                  +inputsValueState.initialValue.replace(/,(?=\d{3})/g, ''),
                  rulesState.action_rules.zimapay_fee,
                )
              : '...................',
          margin_top: '57px',
        },
        {
          icon: <DragHandleIcon fontSize={'small'} />,
          title: t('deposit:final_amount'),
          amount: '',
          margin_top: '75px',
        },
      ];

  const NewIcon = (props: any) => (
    <svg {...props} width="9" height="9" viewBox="0 0 9 9" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M3.65175 7.85617C3.60825 7.81177 3.42225 7.64414 3.26925 7.488C2.307 6.57254 0.732 4.1844 0.25125 2.93445C0.174 2.74462 0.0105 2.2647 0 2.00828C0 1.76258 0.054 1.52836 0.1635 1.30485C0.3165 1.02623 0.55725 0.802727 0.8415 0.680258C1.03875 0.601419 1.629 0.47895 1.6395 0.47895C2.28525 0.356481 3.3345 0.289124 4.494 0.289124C5.59875 0.289124 6.60525 0.356481 7.26075 0.456753C7.27125 0.468234 8.00475 0.590703 8.256 0.724653C8.715 0.970357 9 1.45028 9 1.96389V2.00828C8.98875 2.34277 8.70375 3.0462 8.69325 3.0462C8.21175 4.22879 6.714 6.56183 5.71875 7.49948C5.71875 7.49948 5.463 7.76355 5.30325 7.87837C5.07375 8.05748 4.7895 8.14627 4.50525 8.14627C4.188 8.14627 3.8925 8.04599 3.65175 7.85617Z"
        fill="#298FFE"
      />
    </svg>
  );

  const CreditNewIcon = (props: any) => (
    <svg {...props} width="16" height="9" viewBox="0 0 16 9" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M15 1L8 8L1 1" stroke="#298FFE" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
    </svg>
  );

  const handleChangeCurrency = (event: SelectChangeEvent<any>, child: ReactNode): void => {
    setCurrency(JSON.parse(event.target.value));
    setFinalCurrency(JSON.parse(event.target.value));
  };

  const handleChangeFinalCurrency = (event: SelectChangeEvent<any>, child: ReactNode) => {
    setFinalCurrency(JSON.parse(event.target.value));
    setCurrency(JSON.parse(event.target.value));
  };

  const handleCheckInputValue = (event: any, inputValue: string) => {
    if (inputValue === 'initialValue') {
      const number_reg = /^[0-9]*\.?[0-9]*$/;
      if (event.target.value === '' || number_reg.test(event.target.value)) {
        setInputsValueState((prevState) => ({
          ...prevState,
          [inputValue]: event.target.value,
          finalValue: calcCurrencyConversionCosts(
            isDeposit ? 'deposit' : 'withdraw',
            isRial ? 'rial' : 'cash',
            +rulesState.action_rules.zimapay_fee,
            +rulesState.action_rules.tax,
            +rulesState.action_rules.exchange_rate,
            +event.target.value > 0 ? +event.target.value : 0,
            '',
          ),
        }));
      }
    } else if (inputValue === 'finalValue') {
      const number_reg = /^[0-9\b]+$/;
      if (event.target.value === '' || number_reg.test(event.target.value)) {
        setInputsValueState((prevState) => ({
          ...prevState,
          [inputValue]: event.target.value,
          initialValue: calcCurrencyConversionCosts(
            isDeposit ? 'deposit' : 'withdraw',
            isRial ? 'rial' : 'cash',
            +rulesState.action_rules.zimapay_fee,
            +rulesState.action_rules.tax,
            +rulesState.action_rules.exchange_rate,
            '',
            +event.target.value > 0 ? +event.target.value : 0,
          ),
        }));
      }
    } else {
      setInputsValueState((prevState) => ({ ...prevState, [inputValue]: event.target.value }));
    }
  };

  const handleCheckAmount = () => {
    if (isWithdraw && isCash) {
      +inputsValueState.initialValue % 50 !== 0 ? setIsNotFiftyMultiple(true) : setIsNotFiftyMultiple(false);
    }

    if (
      +inputsValueState.initialValue.replace(/,(?=\d{3})/g, '') < +rulesState.action_rules.min_amount ||
      +inputsValueState.initialValue.replace(/,(?=\d{3})/g, '') > +rulesState.action_rules.max_amount
    ) {
      setInvalidAmount(true);
      setAmountWarning(true);
    } else {
      setInvalidAmount(false);
      setAmountWarning(false);
    }
  };

  useEffect(() => {
    handleCheckAmount();
  }, [inputsValueState.initialValue]);

  const handleChangeCreditCard = (event: SelectChangeEvent<any>) => {
    setActiveCreditCard(JSON.parse(event.target.value));
    // setBankCardNumber(JSON.parse(event.target.value).card_number)
  };

  const handleSubmitOperationType = (event: any) => {
    event.preventDefault();
    const form = new FormData(event.currentTarget);
    let data = Object.fromEntries(form);
    if (!invalidAmount) {
      data['amount'] = inputsValueState.initialValue.replace(/,(?=\d{3})/g, '');
      if (UrlPath.includes(PATHS.CASH_DEPOSIT)) {
        dispatch(fetchWalletDepositCashAmount(wallet_uid, data));
      }
      if (UrlPath.includes(PATHS.RIAL_DEPOSIT)) {
        console.log('what is deposit rial', data);
        dispatch(fetchWalletDepositAmount(wallet_uid, data));
      }
      if (UrlPath.includes(PATHS.CASH_WITHDRAW)) {
        dispatch(fetchWalletWithdrawCashAmount(wallet_uid, data));
      }
      if (UrlPath.includes(PATHS.RIAL_WITHDRAW)) {
        data['bank_account_id'] = activeCreditCard?.id.toString();
        dispatch(fetchWalletWithdrawAmount(wallet_uid, data));
      }
    }
  };

  return (
    <>
      <Box
        sx={{
          width: '100%',
          display: 'flex',
          flexDirection: { xs: 'column-reverse', sm: 'row' },
          alignItems: { xs: 'center', sm: 'flex-start' },
        }}
      >
        <Box
          sx={{
            flex: '1',
          }}
        ></Box>
        <Box
          sx={{
            width: { xs: '327px', sm: '475px' },
            height: 'auto',
            backgroundColor: 'white',
            borderRadius: '22px',
            padding: '24px',
          }}
        >
          <Box
            sx={{
              width: '100%',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'space-between',
              mb: '40px',
            }}
          >
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <Box
                component={'img'}
                src={bulletPoint.src}
                alt={'bullet point'}
                sx={{
                  width: '22px',
                  height: '18px',
                  mr: '9px',
                  transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                }}
              />
              <Typography
                variant="inherit"
                component="span"
                color={'text.primary'}
                sx={{
                  // fontWeight: {xs: '600'},
                  fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                  fontSize: { xs: '16px', xl: '16px' },
                }}
              >
                {isDeposit ? t('deposit:deposit') : t('deposit:withdraw')} {t(`deposit:${operationType}`)}
              </Typography>
            </Box>
            <IconButton
              color="inherit"
              aria-label="prev button"
              edge="start"
              onClick={() => router.push(`/${PATHS.USER_DASHBOARD}`)}
              sx={{
                width: '30px',
                height: '30px',
                display: 'flex',
                justifySelf: 'flex-end',
                padding: '0',
                backgroundColor: '#D9D9D9',
              }}
            >
              <Box
                component={'img'}
                src={backwardArrowDeposit.src}
                alt={'backward icon'}
                sx={{
                  width: '7px',
                  height: '14px',
                  transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                }}
              />
            </IconButton>
          </Box>
          <Box
            component={'form'}
            sx={{
              width: '100%',
              height: 'auto',
              textAlign: 'center',
            }}
            onSubmit={handleSubmitOperationType}
          >
            <Box
              sx={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}
            >
              <Box
                sx={{
                  display: 'flex',
                  alignItems: 'center',
                }}
              >
                <Box
                  component={'img'}
                  src={rulesState.wallet_information.logo}
                  // alt={'backward icon'}
                  sx={{
                    width: '50px',
                    height: '30px',
                    mr: '12px',
                  }}
                />
                <Typography
                  variant="inherit"
                  component="span"
                  color={rulesState.wallet_information.color.two}
                  sx={{
                    // fontWeight: '600',
                    fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                    fontSize: { xs: '14px', sm: '16px' },
                  }}
                >
                  {t('deposit:wallet_balance', { wallet: rulesState.wallet_information.currency })}
                </Typography>
              </Box>
              <Typography
                variant="inherit"
                component="span"
                color={'text.primary'}
                fontFamily={pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Bold'}
                sx={{
                  // fontWeight: '600',
                  fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                  fontSize: { xs: '16px', sm: '24px' },
                }}
              >
                {Number(Number(rulesState.wallet_information.balance).toFixed(2)).toLocaleString('en-US')}
              </Typography>
            </Box>

            <Box
              sx={{
                width: '100%',
                height: 'auto',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'flex-start',
                mt: '40px',
              }}
            >
              <Box
                sx={{
                  width: 'fit-content',
                  height: 'auto',
                  display: 'flex',
                  alignItems: 'center',
                }}
              >
                <Typography
                  variant="inherit"
                  component="span"
                  color={'text.primary'}
                  sx={{
                    fontSize: '17px',
                    fontWeight: '400',
                    lineHeight: '24px',
                  }}
                  // onClick={() => focusInput.current?.focus()}
                >
                  {t('deposit:amount')} {isDeposit ? t('deposit:deposit') : t('deposit:withdraw')}
                </Typography>

                <Tooltip
                  // title={`حداقل واریز ${Math.trunc(+rulesState.action_rules.min_amount).toLocaleString('en-US')} ${rulesState.wallet_information.currency} و حداکثر ${Math.trunc(+rulesState.action_rules.max_amount).toLocaleString('en-US')} میباشد.`}
                  title={t('deposit:min_max_warning', {
                    min: Math.trunc(+rulesState.action_rules.min_amount).toLocaleString('en-US'),
                    max: Math.trunc(+rulesState.action_rules.max_amount).toLocaleString('en-US'),
                    currency: rulesState.wallet_information.currency,
                  })}
                  placement={pageDirection === 'rtl' ? 'left' : 'right'}
                  arrow
                  // disableInteractive={true}
                  open={amountWarning}
                  PopperProps={{
                    sx: {
                      width: { xs: '200px', sm: 'auto' },
                      '& .MuiTooltip-tooltip': {
                        padding: '8px 12px',
                        borderRadius: '16px',
                        border: '1px solid',
                        borderColor: 'error.main',
                        bgcolor: 'error.light',
                        marginLeft: '12px',
                        color: 'text.primary',
                        fontSize: { xs: '10px', sm: '14px' },
                        fontWeight: '400',
                      },
                      '& .MuiTooltip-arrow': {
                        color: 'error.light',
                        // bgcolor: "blue",
                        right: 'auto',
                        left: { xs: '-7px', sm: '-10px' },
                        '&::before': {
                          // borderLeft: '20px solid transparent',
                          // borderRight: '20px solid transparent',
                          // borderBottom: 'calc(2 * 20px * 0.866) solid green',
                          // borderTop: '20px solid transparent',
                          // display: 'inline-block',

                          width: { xs: '7px', sm: '11px' },
                          height: { xs: '7px', sm: '11px' },

                          // backgroundColor: "blue",
                          border: '1px solid',
                          borderColor: 'error.main',
                          overflow: 'hidden',
                        },
                      },
                    },
                  }}
                >
                  <Box
                    component={'img'}
                    src={dangerCircleIcon.src}
                    // alt={'backward icon'}
                    sx={{
                      width: '18.5px',
                      height: '18.5px',
                      ml: '11px',
                    }}
                    onClick={() => setAmountWarning((prevState) => !prevState)}
                  />
                </Tooltip>
              </Box>
              <TextField
                // inputRef={focusInput-index}
                value={inputsValueState.initialValue}
                // onChange={}
                // error={}
                // helperText={}

                // color={'primary.main'}
                fullWidth
                name={`amount`}
                placeholder={t('deposit:enter_amount')}
                type={'tel'}
                autoComplete="off"
                onChange={(event) => {
                  handleCheckInputValue(event, 'initialValue');
                  // handleCheckAmount(event)
                }}
                // onBlur={(event) => handleCheckAmount(event)}
                sx={{
                  '& input::placeholder': {
                    fontSize: '14px',
                    color: 'text.secondary',
                  },
                  width: '100%',
                  height: '100%',
                  bgcolor: 'secondary.main',
                  borderRadius: '12px',
                  mt: '8px',
                  '& .MuiInputAdornment-root': {
                    ml: '8px',
                    height: '36px',
                    maxHeight: '40px',
                    backgroundColor: 'white',
                    borderRadius: '12px',
                    border: 'none',
                  },
                  '& legend': { display: 'none' },
                  '& fieldset': {
                    top: 0,
                    borderRadius: '12px',
                    borderColor: inputsValueState.initialValue ? 'primary.main' : 'secondary.main',
                  },
                  '& .MuiOutlinedInput-root:hover': {
                    '& > fieldset': {
                      borderColor: inputsValueState.initialValue ? 'primary.main' : 'secondary.main',
                    },
                  },
                  // '& fieldset:hover': {top: 0, borderRadius: '12px', border: '1px solid red'},
                  '& .MuiOutlinedInput-root': {
                    padding: 0,
                    borderRadius: '12px',
                    overflow: 'hidden',
                    height: '100%',
                    // "& > fieldset": {
                    //     border: '1.5px solid green',
                    // },
                    // bgcolor: 'secondary.main',
                    // backgroundColor: '#ffffff',
                    '&.Mui-focused > fieldset': {
                      // border: '1px solid',
                      borderColor: 'primary.main',
                    },
                    '&:hover:before fieldset': {
                      borderColor: 'primary.main',
                    },
                    color: 'primary.main',
                  },
                  '.MuiOutlinedInput-root :focus': {
                    backgroundColor: 'white',
                  },
                  '.MuiInputBase-input': {
                    fontFamily: pageDirection === 'ltr' ? 'IRANSansX-Regular' : 'IRANSansXFaNum-Regular',
                    padding: '14px',
                  },
                  '.MuiOutlinedInput-input': {
                    bgcolor: inputsValueState.initialValue ? 'white' : 'secondary.main',
                  },
                  zIndex: 1,
                }}
                InputProps={{
                  autoComplete: 'off',
                  endAdornment: (
                    <InputAdornment position="start">
                      <Select
                        // dir={'rtl'}
                        value={currency}
                        onChange={handleChangeCurrency}
                        displayEmpty
                        IconComponent={NewIcon}
                        renderValue={(value) => {
                          return (
                            value.uid && (
                              <Box
                                // dir={'ltr'}
                                sx={{
                                  width: '100%',
                                  display: 'flex',
                                  alignItems: 'center',
                                  justifyContent: 'space-between',
                                }}
                              >
                                <Typography
                                  variant="inherit"
                                  component="span"
                                  color={'text.secondary'}
                                  sx={{
                                    mt: '4px',
                                    fontSize: '12px',
                                    // fontWeight: '600',
                                    fontFamily:
                                      pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                                    mr: '8px',
                                  }}
                                >
                                  {value.iso_code}
                                </Typography>
                                <Box
                                  component={'img'}
                                  src={value.currency_logo}
                                  // alt={`${value.title} icon`}
                                  sx={{
                                    width: '24px',
                                    height: '24px',
                                  }}
                                />
                              </Box>
                            )
                          );
                        }}
                        MenuProps={{
                          anchorOrigin: {
                            vertical: 'bottom',
                            horizontal: 'left',
                          },
                          transformOrigin: {
                            vertical: 'top',
                            horizontal: 'left',
                          },
                          PaperProps: {
                            sx: {
                              mt: '8px',
                              boxShadow: '0px 4px 38px rgba(68, 68, 68, 0.1)',
                              width: '120px',
                              height: 'auto',
                              // maxHeight: '176px',
                              borderRadius: '10px',
                              overflowY: 'auto',
                            },
                          },
                        }}
                        sx={{
                          // minWidth: '110px',
                          '& .MuiSelect-icon': { top: '40%', mx: '5px' },
                          height: '100%',
                          // ml: '8px',
                          '& .muirtl-bpeome-MuiSvgIcon-root-MuiSelect-icon': { left: 'auto' },
                          '& .muirtl-hfutr2-MuiSvgIcon-root-MuiSelect-icon': { left: 'auto' },
                          '& .muirtl-ittuaa-MuiInputAdornment-root': { margin: '0' },
                          '& .muirtl-keuqm-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input.muirtl-keuqm-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input.muirtl-keuqm-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input':
                            {
                              // px: '12px',
                              pl: '12px',
                              pr: '35px',
                              width: 'fit-content',
                            },
                          '& fieldset': { border: 'none' },
                        }}
                      >
                        {userActiveWalletsState?.map((item: WalletSample, index: number) => {
                          return (
                            <MenuItem
                              value={JSON.stringify(item)}
                              key={index}
                              sx={{
                                width: '100%',
                                px: '8px',
                                my: '5px',
                              }}
                            >
                              <Box
                                // dir={'ltr'}
                                sx={{
                                  width: '100%',
                                  display: 'flex',
                                  alignItems: 'center',
                                  justifyContent: 'space-between',
                                }}
                              >
                                <Box
                                  component={'img'}
                                  src={item.currency_logo}
                                  // alt={`${item.currency} icon`}
                                  sx={{
                                    width: '24px',
                                    height: '24px',
                                  }}
                                />
                                <Box
                                  sx={{
                                    width: 'auto',
                                    height: 'auto',
                                    flexGrow: 1,
                                    display: 'flex',
                                    alignItems: 'center',
                                    justifyContent: 'space-between',
                                    mx: '8px',
                                  }}
                                >
                                  <Typography
                                    variant="inherit"
                                    component="span"
                                    color={'text.secondary'}
                                    sx={{
                                      fontSize: '12px',
                                      // fontWeight: '600',
                                      fontFamily:
                                        pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                                    }}
                                  >
                                    {item.iso_code}
                                  </Typography>
                                  <Typography
                                    variant="inherit"
                                    component="span"
                                    color={'text.secondary'}
                                    sx={{
                                      fontSize: '14px',
                                      fontWeight: '400',
                                    }}
                                  >
                                    {item.currency}
                                  </Typography>
                                </Box>
                              </Box>
                            </MenuItem>
                          );
                        })}
                      </Select>
                    </InputAdornment>
                  ),
                }}
              />
            </Box>

            <Box
              sx={{
                width: '100%',
                height: 'auto',
                // bgcolor: 'yellow',
                position: 'relative',
              }}
            >
              <Box
                sx={{
                  // bgcolor: 'blue',
                  width: '100%',
                  height: 'auto',
                  display: 'flex',
                  justifyContent: 'flex-end',
                  position: 'absolute',
                  top: '205px',
                }}
              >
                <Box
                  sx={{
                    width: '90%',
                    height: '2px',
                    // position: 'absolute',
                    // top: '205px',
                    backgroundImage: 'linear-gradient(to right, #C3C3C3 45%, rgba(255,255,255,0) 0%)',
                    backgroundPosition: 'bottom',
                    backgroundSize: '20px 1px',
                    backgroundRepeat: 'repeat-x',
                  }}
                >
                  {/*<Divider*/}
                  {/*    variant={'middle'}*/}
                  {/*    orientation={'horizontal'}*/}
                  {/*    sx={{*/}
                  {/*        borderBottomWidth: '1px',*/}
                  {/*        borderColor: 'text.disabled',*/}
                  {/*        borderStyle: 'dashed'*/}
                  {/*    }}*/}
                  {/*/>*/}
                </Box>
              </Box>
              <Box
                sx={{
                  width: '100%',
                  height: 'auto',
                  position: 'absolute',
                  top: 0,
                }}
              >
                {calcFinalAmountProcedure.map((item, index) => {
                  return (
                    <Box
                      key={item.title}
                      sx={{
                        // bgcolor: 'green',
                        width: 'auto',
                        height: 'auto',
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        mt: item.margin_top,
                      }}
                    >
                      <Box
                        sx={{
                          width: 'fit-content',
                          display: 'flex',
                          alignItems: 'center',
                        }}
                      >
                        <Avatar
                          sx={{
                            width: '24px',
                            height: '24px',
                            bgcolor: 'secondary.main',
                            color: 'text.secondary',
                            mr: '12px',
                            zIndex: 2,
                          }}
                        >
                          {item.icon}
                        </Avatar>
                        <Typography
                          variant="inherit"
                          component="span"
                          color={calcFinalAmountProcedure.length - 1 === index ? 'text.primary' : 'text.secondary'}
                          // fontFamily={pageDirection === 'rtl' ? 'IRANSansXFaNum-Regular' : 'IRANSansX-Regular'}
                          sx={{
                            fontSize:
                              calcFinalAmountProcedure.length - 1 === index
                                ? {
                                    xs: '17px',
                                    sm: '20px',
                                  }
                                : { xs: '14px', sm: '17px' },
                            // fontWeight: calcFinalAmountProcedure.length - 1 === index ? '700' : '400',
                            fontFamily:
                              calcFinalAmountProcedure.length - 1 === index
                                ? 'IRANSansXFaNum-DemiBold'
                                : 'IRANSansXFaNum-Regular',
                          }}
                        >
                          {item.title}
                        </Typography>
                      </Box>
                      <Typography
                        variant="inherit"
                        component="span"
                        color={'text.secondary'}
                        sx={{ fontSize: '14px', fontWeight: '400' }}
                      >
                        {item.amount}
                      </Typography>
                    </Box>
                  );
                })}
              </Box>
              <Box
                sx={{
                  // bgcolor: "red",
                  width: '14.5px',
                  height: '250px',
                  // position: 'relative'
                }}
              >
                <Divider
                  variant={'fullWidth'}
                  orientation={'vertical'}
                  sx={{
                    borderRightWidth: '5px',
                    borderColor: 'secondary.main',
                  }}
                />
              </Box>
            </Box>

            <TextField
              // inputRef={focusInput-index}
              value={inputsValueState.finalValue}
              // onChange={}
              // error={}
              // helperText={}

              // color={'primary.main'}
              fullWidth
              // name={`final_${operationType}`}
              placeholder={t('deposit:enter_amount')}
              type={'tel'}
              autoComplete="off"
              onChange={(event) => {
                handleCheckInputValue(event, 'finalValue');
                // handleCheckAmount(event)
              }}
              // onBlur={(event) => handleCheckAmount(event)}

              sx={{
                '& input::placeholder': {
                  fontSize: '14px',
                  color: 'text.secondary',
                },
                width: '100%',
                height: '100%',
                bgcolor: 'secondary.main',
                borderRadius: '12px',
                mt: '30px',
                '& .MuiInputAdornment-root':
                  operationType === 'rial'
                    ? { margin: '0' }
                    : {
                        ml: '8px',
                        height: '36px',
                        maxHeight: '40px',
                        backgroundColor: 'white',
                        borderRadius: '12px',
                      },
                '& legend': { display: 'none' },
                '& fieldset': {
                  top: 0,
                  borderRadius: '12px',
                  borderColor: inputsValueState.finalValue ? 'primary.main' : 'secondary.main',
                },
                '& .MuiOutlinedInput-root:hover': {
                  '& > fieldset': {
                    borderColor: inputsValueState.finalValue ? 'primary.main' : 'secondary.main',
                  },
                },
                // '& fieldset:hover': {top: 0, borderRadius: '12px', border: '1px solid red'},
                '& .MuiOutlinedInput-root': {
                  padding: 0,
                  borderRadius: '12px',
                  overflow: 'hidden',
                  height: '100%',
                  '&.Mui-focused fieldset': {
                    borderColor: 'primary.main',
                  },
                  '&:hover:before fieldset': {
                    borderColor: 'primary.main',
                  },

                  color: 'primary.main',
                },
                '.MuiOutlinedInput-root :focus': {
                  backgroundColor: 'white',
                },
                '.MuiInputBase-input': {
                  fontFamily: pageDirection === 'ltr' ? 'IRANSansX-Regular' : 'IRANSansXFaNum-Regular',
                  padding: '14px',
                },
                '.MuiOutlinedInput-input': {
                  bgcolor: inputsValueState.finalValue ? 'white' : 'secondary.main',
                },
                zIndex: 1,
              }}
              InputProps={{
                autoComplete: 'off',
                endAdornment: (
                  <InputAdornment position="start">
                    {operationType === 'rial' ? (
                      <Box
                        sx={{
                          width: 'auto',
                          height: '36px',
                          backgroundColor: 'white',
                          borderRadius: '9px',
                          display: 'flex',
                          alignItems: 'center',
                          justifyContent: 'space-between',
                          px: '10px',
                          mx: '8px',
                        }}
                      >
                        <Typography
                          variant="inherit"
                          component="span"
                          color={'text.primary'}
                          sx={{
                            fontSize: '12px',
                            // fontWeight: '600',
                            fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                            mr: '10px',
                          }}
                        >
                          {t('deposit:IRR')}
                        </Typography>
                        <Box
                          component={'img'}
                          src={rectRialIcon.src}
                          // alt={`${value.title} icon`}
                          sx={{
                            width: '24px',
                            height: '24px',
                          }}
                        />
                      </Box>
                    ) : (
                      <Select
                        // dir={'rtl'}
                        value={finalCurrency}
                        onChange={handleChangeFinalCurrency}
                        displayEmpty
                        IconComponent={NewIcon}
                        renderValue={(value) => {
                          return (
                            value.uid && (
                              <Box
                                // dir={'ltr'}
                                sx={{
                                  width: '100%',
                                  display: 'flex',
                                  alignItems: 'center',
                                  justifyContent: 'space-between',
                                }}
                              >
                                <Typography
                                  variant="inherit"
                                  component="span"
                                  color={'text.secondary'}
                                  sx={{
                                    mt: '4px',
                                    fontSize: '12px',
                                    // fontWeight: '600',
                                    fontFamily:
                                      pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                                    mr: '8px',
                                  }}
                                >
                                  {value.iso_code}
                                </Typography>
                                <Box
                                  component={'img'}
                                  src={value.currency_logo}
                                  // alt={`${value.title} icon`}
                                  sx={{
                                    width: '24px',
                                    height: '24px',
                                  }}
                                />
                              </Box>
                            )
                          );
                        }}
                        MenuProps={{
                          anchorOrigin: {
                            vertical: 'bottom',
                            horizontal: 'left',
                          },
                          transformOrigin: {
                            vertical: 'top',
                            horizontal: 'left',
                          },
                          PaperProps: {
                            sx: {
                              mt: '8px',
                              width: '120px',
                              height: 'auto',
                              // maxHeight: '176px',
                              borderRadius: '10px',
                              overflowY: 'auto',
                            },
                          },
                        }}
                        sx={{
                          // minWidth: '110px',
                          '& .MuiSelect-icon': { top: '40%', mx: '5px' },
                          height: '100%',
                          // ml: '8px',
                          '& .muirtl-bpeome-MuiSvgIcon-root-MuiSelect-icon': { left: 'auto' },
                          '& .muirtl-hfutr2-MuiSvgIcon-root-MuiSelect-icon': { left: 'auto' },
                          '& .muirtl-ittuaa-MuiInputAdornment-root': { margin: '0' },
                          '& .muirtl-keuqm-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input.muirtl-keuqm-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input.muirtl-keuqm-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input':
                            {
                              // px: '12px',
                              pl: '12px',
                              pr: '35px',
                              width: 'fit-content',
                            },
                          '& fieldset': { border: 'none' },
                        }}
                      >
                        {userActiveWalletsState?.map((item: WalletSample, index: number) => {
                          return (
                            <MenuItem
                              value={JSON.stringify(item)}
                              key={index}
                              sx={{
                                width: '100%',
                                px: '8px',
                                my: '5px',
                              }}
                            >
                              <Box
                                // dir={'ltr'}
                                sx={{
                                  width: '100%',
                                  display: 'flex',
                                  alignItems: 'center',
                                  justifyContent: 'space-between',
                                }}
                              >
                                <Box
                                  component={'img'}
                                  src={item.currency_logo}
                                  // alt={`${item.currency} icon`}
                                  sx={{
                                    width: '24px',
                                    height: '24px',
                                  }}
                                />
                                <Box
                                  sx={{
                                    width: 'auto',
                                    height: 'auto',
                                    flexGrow: 1,
                                    display: 'flex',
                                    alignItems: 'center',
                                    justifyContent: 'space-between',
                                    mx: '8px',
                                  }}
                                >
                                  <Typography
                                    variant="inherit"
                                    component="span"
                                    color={'text.secondary'}
                                    sx={{
                                      fontSize: '12px',
                                      // fontWeight: '600',
                                      fontFamily:
                                        pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                                    }}
                                  >
                                    {item.iso_code}
                                  </Typography>
                                  <Typography
                                    variant="inherit"
                                    component="span"
                                    color={'text.secondary'}
                                    sx={{
                                      fontSize: '14px',
                                      fontWeight: '400',
                                    }}
                                  >
                                    {item.currency}
                                  </Typography>
                                </Box>
                              </Box>
                            </MenuItem>
                          );
                        })}
                      </Select>
                    )}
                  </InputAdornment>
                ),
              }}
            />

            {isWithdraw && operationType === 'rial' && (
              <Box
                sx={{
                  width: '100%',
                  height: 'auto',
                  textAlign: 'start',
                  display: 'flex',
                  flexDirection: 'column',
                  mt: '40px',
                }}
              >
                <Typography
                  variant="inherit"
                  component="span"
                  color={'text.primary'}
                  sx={{
                    fontWeight: '400',
                    fontSize: '17px',
                  }}
                >
                  {t('deposit:enter_your_bank_account')}
                </Typography>

                {!!userBankAccountsResult?.userBankAccountsResp?.bank_accounts_list.length && (
                  <TextField
                    // inputRef={focusInput-index}
                    dir={'ltr'}
                    value={BankCardFormatCreator(activeCreditCard?.card_number)}
                    fullWidth
                    name={'bank_account_id'}
                    type={'tel'}
                    autoComplete="off"
                    // onChange={(event) => handleCheckInputValue(event, 'initialValue')}
                    // onBlur={(event) => handleCheckAmount(event)}
                    sx={{
                      '& input::placeholder': {
                        fontSize: '14px',
                        color: 'text.secondary',
                      },
                      width: '100%',
                      height: '100%',
                      bgcolor: 'white',
                      borderRadius: '12px',
                      mt: '8px',
                      '& .MuiInputAdornment-root': {
                        ml: '8px',
                        mr: '0',
                        height: '36px',
                        maxHeight: '40px',
                        backgroundColor: 'white',
                        borderRadius: '12px',
                        border: 'none',
                      },
                      '& legend': { display: 'none' },
                      '& fieldset': {
                        top: 0,
                        borderRadius: '12px',
                        borderColor: 'primary.main',
                      },
                      '& .MuiOutlinedInput-root:hover': {
                        '& > fieldset': {
                          borderColor: 'primary.main',
                        },
                      },
                      // '& fieldset:hover': {top: 0, borderRadius: '12px', border: '1px solid red'},
                      '& .MuiOutlinedInput-root': {
                        padding: 0,
                        borderRadius: '12px',
                        overflow: 'hidden',
                        height: '100%',
                        // "& > fieldset": {
                        //     border: '1.5px solid green',
                        // },
                        // bgcolor: 'secondary.main',
                        // backgroundColor: '#ffffff',
                        '&.Mui-focused > fieldset': {
                          // border: '1px solid',
                          borderColor: 'primary.main',
                        },
                        '&:hover:before fieldset': {
                          borderColor: 'primary.main',
                        },
                        color: 'primary.main',
                      },
                      '.MuiOutlinedInput-root :focus': {
                        backgroundColor: 'white',
                      },
                      '.MuiInputBase-input': {
                        fontFamily: pageDirection === 'ltr' ? 'IRANSansX-Regular' : 'IRANSansXFaNum-Regular',
                        padding: '14px',
                      },
                      // '.mui-style-rtl-6ruq82-MuiInputBase-input-MuiOutlinedInput-input': {
                      //     bgcolor: inputsValueState.initialValue ? 'white' : 'secondary.main',
                      // },
                      zIndex: 1,
                    }}
                    InputProps={{
                      readOnly: true,
                      autoComplete: 'off',
                      endAdornment: (
                        <InputAdornment position="end">
                          <Box
                            component={'img'}
                            src={activeCreditCard?.bank_logo}
                            // alt={`${value.title} icon`}
                            sx={{
                              width: { xs: '31px', sm: '49px' },
                              height: { xs: '20px', sm: '32px' },
                            }}
                          />
                        </InputAdornment>
                      ),
                      startAdornment: (
                        <InputAdornment position="start">
                          <Select
                            // dir={'rtl'}
                            value={activeCreditCard}
                            onChange={handleChangeCreditCard}
                            displayEmpty
                            IconComponent={CreditNewIcon}
                            renderValue={(value) => {
                              return (
                                <Typography
                                  variant="inherit"
                                  component="span"
                                  color={'primary.main'}
                                  sx={{
                                    width: '100px',
                                    mt: '4px',
                                    fontSize: '12px',
                                    // fontWeight: '600',
                                    fontFamily:
                                      pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                                    mr: '8px',
                                  }}
                                >
                                  {value?.bank_name}
                                </Typography>
                              );
                            }}
                            MenuProps={{
                              anchorOrigin: {
                                vertical: 'bottom',
                                horizontal: 'left',
                              },
                              transformOrigin: {
                                vertical: 'top',
                                horizontal: 'left',
                              },
                              PaperProps: {
                                sx: {
                                  mt: '8px',
                                  boxShadow: '0px 4px 38px rgba(68, 68, 68, 0.1)',
                                  width: { xs: 'auto', sm: '425px' },
                                  height: 'auto',
                                  // maxHeight: '176px',
                                  borderRadius: '10px',
                                  overflowY: 'auto',
                                },
                              },
                            }}
                            sx={{
                              // minWidth: '110px',
                              '& .MuiSelect-icon': { top: '40%', mx: '5px' },
                              height: '100%',
                              // ml: '8px',
                              '& .muirtl-bpeome-MuiSvgIcon-root-MuiSelect-icon': { left: 'auto' },
                              '& .muirtl-hfutr2-MuiSvgIcon-root-MuiSelect-icon': { left: 'auto' },
                              '& .muirtl-ittuaa-MuiInputAdornment-root': { margin: '0' },
                              '& .muirtl-keuqm-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input.muirtl-keuqm-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input.muirtl-keuqm-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input':
                                {
                                  // px: '12px',
                                  pl: '12px',
                                  pr: '35px',
                                  width: 'fit-content',
                                },
                              '& fieldset': { border: 'none' },
                            }}
                          >
                            {activeBankAccountsState?.map((item: BankAccountSample, index: number) => {
                              return (
                                <MenuItem
                                  value={JSON.stringify(item)}
                                  key={item.id}
                                  sx={{
                                    width: '100%',
                                    // px: '8px',
                                    // my: '5px'
                                  }}
                                >
                                  <Box
                                    // dir={'ltr'}
                                    sx={{
                                      width: '100%',
                                      display: 'flex',
                                      alignItems: 'center',
                                      justifyContent: 'space-between',
                                    }}
                                  >
                                    <Box
                                      component={'img'}
                                      src={item.bank_logo}
                                      // alt={`${item.currency} icon`}
                                      sx={{
                                        width: '49px',
                                        height: '32px',
                                      }}
                                    />
                                    <Typography
                                      variant="inherit"
                                      component="span"
                                      color={'primary.main'}
                                      sx={{
                                        fontSize: '14px',
                                        fontWeight: '500',
                                        ml: '15px',
                                      }}
                                    >
                                      <bdi dir={'ltr'}>{BankCardFormatCreator(item.card_number)}</bdi>
                                    </Typography>
                                    <Typography
                                      variant="inherit"
                                      component="span"
                                      color={'primary.main'}
                                      sx={{
                                        fontSize: '14px',
                                        // fontWeight: '600',
                                        fontFamily:
                                          pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                                        ml: '15px',
                                      }}
                                    >
                                      {item.bank_name}
                                    </Typography>
                                  </Box>
                                </MenuItem>
                              );
                            })}
                          </Select>
                        </InputAdornment>
                      ),
                    }}
                  />
                )}

                <Button
                  variant="contained"
                  // type={'submit'}
                  fullWidth
                  disableRipple
                  sx={{
                    height: { xs: '40px', sm: '48px' },
                    '&:hover': {
                      boxShadow: 'none',
                    },
                    bgcolor: '#D1E7FF',
                    color: 'text.primary',
                    boxShadow: 'none',
                    // fontWeight: '600',
                    borderRadius: '12px',
                    fontSize: '14px',
                    mt: '32px',
                  }}
                  startIcon={
                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path
                        fillRule="evenodd"
                        clipRule="evenodd"
                        d="M13.9061 1.6665H6.09654C3.37432 1.6665 1.66797 3.59324 1.66797 6.3208V13.6789C1.66797 16.4064 3.36638 18.3332 6.09654 18.3332H13.9061C16.6362 18.3332 18.3346 16.4064 18.3346 13.6789V6.3208C18.3346 3.59324 16.6362 1.6665 13.9061 1.6665Z"
                        fill="#298FFE"
                        stroke="#298FFE"
                        strokeWidth="1.5"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                      />
                      <path
                        d="M13.0564 9.99215H6.94531"
                        stroke="white"
                        strokeWidth="1.5"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                      />
                      <path
                        d="M9.99839 6.93945V13.0447"
                        stroke="white"
                        strokeWidth="1.5"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                      />
                    </svg>
                  }
                  onClick={() => router.push(`/${PATHS.USER_BANK_ACCOUNT}/${PATHS.IRANIAN}`)}
                >
                  {t('deposit:add_new_bank_account')}
                </Button>
              </Box>
            )}

            <Box sx={{ display: 'flex', alignItems: 'center', mt: '45px' }}>
              <Box
                component={'img'}
                src={bulletPoint.src}
                alt={'bullet point'}
                sx={{
                  width: '22px',
                  height: '18px',
                  mr: '9px',
                  transform: pageDirection === 'ltr' ? 'scale(-1, 1)' : 'scale(1, 1)',
                }}
              />
              <Typography
                variant="inherit"
                component="span"
                color={'text.primary'}
                sx={{
                  // fontWeight: {xs: '600'},
                  fontFamily: pageDirection === 'rtl' ? 'IRANSansXFaNum-DemiBold' : 'IRANSansX-DemiBold',
                  fontSize: { xs: '16px', xl: '16px' },
                }}
              >
                {t('deposit:description')}
              </Typography>
            </Box>

            <TextField
              // inputRef={focusInput-index}
              value={inputsValueState.descriptionValue}
              // onChange={}
              // error={}
              // helperText={}

              // color={'primary.main'}
              fullWidth
              name={'description'}
              placeholder={t('deposit:enter_your_description')}
              type={'tel'}
              autoComplete="off"
              multiline
              rows={6}
              onChange={(event) => handleCheckInputValue(event, 'descriptionValue')}
              sx={{
                width: '100%',
                height: '100%',
                // bgcolor: 'secondary.main',
                bgcolor: inputsValueState.descriptionValue ? 'white' : 'secondary.main',
                borderRadius: '12px',
                mt: '10px',
                '& .MuiInputAdornment-root': { margin: '0' },
                '& legend': { display: 'none' },
                '& fieldset': {
                  top: 0,
                  borderRadius: '12px',
                  borderColor: inputsValueState.descriptionValue ? 'primary.main' : 'secondary.main',
                },
                '&:hover .fieldset': {
                  border: '1px solid red',
                },
                '& .MuiOutlinedInput-root:hover': {
                  '& > fieldset': {
                    borderColor: inputsValueState.descriptionValue ? 'primary.main' : 'secondary.main',
                  },
                },
                // '& fieldset:hover': {top: 0, borderRadius: '12px', border: '1px solid red'},
                '& .MuiOutlinedInput-root': {
                  padding: 0,
                  borderRadius: '12px',
                  '&.Mui-focused fieldset': {
                    borderColor: 'primary.main',
                  },
                  '&:hover:before fieldset': {
                    borderColor: 'primary.main',
                  },
                },
                '.MuiOutlinedInput-root :focus': {
                  backgroundColor: 'white',
                },
                '.MuiInputBase-input': {
                  fontFamily: pageDirection === 'ltr' ? 'IRANSansX-Regular' : 'IRANSansXFaNum-Regular',
                  padding: '14px',
                },
                '.MuiInputBase-input::placeholder': {
                  fontSize: '14px',
                  color: 'text.secondary',
                },
                zIndex: 1,
              }}
              InputProps={{
                autoComplete: 'off',
              }}
            />

            <Button
              variant="contained"
              disableRipple={true}
              type={'submit'}
              // disabled={!inputsValueState.initialValue}
              sx={{
                width: { xs: '72%', sm: '68%' },
                height: { xs: '40px', sm: '48px' },
                '&:hover': {
                  boxShadow: 'none',
                },
                boxShadow: 'none',
                fontWeight: '500',
                borderRadius: '12px',
                fontSize: { xs: '12px', sm: '14px' },
                mt: '32px',
              }}
              // onClick={handleSubmitOperationType}
            >
              {t('deposit:next')}
            </Button>
          </Box>
        </Box>
        <Box
          sx={{
            flex: '1',
            height: 'auto',
            display: 'flex',
            justifyContent: { xs: 'center', sm: 'flex-start' },
            ml: { xs: '0', sm: '16px' },
            mb: '24px',
          }}
        >
          <Box
            sx={{
              width: { xs: '327px', sm: '234px', lg: '323px' },
              height: 'auto',
              display: 'flex',
              alignItems: 'flex-start',
              bgcolor: '#E1FFEE',
              padding: '16px',
              borderRadius: '21px',
              border: '1px dashed',
              borderColor: 'success.dark',
              // backgroundImage: `url("data:image/svg+xml,%3csvg width='100%25' height='100%25' xmlns='http://www.w3.org/2000/svg'%3e%3crect width='100%25' height='100%25' fill='none' rx='21' ry='21' stroke='%23333' stroke-width='1' stroke-dasharray='5%2c 10' stroke-dashoffset='0' stroke-linecap='square'/%3e%3c/svg%3e")`
            }}
          >
            <Box
              component={'img'}
              src={dangerCircleGreenIcon.src}
              sx={{
                width: '18.5px',
                height: '18.5px',
                mt: '2px',
                mr: '11px',
              }}
            />

            <Typography
              variant="inherit"
              component="span"
              color={'success.dark'}
              sx={{
                fontSize: { xs: '12px', lg: '14px' },
                fontWeight: '400',
                textAlign: 'justify',
              }}
            >
              {t('deposit:suspension_warning')}
            </Typography>
          </Box>
        </Box>
      </Box>

      {openResultDialogBox && (
        <AlertDialogBox
          mode={'result'}
          data={operationResultState}
          open={openResultDialogBox}
          close={handleCloseResultDB}
        />
      )}
    </>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [...localesNS])),

      // Will be passed to the page component as props
    },
  };
}

export default Deposit;
