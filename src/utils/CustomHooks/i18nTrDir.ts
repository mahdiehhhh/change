import { useTranslation } from 'next-i18next';

export const useI18nTrDir = () => {
  const { t, i18n } = useTranslation();
  const pageDirection = i18n.dir();
  return { t, i18n, pageDirection };
};
