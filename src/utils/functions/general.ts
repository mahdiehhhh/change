import { LOGIN_INFO, LOGOUT_HAPPENED } from '../../configs/variables.config';

export const handleSaveUserLoginInfos = (data: object) => {
  localStorage.setItem(LOGIN_INFO, JSON.stringify(data));
};

export const handleReadUserLoginInfos = () => {
  // @ts-ignore
  return typeof window !== 'undefined' && JSON.parse(localStorage.getItem(LOGIN_INFO));
};

export const handleSetLogOutEvent = () => {
  localStorage.setItem(LOGOUT_HAPPENED, JSON.stringify('logout happened'));
  setTimeout(() => {
    localStorage.removeItem(LOGOUT_HAPPENED);
  }, 5000);
};
