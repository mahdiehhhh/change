import { ACCESS_TOKEN } from '../../configs/variables.config';

export const handleSetTokens = (type: string, data: any) => {
  return localStorage.setItem(type, data);
};

export const handleGetTokens = (type: string) => {
  return localStorage.getItem(type);
};

export const handleRemoveTokens = (type: string) => {
  return localStorage.removeItem(type);
};

export const setCookie = (name: string, value: string | number, exp?: string | number) => {
  console.log('thisi si access tokeendnndndn', name, value, exp);

  if (name === ACCESS_TOKEN) {
    console.log('thisi si access tokeendnndndn', name, value, exp);
  }
  let expires = '';
  if (exp) {
    expires = '; expires=' + exp;
  }
  document.cookie = name + '=' + value + expires + '; path=/';
};

export const getCookie = (name: string) => {
  const nameEQ = name + '=';
  const ca = document.cookie.split(';');
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
  }
  return null;
};

export const eraseCookie = (name: string) => {
  document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
};
