export const calcZimaPayWage = (amount: number, wage: number) => {
  const result = (amount * wage) / 100;
  return Number(Number(result).toFixed(2)).toLocaleString('en-US');
};

export const calcCurrencyConversionCosts = (
  operation: string,
  operationType: string,
  wage: number | string,
  tax: number | string,
  currencyExRate: number | string,
  initialAmount?: number | string,
  finalAmount?: number | string,
  originWallet?: string | undefined,
) => {
  let result = 'undefined';
  if (operation === 'deposit') {
    if (operationType === 'rial') {
      if (initialAmount) {
        // @ts-ignore
        let quantity = (initialAmount + (initialAmount * wage) / 100) * currencyExRate;
        // @ts-ignore
        finalAmount = quantity + quantity * (tax / 100);
        result = Number(Number(finalAmount).toFixed(2)).toLocaleString('en-US');
        return +result === 0 ? '' : result;
      } else {
        // @ts-ignore
        initialAmount = finalAmount / ((1 + wage / 100 + tax / 100 + (wage / 100) * (tax / 100)) * currencyExRate);
        result = Number(Number(initialAmount).toFixed(2)).toLocaleString('en-US');
        return +result === 0 ? '' : result;
      }
    } else {
      if (initialAmount) {
        // @ts-ignore
        finalAmount = initialAmount - initialAmount * (wage / 100) - initialAmount * (tax / 100);
        result = Number(Number(finalAmount).toFixed(2)).toLocaleString('en-US');
        return +result === 0 ? '' : result;
      } else {
        // @ts-ignore
        initialAmount = finalAmount / (1 - wage / 100 - tax / 100);
        result = Number(Number(initialAmount).toFixed(2)).toLocaleString('en-US');
        return +result === 0 ? '' : result;
      }
    }
  } else if (operation === 'withdraw') {
    if (operationType === 'rial') {
      if (initialAmount) {
        // @ts-ignore
        let quantity = (initialAmount - (initialAmount * wage) / 100) * currencyExRate;
        // @ts-ignore
        finalAmount = quantity - quantity * (tax / 100);
        result = Number(Number(finalAmount).toFixed(2)).toLocaleString('en-US');
        return +result === 0 ? '' : result;
      } else {
        // @ts-ignore
        initialAmount = finalAmount / ((1 - wage / 100 - tax / 100 + (wage / 100) * (tax / 100)) * currencyExRate);
        result = Number(Number(initialAmount).toFixed(2)).toLocaleString('en-US');
        return +result === 0 ? '' : result;
      }
    } else {
      if (initialAmount) {
        // @ts-ignore
        finalAmount = initialAmount - initialAmount * (wage / 100) - initialAmount * (tax / 100);
        result = Number(Number(finalAmount).toFixed(2)).toLocaleString('en-US');
        return +result === 0 ? '' : result;
      } else {
        // @ts-ignore
        initialAmount = finalAmount / (1 - wage / 100 - tax / 100);
        result = Number(Number(initialAmount).toFixed(2)).toLocaleString('en-US');
        return +result === 0 ? '' : result;
      }
    }
  } else if (operation === 'transfer') {
    if (operationType === 'internal' || operationType === 'others') {
      if (initialAmount) {
        // @ts-ignore
        let quantity =
          (initialAmount - (initialAmount * wage) / 100) *
          (originWallet === 'IRR' ? 1 / currencyExRate : currencyExRate);
        // @ts-ignore
        finalAmount = quantity - quantity * (tax / 100);
        result = Number(Number(finalAmount).toFixed(2)).toLocaleString('en-US');
        return +result === 0 ? '' : result;
      } else {
        // @ts-ignore
        initialAmount =
          finalAmount /
          ((1 - wage / 100 - tax / 100 + (wage / 100) * (tax / 100)) *
            (originWallet === 'IRR' ? 1 / currencyExRate : currencyExRate));
        result = Number(Number(initialAmount).toFixed(2)).toLocaleString('en-US');
        return +result === 0 ? '' : result;
      }
    }
  }
  // @ts-ignore
  return +result === 0 ? '' : result;
};
