import { BASE_URL } from '../../configs/variables.config';

export const notifications = {
  data: {
    notifications: [
      {
        id: 'f67fb6cf-2ccf-421a-915b-14e16a542d991',
        type: 'VALIDATE_NATURAL_ACCOUNT',
        message: 'کاربر گرامی اکانت کاربری شما تایید شده است. می توانید از خدمات زیما پی استفاده نمایید.',
        url: '',
        is_read: 0,
        date: '1401/08/25',
        time: '17:02',
      },
      {
        id: 'f67fb6cf-2ccf-421a-915b-14e16a542d992',
        type: 'VALIDATE_NATURAL_ACCOUNT',
        message: 'کاربر گرامی اکانت کاربری شما تایید شده است. می توانید از خدمات زیما پی استفاده نمایید.',
        url: '',
        is_read: 1,
        date: '1401/08/25',
        time: '17:02',
      },
      {
        id: 'f67fb6cf-2ccf-421a-915b-14e16a542d993',
        type: 'VALIDATE_NATURAL_ACCOUNT',
        message: 'کاربر گرامی اکانت کاربری شما تایید شده است. می توانید از خدمات زیما پی استفاده نمایید.',
        url: '',
        is_read: 0,
        date: '1401/08/25',
        time: '17:02',
      },
      {
        id: 'f67fb6cf-2ccf-421a-915b-14e16a542d994',
        type: 'VALIDATE_NATURAL_ACCOUNT',
        message: 'کاربر گرامی اکانت کاربری شما تایید شده است. می توانید از خدمات زیما پی استفاده نمایید.',
        url: '',
        is_read: 1,
        date: '1401/08/24',
        time: '17:02',
      },
      {
        id: '58894385-b851-4347-80bd-11d42b5d160311',
        type: 'ORDER',
        message: 'هزینه و کارمزد مرتبط به سفارش  نقد کردن درآمد ارزی در پنل به روز رسانی شد.',
        url: 'http://127.0.0.1:8000/orders/my-orders/256',
        is_read: 1,
        date: '1401/08/24',
        time: '17:01',
      },
      {
        id: '58894385-b851-4347-80bd-11d42b5d160312',
        type: 'ORDER',
        message: 'هزینه و کارمزد مرتبط به سفارش  نقد کردن درآمد ارزی در پنل به روز رسانی شد.',
        url: 'http://127.0.0.1:8000/orders/my-orders/256',
        is_read: 0,
        date: '1401/08/24',
        time: '17:01',
      },
      {
        id: '58894385-b851-4347-80bd-11d42b5d160313',
        type: 'ORDER',
        message: 'هزینه و کارمزد مرتبط به سفارش  نقد کردن درآمد ارزی در پنل به روز رسانی شد.',
        url: 'http://127.0.0.1:8000/orders/my-orders/256',
        is_read: 0,
        date: '1401/08/24',
        time: '17:01',
      },
      {
        id: '58894385-b851-4347-80bd-11d42b5d160321',
        type: 'ORDER',
        message: 'هزینه و کارمزد مرتبط به سفارش  نقد کردن درآمد ارزی در پنل به روز رسانی شد.',
        url: 'http://127.0.0.1:8000/orders/my-orders/256',
        is_read: 1,
        date: '1401/08/23',
        time: '17:01',
      },
      {
        id: '58894385-b851-4347-80bd-11d42b5d160322',
        type: 'ORDER',
        message: 'هزینه و کارمزد مرتبط به سفارش  نقد کردن درآمد ارزی در پنل به روز رسانی شد.',
        url: 'http://127.0.0.1:8000/orders/my-orders/256',
        is_read: 0,
        date: '1401/08/23',
        time: '17:01',
      },
      {
        id: '58894385-b851-4347-80bd-11d42b5d160323',
        type: 'ORDER',
        message: 'هزینه و کارمزد مرتبط به سفارش  نقد کردن درآمد ارزی در پنل به روز رسانی شد.',
        url: 'http://127.0.0.1:8000/orders/my-orders/256',
        is_read: 1,
        date: '1401/08/23',
        time: '17:01',
      },
    ],
    total_messages: 235,
    last_page: 118,
    current_page: 7,
    unread_messages: 0,
  },
};

export const default_country = {
  phoneCode: {
    icon: `${BASE_URL}/storage/images/countries/flags/NOD6o0balfUB8Q2BterLmRhcLNu2OGhrD7RQdOQO.png`,
    iso_code: 'IR',
    phone_prefix: '+98',
  },
  language: {
    id: 2,
    name: 'فارسی',
    code: 'fa',
    direction: 'rtl',
    is_master: 1,
    logo: '',
    status: 'enabled',
  },
};

// export const accountTypesSwitch = [
//     {
//         id: 1,
//         title: 'حساب ایرانی'
//     }, {
//         id: 2,
//         title: 'حساب ارزی'
//     }, {
//         id: 3,
//         title: 'کارت ارزی'
//     }, {
//         id: 4,
//         title: 'حساب کریپتو'
//     }
// ]
//
// export const iranianAccountOptions = {
//     id: 1,
//     formTitle: 'معرفی حساب ایرانی',
//     formOptions: [{
//         id: 'account_title',
//         title: 'عنوان حساب',
//         placeholder: 'عنوان حساب را وارد کنید',
//         hasSelect: false,
//         width: '50%'
//     }, {
//         id: 'bank_name',
//         title: 'نام بانک',
//         placeholder: 'انتخاب کنید',
//         hasSelect: true,
//         width: '50%'
//     }, {
//         id: 'monetary_unit',
//         title: 'واحد پولی',
//         placeholder: 'انتخاب کنید',
//         hasSelect: true,
//         width: '50%'
//     }, {
//         id: 'card_owner_name',
//         title: 'نام مالک کارت',
//         placeholder: 'نام را وارد کنید',
//         hasSelect: false,
//         width: '50%'
//     }, {
//         id: 'card_number',
//         title: 'شماره کارت',
//         placeholder: 'شماره کارت را وارد کنید',
//         hasSelect: false,
//         width: '50%'
//     }, {
//         id: 'shaba_number',
//         title: 'شماره شبا',
//         placeholder: 'شماره شبا خود را وارد کنید',
//         hasSelect: false,
//         width: '50%'
//     }, {
//         id: 'account_number',
//         title: 'شماره حساب',
//         placeholder: 'شماره حساب خود را وارد کنید',
//         hasSelect: false,
//         width: '100%'
//     },]
//
// }
//
// export const currencyAccountOptions = {
//     id: 2,
//     formTitle: 'معرفی حساب ارزی',
//     formOptions: [{
//         id: 'account_title',
//         title: 'عنوان حساب',
//         placeholder: 'عنوان حساب را وارد کنید',
//         hasSelect: false,
//         width: '50%'
//     }, {
//         id: 'bank_name',
//         title: 'نام بانک',
//         placeholder: 'انتخاب کنید',
//         hasSelect: false,
//         width: '50%'
//     }, {
//         id: 'monetary_unit',
//         title: 'واحد پولی',
//         placeholder: 'انتخاب کنید',
//         hasSelect: true,
//         width: '50%'
//     }, {
//         id: 'account_owner_name',
//         title: 'نام مالک حساب',
//         placeholder: 'نام را وارد کنید',
//         hasSelect: false,
//         width: '50%'
//     }, {
//         id: 'account_code',
//         title: 'کد حساب',
//         placeholder: 'کد حساب را وارد کنید',
//         hasSelect: false,
//         width: '50%'
//     }, {
//         id: 'swift_code',
//         title: 'کد سوئیفت',
//         placeholder: 'کد سوئیفت را وارد کنید',
//         hasSelect: false,
//         width: '50%'
//     }, {
//         id: 'tel_bank',
//         title: 'تلفن بانک',
//         placeholder: 'تلفن بانک خود را وارد کنید',
//         hasSelect: false,
//         width: '50%'
//     }, {
//         id: 'passport_image',
//         title: 'تصویر گذرنامه/پروفرمای صاحب حساب',
//         placeholder: 'فایل خود را بارگزاری نمایید',
//         hasSelect: false,
//         width: '50%'
//     }, {
//         id: 'account_number',
//         title: 'شماره حساب',
//         placeholder: 'شماره حساب خود را وارد کنید',
//         hasSelect: false,
//         width: '50%'
//     }, {
//         id: 'branch_code',
//         title: 'کد شعبه',
//         placeholder: 'کد شعبه را وارد کنید',
//         hasSelect: false,
//         width: '50%'
//     }, {
//         id: 'bank_address',
//         title: 'آدرس بانک',
//         placeholder: 'آدرس را وارد کنید',
//         hasSelect: false,
//         width: '100%'
//     },]
//
// }
//
//
// export const currencyCardOptions = {
//     id: 3,
//     formTitle: 'معرفی کارت ارزی',
//     formOptions: [
//         {
//             id: 'account_title',
//             title: 'عنوان حساب',
//             placeholder: 'عنوان حساب را وارد کنید',
//             hasSelect: false,
//             width: '50%'
//         }, {
//             id: 'card_type',
//             title: 'نوع کارت',
//             placeholder: 'انتخاب کنید',
//             hasSelect: true,
//             width: '50%'
//         }, {
//             id: 'card_name',
//             title: 'نام روی کارت',
//             placeholder: 'نام را وارد کنید',
//             hasSelect: false,
//             width: '50%'
//         }, {
//             id: 'monetary_unit',
//             title: 'واحد پولی',
//             placeholder: 'انتخاب کنید',
//             hasSelect: true,
//             width: '50%'
//         }, {
//             id: 'account_number',
//             title: 'شماره حساب',
//             placeholder: 'شماره حساب را وارد کنید',
//             hasSelect: false,
//             width: '50%'
//         }
//     ]
// }
//
// export const cryptoAccountOptions = {
//     id: 4,
//     formTitle: 'معرفی حساب کریپتو',
//     formOptions: [
//         {
//             id: 'account_title',
//             title: 'عنوان حساب',
//             placeholder: 'عنوان حساب را وارد کنید',
//             hasSelect: false,
//             width: '50%'
//         }, {
//             id: 'currency_type',
//             title: 'نوع ارز',
//             placeholder: 'انتخاب کنید',
//             hasSelect: true,
//             width: '50%'
//         }, {
//             id: 'network_type',
//             title: 'نوع شبکه',
//             placeholder: '',
//             hasSelect: false,
//             width: '50%'
//         }, {
//             id: 'address',
//             title: 'آدرس',
//             placeholder: 'آدرس را وارد کنید',
//             hasSelect: false,
//             width: '50%'
//         }
//     ]
// }
//
//
// export const importantPoints = [
//     {
//         id: 1,
//         title: 'برداشت موجودی از کیف پول های کاربر و واریز به حساب بانکی تنها در صورت همنام بودن نام صاحب حساب با کاربر امکان پذیر است.'
//     }, {
//         id: 2,
//         title: 'تنها حساب های بین المللی سوئیفت را میتوانید با نام دیگر افراد ثبت کنید.'
//     }, {
//         id: 3,
//         title: 'از صحت اطلاعات بانکی وارد شده مطمئن شوید.'
//     }, {
//         id: 4,
//         title: 'در صورت مغایرت اطلاعات بانکی، حساب شما تایید نمی شود.'
//     },
// ]
