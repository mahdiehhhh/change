import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { getWalletWithdrawCashRules } from 'pages/api/dynamic/getData';
import { ACCESS_TOKEN, IS_LOGGED_IN } from '../../configs/variables.config';

// Define a type for the slice state
interface WalletWithdrawCashState {
  loading: boolean;
  walletWithdrawCashResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: WalletWithdrawCashState = {
  loading: false,
  walletWithdrawCashResp: null,
  error: undefined,
};

export const WalletWithdrawCashSlice = createSlice({
  name: 'wallet withdraw cash',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchWalletWithdrawCashRequest: (state) => {
      return {
        loading: true,
        walletWithdrawCashResp: null,
        error: undefined,
      };
    },
    fetchWalletWithdrawCashSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        walletWithdrawCashResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchWalletWithdrawCashFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        walletWithdrawCashResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchWalletWithdrawCashRequest, fetchWalletWithdrawCashSuccess, fetchWalletWithdrawCashFailure } =
  WalletWithdrawCashSlice.actions;

const fetchWalletWithdrawCash = (uid: string) => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchWalletWithdrawCashRequest());
    await getWalletWithdrawCashRules(uid)
      .then((response) => {
        dispatch(fetchWalletWithdrawCashSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchWalletWithdrawCashFailure(error.message));
      });
  };
};
export { fetchWalletWithdrawCash };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.WalletWithdrawCash;

export default WalletWithdrawCashSlice.reducer;
