import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { getBanksLists } from 'pages/api/dynamic/getData';

// Define a type for the slice state
interface BanksListsState {
  loading: boolean;
  banksListsResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: BanksListsState = {
  loading: false,
  banksListsResp: null,
  error: undefined,
};

export const BanksListsSlice = createSlice({
  name: 'banks lists',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchBanksListsRequest: (state) => {
      return {
        loading: true,
        banksListsResp: null,
        error: undefined,
      };
    },
    fetchBanksListsSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        banksListsResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchBanksListsFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        banksListsResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchBanksListsRequest, fetchBanksListsSuccess, fetchBanksListsFailure } = BanksListsSlice.actions;

const fetchBanksLists = () => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchBanksListsRequest());
    await getBanksLists()
      .then((response) => {
        dispatch(fetchBanksListsSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchBanksListsFailure(error.message));
      });
  };
};
export { fetchBanksLists };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.BanksLists;

export default BanksListsSlice.reducer;
