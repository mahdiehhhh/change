import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { registerVerifyPhone } from 'pages/api/dynamic/getData';
import { ACCESS_TOKEN, IS_LOGGED_IN } from '../../configs/variables.config';

// Define a type for the slice state
interface RegVerifyPhoneState {
  loading: boolean;
  regVerifyPhoneResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: RegVerifyPhoneState = {
  loading: false,
  regVerifyPhoneResp: null,
  error: undefined,
};

export const RegVerifyPhoneSlice = createSlice({
  name: 'register verify phone',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    resetToInitial: () => {
      return initialState;
    },
    fetchRegVerifyPhoneRequest: (state) => {
      return {
        loading: true,
        regVerifyPhoneResp: null,
        error: undefined,
      };
    },
    fetchRegVerifyPhoneSuccess: (state, action: PayloadAction<any>) => {
      return {
        ...state,
        loading: false,
        regVerifyPhoneResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchRegVerifyPhoneFailure: (state, action: PayloadAction<any>) => {
      return {
        ...state,
        loading: false,
        regVerifyPhoneResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { resetToInitial, fetchRegVerifyPhoneRequest, fetchRegVerifyPhoneSuccess, fetchRegVerifyPhoneFailure } =
  RegVerifyPhoneSlice.actions;

const fetchRegVerifyPhone = (req: object | string) => {
  return async (dispatch: Dispatch) => {
    if (typeof req === 'string') {
      req === 'reset' && dispatch(resetToInitial());
    } else {
      dispatch(fetchRegVerifyPhoneRequest());
      await registerVerifyPhone(req)
        .then((response) => {
          dispatch(fetchRegVerifyPhoneSuccess(response));
        })
        .catch((error) => {
          console.log('errrrorrr', error);
          dispatch(fetchRegVerifyPhoneFailure(error.message));
        });
    }
  };
};
export { fetchRegVerifyPhone };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.RegisterVerifyPhone;

export default RegVerifyPhoneSlice.reducer;
