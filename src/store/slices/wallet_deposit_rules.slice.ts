import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { getWalletDepositRules } from 'pages/api/dynamic/getData';
import { ACCESS_TOKEN, IS_LOGGED_IN } from '../../configs/variables.config';

// Define a type for the slice state
interface WalletDepositRulesState {
  loading: boolean;
  walletDepositRulesResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: WalletDepositRulesState = {
  loading: false,
  walletDepositRulesResp: null,
  error: undefined,
};

export const WalletDepositRulesSlice = createSlice({
  name: 'wallet deposit rules',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchWalletDepositRulesRequest: (state) => {
      return {
        loading: true,
        walletDepositRulesResp: null,
        error: undefined,
      };
    },
    fetchWalletDepositRulesSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        walletDepositRulesResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchWalletDepositRulesFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        walletDepositRulesResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchWalletDepositRulesRequest, fetchWalletDepositRulesSuccess, fetchWalletDepositRulesFailure } =
  WalletDepositRulesSlice.actions;

const fetchWalletDepositRules = (uid: string) => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchWalletDepositRulesRequest());
    await getWalletDepositRules(uid)
      .then((response) => {
        dispatch(fetchWalletDepositRulesSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchWalletDepositRulesFailure(error.message));
      });
  };
};
export { fetchWalletDepositRules };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.WalletDepositRules;

export default WalletDepositRulesSlice.reducer;
