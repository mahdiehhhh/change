import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { createCreditBankAccount } from 'pages/api/dynamic/getData';

// Define a type for the slice state
interface CreateCreditBankAccountState {
  loading: boolean;
  createCreditBankAccResp: any;
  error: any;
}

// Define the initial state using that type
const initialState: CreateCreditBankAccountState = {
  loading: false,
  createCreditBankAccResp: null,
  error: {},
};

export const CreateCreditBankAccountSlice = createSlice({
  name: 'create credit bank account',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchCreateCreditBankAccRequest: (state) => {
      return {
        loading: true,
        createCreditBankAccResp: null,
        error: {},
      };
    },
    fetchCreateCreditBankAccSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        createCreditBankAccResp: action.payload,
        error: {},
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchCreateCreditBankAccFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        createCreditBankAccResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchCreateCreditBankAccRequest, fetchCreateCreditBankAccSuccess, fetchCreateCreditBankAccFailure } =
  CreateCreditBankAccountSlice.actions;

const fetchCreateCreditBankAccount = (data: object) => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchCreateCreditBankAccRequest());
    await createCreditBankAccount(data)
      .then((response) => {
        dispatch(fetchCreateCreditBankAccSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchCreateCreditBankAccFailure(error.response.data.errors));
      });
  };
};
export { fetchCreateCreditBankAccount };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.CreateCreditBankAccount;

export default CreateCreditBankAccountSlice.reducer;
