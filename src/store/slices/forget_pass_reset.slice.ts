import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { forgetPassReset } from 'pages/api/dynamic/getData';
import { ACCESS_TOKEN, IS_LOGGED_IN } from '../../configs/variables.config';

// Define a type for the slice state
interface ForgetPassResetState {
  loading: boolean;
  forgetPassResetResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: ForgetPassResetState = {
  loading: false,
  forgetPassResetResp: null,
  error: undefined,
};

export const ForgetPasswordResetSlice = createSlice({
  name: 'forget password reset',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchForgetPassResetRequest: (state) => {
      return {
        loading: true,
        forgetPassResetResp: null,
        error: undefined,
      };
    },
    fetchForgetPassResetSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        forgetPassResetResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchForgetPassResetFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        forgetPassResetResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchForgetPassResetRequest, fetchForgetPassResetSuccess, fetchForgetPassResetFailure } =
  ForgetPasswordResetSlice.actions;

const fetchForgetPassReset = (req: object) => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchForgetPassResetRequest());
    await forgetPassReset(req)
      .then((response) => {
        dispatch(fetchForgetPassResetSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchForgetPassResetFailure(error.message));
      });
  };
};
export { fetchForgetPassReset };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.ForgetPasswordReset;

export default ForgetPasswordResetSlice.reducer;
