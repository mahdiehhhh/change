import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { getWalletDepositCashRules } from 'pages/api/dynamic/getData';
import { ACCESS_TOKEN, IS_LOGGED_IN } from '../../configs/variables.config';

// Define a type for the slice state
interface WalletDepositCashRulesState {
  loading: boolean;
  walletDepositCashResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: WalletDepositCashRulesState = {
  loading: false,
  walletDepositCashResp: null,
  error: undefined,
};

export const WalletDepositCashSlice = createSlice({
  name: 'wallet deposit cash',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchWalletDepositCashRequest: (state) => {
      return {
        loading: true,
        walletDepositCashResp: null,
        error: undefined,
      };
    },
    fetchWalletDepositCashSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        walletDepositCashResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchWalletDepositCashFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        walletDepositCashResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchWalletDepositCashRequest, fetchWalletDepositCashSuccess, fetchWalletDepositCashFailure } =
  WalletDepositCashSlice.actions;

const fetchWalletDepositCash = (uid: string) => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchWalletDepositCashRequest());
    await getWalletDepositCashRules(uid)
      .then((response) => {
        dispatch(fetchWalletDepositCashSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchWalletDepositCashFailure(error.message));
      });
  };
};
export { fetchWalletDepositCash };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.WalletDepositCash;

export default WalletDepositCashSlice.reducer;
