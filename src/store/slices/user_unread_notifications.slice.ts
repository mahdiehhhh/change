import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { getUserUnreadNotifications } from 'pages/api/dynamic/getData';
import { ACCESS_TOKEN, IS_LOGGED_IN } from '../../configs/variables.config';

// Define a type for the slice state
interface UserUnreadNotificationsState {
  loading: boolean;
  userUnreadNotificationsResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: UserUnreadNotificationsState = {
  loading: false,
  userUnreadNotificationsResp: null,
  error: undefined,
};

export const UserUnreadNotificationsSlice = createSlice({
  name: 'user unread notifications',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchUserUnreadNotificationsRequest: (state) => {
      return {
        loading: true,
        userUnreadNotificationsResp: null,
        error: undefined,
      };
    },
    fetchUserUnreadNotificationsSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        userUnreadNotificationsResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchUserUnreadNotificationsFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        userUnreadNotificationsResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const {
  fetchUserUnreadNotificationsRequest,
  fetchUserUnreadNotificationsSuccess,
  fetchUserUnreadNotificationsFailure,
} = UserUnreadNotificationsSlice.actions;

const fetchUserUnreadNotifications = () => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchUserUnreadNotificationsRequest());
    await getUserUnreadNotifications()
      .then((response) => {
        dispatch(fetchUserUnreadNotificationsSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchUserUnreadNotificationsFailure(error.message));
      });
  };
};
export { fetchUserUnreadNotifications };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.UserUnreadNotifications;

export default UserUnreadNotificationsSlice.reducer;
