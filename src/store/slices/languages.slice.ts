import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { getLanguages } from 'pages/api/dynamic/getData';

// Define a type for the slice state

// interface Lang {
//     code?: string
//     direction?: string
//     icon?: string
//     id?: number
//     name?: string
// }
//
// interface LanguagesApi {
//     language_list?: Lang[],
//     default_language?: Lang
// }

interface LanguagesState {
  loading: boolean;
  lang: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: LanguagesState = {
  loading: false,
  lang: {},
  error: undefined,
};

export const LanguagesSlice = createSlice({
  name: 'lang',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchLanguagesRequest: (state) => {
      return {
        ...state,
        loading: true,
      };
    },
    fetchLanguagesSuccess: (state, action: PayloadAction<any>) => {
      return {
        ...state,
        loading: false,
        lang: action.payload.data,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchLanguagesFailure: (state, action: PayloadAction<any>) => {
      return {
        ...state,
        loading: false,
        lang: {},
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchLanguagesRequest, fetchLanguagesSuccess, fetchLanguagesFailure } = LanguagesSlice.actions;

const fetchLanguages = () => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchLanguagesRequest());
    await getLanguages()
      .then((response) => {
        dispatch(fetchLanguagesSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchLanguagesFailure(error.message));
      });
  };
};
export { fetchLanguages };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.Languages;

export default LanguagesSlice.reducer;
