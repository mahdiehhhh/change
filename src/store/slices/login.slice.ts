import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { login } from 'pages/api/dynamic/getData';
import { ACCESS_TOKEN, IS_LOGGED_IN } from 'configs/variables.config';
import { handleSetTokens, setCookie } from 'utils/functions/local';

// Define a type for the slice state
interface LoginState {
  loading: boolean;
  loginResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: LoginState = {
  loading: false,
  loginResp: null,
  error: undefined,
};

export const LoginSlice = createSlice({
  name: 'login',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchLoginRequest: (state) => {
      return {
        loading: true,
        loginResp: null,
        error: undefined,
      };
    },
    fetchLoginSuccess: (state, action: PayloadAction<any>) => {
      // handleSetTokens(IS_LOGGED_IN, JSON.stringify(action.payload.data.token));
      setCookie(IS_LOGGED_IN, action.payload.data.token);
      return {
        loading: false,
        loginResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchLoginFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        loginResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchLoginRequest, fetchLoginSuccess, fetchLoginFailure } = LoginSlice.actions;

const fetchLogin = (req: object) => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchLoginRequest());
    await login(req)
      .then((response) => {
        dispatch(fetchLoginSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchLoginFailure(error.message));
      });
  };
};
export { fetchLogin };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.Login;

export default LoginSlice.reducer;
