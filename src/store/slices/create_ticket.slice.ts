import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { createTicket } from 'pages/api/dynamic/getData';

// Define a type for the slice state
interface CreateTicketState {
  loading: boolean;
  createTicketResp: any;
  error: any;
}

// Define the initial state using that type
const initialState: CreateTicketState = {
  loading: false,
  createTicketResp: null,
  error: undefined,
};

export const CreateTicketSlice = createSlice({
  name: 'create ticket',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    resetToInitial: () => {
      return initialState;
    },
    fetchCreateTicketRequest: (state) => {
      return {
        loading: true,
        createTicketResp: null,
        error: undefined,
      };
    },
    fetchCreateTicketSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        createTicketResp: action.payload,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchCreateTicketFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        createTicketResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { resetToInitial, fetchCreateTicketRequest, fetchCreateTicketSuccess, fetchCreateTicketFailure } =
  CreateTicketSlice.actions;

const fetchCreateNewTicket = (data: object | string) => {
  return async (dispatch: Dispatch) => {
    if (typeof data === 'string') {
      data === 'reset' && dispatch(resetToInitial());
    } else {
      dispatch(fetchCreateTicketRequest());
      await createTicket(data)
        .then((response) => {
          dispatch(fetchCreateTicketSuccess(response));
        })
        .catch((error) => {
          dispatch(fetchCreateTicketFailure(error));
        });
    }
  };
};
export { fetchCreateNewTicket };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.CreateTicket;

export default CreateTicketSlice.reducer;
