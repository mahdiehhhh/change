import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { getWalletTransactionTypes } from 'pages/api/dynamic/getData';
import { ACCESS_TOKEN, IS_LOGGED_IN } from '../../configs/variables.config';

// Define a type for the slice state
interface WalletTransactionTypesState {
  loading: boolean;
  walletTransactionTypesResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: WalletTransactionTypesState = {
  loading: false,
  walletTransactionTypesResp: null,
  error: undefined,
};

export const WalletTransactionTypesSlice = createSlice({
  name: 'wallet transaction types',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchWalletTransactionTypesRequest: (state) => {
      return {
        loading: true,
        walletTransactionTypesResp: null,
        error: undefined,
      };
    },
    fetchWalletTransactionTypesSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        walletTransactionTypesResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchWalletTransactionTypesFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        walletTransactionTypesResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const {
  fetchWalletTransactionTypesRequest,
  fetchWalletTransactionTypesSuccess,
  fetchWalletTransactionTypesFailure,
} = WalletTransactionTypesSlice.actions;

const fetchWalletTransactionTypes = (uid: string, transactionType: string) => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchWalletTransactionTypesRequest());
    await getWalletTransactionTypes(uid, transactionType)
      .then((response) => {
        dispatch(fetchWalletTransactionTypesSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchWalletTransactionTypesFailure(error.message));
      });
  };
};
export { fetchWalletTransactionTypes };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.WalletTransactionTypes;

export default WalletTransactionTypesSlice.reducer;
