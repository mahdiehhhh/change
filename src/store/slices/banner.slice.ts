import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { getBanner } from 'pages/api/dynamic/getData';
import { ACCESS_TOKEN, IS_LOGGED_IN } from '../../configs/variables.config';

// Define a type for the slice state
interface BannerState {
  loading: boolean;
  bannerResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: BannerState = {
  loading: false,
  bannerResp: null,
  error: undefined,
};

export const BannerSlice = createSlice({
  name: 'banner',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchBannerRequest: (state) => {
      return {
        loading: true,
        bannerResp: null,
        error: undefined,
      };
    },
    fetchBannerSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        bannerResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchBannerFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        bannerResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchBannerRequest, fetchBannerSuccess, fetchBannerFailure } = BannerSlice.actions;

const fetchBanner = () => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchBannerRequest());
    await getBanner()
      .then((response) => {
        dispatch(fetchBannerSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchBannerFailure(error.message));
      });
  };
};
export { fetchBanner };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.Banner;

export default BannerSlice.reducer;
