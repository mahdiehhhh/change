import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { getTicketsMessages } from 'pages/api/dynamic/getData';

// Define a type for the slice state
interface GetTicketsMessagesState {
  loading: boolean;
  getTicketsMsgResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: GetTicketsMessagesState = {
  loading: false,
  getTicketsMsgResp: null,
  error: undefined,
};

export const GetTicketsMessagesSlice = createSlice({
  name: 'get tickets messages',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchGetTicketsMsgRequest: (state) => {
      return {
        loading: true,
        getTicketsMsgResp: null,
        error: undefined,
      };
    },
    fetchGetTicketsMsgSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        getTicketsMsgResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchGetTicketsMsgFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        getTicketsMsgResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchGetTicketsMsgRequest, fetchGetTicketsMsgSuccess, fetchGetTicketsMsgFailure } =
  GetTicketsMessagesSlice.actions;

const fetchGetTicketsMessages = (id: string) => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchGetTicketsMsgRequest());
    await getTicketsMessages(id)
      .then((response) => {
        dispatch(fetchGetTicketsMsgSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchGetTicketsMsgFailure(error.message));
      });
  };
};
export { fetchGetTicketsMessages };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.GetTicketsMessages;

export default GetTicketsMessagesSlice.reducer;
