import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { postWalletDepositCashAmount } from 'pages/api/dynamic/getData';
import { ACCESS_TOKEN, IS_LOGGED_IN } from '../../configs/variables.config';

// Define a type for the slice state
interface WalletDepositCashAmountState {
  loading: boolean;
  walletDepositCashAmountResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: WalletDepositCashAmountState = {
  loading: false,
  walletDepositCashAmountResp: null,
  error: undefined,
};

export const WalletDepositCashAmountSlice = createSlice({
  name: 'wallet deposit cash amount',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchWalletDepositCashAmountRequest: (state) => {
      return {
        loading: true,
        walletDepositCashAmountResp: null,
        error: undefined,
      };
    },
    fetchWalletDepositCashAmountSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        walletDepositCashAmountResp: action.payload,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchWalletDepositCashAmountFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        walletDepositCashAmountResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const {
  fetchWalletDepositCashAmountRequest,
  fetchWalletDepositCashAmountSuccess,
  fetchWalletDepositCashAmountFailure,
} = WalletDepositCashAmountSlice.actions;

const fetchWalletDepositCashAmount = (uid: string, data: object) => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchWalletDepositCashAmountRequest());
    await postWalletDepositCashAmount(uid, data)
      .then((response) => {
        dispatch(fetchWalletDepositCashAmountSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchWalletDepositCashAmountFailure(error.response.statusCode));
      });
  };
};
export { fetchWalletDepositCashAmount };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.WalletDepositCashAmount;

export default WalletDepositCashAmountSlice.reducer;
