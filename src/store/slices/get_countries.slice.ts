import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { getCountries } from 'pages/api/dynamic/getData';

// Define a type for the slice state
interface GetCountriesState {
  loading: boolean;
  CountriesResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: GetCountriesState = {
  loading: false,
  CountriesResp: null,
  error: undefined,
};

export const GetCountriesSlice = createSlice({
  name: 'get countries',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchGetCountriesRequest: (state) => {
      return {
        loading: true,
        CountriesResp: null,
        error: undefined,
      };
    },
    fetchGetCountriesSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        CountriesResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchGetCountriesFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        CountriesResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchGetCountriesRequest, fetchGetCountriesSuccess, fetchGetCountriesFailure } =
  GetCountriesSlice.actions;

const fetchGetCountries = () => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchGetCountriesRequest());
    await getCountries()
      .then((response) => {
        dispatch(fetchGetCountriesSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchGetCountriesFailure(error.message));
      });
  };
};
export { fetchGetCountries };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.GetCountries;

export default GetCountriesSlice.reducer;
