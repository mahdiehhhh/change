import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { postWalletWithdrawCashAmount } from 'pages/api/dynamic/getData';
import { ACCESS_TOKEN, IS_LOGGED_IN } from '../../configs/variables.config';

// Define a type for the slice state
interface WalletWithdrawCashAmountState {
  loading: boolean;
  walletWithdrawCashAmountResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: WalletWithdrawCashAmountState = {
  loading: false,
  walletWithdrawCashAmountResp: null,
  error: undefined,
};

export const WalletWithdrawCashAmountSlice = createSlice({
  name: 'wallet withdraw cash amount',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchWalletWithdrawCashAmountRequest: (state) => {
      return {
        loading: true,
        walletWithdrawCashAmountResp: null,
        error: undefined,
      };
    },
    fetchWalletWithdrawCashAmountSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        walletWithdrawCashAmountResp: action.payload,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchWalletWithdrawCashAmountFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        walletWithdrawCashAmountResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const {
  fetchWalletWithdrawCashAmountRequest,
  fetchWalletWithdrawCashAmountSuccess,
  fetchWalletWithdrawCashAmountFailure,
} = WalletWithdrawCashAmountSlice.actions;

const fetchWalletWithdrawCashAmount = (uid: string, data: object) => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchWalletWithdrawCashAmountRequest());
    await postWalletWithdrawCashAmount(uid, data)
      .then((response) => {
        dispatch(fetchWalletWithdrawCashAmountSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchWalletWithdrawCashAmountFailure(error.response.statusCode));
      });
  };
};
export { fetchWalletWithdrawCashAmount };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.WalletWithdrawCashAmount;

export default WalletWithdrawCashAmountSlice.reducer;
