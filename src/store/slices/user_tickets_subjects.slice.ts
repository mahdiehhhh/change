import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { getUserTicketsSubjects } from 'pages/api/dynamic/getData';
import { ACCESS_TOKEN, IS_LOGGED_IN } from '../../configs/variables.config';

// Define a type for the slice state
interface UserTicketsSubjectsState {
  loading: boolean;
  userTicketsSubjectsResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: UserTicketsSubjectsState = {
  loading: false,
  userTicketsSubjectsResp: null,
  error: undefined,
};

export const UserTicketsSubjectsSlice = createSlice({
  name: 'user tickets subjects',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchUserTicketsSubjectsRequest: (state) => {
      return {
        loading: true,
        userTicketsSubjectsResp: null,
        error: undefined,
      };
    },
    fetchUserTicketsSubjectsSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        userTicketsSubjectsResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchUserTicketsSubjectsFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        userTicketsSubjectsResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchUserTicketsSubjectsRequest, fetchUserTicketsSubjectsSuccess, fetchUserTicketsSubjectsFailure } =
  UserTicketsSubjectsSlice.actions;

const fetchUserTicketsSubjects = () => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchUserTicketsSubjectsRequest());
    await getUserTicketsSubjects()
      .then((response) => {
        console.log('oiiweuroiwueopruoiwer', response);
        dispatch(fetchUserTicketsSubjectsSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchUserTicketsSubjectsFailure(error.message));
      });
  };
};
export { fetchUserTicketsSubjects };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.UserTicketsSubjects;

export default UserTicketsSubjectsSlice.reducer;
