import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { getUserFinance } from 'pages/api/dynamic/getData';
import { ACCESS_TOKEN, IS_LOGGED_IN } from '../../configs/variables.config';

// Define a type for the slice state
interface UserFinanceState {
  loading: boolean;
  userFinanceResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: UserFinanceState = {
  loading: false,
  userFinanceResp: null,
  error: undefined,
};

export const UserFinanceSlice = createSlice({
  name: 'user finance',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchUserFinanceRequest: (state) => {
      return {
        loading: true,
        userFinanceResp: null,
        error: undefined,
      };
    },
    fetchUserFinanceSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        userFinanceResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchUserFinanceFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        userFinanceResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchUserFinanceRequest, fetchUserFinanceSuccess, fetchUserFinanceFailure } = UserFinanceSlice.actions;

const fetchUserFinance = () => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchUserFinanceRequest());
    await getUserFinance()
      .then((response) => {
        dispatch(fetchUserFinanceSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchUserFinanceFailure(error.message));
      });
  };
};
export { fetchUserFinance };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.UserFinance;

export default UserFinanceSlice.reducer;
