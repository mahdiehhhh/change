import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { addCurrencyToFavorite } from 'pages/api/dynamic/getData';

// Define a type for the slice state
interface AddCurrencyToFavoriteState {
  loading: boolean;
  addCurrencyFavoriteResp: any;
  error: any;
}

// Define the initial state using that type
const initialState: AddCurrencyToFavoriteState = {
  loading: false,
  addCurrencyFavoriteResp: null,
  error: {},
};

export const AddCurrencyToFavoriteSlice = createSlice({
  name: 'add currency to favorite',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchAddCurrencyToFavoriteRequest: (state) => {
      return {
        loading: true,
        addCurrencyFavoriteResp: null,
        error: {},
      };
    },
    fetchAddCurrencyToFavoriteSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        addCurrencyFavoriteResp: action.payload.data,
        error: {},
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchAddCurrencyToFavoriteFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        addCurrencyFavoriteResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const {
  fetchAddCurrencyToFavoriteRequest,
  fetchAddCurrencyToFavoriteSuccess,
  fetchAddCurrencyToFavoriteFailure,
} = AddCurrencyToFavoriteSlice.actions;

const fetchAddCurrencyFavorite = (data: object) => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchAddCurrencyToFavoriteRequest());
    await addCurrencyToFavorite(data)
      .then((response) => {
        dispatch(fetchAddCurrencyToFavoriteSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchAddCurrencyToFavoriteFailure(error.message));
      });
  };
};
export { fetchAddCurrencyFavorite };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.AddCurrencyToFavorite;

export default AddCurrencyToFavoriteSlice.reducer;
