import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { postWalletTransferInfoCheck } from 'pages/api/dynamic/getData';

// Define a type for the slice state
interface checkWalletTransferInfoState {
  loading: boolean;
  checkWalletTransferResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: checkWalletTransferInfoState = {
  loading: false,
  checkWalletTransferResp: null,
  error: undefined,
};

export const CheckWalletTransferInfoSlice = createSlice({
  name: 'check wallet transfer information',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    resetToInitial: () => {
      return initialState;
    },
    fetchCheckWalletTransferRequest: (state) => {
      return {
        loading: true,
        checkWalletTransferResp: null,
        error: undefined,
      };
    },
    fetchCheckWalletTransferSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        checkWalletTransferResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchCheckWalletTransferFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        checkWalletTransferResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const {
  resetToInitial,
  fetchCheckWalletTransferRequest,
  fetchCheckWalletTransferSuccess,
  fetchCheckWalletTransferFailure,
} = CheckWalletTransferInfoSlice.actions;

const fetchCheckWalletTransfer = (originUid: string, data?: object) => {
  return async (dispatch: Dispatch) => {
    if (!data) {
      originUid === 'reset' && dispatch(resetToInitial());
    } else {
      dispatch(fetchCheckWalletTransferRequest());
      await postWalletTransferInfoCheck(originUid, data)
        .then((response) => {
          dispatch(fetchCheckWalletTransferSuccess(response));
        })
        .catch((error) => {
          dispatch(fetchCheckWalletTransferFailure(error.message));
        });
    }
  };
};
export { fetchCheckWalletTransfer };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.CheckWalletTransfer;

export default CheckWalletTransferInfoSlice.reducer;
