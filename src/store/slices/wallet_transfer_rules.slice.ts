import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { getWalletTransferRules } from 'pages/api/dynamic/getData';

// Define a type for the slice state
interface WalletTransferRulesState {
  loading: boolean;
  walletTransferRulesResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: WalletTransferRulesState = {
  loading: false,
  walletTransferRulesResp: null,
  error: undefined,
};

export const WalletTransferRulesSlice = createSlice({
  name: 'wallet transfer rules',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchWalletTransferRulesRequest: (state) => {
      return {
        loading: true,
        walletTransferRulesResp: null,
        error: undefined,
      };
    },
    fetchWalletTransferRulesSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        walletTransferRulesResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchWalletTransferRulesFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        walletTransferRulesResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchWalletTransferRulesRequest, fetchWalletTransferRulesSuccess, fetchWalletTransferRulesFailure } =
  WalletTransferRulesSlice.actions;

const fetchWalletTransferRules = (originUid: string, destinationUid: string) => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchWalletTransferRulesRequest());
    await getWalletTransferRules(originUid, destinationUid)
      .then((response) => {
        dispatch(fetchWalletTransferRulesSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchWalletTransferRulesFailure(error.message));
      });
  };
};
export { fetchWalletTransferRules };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.WalletTransferRules;

export default WalletTransferRulesSlice.reducer;
