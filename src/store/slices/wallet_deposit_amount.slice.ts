import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { postWalletDepositAmount } from 'pages/api/dynamic/getData';
import { ACCESS_TOKEN, IS_LOGGED_IN } from '../../configs/variables.config';

// Define a type for the slice state
interface WalletDepositAmountState {
  loading: boolean;
  walletDepositAmountResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: WalletDepositAmountState = {
  loading: false,
  walletDepositAmountResp: null,
  error: undefined,
};

export const WalletDepositAmountSlice = createSlice({
  name: 'wallet deposit amount',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    resetToInitial: () => {
      return initialState;
    },
    fetchWalletDepositAmountRequest: (state) => {
      return {
        loading: true,
        walletDepositAmountResp: null,
        error: undefined,
      };
    },
    fetchWalletDepositAmountSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        walletDepositAmountResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchWalletDepositAmountFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        walletDepositAmountResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const {
  resetToInitial,
  fetchWalletDepositAmountRequest,
  fetchWalletDepositAmountSuccess,
  fetchWalletDepositAmountFailure,
} = WalletDepositAmountSlice.actions;

const fetchWalletDepositAmount = (uid: string, data?: object) => {
  return async (dispatch: Dispatch) => {
    if (!data) {
      uid === 'reset' && dispatch(resetToInitial());
    } else {
      dispatch(fetchWalletDepositAmountRequest());
      await postWalletDepositAmount(uid, data)
        .then((response) => {
          dispatch(fetchWalletDepositAmountSuccess(response));
        })
        .catch((error) => {
          dispatch(fetchWalletDepositAmountFailure(error.response.statusCode));
        });
    }
  };
};
export { fetchWalletDepositAmount };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.WalletDepositAmount;

export default WalletDepositAmountSlice.reducer;
