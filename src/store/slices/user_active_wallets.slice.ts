import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { getUserActiveWallets } from 'pages/api/dynamic/getData';

// Define a type for the slice state
interface UserActiveWalletsState {
  loading: boolean;
  userActWalletsResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: UserActiveWalletsState = {
  loading: false,
  userActWalletsResp: null,
  error: undefined,
};

export const UserActiveWalletsSlice = createSlice({
  name: 'currencies dashboard',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchUserActiveWalletsRequest: (state) => {
      return {
        loading: true,
        userActWalletsResp: null,
        error: undefined,
      };
    },
    fetchUserActiveWalletsSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        userActWalletsResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchUserActiveWalletsFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        userActWalletsResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchUserActiveWalletsRequest, fetchUserActiveWalletsSuccess, fetchUserActiveWalletsFailure } =
  UserActiveWalletsSlice.actions;

const fetchUserActiveWallets = () => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchUserActiveWalletsRequest());
    await getUserActiveWallets()
      .then((response) => {
        dispatch(fetchUserActiveWalletsSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchUserActiveWalletsFailure(error.message));
      });
  };
};
export { fetchUserActiveWallets };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.UserActiveWallets;

export default UserActiveWalletsSlice.reducer;
