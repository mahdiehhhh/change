import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { getCurrenciesDashboard } from 'pages/api/dynamic/getData';

// Define a type for the slice state
interface CurrenciesDashboardState {
  loading: boolean;
  currenciesDashResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: CurrenciesDashboardState = {
  loading: false,
  currenciesDashResp: null,
  error: undefined,
};

export const CurrenciesDashboardSlice = createSlice({
  name: 'currencies dashboard',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchCurrenciesDashboardRequest: (state) => {
      return {
        loading: true,
        currenciesDashResp: null,
        error: undefined,
      };
    },
    fetchCurrenciesDashboardSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        currenciesDashResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchCurrenciesDashboardFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        currenciesDashResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchCurrenciesDashboardRequest, fetchCurrenciesDashboardSuccess, fetchCurrenciesDashboardFailure } =
  CurrenciesDashboardSlice.actions;

const fetchCurrenciesDashboard = () => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchCurrenciesDashboardRequest());
    await getCurrenciesDashboard()
      .then((response) => {
        dispatch(fetchCurrenciesDashboardSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchCurrenciesDashboardFailure(error.message));
      });
  };
};
export { fetchCurrenciesDashboard };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.CurrenciesDashboard;

export default CurrenciesDashboardSlice.reducer;
