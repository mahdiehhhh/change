import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { sendTicketsMessages } from 'pages/api/dynamic/getData';

// Define a type for the slice state
interface SendTicketsMessagesState {
  loading: boolean;
  sendTicketsMsgResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: SendTicketsMessagesState = {
  loading: false,
  sendTicketsMsgResp: null,
  error: undefined,
};

export const SendTicketsMessagesSlice = createSlice({
  name: 'send tickets messages',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchSendTicketsMsgRequest: (state) => {
      return {
        loading: true,
        sendTicketsMsgResp: null,
        error: undefined,
      };
    },
    fetchSendTicketsMsgSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        sendTicketsMsgResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchSendTicketsMsgFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        sendTicketsMsgResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchSendTicketsMsgRequest, fetchSendTicketsMsgSuccess, fetchSendTicketsMsgFailure } =
  SendTicketsMessagesSlice.actions;

const fetchSendTicketsMessages = (id: string, data: object) => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchSendTicketsMsgRequest());
    await sendTicketsMessages(id, data)
      .then((response) => {
        dispatch(fetchSendTicketsMsgSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchSendTicketsMsgFailure(error.message));
      });
  };
};
export { fetchSendTicketsMessages };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.SendTicketsMessages;

export default SendTicketsMessagesSlice.reducer;
