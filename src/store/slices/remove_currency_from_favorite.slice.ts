import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { removeCurrencyFromFavorite } from 'pages/api/dynamic/getData';

// Define a type for the slice state
interface RemoveCurrencyFromFavoriteState {
  loading: boolean;
  RemoveCurrencyFavoriteResp: any;
  error: any;
}

// Define the initial state using that type
const initialState: RemoveCurrencyFromFavoriteState = {
  loading: false,
  RemoveCurrencyFavoriteResp: null,
  error: {},
};

export const RemoveCurrencyFromFavoriteSlice = createSlice({
  name: 'remove currency from favorite',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchRemoveCurrencyFromFavoriteRequest: (state) => {
      return {
        loading: true,
        RemoveCurrencyFavoriteResp: null,
        error: {},
      };
    },
    fetchRemoveCurrencyFromFavoriteSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        RemoveCurrencyFavoriteResp: action.payload.data,
        error: {},
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchRemoveCurrencyFromFavoriteFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        RemoveCurrencyFavoriteResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const {
  fetchRemoveCurrencyFromFavoriteRequest,
  fetchRemoveCurrencyFromFavoriteSuccess,
  fetchRemoveCurrencyFromFavoriteFailure,
} = RemoveCurrencyFromFavoriteSlice.actions;

const fetchRemoveCurrencyFavorite = (data: object) => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchRemoveCurrencyFromFavoriteRequest());
    await removeCurrencyFromFavorite(data)
      .then((response) => {
        dispatch(fetchRemoveCurrencyFromFavoriteSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchRemoveCurrencyFromFavoriteFailure(error.message));
      });
  };
};
export { fetchRemoveCurrencyFavorite };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.RemoveCurrencyFromFavorite;

export default RemoveCurrencyFromFavoriteSlice.reducer;
