import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { getUserBankAccounts } from 'pages/api/dynamic/getData';

// Define a type for the slice state
interface UserBankAccountsState {
  loading: boolean;
  userBankAccountsResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: UserBankAccountsState = {
  loading: false,
  userBankAccountsResp: null,
  error: undefined,
};

export const UserBankAccountsSlice = createSlice({
  name: 'user bank accounts',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchUserBankAccountsRequest: (state) => {
      return {
        loading: true,
        userBankAccountsResp: null,
        error: undefined,
      };
    },
    fetchUserBankAccountsSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        userBankAccountsResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchUserBankAccountsFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        userBankAccountsResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchUserBankAccountsRequest, fetchUserBankAccountsSuccess, fetchUserBankAccountsFailure } =
  UserBankAccountsSlice.actions;

const fetchUserBankAccounts = (country: string) => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchUserBankAccountsRequest());
    await getUserBankAccounts(country)
      .then((response) => {
        dispatch(fetchUserBankAccountsSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchUserBankAccountsFailure(error.message));
      });
  };
};
export { fetchUserBankAccounts };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.UserBankAccounts;

export default UserBankAccountsSlice.reducer;
