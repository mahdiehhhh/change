import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { postWalletTransferInfoConfirm } from 'pages/api/dynamic/getData';

// Define a type for the slice state
interface confirmWalletTransferInfoState {
  loading: boolean;
  confirmWalletTransferResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: confirmWalletTransferInfoState = {
  loading: false,
  confirmWalletTransferResp: null,
  error: undefined,
};

export const ConfirmWalletTransferInfoSlice = createSlice({
  name: 'confirm wallet transfer information',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    resetToInitial: () => {
      return initialState;
    },
    fetchConfirmWalletTransferRequest: (state) => {
      return {
        loading: true,
        confirmWalletTransferResp: null,
        error: undefined,
      };
    },
    fetchConfirmWalletTransferSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        confirmWalletTransferResp: action.payload,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchConfirmWalletTransferFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        confirmWalletTransferResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const {
  resetToInitial,
  fetchConfirmWalletTransferRequest,
  fetchConfirmWalletTransferSuccess,
  fetchConfirmWalletTransferFailure,
} = ConfirmWalletTransferInfoSlice.actions;

const fetchConfirmWalletTransfer = (originUid: string, data?: object) => {
  return async (dispatch: Dispatch) => {
    if (!data) {
      originUid === 'reset' && dispatch(resetToInitial());
    } else {
      dispatch(fetchConfirmWalletTransferRequest());
      await postWalletTransferInfoConfirm(originUid, data)
        .then((response) => {
          dispatch(fetchConfirmWalletTransferSuccess(response));
        })
        .catch((error) => {
          dispatch(fetchConfirmWalletTransferFailure(error.message));
        });
    }
  };
};
export { fetchConfirmWalletTransfer };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.ConfirmWalletTransfer;

export default ConfirmWalletTransferInfoSlice.reducer;
