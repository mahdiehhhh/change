import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { forgetPassword } from 'pages/api/dynamic/getData';
import { ACCESS_TOKEN, IS_LOGGED_IN } from '../../configs/variables.config';

// Define a type for the slice state
interface ForgetPasswordState {
  loading: boolean;
  forgetPassResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: ForgetPasswordState = {
  loading: false,
  forgetPassResp: null,
  error: undefined,
};

export const ForgetPasswordSlice = createSlice({
  name: 'forget password',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchForgetPasswordRequest: (state) => {
      return {
        loading: true,
        forgetPassResp: null,
        error: undefined,
      };
    },
    fetchForgetPasswordSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        forgetPassResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchForgetPasswordFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        forgetPassResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchForgetPasswordRequest, fetchForgetPasswordSuccess, fetchForgetPasswordFailure } =
  ForgetPasswordSlice.actions;

const fetchForgetPassword = (req: object) => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchForgetPasswordRequest());
    await forgetPassword(req)
      .then((response) => {
        dispatch(fetchForgetPasswordSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchForgetPasswordFailure(error.message));
      });
  };
};
export { fetchForgetPassword };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.ForgetPassword;

export default ForgetPasswordSlice.reducer;
