import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { getWalletLatestTransaction } from 'pages/api/dynamic/getData';
import { ACCESS_TOKEN, IS_LOGGED_IN } from '../../configs/variables.config';

// Define a type for the slice state
interface WalletLatestTransactionState {
  loading: boolean;
  walletLatestTransResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: WalletLatestTransactionState = {
  loading: false,
  walletLatestTransResp: null,
  error: undefined,
};

export const WalletLatestTransactionSlice = createSlice({
  name: 'wallet latest transaction',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchWalletLatestTransactionRequest: (state) => {
      return {
        loading: true,
        walletLatestTransResp: null,
        error: undefined,
      };
    },
    fetchWalletLatestTransactionSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        walletLatestTransResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchWalletLatestTransactionFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        walletLatestTransResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const {
  fetchWalletLatestTransactionRequest,
  fetchWalletLatestTransactionSuccess,
  fetchWalletLatestTransactionFailure,
} = WalletLatestTransactionSlice.actions;

const fetchWalletLatestTransaction = (uid: string) => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchWalletLatestTransactionRequest());
    await getWalletLatestTransaction(uid)
      .then((response) => {
        dispatch(fetchWalletLatestTransactionSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchWalletLatestTransactionFailure(error.message));
      });
  };
};
export { fetchWalletLatestTransaction };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.WalletLatestTransaction;

export default WalletLatestTransactionSlice.reducer;
