import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { getLatestWalletTransfers } from 'pages/api/dynamic/getData';
import { ACCESS_TOKEN, IS_LOGGED_IN } from '../../configs/variables.config';

// Define a type for the slice state
interface WalletLatestTransfersState {
  loading: boolean;
  walletLatestTransfersResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: WalletLatestTransfersState = {
  loading: false,
  walletLatestTransfersResp: null,
  error: undefined,
};

export const WalletLatestTransfersSlice = createSlice({
  name: 'wallet latest transfers',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchWalletLatestTransfersRequest: (state) => {
      return {
        loading: true,
        walletLatestTransfersResp: null,
        error: undefined,
      };
    },
    fetchWalletLatestTransfersSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        walletLatestTransfersResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchWalletLatestTransfersFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        walletLatestTransfersResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const {
  fetchWalletLatestTransfersRequest,
  fetchWalletLatestTransfersSuccess,
  fetchWalletLatestTransfersFailure,
} = WalletLatestTransfersSlice.actions;

const fetchWalletLatestTransfers = () => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchWalletLatestTransfersRequest());
    await getLatestWalletTransfers()
      .then((response) => {
        dispatch(fetchWalletLatestTransfersSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchWalletLatestTransfersFailure(error.message));
      });
  };
};
export { fetchWalletLatestTransfers };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.WalletLatestTransfers;

export default WalletLatestTransfersSlice.reducer;
