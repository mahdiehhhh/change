import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { getFeesList } from 'pages/api/dynamic/getData';

// Define a type for the slice state
interface FeesListState {
  loading: boolean;
  feesListResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: FeesListState = {
  loading: false,
  feesListResp: null,
  error: undefined,
};

export const FeesListSlice = createSlice({
  name: 'fees list',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchFeesListRequest: (state) => {
      return {
        loading: true,
        feesListResp: null,
        error: undefined,
      };
    },
    fetchFeesListSuccess: (state, action: PayloadAction<any>) => {
      console.log('fee list success', action.payload);
      return {
        loading: false,
        feesListResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchFeesListFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        feesListResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchFeesListRequest, fetchFeesListSuccess, fetchFeesListFailure } = FeesListSlice.actions;

const fetchFeesList = () => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchFeesListRequest());
    await getFeesList()
      .then((response) => {
        dispatch(fetchFeesListSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchFeesListFailure(error.message));
      });
  };
};
export { fetchFeesList };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.FeesList;

export default FeesListSlice.reducer;
