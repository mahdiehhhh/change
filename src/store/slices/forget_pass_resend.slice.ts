import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { forgetPassResend } from 'pages/api/dynamic/getData';
import { ACCESS_TOKEN, IS_LOGGED_IN } from '../../configs/variables.config';

// Define a type for the slice state
interface ForgetPassResendState {
  loading: boolean;
  forgetPassResendResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: ForgetPassResendState = {
  loading: false,
  forgetPassResendResp: null,
  error: undefined,
};

export const ForgetPasswordResendSlice = createSlice({
  name: 'forget password resend',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    resetToInitial: () => {
      return initialState;
    },
    fetchForgetPassResendRequest: (state) => {
      return {
        loading: true,
        forgetPassResendResp: null,
        error: undefined,
      };
    },
    fetchForgetPassResendSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        forgetPassResendResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchForgetPassResendFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        forgetPassResendResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const {
  resetToInitial,
  fetchForgetPassResendRequest,
  fetchForgetPassResendSuccess,
  fetchForgetPassResendFailure,
} = ForgetPasswordResendSlice.actions;

const fetchForgetPassResend = (req: object | string) => {
  return async (dispatch: Dispatch) => {
    if (typeof req === 'string') {
      req === 'reset' && dispatch(resetToInitial());
    } else {
      dispatch(fetchForgetPassResendRequest());
      await forgetPassResend(req)
        .then((response) => {
          dispatch(fetchForgetPassResendSuccess(response));
        })
        .catch((error) => {
          dispatch(fetchForgetPassResendFailure(error.message));
        });
    }
  };
};
export { fetchForgetPassResend };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.ForgetPasswordResend;

export default ForgetPasswordResendSlice.reducer;
