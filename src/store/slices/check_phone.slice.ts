import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { checkPhone } from 'pages/api/dynamic/getData';

// Define a type for the slice state
interface CheckPhoneState {
  loading: boolean;
  checkPhoneResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: CheckPhoneState = {
  loading: true,
  checkPhoneResp: null,
  error: undefined,
};

export const CheckPhoneSlice = createSlice({
  name: 'check phone',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    resetToInitial: () => {
      return initialState;
    },
    fetchCheckPhoneRequest: (state) => {
      return {
        ...state,
        loading: true,
      };
    },
    fetchCheckPhoneSuccess: (state, action: PayloadAction<any>) => {
      return {
        ...state,
        loading: true,
        checkPhoneResp: action.payload.data.action,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchCheckPhoneFailure: (state, action: PayloadAction<any>) => {
      return {
        ...state,
        loading: false,
        checkPhoneResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { resetToInitial, fetchCheckPhoneRequest, fetchCheckPhoneSuccess, fetchCheckPhoneFailure } =
  CheckPhoneSlice.actions;

const fetchCheckPhone = (req: object | string) => {
  return async (dispatch: Dispatch) => {
    if (typeof req === 'string') {
      req === 'reset' && dispatch(resetToInitial());
    } else {
      dispatch(fetchCheckPhoneRequest());
      await checkPhone(req)
        .then((response) => {
          dispatch(fetchCheckPhoneSuccess(response));
        })
        .catch((error) => {
          dispatch(fetchCheckPhoneFailure(error.message));
        });
    }
  };
};
export { fetchCheckPhone };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.CheckPhone;

export default CheckPhoneSlice.reducer;
