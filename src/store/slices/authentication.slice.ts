import { createSlice, Dispatch, PayloadAction, createAsyncThunk } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { getAccessToken } from 'pages/api/dynamic/getData';
import { ACCESS_TOKEN, ACCESS_TOKEN_EXPIRE, IS_LOGGED_IN } from 'configs/variables.config';
import { handleGetTokens, handleSetTokens, setCookie } from '../../utils/functions/local';

// Define a type for the slice state
interface LoginState {
  loading: boolean;
  authResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: LoginState = {
  loading: false,
  authResp: {},
  error: undefined,
};

export const AccessTokenSlice = createSlice({
  name: 'access token',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchAccessTokenRequest: (state) => {
      return {
        ...state,
        loading: true,
      };
    },
    fetchAccessTokenSuccess: (state, action: PayloadAction<any>) => {
      // handleSetTokens(ACCESS_TOKEN, JSON.stringify(action.payload.access_token));
      setCookie(ACCESS_TOKEN, action.payload.access_token);
      // handleSetTokens(ACCESS_TOKEN_EXPIRE, JSON.stringify(+Date.now() + +action.payload.expires_in));
      setCookie(ACCESS_TOKEN_EXPIRE, +Date.now() + +action.payload.expires_in);
      return {
        ...state,
        loading: false,
        authResp: action.payload,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchAccessTokenFailure: (state, action: PayloadAction<any>) => {
      return {
        ...state,
        loading: false,
        authResp: undefined,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchAccessTokenRequest, fetchAccessTokenSuccess, fetchAccessTokenFailure } = AccessTokenSlice.actions;

const fetchAccessToken = () => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchAccessTokenRequest());
    await getAccessToken()
      .then((response) => {
        dispatch(fetchAccessTokenSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchAccessTokenFailure(error.message));
      });
  };
};
export { fetchAccessToken };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.AccessToken;

export default AccessTokenSlice.reducer;
