import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { postTransferDestinationWallet } from 'pages/api/dynamic/getData';

// Define a type for the slice state
interface transferDestinationWalletState {
  loading: boolean;
  transferDestWalletResp: any;
  error: any;
}

// Define the initial state using that type
const initialState: transferDestinationWalletState = {
  loading: false,
  transferDestWalletResp: null,
  error: undefined,
};

export const TransferDestinationWalletSlice = createSlice({
  name: 'transfer destination wallet',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    resetToInitial: () => {
      console.log('dest wallet resettttted');
      return initialState;
    },
    fetchTransferDestWalletRequest: (state) => {
      return {
        loading: true,
        transferDestWalletResp: null,
        error: undefined,
      };
    },
    fetchTransferDestWalletSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        transferDestWalletResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchTransferDestWalletFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        transferDestWalletResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const {
  resetToInitial,
  fetchTransferDestWalletRequest,
  fetchTransferDestWalletSuccess,
  fetchTransferDestWalletFailure,
} = TransferDestinationWalletSlice.actions;

const fetchFindTransferDestWallet = (originUid: string, data?: object) => {
  return async (dispatch: Dispatch) => {
    if (!data) {
      originUid === 'reset' && dispatch(resetToInitial());
    } else {
      dispatch(fetchTransferDestWalletRequest());
      await postTransferDestinationWallet(originUid, data)
        .then((response) => {
          dispatch(fetchTransferDestWalletSuccess(response));
        })
        .catch((error) => {
          console.log('oweiruiowerw', error);
          dispatch(fetchTransferDestWalletFailure(error.response.data));
        });
    }
  };
};
export { fetchFindTransferDestWallet };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.FindTransferDestWallet;

export default TransferDestinationWalletSlice.reducer;
