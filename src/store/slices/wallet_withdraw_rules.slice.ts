import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { getWalletWithdrawRules } from 'pages/api/dynamic/getData';
import { ACCESS_TOKEN, IS_LOGGED_IN } from '../../configs/variables.config';

// Define a type for the slice state
interface WalletWithdrawRulesState {
  loading: boolean;
  walletWithdrawRulesResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: WalletWithdrawRulesState = {
  loading: false,
  walletWithdrawRulesResp: null,
  error: undefined,
};

export const WalletWithdrawRulesSlice = createSlice({
  name: 'wallet withdraw rules',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchWalletWithdrawRulesRequest: (state) => {
      return {
        loading: true,
        walletWithdrawRulesResp: null,
        error: undefined,
      };
    },
    fetchWalletWithdrawRulesSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        walletWithdrawRulesResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchWalletWithdrawRulesFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        walletWithdrawRulesResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchWalletWithdrawRulesRequest, fetchWalletWithdrawRulesSuccess, fetchWalletWithdrawRulesFailure } =
  WalletWithdrawRulesSlice.actions;

const fetchWalletWithdrawRules = (uid: string) => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchWalletWithdrawRulesRequest());
    await getWalletWithdrawRules(uid)
      .then((response) => {
        dispatch(fetchWalletWithdrawRulesSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchWalletWithdrawRulesFailure(error.message));
      });
  };
};
export { fetchWalletWithdrawRules };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.WalletWithdrawRules;

export default WalletWithdrawRulesSlice.reducer;
