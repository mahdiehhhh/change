import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { postTransferDestinationUser } from 'pages/api/dynamic/getData';

// Define a type for the slice state
interface transferDestinationUserState {
  loading: boolean;
  transferDestUserResp: any;
  error: any;
}

// Define the initial state using that type
const initialState: transferDestinationUserState = {
  loading: false,
  transferDestUserResp: null,
  error: undefined,
};

export const TransferDestinationUserSlice = createSlice({
  name: 'transfer destination user',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    resetToInitial: () => {
      console.log('dest user resettttted');
      return initialState;
    },
    fetchTransferDestUserRequest: (state) => {
      return {
        loading: true,
        transferDestUserResp: null,
        error: undefined,
      };
    },
    fetchTransferDestUserSuccess: (state, action: PayloadAction<any>) => {
      console.log('dest user action payload', action.payload);
      return {
        loading: false,
        transferDestUserResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchTransferDestUserFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        transferDestUserResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const {
  resetToInitial,
  fetchTransferDestUserRequest,
  fetchTransferDestUserSuccess,
  fetchTransferDestUserFailure,
} = TransferDestinationUserSlice.actions;

const fetchFindTransferDestUser = (originUid: string, data?: object) => {
  return async (dispatch: Dispatch) => {
    if (!data) {
      originUid === 'reset' && dispatch(resetToInitial());
    } else {
      console.log('response for user 11111');
      dispatch(fetchTransferDestUserRequest());
      await postTransferDestinationUser(originUid, data)
        .then((response) => {
          console.log('response for user', response);
          dispatch(fetchTransferDestUserSuccess(response));
        })
        .catch((error) => {
          console.log('oweiruiowerw', error);
          dispatch(fetchTransferDestUserFailure(error.response.data));
        });
    }
  };
};
export { fetchFindTransferDestUser };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.FindTransferDestUser;

export default TransferDestinationUserSlice.reducer;
