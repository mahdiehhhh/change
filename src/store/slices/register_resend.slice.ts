import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { registerResend } from 'pages/api/dynamic/getData';
import { ACCESS_TOKEN, IS_LOGGED_IN } from '../../configs/variables.config';

// Define a type for the slice state
interface RegResendState {
  loading: boolean;
  regResendResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: RegResendState = {
  loading: false,
  regResendResp: null,
  error: undefined,
};

export const RegResendSlice = createSlice({
  name: 'register resend',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    resetToInitial: () => {
      return initialState;
    },
    fetchRegResendRequest: (state) => {
      return {
        loading: true,
        regResendResp: null,
        error: undefined,
      };
    },
    fetchRegResendSuccess: (state, action: PayloadAction<any>) => {
      return {
        ...state,
        loading: false,
        regResendResp: action.payload,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchRegResendFailure: (state, action: PayloadAction<any>) => {
      return {
        ...state,
        loading: false,
        regResendResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { resetToInitial, fetchRegResendRequest, fetchRegResendSuccess, fetchRegResendFailure } =
  RegResendSlice.actions;

const fetchRegResend = (req: object | string) => {
  return async (dispatch: Dispatch) => {
    if (typeof req === 'string') {
      req === 'reset' && dispatch(resetToInitial());
    } else {
      dispatch(fetchRegResendRequest());
      await registerResend(req)
        .then((response) => {
          dispatch(fetchRegResendSuccess(response));
        })
        .catch((error) => {
          dispatch(fetchRegResendFailure(error.message));
        });
    }
  };
};
export { fetchRegResend };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.RegisterResend;

export default RegResendSlice.reducer;
