import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { createIranBankAccount } from 'pages/api/dynamic/getData';

// Define a type for the slice state
interface CreateIranBankAccountState {
  loading: boolean;
  createIranBankAccResp: any;
  error: any;
}

// Define the initial state using that type
const initialState: CreateIranBankAccountState = {
  loading: false,
  createIranBankAccResp: null,
  error: {},
};

export const CreateIranBankAccountSlice = createSlice({
  name: 'create iran bank account',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchCreateIranBankAccRequest: (state) => {
      return {
        loading: true,
        createIranBankAccResp: null,
        error: {},
      };
    },
    fetchCreateIranBankAccSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        createIranBankAccResp: action.payload,
        error: {},
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchCreateIranBankAccFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        createIranBankAccResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchCreateIranBankAccRequest, fetchCreateIranBankAccSuccess, fetchCreateIranBankAccFailure } =
  CreateIranBankAccountSlice.actions;

const fetchCreateIranBankAccount = (data: object) => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchCreateIranBankAccRequest());
    await createIranBankAccount(data)
      .then((response) => {
        dispatch(fetchCreateIranBankAccSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchCreateIranBankAccFailure(error.response.data.errors));
      });
  };
};
export { fetchCreateIranBankAccount };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.CreateIranBankAccount;

export default CreateIranBankAccountSlice.reducer;
