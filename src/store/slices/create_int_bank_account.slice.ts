import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { createIntBankAccount } from 'pages/api/dynamic/getData';

// Define a type for the slice state
interface CreateIntBankAccountState {
  loading: boolean;
  createIntBankAccResp: any;
  error: any;
}

// Define the initial state using that type
const initialState: CreateIntBankAccountState = {
  loading: false,
  createIntBankAccResp: null,
  error: {},
};

export const CreateIntBankAccountSlice = createSlice({
  name: 'create international bank account',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchCreateIntBankAccRequest: (state) => {
      return {
        loading: true,
        createIntBankAccResp: null,
        error: {},
      };
    },
    fetchCreateIntBankAccSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        createIntBankAccResp: action.payload,
        error: {},
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchCreateIntBankAccFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        createIntBankAccResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchCreateIntBankAccRequest, fetchCreateIntBankAccSuccess, fetchCreateIntBankAccFailure } =
  CreateIntBankAccountSlice.actions;

const fetchCreateIntBankAccount = (data: object) => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchCreateIntBankAccRequest());
    await createIntBankAccount(data)
      .then((response) => {
        dispatch(fetchCreateIntBankAccSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchCreateIntBankAccFailure(error.response.data.errors));
      });
  };
};
export { fetchCreateIntBankAccount };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.CreateInternationalBankAccount;

export default CreateIntBankAccountSlice.reducer;
