import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { postAuthenticationVerifyData } from 'pages/api/dynamic/getData';
import { ACCESS_TOKEN, IS_LOGGED_IN } from '../../configs/variables.config';

// Define a type for the slice state
interface profileVerifyAuthState {
  loading: boolean;
  profileVerifyAuthResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: profileVerifyAuthState = {
  loading: false,
  profileVerifyAuthResp: null,
  error: undefined,
};

export const ProfileVerifyAuthenticationSlice = createSlice({
  name: 'profile verify authentication',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    resetToInitial: () => {
      console.log('profile verify authentication reseted');
      return initialState;
    },
    fetchProfileVerifyAuthRequest: (state) => {
      return {
        loading: true,
        profileVerifyAuthResp: null,
        error: undefined,
      };
    },
    fetchProfileVerifyAuthSuccess: (state, action: PayloadAction<any>) => {
      return {
        ...state,
        loading: false,
        profileVerifyAuthResp: action.payload,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchProfileVerifyAuthFailure: (state, action: PayloadAction<any>) => {
      return {
        ...state,
        loading: false,
        profileVerifyAuthResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const {
  resetToInitial,
  fetchProfileVerifyAuthRequest,
  fetchProfileVerifyAuthSuccess,
  fetchProfileVerifyAuthFailure,
} = ProfileVerifyAuthenticationSlice.actions;

const fetchProfileVerifyAuth = (req: object | string) => {
  return async (dispatch: Dispatch) => {
    if (typeof req === 'string') {
      req === 'reset' && dispatch(resetToInitial());
    } else {
      dispatch(fetchProfileVerifyAuthRequest());
      await postAuthenticationVerifyData(req)
        .then((response) => {
          dispatch(fetchProfileVerifyAuthSuccess(response));
        })
        .catch((error) => {
          console.log('what is error.res', error.response);
          dispatch(fetchProfileVerifyAuthFailure(error.response));
        });
    }
  };
};
export { fetchProfileVerifyAuth };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.ProfileVerifyAuthentication;

export default ProfileVerifyAuthenticationSlice.reducer;
