import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { getCurrenciesList } from 'pages/api/dynamic/getData';

// Define a type for the slice state
interface CurrenciesListState {
  loading: boolean;
  currenciesListResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: CurrenciesListState = {
  loading: false,
  currenciesListResp: null,
  error: undefined,
};

export const CurrenciesListSlice = createSlice({
  name: 'currencies list',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchCurrenciesListRequest: (state) => {
      return {
        loading: true,
        currenciesListResp: null,
        error: undefined,
      };
    },
    fetchCurrenciesListSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        currenciesListResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchCurrenciesListFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        currenciesListResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchCurrenciesListRequest, fetchCurrenciesListSuccess, fetchCurrenciesListFailure } =
  CurrenciesListSlice.actions;

const fetchCurrenciesList = () => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchCurrenciesListRequest());
    await getCurrenciesList()
      .then((response) => {
        console.log('debugging response', response);
        dispatch(fetchCurrenciesListSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchCurrenciesListFailure(error.message));
      });
  };
};
export { fetchCurrenciesList };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.CurrenciesList;

export default CurrenciesListSlice.reducer;
