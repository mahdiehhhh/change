import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { getUserAllNotifications } from 'pages/api/dynamic/getData';
import { ACCESS_TOKEN, IS_LOGGED_IN } from '../../configs/variables.config';

// Define a type for the slice state
interface UserAllNotificationsState {
  loading: boolean;
  userAllNotificationsResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: UserAllNotificationsState = {
  loading: false,
  userAllNotificationsResp: null,
  error: undefined,
};

export const UserAllNotificationsSlice = createSlice({
  name: 'user all notifications',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchUserAllNotificationsRequest: (state) => {
      return {
        loading: true,
        userAllNotificationsResp: null,
        error: undefined,
      };
    },
    fetchUserAllNotificationsSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        userAllNotificationsResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchUserAllNotificationsFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        userAllNotificationsResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchUserAllNotificationsRequest, fetchUserAllNotificationsSuccess, fetchUserAllNotificationsFailure } =
  UserAllNotificationsSlice.actions;

const fetchUserAllNotifications = (page: number) => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchUserAllNotificationsRequest());
    await getUserAllNotifications(page)
      .then((response) => {
        dispatch(fetchUserAllNotificationsSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchUserAllNotificationsFailure(error.message));
      });
  };
};
export { fetchUserAllNotifications };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.UserAllNotifications;

export default UserAllNotificationsSlice.reducer;
