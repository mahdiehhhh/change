import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { getUserTickets } from 'pages/api/dynamic/getData';
import { ACCESS_TOKEN, IS_LOGGED_IN } from '../../configs/variables.config';

// Define a type for the slice state
interface UserTicketsState {
  loading: boolean;
  userTicketsResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: UserTicketsState = {
  loading: false,
  userTicketsResp: null,
  error: undefined,
};

export const UserTicketsSlice = createSlice({
  name: 'user tickets',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchUserTicketsRequest: (state) => {
      return {
        loading: true,
        userTicketsResp: null,
        error: undefined,
      };
    },
    fetchUserTicketsSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        userTicketsResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchUserTicketsFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        userTicketsResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchUserTicketsRequest, fetchUserTicketsSuccess, fetchUserTicketsFailure } = UserTicketsSlice.actions;

const fetchUserTickets = () => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchUserTicketsRequest());
    await getUserTickets()
      .then((response) => {
        console.log('what is server rwsponse', response);
        dispatch(fetchUserTicketsSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchUserTicketsFailure(error.message));
      });
  };
};
export { fetchUserTickets };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.UserTickets;

export default UserTicketsSlice.reducer;
