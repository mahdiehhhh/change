import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { getUserProfile } from 'pages/api/dynamic/getData';
import { ACCESS_TOKEN, IS_LOGGED_IN } from '../../configs/variables.config';

// Define a type for the slice state
interface UserProfileState {
  loading: boolean;
  userProfileResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: UserProfileState = {
  loading: false,
  userProfileResp: null,
  error: undefined,
};

export const UserProfileSlice = createSlice({
  name: 'user profile',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchUserProfileRequest: (state) => {
      return {
        loading: true,
        userProfileResp: null,
        error: undefined,
      };
    },
    fetchUserProfileSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        userProfileResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchUserProfileFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        userProfileResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchUserProfileRequest, fetchUserProfileSuccess, fetchUserProfileFailure } = UserProfileSlice.actions;

const fetchUserProfile = () => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchUserProfileRequest());
    await getUserProfile()
      .then((response) => {
        dispatch(fetchUserProfileSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchUserProfileFailure(error.message));
      });
  };
};
export { fetchUserProfile };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.UserProfile;

export default UserProfileSlice.reducer;
