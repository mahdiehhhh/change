import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { postWalletWithdrawAmount } from 'pages/api/dynamic/getData';
import { ACCESS_TOKEN, IS_LOGGED_IN } from '../../configs/variables.config';

// Define a type for the slice state
interface WalletWithdrawAmountState {
  loading: boolean;
  walletWithdrawAmountResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: WalletWithdrawAmountState = {
  loading: false,
  walletWithdrawAmountResp: null,
  error: undefined,
};

export const WalletWithdrawAmountSlice = createSlice({
  name: 'wallet withdraw amount',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchWalletWithdrawAmountRequest: (state) => {
      return {
        loading: true,
        walletWithdrawAmountResp: null,
        error: undefined,
      };
    },
    fetchWalletWithdrawAmountSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        walletWithdrawAmountResp: action.payload,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchWalletWithdrawAmountFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        walletWithdrawAmountResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchWalletWithdrawAmountRequest, fetchWalletWithdrawAmountSuccess, fetchWalletWithdrawAmountFailure } =
  WalletWithdrawAmountSlice.actions;

const fetchWalletWithdrawAmount = (uid: string, data: object) => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchWalletWithdrawAmountRequest());
    await postWalletWithdrawAmount(uid, data)
      .then((response) => {
        dispatch(fetchWalletWithdrawAmountSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchWalletWithdrawAmountFailure(error.response.statusCode));
      });
  };
};
export { fetchWalletWithdrawAmount };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.WalletDepositAmount;

export default WalletWithdrawAmountSlice.reducer;
