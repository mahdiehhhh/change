import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { getPhoneCodes } from 'pages/api/dynamic/getData';
// Define a type for the slice state
interface LanguagesState {
  loading: boolean;
  codes: object;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: LanguagesState = {
  loading: false,
  codes: [],
  error: undefined,
};

export const PhoneCodesSlice = createSlice({
  name: 'phone_codes',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchPhoneCodesRequest: (state) => {
      return {
        ...state,
        loading: true,
      };
    },
    fetchPhoneCodesSuccess: (state, action: PayloadAction<any>) => {
      return {
        ...state,
        loading: false,
        codes: action.payload.data,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchPhoneCodesFailure: (state, action: PayloadAction<any>) => {
      return {
        ...state,
        loading: false,
        codes: [],
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchPhoneCodesRequest, fetchPhoneCodesSuccess, fetchPhoneCodesFailure } = PhoneCodesSlice.actions;

const fetchPhoneCodes = () => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchPhoneCodesRequest());
    await getPhoneCodes()
      .then((response) => {
        dispatch(fetchPhoneCodesSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchPhoneCodesFailure(error.message));
      });
  };
};
export { fetchPhoneCodes };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.PhoneCodes;

export default PhoneCodesSlice.reducer;
