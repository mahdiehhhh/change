import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { register } from 'pages/api/dynamic/getData';
import { ACCESS_TOKEN, IS_LOGGED_IN } from '../../configs/variables.config';

// Define a type for the slice state
interface RegisterState {
  loading: boolean;
  registerResp: any;
  error: any;
}

// Define the initial state using that type
const initialState: RegisterState = {
  loading: false,
  registerResp: null,
  error: undefined,
};

export const RegisterSlice = createSlice({
  name: 'register',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    resetToInitial: () => {
      return initialState;
    },
    fetchRegisterRequest: (state) => {
      return {
        loading: true,
        registerResp: null,
        error: undefined,
      };
    },
    fetchRegisterSuccess: (state, action: PayloadAction<any>) => {
      return {
        ...state,
        loading: false,
        registerResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchRegisterFailure: (state, action: PayloadAction<any>) => {
      return {
        ...state,
        loading: false,
        registerResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { resetToInitial, fetchRegisterRequest, fetchRegisterSuccess, fetchRegisterFailure } =
  RegisterSlice.actions;

const fetchRegister = (req: object | string) => {
  return async (dispatch: Dispatch) => {
    if (typeof req === 'string') {
      req === 'reset' && dispatch(resetToInitial());
    } else {
      dispatch(fetchRegisterRequest());
      await register(req)
        .then((response) => {
          dispatch(fetchRegisterSuccess(response));
        })
        .catch((error) => {
          dispatch(fetchRegisterFailure(error.response.data));
        });
    }
  };
};
export { fetchRegister };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.Register;

export default RegisterSlice.reducer;
