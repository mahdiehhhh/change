import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { getExchangeCurrenciesInfo } from 'pages/api/dynamic/getData';

// Define a type for the slice state
interface ExchangeCurrenciesInfoState {
  loading: boolean;
  ExchangeCurrenciesInfoResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: ExchangeCurrenciesInfoState = {
  loading: false,
  ExchangeCurrenciesInfoResp: null,
  error: undefined,
};

export const ExchangeCurrenciesInfoSlice = createSlice({
  name: 'exchange currencies info',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchExchangeCurrenciesInfoRequest: (state) => {
      return {
        loading: true,
        ExchangeCurrenciesInfoResp: null,
        error: undefined,
      };
    },
    fetchExchangeCurrenciesInfoSuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        ExchangeCurrenciesInfoResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchExchangeCurrenciesInfoFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        ExchangeCurrenciesInfoResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const {
  fetchExchangeCurrenciesInfoRequest,
  fetchExchangeCurrenciesInfoSuccess,
  fetchExchangeCurrenciesInfoFailure,
} = ExchangeCurrenciesInfoSlice.actions;

const fetchExchangeCurrenciesInfo = () => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchExchangeCurrenciesInfoRequest());
    await getExchangeCurrenciesInfo()
      .then((response) => {
        dispatch(fetchExchangeCurrenciesInfoSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchExchangeCurrenciesInfoFailure(error.message));
      });
  };
};
export { fetchExchangeCurrenciesInfo };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.ExchangeCurrenciesInfo;

export default ExchangeCurrenciesInfoSlice.reducer;
