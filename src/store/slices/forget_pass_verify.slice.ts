import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import type { RootState } from '../store';
import { forgetPassVerify } from 'pages/api/dynamic/getData';
import { ACCESS_TOKEN, IS_LOGGED_IN } from '../../configs/variables.config';

// Define a type for the slice state
interface ForgetPassVerifyState {
  loading: boolean;
  forgetPassVerifyResp: any;
  error: string | undefined;
}

// Define the initial state using that type
const initialState: ForgetPassVerifyState = {
  loading: false,
  forgetPassVerifyResp: null,
  error: undefined,
};

export const ForgetPasswordVerifySlice = createSlice({
  name: 'forget password verify',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    fetchForgetPassVerifyRequest: (state) => {
      return {
        loading: true,
        forgetPassVerifyResp: null,
        error: undefined,
      };
    },
    fetchForgetPassVerifySuccess: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        forgetPassVerifyResp: action.payload.data,
        error: undefined,
      };
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    fetchForgetPassVerifyFailure: (state, action: PayloadAction<any>) => {
      return {
        loading: false,
        forgetPassVerifyResp: null,
        error: action.payload,
      };
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.auth,
      };
    },
  },
});

export const { fetchForgetPassVerifyRequest, fetchForgetPassVerifySuccess, fetchForgetPassVerifyFailure } =
  ForgetPasswordVerifySlice.actions;

const fetchForgetPassVerify = (req: object) => {
  return async (dispatch: Dispatch) => {
    dispatch(fetchForgetPassVerifyRequest());
    await forgetPassVerify(req)
      .then((response) => {
        dispatch(fetchForgetPassVerifySuccess(response));
      })
      .catch((error) => {
        dispatch(fetchForgetPassVerifyFailure(error.message));
      });
  };
};
export { fetchForgetPassVerify };
// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.ForgetPasswordVerify;

export default ForgetPasswordVerifySlice.reducer;
