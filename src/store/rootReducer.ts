import { combineReducers } from 'redux';
import { CheckPhoneSlice } from './slices/check_phone.slice';
import { LanguagesSlice } from './slices/languages.slice';
import { PhoneCodesSlice } from './slices/phone_codes.slice';
import { AccessTokenSlice } from './slices/authentication.slice';
import { LoginSlice } from './slices/login.slice';
import { RegisterSlice } from './slices/register.slice';
import { RegVerifyPhoneSlice } from './slices/register_verify_phone.slice';
import { ForgetPasswordSlice } from './slices/forget_password.slice';
import { ForgetPasswordVerifySlice } from './slices/forget_pass_verify.slice';
import { ForgetPasswordResetSlice } from './slices/forget_pass_reset.slice';
import { ForgetPasswordResendSlice } from './slices/forget_pass_resend.slice';
import { CurrenciesDashboardSlice } from './slices/currencies_dashboard.slice';
import { UserProfileSlice } from './slices/user_profile.slice';
import { UserActiveWalletsSlice } from './slices/user_active_wallets.slice';
import { WalletLatestTransactionSlice } from './slices/wallet_latest_transaction.slice';
import { BannerSlice } from './slices/banner.slice';
import { UserFinanceSlice } from './slices/user_finance.slice';
import { WalletDepositRulesSlice } from './slices/wallet_deposit_rules.slice';
import { WalletDepositCashSlice } from './slices/wallet_deposit_cash_rules.slice';
import { WalletWithdrawRulesSlice } from './slices/wallet_withdraw_rules.slice';
import { WalletWithdrawCashSlice } from './slices/wallet_withdraw_cash_rules.slice';
import { UserBankAccountsSlice } from './slices/user_bank_accounts.slice';
import { WalletTransactionTypesSlice } from './slices/wallet_transaction_types.slice';
import { UserUnreadNotificationsSlice } from './slices/user_unread_notifications.slice';
import { CurrenciesListSlice } from './slices/currencies_list.slice';
import { CreateIranBankAccountSlice } from './slices/create_iran_bank_account.slice';
import { CreateIntBankAccountSlice } from './slices/create_int_bank_account.slice';
import { CreateCreditBankAccountSlice } from './slices/create_credit_bank_account.slice';
import { ExchangeCurrenciesInfoSlice } from './slices/currencies_exchange_infos.slice';
import { WalletDepositAmountSlice } from './slices/wallet_deposit_amount.slice';
import { WalletDepositCashAmountSlice } from './slices/wallet_deposit_cash_amount.slice';
import { WalletWithdrawAmountSlice } from './slices/wallet_withdraw_amount.slice';
import { WalletWithdrawCashAmountSlice } from './slices/wallet_withdraw_cash_amount.slice';
import { UserAllNotificationsSlice } from './slices/user_all_notifications.slice';
import { AddCurrencyToFavoriteSlice } from './slices/add_currency_to_favorite.slice';
import { RemoveCurrencyFromFavoriteSlice } from './slices/remove_currency_from_favorite.slice';
import { BanksListsSlice } from './slices/banks_lists.slice';
import { RegResendSlice } from './slices/register_resend.slice';
import { ProfileVerifyAuthenticationSlice } from './slices/profile_verify_authentication.slice';
import { UserTicketsSlice } from './slices/user_tickets.slice';
import { UserTicketsSubjectsSlice } from './slices/user_tickets_subjects.slice';
import { CreateTicketSlice } from './slices/create_ticket.slice';
import { FeesListSlice } from './slices/fees_list.slice';
import { WalletTransferRulesSlice } from './slices/wallet_transfer_rules.slice';
import { CheckWalletTransferInfoSlice } from './slices/wallet_transfer_info_check.slice';
import { ConfirmWalletTransferInfoSlice } from './slices/wallet_transfer_info_confirm.slice';
import { TransferDestinationWalletSlice } from './slices/transfer_destination_wallet.slice';
import { TransferDestinationUserSlice } from './slices/transfer_destination_user.slice';
import { WalletLatestTransfersSlice } from './slices/wallet_latest_transfers.slice';
import { GetTicketsMessagesSlice } from './slices/get_tickets_messages.slice';
import { SendTicketsMessagesSlice } from './slices/send_tickets_messages.slice';
import { GetCountriesSlice } from './slices/get_countries.slice';

const rootReducer = combineReducers({
  CheckPhone: CheckPhoneSlice.reducer,
  Login: LoginSlice.reducer,
  Register: RegisterSlice.reducer,
  RegisterResend: RegResendSlice.reducer,
  RegisterVerifyPhone: RegVerifyPhoneSlice.reducer,
  ForgetPassword: ForgetPasswordSlice.reducer,
  ForgetPasswordVerify: ForgetPasswordVerifySlice.reducer,
  ForgetPasswordReset: ForgetPasswordResetSlice.reducer,
  ForgetPasswordResend: ForgetPasswordResendSlice.reducer,
  CurrenciesDashboard: CurrenciesDashboardSlice.reducer,
  CurrenciesList: CurrenciesListSlice.reducer,
  FeesList: FeesListSlice.reducer,
  ExchangeCurrenciesInfo: ExchangeCurrenciesInfoSlice.reducer,
  UserProfile: UserProfileSlice.reducer,
  UserTickets: UserTicketsSlice.reducer,
  GetTicketsMessages: GetTicketsMessagesSlice.reducer,
  GetCountries: GetCountriesSlice.reducer,
  SendTicketsMessages: SendTicketsMessagesSlice.reducer,
  UserTicketsSubjects: UserTicketsSubjectsSlice.reducer,
  UserActiveWallets: UserActiveWalletsSlice.reducer,
  WalletLatestTransaction: WalletLatestTransactionSlice.reducer,
  WalletLatestTransfers: WalletLatestTransfersSlice.reducer,
  WalletDepositRules: WalletDepositRulesSlice.reducer,
  WalletWithdrawRules: WalletWithdrawRulesSlice.reducer,
  WalletTransferRules: WalletTransferRulesSlice.reducer,
  WalletDepositCash: WalletDepositCashSlice.reducer,
  WalletDepositAmount: WalletDepositAmountSlice.reducer,
  WalletWithdrawAmount: WalletWithdrawAmountSlice.reducer,
  WalletDepositCashAmount: WalletDepositCashAmountSlice.reducer,
  WalletWithdrawCashAmount: WalletWithdrawCashAmountSlice.reducer,
  WalletWithdrawCash: WalletWithdrawCashSlice.reducer,
  CheckWalletTransfer: CheckWalletTransferInfoSlice.reducer,
  ConfirmWalletTransfer: ConfirmWalletTransferInfoSlice.reducer,
  FindTransferDestWallet: TransferDestinationWalletSlice.reducer,
  FindTransferDestUser: TransferDestinationUserSlice.reducer,
  UserBankAccounts: UserBankAccountsSlice.reducer,
  BanksLists: BanksListsSlice.reducer,
  WalletTransactionTypes: WalletTransactionTypesSlice.reducer,
  UserUnreadNotifications: UserUnreadNotificationsSlice.reducer,
  UserAllNotifications: UserAllNotificationsSlice.reducer,
  CreateIranBankAccount: CreateIranBankAccountSlice.reducer,
  CreateTicket: CreateTicketSlice.reducer,
  AddCurrencyToFavorite: AddCurrencyToFavoriteSlice.reducer,
  RemoveCurrencyFromFavorite: RemoveCurrencyFromFavoriteSlice.reducer,
  CreateInternationalBankAccount: CreateIntBankAccountSlice.reducer,
  CreateCreditBankAccount: CreateCreditBankAccountSlice.reducer,
  ProfileVerifyAuthentication: ProfileVerifyAuthenticationSlice.reducer,
  Banner: BannerSlice.reducer,
  UserFinance: UserFinanceSlice.reducer,
  Languages: LanguagesSlice.reducer,
  PhoneCodes: PhoneCodesSlice.reducer,
  AccessToken: AccessTokenSlice.reducer,
});
// export type RootState = ReturnType<typeof rootReducer>;

export { rootReducer };
