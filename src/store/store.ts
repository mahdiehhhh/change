import { configureStore, Action, ThunkAction } from '@reduxjs/toolkit';
import { rootReducer } from './rootReducer';
import { createWrapper } from 'next-redux-wrapper';

const store = configureStore({ reducer: rootReducer, devTools: true });

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>;
const makeStore = () => store;

export const wrapper = createWrapper(makeStore);
